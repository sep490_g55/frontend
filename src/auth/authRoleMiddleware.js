// authRoleMiddleware.js
import {getLocalStorage} from "@/dataProvider/agent"
import { isValidToken, jwtDecode, setupLocalStorage } from './utils';

const checkAuthAndRole = (req, res, next) => {
   const token = getLocalStorage("access_token")

   
   if (token ===  undefined) {

    res.writeHead(302, { Location: '/auth/login' });
    res.end();
    return;
  }
   

   if (!isValidToken(token)) {

    res.writeHead(302, { Location: '/auth/login' });
    res.end();
    return;
  }
   
   const decoded = jwtDecode(token);
   const roles = decoded?.roles

    if (req.url.includes('/dashboards/admin') && roles.includes('ADMIN')) {
      // Nếu không có quyền ADMIN khi truy cập trang dashboard, chuyển hướng về trang mặc định
      res.writeHead(302, { Location: '/home' });
      res.end();
      return;
    }

    if (req.url.includes('/materials/moderator/list') && roles.includes('MODERATOR')) {
        // Nếu không có quyền ADMIN khi truy cập trang dashboard, chuyển hướng về trang mặc định
        res.writeHead(302, { Location: '/home' });
        res.end();
        return;
      }
  
    // Nếu đăng nhập và có quyền, chuyển quyền điều khiển cho middleware tiếp theo
    next();
  };
  
  export default checkAuthAndRole;
  