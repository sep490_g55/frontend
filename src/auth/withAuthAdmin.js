// withAuth.js
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useAuthContext } from "@/auth/useAuthContext";
import LoadingScreen from '@/components/loading-screen/LoadingScreen';


const withAuth = (WrappedComponent) => {
  const AuthComponent = (props) => {
    const router = useRouter();
const {isAuthenticated,ROLES} =useAuthContext();
    useEffect(() => {
      // Kiểm tra trạng thái đăng nhập ở đây, nếu chưa đăng nhập thì chuyển hướng về trang login
      if (!isAuthenticated) {
        router.push('/auth/login');
      }else{
        const role = ROLES.find((role) => role === "ADMIN")
        console.log("role",role)
        if(role  !== "ADMIN"){
        router.push('/home');
        return <LoadingScreen/>;
        }else{
          <WrappedComponent {...props} />
        }
      }
    }, []);

    // Trả về component gốc nếu đã đăng nhập
    return <WrappedComponent {...props} />
  };

  return AuthComponent;
};

export default withAuth;
