import PropTypes from 'prop-types';
import { createContext, useEffect, useReducer, useCallback } from 'react';
import Cookies from "js-cookie";
// utils
import axios from '../utils/axios';
//
import { isValidToken, jwtDecode, setupLocalStorage } from './utils';
import { clearLocalStorage, loginAuth, getUserById, getLocalStorage, setLocalStorage, getProfile, register,updateRegister,getLogOut, updateUser } from '../dataProvider/agent';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import HomePage from '@/pages';
import SwitchRole from '@/pages/auth/switch-role';
import Admin from '@/pages/dashboards';
import { NextResponse } from 'next/server'
import { PATH_PAGE } from '@/routes/paths';
// ----------------------------------------------------------------------

// NOTE:
// We only build demo at basic level.
// Customer will need to do some extra handling yourself if you want to extend the logic and other features...

// ----------------------------------------------------------------------

const initialState = {
    isInitialized: false,
    isAuthenticated: false,
    isValidInformation: false,
    user: null,
    ROLES: [],
   

    
};
function clearAllCookies() {
    const cookies = Cookies.get();
    for (const cookie in cookies) {
      Cookies.remove(cookie);
    }
  }
const reducer = (state, action) => {
    switch(action.type){
        case 'INITIAL':
            return {
                isInitialized: true,
                isAuthenticated: action.payload.isAuthenticated,
                user: action.payload.user,
                ROLES: action.payload.ROLES,
                
            };
        case 'LOGIN':
        // //console.log("roles 43",action.payload.user?.roleDTOResponses)

            return {
                ...state,
                isAuthenticated: true,
                user: action.payload.user,
                ROLES: action.payload.ROLES,
                
            };
        case 'REGISTER':
            return {
                ...state,
                isAuthenticated: false,
                user: action.payload.user,
            };
            case 'GET-INFORMATION':
            return {
                ...state,
                isAuthenticated: false,
                // user: action.payload.user,

            };
        case 'LOGOUT':
            return {
                ...state,
                isAuthenticated: false,
                user: null,
            };
        case 'SET-ROLE-CURRENT':
            return {
                ...state,
                roleCurrent: action.payload.roleCurrent,

            };
    
    case 'RESET-USER':
        // //console.log("roles 43",action.payload.user?.roleDTOResponses)

            return {
                ...state,
                isAuthenticated: true,
                user: action.payload.user,
            
                
            };

            case 'CHANGE-AVATAR':
                // //console.log("roles 43",action.payload.user?.roleDTOResponses)
        
                    return {
                        ...state,
                      
                        user: action.payload.user,
                    
                        
                    };
        }    
    return state;
};

// ----------------------------------------------------------------------

export const AuthContext = createContext(null);

// ----------------------------------------------------------------------

AuthProvider.propTypes = {
    children: PropTypes.node,
};

export function AuthProvider({ children }) {
    const [state, dispatch] = useReducer(reducer, initialState);

    const initialize = useCallback(async () => {
        try {
            const accessToken = typeof window !== 'undefined' ? getLocalStorage('access_token') : '';

            if (isValidToken(accessToken)) {
                // console.log("vao day129")

                // setupLocalStorage(accessToken);
                const responseUser = await getProfile();
                if(responseUser.status === 200){
                    const decoded = jwtDecode(accessToken);
                
                    const roles = decoded?.roles
                    //console.log("roles",roles)
                    //console.log("responseUser",responseUser)
                    
                    dispatch({
                        type: 'INITIAL',
                        payload: {
                            isAuthenticated: true,
                            user: { ...responseUser?.data },
                            ROLES: roles,
                            
                          
                        },
                    });
                }else{
                    logout();
                }
                
            } else {
                // console.log("vao day")
                dispatch({
                    type: 'INITIAL',
                    payload: {
                        isAuthenticated: false,
                        user: null,
                    },
                });
                // if(accessToken !== null){
                // }
               
                
            }
        } catch (error) {
            console.error(error);
            dispatch({
                type: 'INITIAL',
                payload: {
                    isAuthenticated: false,
                    user: null,
                },
            });
        }
    }, []);

    useEffect(() => {
        initialize();
    }, [initialize]);

    // LOGIN
    const login = async (email, password) => {
        const responseLogin = await loginAuth({ email, password });
        // //console.log("responseLogin",responseLogin)
        
        if (!responseLogin) {
            throw new Error("Kiểm tra kết nối mạng");
        }
        const accessToken  = responseLogin.data.access_token;
        // //console.log("accessToken",accessToken)
        Cookies.set("token", accessToken);

        clearLocalStorage();
        setupLocalStorage(accessToken);
        
        const decoded = jwtDecode(accessToken);
       
        const roles = decoded?.roles
        // //console.log("roles",roles)
        Cookies.set("roles", roles);

        let responseUser = null;
        if((decoded.isValid)){
             responseUser = await getProfile();
            // //console.log("decoded",decoded)
            //console.log("responseUser",responseUser?.data)
        }
        dispatch({
            type: 'LOGIN',
            payload: {
                user:{
                    ...!(decoded.isValid)? {  id:decoded.userId}:responseUser?.data,
                 },
                ROLES: roles,
            
               
            },
        });
        if(decoded?.isValid){
            return responseLogin?.status
        }else{
            return -1
        }
    };

    // REGISTER
    const registerUser = async (username,email, password) => {
        const response = await register( {
            username,
            email,
            password,
    });
    //console.log("response register",response)
    if(response.status === 200){
            const id = response.data.id;
            //console.log("response id",id)

            dispatch({
            type: 'REGISTER',
            payload:{
                user: {
                    ...{
                        "id": id,
                        "username": username,
                        "email": email,
                        "firstname": "",
                        "lastname": "",
                        "phone": "",
                        "gender": true,
                        "school": "",
                        "province": "",
                        "district": "",
                        "village": "",
                        "dateOfBirth": "",
                        "classId": 0
                    }
                },
            }
        });
            return 200
        }else{
            return response.status

        }
    };

    // REGISTER
    const getInformation = async (id,username,firstname, lastname,email,dateOfBirth,phone,gender,school,province,district,ward,classId) => {
    dispatch({
        type: 'GET-INFORMATION',
        payload:{
            user: {
                ...{
                    "id": id,
                    "username": username,
                    "email": email,
                    "firstname": firstname,
                    "lastname": lastname,
                    "phone": phone,
                    "gender": gender,
                    "school": school,
                    "province": province,
                    "district": district,
                    "village": "",
                    "dateOfBirth": dateOfBirth,
                    "classId": classId
                }
            },
        }
    })
    //console.log("thay doi")
    };

    // REGISTER
    const updateInformation = async (id,firstname, lastname,phone,gender,province,district,ward,dateOfBirth,school,classId) => {
        //console.log("id",id)
        const village = ward
       
        


        const response = await updateRegister( {
            id,firstname, lastname,phone,gender,school,province,district,village,dateOfBirth,classId
    });
    //console.log("response register",response)
    if(response.status === 200){
       
            return response
        }else{
            return response

        }
    };
    const updateProfile = async (id,firstname, lastname,phone,gender,province,district,ward,dateOfBirth,school,classId) => {
        //console.log("id",id)
        const village = ward

        const response = await updateUser( {
            firstname, lastname,phone,gender,school,province,district,village,dateOfBirth,classId
    });
    // //console.log("response register",response)
    if(response.status === 200){
       
            return response
        }else{
            return response

        }
    };
    // LOGOUT
    const logout = async () => {
        const res =  await getLogOut();
        //console.log("res logou",res)
        clearLocalStorage();
        setupLocalStorage(null);
        clearAllCookies()
        dispatch({
            type: 'LOGOUT',
        });
    };
    // LOGOUT
    const resetUser = async (user) => {
       
        //console.log("res logou",res)
       
      
        
        dispatch({
            type: 'RESET-USER',
            payload:{
                user: {...user},
            }
        });
    };
    const changeAvatar = async (img) => {
      const res = await getProfile();
        dispatch({
            type: 'CHANGE-AVATAR',
            payload:{
                user: {...res.data},
            }
        });
    };
    // 
    const setRoleCurrent = (role) => {
        
        dispatch({
            type: 'SET-ROLE-CURRENT',
            payload:{
                roleCurrent: role,
            }
        });
    };

    return (
        <AuthContext.Provider
            value={{
                ...state,
                method: 'jwt',
                login,
                logout,
                registerUser,
                setRoleCurrent,
                getInformation,
                updateInformation,
                updateProfile,
                resetUser,
                changeAvatar,
            }}
        >
            {children}
        </AuthContext.Provider>
    );
}
