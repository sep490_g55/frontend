// withAuth.js
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useAuthContext } from "@/auth/useAuthContext";
import { setRole } from './utils';


const withInfo = (WrappedComponent) => {
  const AuthComponent = (props) => {
    const router = useRouter();
const {isAuthenticated,ROLES} =useAuthContext();
    useEffect(() => {
      // Kiểm tra trạng thái đăng nhập ở đây, nếu chưa đăng nhập thì chuyển hướng về trang login
      if (isAuthenticated) {
        setRole(router,ROLES);
      }
    }, []);

    // Trả về component gốc nếu đã đăng nhập
    return <WrappedComponent {...props} />;
  };

  return AuthComponent;
};

export default withInfo;
