// routes
import { getLocalStorage, setLocalStorage } from '@/dataProvider/agent';
import { PATH_AUTH, PATH_PAGE } from '../routes/paths';
// utils
import axios from '../utils/axios';
// import Cookies from 'cookies'
import Cookies from "js-cookie";

// ----------------------------------------------------------------------
export const setRole = (route) =>{
  const accessToken = typeof window !== 'undefined' ? getLocalStorage('access_token') : '';
  const decode = jwtDecode(accessToken);
  const ROLES = decode?.roles;
  if (ROLES?.length > 1) {
    route.push("/auth/switch-role")
  }else if(ROLES?.length === 1){
  //  console.log("ROLES?.length === 1",ROLES)
      switch (ROLES[0]) {
        case "ADMIN":
          setLocalStorage("role","ADMIN");
          Cookies.set("role", "ADMIN");

          route.push("/dashboards/admin");
          break;
        case "TEACHER":
          setLocalStorage("role","TEACHER");
          Cookies.set("role", "TEACHER");
          route.push("/home");
          break;
        case "MODERATOR":
          setLocalStorage("role","MODERATOR");
          Cookies.set("role", "MODERATOR");
          route.push("/materials/moderator/list");
          break;
        default:
          setLocalStorage("role","TEACHER");
          Cookies.set("role", "TEACHER");
          route.push("/home");
          break;
      }
  }
}
export const jwtDecode = (token) => {
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  const jsonPayload = decodeURIComponent(
    window
      .atob(base64)
      .split('')
      .map((c) => `%${`00${c.charCodeAt(0).toString(16)}`.slice(-2)}`)
      .join('')
  );

  return JSON.parse(jsonPayload);
};

// ----------------------------------------------------------------------

export const isValidToken = (accessToken) => {
  if (!accessToken) {
    return false;
  }

  const decoded = jwtDecode(accessToken);

  const currentTime = Date.now() / 1000;
//console.log("currentTime",currentTime)
// console.log("decoded",decoded)
return decoded.exp > currentTime 
};

// ----------------------------------------------------------------------

export const tokenExpired = (exp,iat) => {
  // eslint-disable-next-line prefer-const
  let expiredTimer;

  const currentTime = Date.now();
  // console.log("currentTime",currentTime)
  // console.log("exp",exp)

  // Test token expires after 10s
  // const timeLeft = currentTime + 10000 - currentTime; // ~10s
  const timeLeft = exp -iat;
  if(timeLeft <= 0){
    window.location.href = PATH_PAGE.outTime;

  } 
  // clearTimeout(expiredTimer);

  // expiredTimer = setTimeout(() => {
  //   // alert('Hết hạn đăng nhập, xin mời đăng nhập lại');

  //   localStorage.removeItem('access_token');

    
  // }, timeLeft);
};
 
// ----------------------------------------------------------------------

export const setupLocalStorage = (accessToken) => {
  if (accessToken) {
    setLocalStorage('access_token', accessToken);

    axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;

    // This function below will handle when token is expired
    const { exp ,iat} = jwtDecode(accessToken); // ~3 days by minimals server
    tokenExpired(exp,iat);
  } else {
    localStorage.removeItem('access_token');

    delete axios.defaults.headers.common.Authorization;
  }
};
