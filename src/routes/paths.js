
function path(root, sublink) {
    return `${root}${sublink}`;
}

const ROOTS_AUTH = '/auth';
const ROOTS_DASHBOARD = '/dashboard';

// ----------------------------------------------------------------------

export const PATH_AUTH = {
    root: ROOTS_AUTH,
    login: path(ROOTS_AUTH, '/login'),
    register: path(ROOTS_AUTH, '/register'),
    forGetPassword: path(ROOTS_AUTH, '/forget-password'),
    getInformation: path(ROOTS_AUTH, '/get-information'),
    switchRole: path(ROOTS_AUTH, '/switch-role'),

};

export const PATH_PAGE = {
    home: '/home',
    media: '/media',
    profile: '/profile',
    faqs: '/faqs',
    page403: '/403',
    page404: '/404',
    page500: '/500',
    outTime:'/out-time',
};
