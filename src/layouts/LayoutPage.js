import Head from 'next/head'
import React, { Children } from 'react'
import Footer from './common/footer/Footer'
const LayoutPage = ({children,title}) => {
  return (
    <>
        <Head>
       
          <title>{title}</title>
        </Head>
        {children}
        {/* <Footer/> */}
    </>
  )
}

export default LayoutPage