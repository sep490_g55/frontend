import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import { SiGoogleclassroom } from "react-icons/si";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
//icon
import { MdCircleNotifications } from "react-icons/md";
import { Link, Stack } from "@mui/material";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import axiosClient from "@/utils/axios";
import { KHOI_URL } from "@/constants/baseApi";
import { StorePage } from "./Store";
import { LogoSystem, NameSystem } from "@/theme/system";
import { useAuthContext } from "@/auth/useAuthContext";
import { getLocalStorage,clearLocalStorage } from "@/dataProvider/agent";
import { CenterFlexBox } from "@/components/common_sections";
import { isValidToken, jwtDecode } from "@/auth/utils";
import { ToastContainer, toast } from 'react-toastify';
import ro from "date-fns/locale/ro/index";

function Header({ tab }) {
  const route = useRouter();
  const accessToken = typeof window !== 'undefined' ? getLocalStorage('access_token') : '';

  
  // if (!isValidToken(accessToken)) {
  //   // toast.info("Hết hạn đăng nhập, vui lòng đăng nhập lại")
  //   //console.log("đăng nhập lại")    
  //   // route.push("/auth/login")
        
  // }else{
  //   //console.log("khong can")
  // }
  const { user, isAuthenticated, logout,ROLES } = useAuthContext();
  const fullName = user?.firstname + " " + user?.lastname;
  const avatar = user?.avatar;
  //console.log("isAuthenticated",isAuthenticated);
  const roleCurrent = getLocalStorage("role")?getLocalStorage("role"):"";
  const currentUrl = route.asPath;
  // console.log("roleCurrent",roleCurrent)
  // console.log("currentUrl",currentUrl)
  
  const settings = [
    {
      href: "/profile",
      name: "Hồ sơ",
    },
    {
      href:
        roleCurrent === "TEACHER"
          ? "/materials/mymaterials/list"
          : roleCurrent === "MODERATOR"
          ? "/materials/moderator/list"
          : roleCurrent === "ADMIN"
          ? "/dashboards/admin"
          : "",
      name:
        roleCurrent === "TEACHER"
          ? "Tài liệu của tôi"
          : roleCurrent === "MODERATOR"
          ? "Tài liệu kiểm duyệt"
          : roleCurrent === "ADMIN"
          ? "Quản lý hệ thống"
          : "",
    },
   
    {
      href: "/auth/login",
      name: "Đăng xuất",
    },
  ];
  ROLES?.length > 1 && settings.push( {
    href: "/auth/switch-role",
    name: "Vai trò khác",
  });
  const notifications = [
    {
      href: "/1",
      name: "Hệ thống đã cập nhật thêm nhiều tính năng mới, hãy trải nghiệm chúng",
    },
    {
      href: "/2",
      name: "Hệ thống đã cập nhật thêm nhiều tính năng mới, hãy trải nghiệm chúng",
    },
    {
      href: "/3",
      name: "Hệ thống đã cập nhật thêm nhiều tính năng mới, hãy trải nghiệm chúng",
    },
    {
      href: "/4",
      name: "Hệ thống đã cập nhật thêm nhiều tính năng mới, hãy trải nghiệm chúng",
    },
  ];

  const pages = [
    {
      href: "/home",
      name: "Kho Học liệu",
    },
    {
      href: "/media",
      name: "Kho Media",
    },
  ];
  // responsive

  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{
        width: anchor === "top" || anchor === "bottom" ? "auto" : 380,
        padding: "20px",
       height:"100%"
      }}
      role="presentation"
      // onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      {tab}
    </Box>
  );
  // end responsive

  if (route.isFallback) {
    return <Box sx={{ textAlign: "center", color: "red" }}>Loading...</Box>;
  }
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const [anchorElNotification, setAnchorElNotification] = React.useState(null);

  // //console.log('currentUrl: ',currentUrl);
  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };
  const handleOpenNotification = (event) => {
    setAnchorElNotification(event.currentTarget);
  };

  const handleCloseNavMenu = (path) => {
    setAnchorElNav(null);
    route.push(path);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  const handleCloseNotification = () => {
    setAnchorElNotification(null);
  };
  const handleClickMenu = (setting) => {
    setAnchorElUser(null);
    if(setting.href === "/auth/login"){
    handleLogout(setting.href)
    }
    route.push(setting.href);
  };
  const handleLogout = async (path) => {
    await logout();
    route.push(path);
  };
  const handleLogin = async () => {
    clearLocalStorage()
    route.push("/auth/login")
  };
  const [store,setStore] = React.useState(<Box sx={{width:{xs:0,md:"100%"}}}></Box>)
  // React.useEffect(()=>{
  //   !currentUrl?.includes("/auth/login")&&!currentUrl?.includes("/auth/register")&&!currentUrl?.includes("/auth/get-information")
  //    &&(roleCurrent === "TEACHER" ||roleCurrent === "" ||roleCurrent === undefined)  
  //    && setStore(<StorePage/>)
  // },[])
  return (
    <AppBar
      position="static"
      sx={{
        backgroundColor: "white",
        marginBottom: "20px",
        position: "sticky",
        top: 0,
        zIndex: 999,
      }}
    >
      <ToastContainer/>
      <React.Fragment key="left">
        <SwipeableDrawer
          anchor={"left"}
          open={state["left"]}
          onClose={toggleDrawer("left", false)}
          onOpen={toggleDrawer("left", true)}
        >
          {list("left")}
        </SwipeableDrawer>
      </React.Fragment>

      <Container maxWidth="xl">
        <Toolbar disableGutters>
          {/* <SiGoogleclassroom  style={{fontSize:'32px',color: "#198754",cursor:'pointer'}} onClick={() => route.push("/home")} title="Trang chủ"/> */}
          <LogoSystem />
          {/* <Link href="/home">
            <Avatar src="https://c8.alamy.com/comp/2C6PYE2/business-meeting-group-presentation-icon-vector-graphics-for-various-use-2C6PYE2.jpg" />
          </Link> */}
          <Typography
            variant="h6"
            noWrap
            component="a"

            sx={{
              width:"200px",
              mr: 2,
              display: { xs: "none", md: "none",lg:"flex" },
              fontFamily: "monospace",
              fontWeight: 700,
              letterSpacing: ".3rem",
              color: "#198754",
              textDecoration: "none",
              marginLeft: "10px",
              cursor:"pointer"

            }}
            onClick={() => route.push("/home")}
            title="Trang chủ"
          >
            <NameSystem />
          </Typography>

          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={toggleDrawer("left", true)}
              color="#198754"
            >
              <MenuIcon />
              {/* use when responsive */}
            </IconButton>
          </Box>

          {/* <AdbIcon sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }} /> */}
          <Typography
            variant="h5"
            noWrap
            component="a"
            onClick={() => route.push("/home")}
            
            sx={{
              mr: 2,
              display: { xs: "flex", md: "none" },
              flexGrow: 1,
              fontFamily: "monospace",
              fontWeight: 700,
              letterSpacing: ".3rem",
              color: "white",
              textDecoration: "none",
              cursor:"pointer"
            }}
            title="Trang chủ"
          >
           <NameSystem />
          </Typography>
         {/* {!currentUrl?.includes("/auth/login")&&!currentUrl?.includes("/auth/register")&&!currentUrl?.includes("/auth/get-information") &&(roleCurrent === "TEACHER" ||roleCurrent === "" ||roleCurrent === undefined)  ? <StorePage/>:<Box sx={{width:{xs:0,md:"100%"}}}></Box>} */}
         {/* <StorePage/> */}
       {store}
          {isAuthenticated ? (
            <Box sx={{ width: 150, flexGrow: 0 }}>
              {roleCurrent === "TEACHER" || roleCurrent !== "ADMIN" || roleCurrent !== "MODERATOR"  ? (
                <>
                  <Tooltip title="Thông báo" sx={{ marginRight: 150 }}>
                    <IconButton
                      onClick={handleOpenNotification}
                      sx={{ p: 0, marginRight: 5, backgroundColor: "inherit" }}
                    >
                      <MdCircleNotifications
                        style={{
                          backgroundColor: "#183700",
                          borderRadius: "50%",
                          color: "white",
                        }}
                      />
                    </IconButton>
                  </Tooltip>
                  <Menu
                    sx={{ mt: "45px" }}
                    id="menu-appbar"
                    anchorEl={anchorElNotification}
                    anchorOrigin={{
                      vertical: "top",
                      horizontal: "right",
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: "top",
                      horizontal: "right",
                    }}
                    open={Boolean(anchorElNotification)}
                    onClose={handleCloseNotification}
                  >
                    {notifications.map((notification) => (
                      <MenuItem
                        key={notification.href}
                        onClick={handleCloseNotification}
                      >
                        <Typography textAlign="center">
                          {notification.name}
                        </Typography>
                      </MenuItem>
                    ))}
                  </Menu>

                  {/* avatar */}

                  <Tooltip title={fullName}>
                    <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                      <Avatar alt={fullName} src={avatar} />
                    </IconButton>
                  </Tooltip>
                </>
              ) : (
                <Box
                  sx={{
                    width: 300,
                    hight: "15px",
                    display: "flex",
                    justifyContent: "flex-end",
                    alignItems: "center",
                    margin: "10px 0",
                  }}
                >
                  <Box sx={{ color: "green" }}>
                    <Box sx={{ fontWeight: 500 }}>
                      {/* {title1} */}
                      {fullName}
                    </Box>
                    <Box>
                      {roleCurrent}
                      {/* {title2} */}
                      {/* <BiSolidSchool /> THPT Pham Van Nghi */}
                    </Box>
                  </Box>
                  <Box sx={{ display: "flex", alignItems: "center" }}>
                    <IconButton
                      size="large"
                      aria-label="account of current user"
                      aria-controls="menu-appbar"
                      aria-haspopup="true"
                      onClick={handleOpenUserMenu}
                      color="inherit"
                    >
                      <Avatar src={avatar} />
                    </IconButton>
                    {/* <Avatar src="https://d.ibtimes.co.uk/en/full/1620522/johnny-depp.jpg" /> */}
                  </Box>
                </Box>
              )}
              <Menu
                sx={{ mt: "45px" }}
                id="menu-appbar"
                anchorEl={anchorElUser}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={Boolean(anchorElUser)}
                onClose={handleCloseUserMenu}
              >
                {settings.map((setting) => (
                  <MenuItem key={setting.href} onClick={()=>handleClickMenu(setting)}>
                     <Typography textAlign="start">  {setting.name}  </Typography>
                  </MenuItem>
                ))}
              </Menu>
            </Box>
          ) : (
            <Button  sx={{width:150}} onClick={() => handleLogin()}>Đăng nhập</Button>
          )}
          
        </Toolbar>
      </Container>
    </AppBar>
  );
}


export default Header;

 