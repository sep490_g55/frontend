// import * as React from "react";
// import AppBar from "@mui/material/AppBar";
// import Box from "@mui/material/Box";
// import Toolbar from "@mui/material/Toolbar";
// import IconButton from "@mui/material/IconButton";
// import Typography from "@mui/material/Typography";
// import Menu from "@mui/material/Menu";
// import MenuIcon from "@mui/icons-material/Menu";
// import Container from "@mui/material/Container";
// import Avatar from "@mui/material/Avatar";
// import Button from "@mui/material/Button";
// import Tooltip from "@mui/material/Tooltip";
// import MenuItem from "@mui/material/MenuItem";
// import AdbIcon from "@mui/icons-material/Adb";

// //icon
// import { MdCircleNotifications } from "react-icons/md";
// import { Link } from "@mui/material";
// import { useRouter } from "next/router";
// import {
//   FullColorAvatarAndInfoDiv,
//   FullColorInfoAndAvatarDiv,
// } from "@/components/common_sections";
// import { BiSolidSchool } from "react-icons/bi";

// const pages = [
//   {
//     href: "/home",
//     name: "Kho Học liệu",
//   },
//   {
//     href: "/media",
//     name: "Kho Media",
//   },
// ];
// const settings = [
//   {
//     href: "/profile",
//     name: "Profile",
//   },
//   {
//     href: "/account",
//     name: "Account",
//   },
//   {
//     href: "/dashboard",
//     name: "Dashboard",
//   },
//   {
//     href: "/auth/login",
//     name: "Login",
//   },
// ];
// const notifications = [
//   {
//     href: "/1",
//     name: "Hệ thống đã cập nhật thêm nhiều tính năng mới, hãy trải nghiệm chúng",
//   },
//   {
//     href: "/2",
//     name: "Hệ thống đã cập nhật thêm nhiều tính năng mới, hãy trải nghiệm chúng",
//   },
//   {
//     href: "/3",
//     name: "Hệ thống đã cập nhật thêm nhiều tính năng mới, hãy trải nghiệm chúng",
//   },
//   {
//     href: "/4",
//     name: "Hệ thống đã cập nhật thêm nhiều tính năng mới, hãy trải nghiệm chúng",
//   },
// ];

// function HeaderAdmin() {
//   const route = useRouter();
//   const [anchorElNav, setAnchorElNav] = React.useState(null);
//   const [anchorElUser, setAnchorElUser] = React.useState(null);
//   const [anchorElNotification, setAnchorElNotification] = React.useState(null);

//   const currentUrl = route.asPath;
//   console.log("currentUrl: ", currentUrl);
//   const handleOpenNavMenu = (event) => {
//     setAnchorElNav(event.currentTarget);
//   };
//   const handleOpenUserMenu = (event) => {
//     setAnchorElUser(event.currentTarget);
//   };
//   const handleOpenNotification = (event) => {
//     setAnchorElNotification(event.currentTarget);
//   };

//   const handleCloseNavMenu = (path) => {
//     setAnchorElNav(null);
//     route.push(path);
//   };

//   const handleCloseUserMenu = () => {
//     setAnchorElUser(null);
//   };
//   const handleCloseNotification = () => {
//     setAnchorElNotification(null);
//   };

//   return (
//     <AppBar
//       position="static"
//       sx={{
//         backgroundColor: "white",
//         marginBottom: "20px",
//         position: "sticky",
//         top: 0,
//         zIndex: 999,
//       }}
//     >
//       <Container maxWidth="xl">
//         <Toolbar disableGutters sx={{display:'flex',justifyContent: 'space-around'}}>
//           <Box><AdbIcon sx={{ display: { xs: "none", md: "flex" }, mr: 1 }} />
//           <Typography
//             variant="h6"
//             noWrap
//             component="a"
//             href="#app-bar-with-responsive-menu"
//             sx={{
//               mr: 2,
//               display: { xs: "none", md: "flex" },
//               fontFamily: "monospace",
//               fontWeight: 700,
//               letterSpacing: ".3rem",
//               color: "inherit",
//               textDecoration: "none",
//             }}
//           >
//             LOGO
//           </Typography>

//           <Link href="/home">
//             <Avatar src="https://c8.alamy.com/comp/2C6PYE2/business-meeting-group-presentation-icon-vector-graphics-for-various-use-2C6PYE2.jpg" />
//           </Link>
//           {/* <AdbIcon sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }} /> */}
//           <Typography
//             variant="h5"
//             noWrap
//             component="a"
//             href="#app-bar-with-responsive-menu"
//             sx={{
//               mr: 2,
//               display: { xs: "flex", md: "none" },
//               flexGrow: 1,
//               fontFamily: "monospace",
//               fontWeight: 700,
//               letterSpacing: ".3rem",
//               color: "green",
//               textDecoration: "none",
//             }}
//           >
//             VISSEDU
//           </Typography>
//           </Box>
//           <Box sx={{ flexGrow: 0 }}>
//             {/* avatar */}
//             <Tooltip title="Open settings">
//               <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
//                 <FullColorInfoAndAvatarDiv
//                   avatarsrc="https://i.pinimg.com/736x/7c/6e/68/7c6e68ab8087250c0aa727b4433724f1--heres-johnny-johnny-depp.jpg"
//                   title1={
//                     <Box sx={{ fontWeight: 500, fontSize: "16px" }}>
//                       Viet Hoang
//                     </Box>
//                   }
//                   title2={<Box sx={{ fontSize: "16px" }}>Moderator</Box>}
//                 />
//               </IconButton>
//             </Tooltip>
//             <Menu
//               sx={{ mt: "45px" }}
//               id="menu-appbar"
//               anchorEl={anchorElUser}
//               anchorOrigin={{
//                 vertical: "top",
//                 horizontal: "right",
//               }}
//               keepMounted
//               transformOrigin={{
//                 vertical: "top",
//                 horizontal: "right",
//               }}
//               open={Boolean(anchorElUser)}
//               onClose={handleCloseUserMenu}
//             >
//               {settings.map((setting) => (
//                 <MenuItem key={setting.href} onClick={handleCloseUserMenu}>
//                   <Typography textAlign="center">
//                     <Button sx={{ color: "green" }} href={setting.href}>
//                       {setting.name}
//                     </Button>
//                   </Typography>
//                 </MenuItem>
//               ))}
//             </Menu>
//           </Box>
//         </Toolbar>
//       </Container>
//     </AppBar>
//   );
// }
// export default HeaderAdmin;


import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import AccountCircle from '@mui/icons-material/AccountCircle';
import Switch from '@mui/material/Switch';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import { Avatar, Button } from '@mui/material';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { LogoSystem, NameSystem } from '@/theme/system';

export default function HeaderAdmin({tab}) {
  const route = useRouter();

  const profile = useSelector(state => state.profile.profile)
  const token = useSelector(state => state.account.access_token)

  const fullName = token ? profile.firstname + " "+profile.lastname :  ""
console.log("token",token);
console.log("profile",profile);

// responsive
    
const [state, setState] = React.useState({
  top: false,
  left: false,
  bottom: false,
  right: false,
});

const toggleDrawer = (anchor, open) => (event) => {
  if (
    event &&
    event.type === 'keydown' &&
    (event.key === 'Tab' || event.key === 'Shift')
  ) {
    return;
  }

  setState({ ...state, [anchor]: open });
};

const list = (anchor) => (
  <Box
    sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 300 }}
    role="presentation"
    // onClick={toggleDrawer(anchor, false)}
    onKeyDown={toggleDrawer(anchor, false)}
    
  >
    {tab}
  </Box>
);
// end responsive

  const [auth, setAuth] = React.useState(true);
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleChange = (event) => {
    setAuth(event.target.checked);
  };

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      {/* <FormGroup>
        <FormControlLabel
          control={
            <Switch
              checked={auth}
              onChange={handleChange}
              aria-label="login switch"
            />
          }
          label={auth ? 'Logout' : 'Login'}
        />
      </FormGroup> */}
      <AppBar position="static" sx={{ flexGrow: 1,backgroundColor:'white' }}>
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <LogoSystem/> 
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'green' }}>
            <NameSystem/>
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={toggleDrawer('left', true)}
              color="#198754"
            >
              <MenuIcon/> 
              {/* use when responsive */}
            </IconButton>
            
          </Box>
          
          {!token ? <Button onClick={() => route.push('/auth/login')}>Login</Button> : (<Box>
              <Box
      sx={{
        width: 300,
        hight: "15px",
        display: "flex",
        justifyContent: "flex-end",
        alignItems: 'center',
        margin: '10px 0'
      }}
    >
      
      <Box sx={{color:'green'}}>
        <Box sx={{fontWeight: 500}}>
          {/* {title1} */}
          {fullName}
          </Box>
        <Box>
          {fullName}
          {/* {title2} */}
          {/* <BiSolidSchool /> THPT Pham Van Nghi */}
        </Box>
      </Box>
      <Box sx={{ display:'flex',alignItems:'center' }}>
      
      <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                <Avatar src="https://d.ibtimes.co.uk/en/full/1620522/johnny-depp.jpg" /> 
              </IconButton>
        {/* <Avatar src="https://d.ibtimes.co.uk/en/full/1620522/johnny-depp.jpg" /> */}
      </Box>
    </Box>
              
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <MenuItem onClick={handleClose}>Profile</MenuItem>
                <MenuItem onClick={handleClose}>My account</MenuItem>
              </Menu>
            </Box>) }
        </Toolbar>
      </AppBar>
    </Box>
  );
}