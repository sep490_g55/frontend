import { ActiveGreenButton, CenterFlexBox, ResponsiveCenterFlexBox, UnActiveGreenButton } from "@/components/common_sections";
import { Box, Button } from "@mui/material";
import { useRouter } from "next/router";
import React from "react";

const pages = [
  {
    href: "/home",
    name: "Kho Học liệu",
  },
  {
    href: "/media",
    name: "Kho Media",
  },
];

export const StorePage = () => {
  const router = useRouter();
  const currentUrl = router.asPath;
  
  const [store,setStore] = React.useState(
<CenterFlexBox style={{width:"387px",justifyContent:"space-between", margin:"36px 0"}}>
            <ActiveGreenButton>Kho học liệu</ActiveGreenButton>
            <UnActiveGreenButton onClick={() => router.push("/media")}>Kho Media</UnActiveGreenButton>
    </CenterFlexBox>

  )
  React.useEffect(()=>{
    currentUrl?.includes("/media")&&
     setStore(
      <CenterFlexBox style={{width:"387px",justifyContent:"space-between", margin:"36px 0"}}>
            <UnActiveGreenButton onClick={() => router.push("/home")}>Kho học liệu</UnActiveGreenButton>
            <ActiveGreenButton >Kho Media</ActiveGreenButton>
    </CenterFlexBox>
     )
  },[currentUrl])
  return (
    <>{store}</>
  );
};
