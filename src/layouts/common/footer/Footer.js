import { Box } from '@mui/material'
import React from 'react'

const Footer = () => {
  return (
    <Box
    sx={{width:"100%",height:"200px",backgroundColor:'#EEEEEF',display:'flex',justifyContent: 'center',alignItems: 'center'}}
    >Footer</Box>
  )
}

export default Footer