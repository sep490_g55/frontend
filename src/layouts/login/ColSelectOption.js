import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Button,
  Col,
  Container,
  FormGroup,
  Input,
  Label,
  Row,
} from "reactstrap";
import Form from "react-bootstrap/Form";
import {StyledDivLeftContent} from '@/components/auth/style';
const ColSelectOption = ({label,objects,onSelect}) => {
  return (
    <Col>
      <FormGroup>
        <StyledDivLeftContent>
          <Form.Label>{label} </Form.Label>
          <Form.Select onChange={onSelect}>
                {objects.map(obj =>
                  <option key={obj.code}  value={obj.code}>{obj.name}</option>
                )}
              </Form.Select>
        </StyledDivLeftContent>
      </FormGroup>
    </Col>
  );
};

export default ColSelectOption;
