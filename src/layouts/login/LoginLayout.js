import React, { Children } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

import {
    Button,
    Col,
    Container,
    Form,
    FormGroup,
    Input,
    Label,
    Row,
  } from "reactstrap";
  
import {StyledDivContent,StyledSection,StyledLayout,StyledDivLeftContent,StyledDivRightContent,StyledButtonFull, LoginLayoutContainer} from '@/components/auth/style'
import Link from 'next/link';
import {OverView} from '@/components/auth/common'
import { Box } from '@mui/material';
import Header from '../common/header/Header';
const LoginLayout = ({children}) => {
  return (
    // <Box className='login-box' sx={{width: '100%',height:'100%'}}>
    <LoginLayoutContainer >
      
      <Box  className='body-left'>
        <OverView/>
      </Box>
      <Box className='body-right'>
        {children}
      </Box>
      
    </LoginLayoutContainer>
    // </Box>
  )
}

export default LoginLayout