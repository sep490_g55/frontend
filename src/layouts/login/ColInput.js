import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Button,
  Col,
  Container,
  FormGroup,
  Input,
  Label,
  Row,
} from "reactstrap";
import Form from "react-bootstrap/Form";
import {StyledDivLeftContent} from '@/components/auth/style';
const ColInputUserName = ({label,type,placeholder,name,onInput}) => {
  return (
    <Col>
      <FormGroup>
        <StyledDivLeftContent>
          <Form.Label>{label}</Form.Label>
          <Form.Control type={type} placeholder={placeholder}
                        name = {name} 
                        onChange={onInput}
          />
          
        </StyledDivLeftContent>
      </FormGroup>
    </Col>
  );
};

export default ColInputUserName;
