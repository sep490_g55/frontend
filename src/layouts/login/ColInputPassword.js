import React,{useState} from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { styled, alpha } from '@mui/material/styles';

import {
  Button,
  Col,
  Container,
  FormGroup,
  Input,
  Label,
  Row,
} from "reactstrap";
import Form from "react-bootstrap/Form";
import {StyledDivLeftContent} from '@/components/auth/style';
import {BsFillEyeFill,BsFillEyeSlashFill} from 'react-icons/bs'
const DivPass = styled('div')(({ theme }) => ({
    width:'100%',
    display:'flex',
    justifyContent: 'center',
    alignItems: 'center'
}));
const BtnShow = styled('div')(({ theme }) => ({

    width: '50%',
    cursor: 'pointer',
    display:'flex',
    position: 'absolute',
    zIndex: '100',
    float: 'right',

    justifyContent: 'end'

    
}));
 
const ColInputPassword = ({label,type,placeholder,name,onInput}) => {
    const [isShow, setIsShow] = useState(false)
  const tag = isShow ? <BsFillEyeSlashFill/>:<BsFillEyeFill/>
  return (
    <Col>
      <FormGroup>
        <StyledDivLeftContent>
          <Form.Label>{label}</Form.Label>
          <DivPass>
                <Form.Control type={type} placeholder={placeholder} 
                 name = {name}
                 onChange={onInput}
                />
                <BtnShow 
            onClick={()=>{
                isShow ? setIsShow(false):setIsShow(true)
            }}>
                
            {/* {tag} */}
            </BtnShow>
            </DivPass>
        </StyledDivLeftContent>
      </FormGroup>
    </Col>
  );
};

export default ColInputPassword;
