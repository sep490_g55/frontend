import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import {
    Button,
    Col,
    Container,
    Form,
    FormGroup,
    Input,
    Label,
    Row,
  } from "reactstrap";
const FormInput = ( {children}) => {
  return (
    <Col>
      <FormGroup>
       
        {children}
      </FormGroup>
    </Col>
  );
};

export default FormInput;
