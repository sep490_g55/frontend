import React, { useRef, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Col, Container, Row } from "react-bootstrap";
import { SearchSection } from "@/components/Search/section";
import { FilterMedia, FolderImage } from "@/components/FolderImage/section";

import { Box, Button } from "@mui/material";

//component
import LayoutPage from "@/layouts/LayoutPage";
import Header from "@/layouts/common/header/Header";
import { StorePage } from "@/layouts/common/header/Store";
const media = () => {
  const [type, setType] = useState("IMAGE")

  return (
    <LayoutPage title="Kho media">
      <Header isKhoHocLieu={false} />
      <Container className="mw-100" style={{}}>
        <SearchSection />
        <StorePage/>
        <Box
          sx={{
            width: "100%",
            height: "auto",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >

          <FilterMedia setType={setType}  type={type}/>
          
        </Box>
        <Box
            sx={{float: 'clear'}}
          >
              <FolderImage type={type}/>
          </Box>
      </Container>
    </LayoutPage>
  );
};

export default media;
