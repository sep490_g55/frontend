import { useEffect, useState, useRef } from "react";
import Clipboard from "react-clipboard.js";
import Header from "@/layouts/common/header/Header";
import LayoutPage from "@/layouts/LayoutPage";
import { NextResponse } from 'next/server'
import Cookies from 'cookies'
//icon
import {
  BsDownload,
  BsBookmark,
  BsShareFill,
  BsFillBookmarkFill,
  BsBookmarkFill,
} from "react-icons/bs";

//components
import { SuggestMedias } from "@/components/FolderImage/section";

import {
  ActionButton,
  ActionGreenButton,
  MdActionButton,
} from "@/components/InfoMaterial/styles";

//style components
import {
  ContainerDiv,
  LeftDiv,
  RightDiv,
  InputCmt,
  ContainerInputCommentDiv,
} from "@/components/DetailMaterial/styles";
import { Box, Button, Grid, Skeleton } from "@mui/material";
import {
  DetailMaterialLeftSection,
  LeftInfoDiv,
} from "@/components/InfoMaterial/section";
import SkeletonDetailMaterial from "@/components/Skeleton/DetailMaterial";

import { styled } from "@mui/material/styles";
import { useRouter } from "next/router";
import LoadingTab from "@/components/loading-screen/LoadingTab";
import CommonTableMaterial from "@/components/TableMaterial/SuggestTableMaterial";

//icon
import { AiOutlineCamera } from "react-icons/ai";
import { MdInsertEmoticon } from "react-icons/md";
import { VscSend } from "react-icons/vsc";

import { CenterFlexBox, ImageDetail } from "@/components/common_sections";
import { NormalButton } from "@/components/FolderImage/styles";
import { BeatLoader } from "react-spinners";
import { useDispatch, useSelector } from "react-redux";
import { setSearchInputText } from "@/redux/slice/SearchInputTextSlice";
import {
  actionResource,
  getDetailMaterial,
  getLocalStorage,
  approveResource,
  rejectResource,
  downloadResource,
  createComment,
} from "@/dataProvider/agent";
import { useAuthContext } from "@/auth/useAuthContext";
import { TiTick } from "react-icons/ti";
import { CiCircleRemove } from "react-icons/ci";
import { BsLink } from "react-icons/bs";
import withAuth from "@/auth/withAuthenticated";
// import PowerPointReader from "@/components/File-Reader/PPTXReader";
import { toast, ToastContainer } from "react-toastify";
import { ACTION_SAVED, ACTION_UNSAVED } from "@/config";
import {
  downloadFile,
  getFileNameFromUrl,
  handleCopyClick,
} from "@/utils/handleAction";
import FormDialog from "@/components/Dialog/section";
import ReadALLFile from "@/components/File-Reader/FileReader";
import Player from "@/components/File-Reader/Audio/Player";

import { Form } from "react-bootstrap";
import { setInfoUserReply } from "@/redux/slice/ReplyUser";
import { setIsReload } from "@/redux/slice/IsReLoad";
import { PDFReader } from "@/components/File-Reader/PDFReader";

export default function DetailMaterial () {
  // console.log("responseData",responseData)
  const route = useRouter();
  const [userReply, setUserReply] = useState(null);
  const [material, setMaterial] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingModerator, setIsLoadingModerator] = useState(false);
  const [isLoadingComment, setIsLoadingComment] = useState(false);
  const inputRef = useRef(null);

  const [isSave, setIsSave] = useState(false);
  const [isF5, setIsF5] = useState(false);
  const reloading = useSelector((state) => state.reloading.isReLoad);

  const { user, isAuthenticated } = useAuthContext();
  const roleCurrent = getLocalStorage("role");
  const replyUser = useSelector((state) => state.replyUser.info);
  const [isDownLoading, setIsDownLoading] = useState(false);
  // if(!isAuthenticated){
  //   return <NotAuthPage/>
  // }
  const dispatch = useDispatch();
  const handleClickSuggestTag = (value) => {
    dispatch(setSearchInputText(value));
    route.push("/media");
  };
  const {
    query: { id },
  } = route;

  useEffect(() => {
    if(id){
      getMaterial()
    }
    // focusInput();
    dispatch(setInfoUserReply(null));
  }, [id,isF5]);

  const getMaterial = async () => {

    setIsLoading(true);

    try {
      const res = await getDetailMaterial(id);

      if (res.status === 200) {


        setMaterial(res?.data);
        setIsSave(res.data.isSave);
      }
       else if (res.status === 202) {
        route.push("/not-resource-permission")
        
      }else{

      }
    } catch (e) {
      //console.log("e", e);
    }
    setIsLoading(false);
  };
  //console.log("material", material);

  const onHind = () => {
    // setIsBookSeries(false);
    // setIsClass(false);
    focusInput();
  };

  ///handle expand tab
  const [isExpand, setIsExpand] = useState(true);
  const onExpand = () => {
    isExpand ? setIsExpand(false) : setIsExpand(true);
  };
  // end handle expand tab

  const styleNavBar = !isExpand
    ? { display: "block", fontSize: "24px" }
    : { display: "none" };
  const styleLeftPage = isExpand
    ? { display: "block", paddingLeft: "40px" }
    : { display: "none" };
  const styleRightPage = isExpand ? { width: "70%" } : { width: "100%" };
  const token = getLocalStorage("access_token");
  const handleDownload = async (file) => {
    setIsDownLoading(true)
     downloadFile(getFileNameFromUrl(file), token);
     setIsDownLoading(false)
    };
  //

  const headers = {
    "X-Access-Token": `${token}`,
    Authorization: token ? `Bearer ${token}` : "no auth",
  };

  const handleActive = async (id, type) => {
    if (type === "COPY") {
      toast.success("Lấy đường link thành công");
    } else {
      if (isSave) {
        type = ACTION_UNSAVED;
      }
      if (type === ACTION_SAVED) {
        toast.success("Lưu tài liệu thành công");
        setIsSave(true);
      } else if (type === ACTION_UNSAVED) {
        toast.info("Đã bỏ lưu tài liệu");
        setIsSave(false);
      }
      const res = await actionResource({
        resourceId: id,
        actionType: "" + type,
      });

      if (res.status === 200) {
      } else {
        //console.log("thất bại "+type)
      }
    }
  };

  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const [open2, setOpen2] = useState(false);
  const handleOpen2 = () => {
    setOpen2(true);
  };
  const handleClose2 = () => {
    setOpen2(false);
  };
  const [message, setMessage] = useState("");
  const [comment, setComment] = useState("");

  const handleSubmitApprove = async () => {
    setIsLoadingModerator(true);

    // nếu thành công thì setOpent false

    const res = await approveResource({
      message: message,
      resourceId: id,
      // "reporterId": 1
    });
    if (res.status === 200) {
      setOpen2(false);
      toast.success("Đã xác nhận");
      setIsF5(!isF5);
    } else if (res.status === 202) {
      toast.warning("Kiểm tra lại thông tin");
    } else {
      toast.error("Thất bại");
    }
    setIsLoadingModerator(false);

    // toast.error("Đăng nhập không thành công")
  };
  const handleSubmitReject = async () => {
    setIsLoadingModerator(true);

    // nếu thành công thì setOpent false

    const res = await rejectResource({
      message: message,
      resourceId: id,
      // "reporterId": 1
    });
    if (res.status === 200) {
      setOpen(false);
      toast.success("Đã từ chối tài liệu");
      setIsF5(!isF5);
    } else if (res.status === 202) {
      toast.warning("Kiểm tra lại thông tin");
    } else {
      toast.error("Thất bại");
    }
    setIsLoadingModerator(false);

    // toast.error("Đăng nhập không thành công")
  };

  const handleComment = async () => {
    setIsLoadingComment(true);
    if (!isLoadingComment && comment !== "") {
      const res = await createComment({
        resourceId: id,
        content: comment,
        commentRootId: replyUser?.commentDTOResponse?.commentId,
      });
      if (res.status === 200) {
        // setIsF5((pre) => !pre);
        dispatch(setIsReload(!reloading));

        setComment("");
      }
    }
    setIsLoadingComment(false);
  };
  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      handleComment();
    }
  };
  const handleReplyComment = async (rootId) => {
    const res = await createComment({
      resourceId: id,
      content: comment,
      commentRootId: rootId,
    });
    if (res.status === 200) {
      setIsF5((pre) => !pre);
      setComment("");
    }
  };

  const focusInput = () => {
    // console.log("clicked");
    // inputRef.current?.focus();
  };
  const handleMediaComment = () => {
  
    inputRef.current?.focus();
  };
  return (
    <LayoutPage title="Detail Material page">
      <ToastContainer />

      <Header
        isKhoHocLieu={true}
        tab={
          <LeftInfoDiv
            titleheader={
              <Box
                sx={{ color: "#198754", fontSize: "18px", fontWeight: "bold" }}
              >
                Thông tin học liệu
              </Box>
            }
            onExpand={onExpand}
          >
            {!isLoading ? (
              <DetailMaterialLeftSection
                material={material}
                detailMaterial={material?.resourceDTOResponse}
                owner={material?.owner}
                inputRef={inputRef}
                focusInput={focusInput}
                isF5={isF5}
              />
            ) : (
              <LoadingTab />
            )}

            <ContainerInputCommentDiv>
              {/* {replyUser !== null &&<Box sx={{color:"gray"}}>Trả lời: {replyUser.commentDTOResponse.commenterDTOResponse?.firstname+" "+replyUser.commentDTOResponse.commenterDTOResponse?.lastname}</Box>} */}
              <CenterFlexBox>
                <InputCmt
                  ref={inputRef}
                  type="text"
                  name="comment"
                  id=""
                  value={comment}
                  placeholder={
                    replyUser !== null
                      ? `Trả lời: ${
                          replyUser?.commentDTOResponse?.commenterDTOResponse
                            ?.firstname ||
                          replyUser?.commenterDTOResponse?.firstname
                        } ${
                          replyUser?.commentDTOResponse?.commenterDTOResponse
                            ?.lastname ||
                          replyUser?.commenterDTOResponse?.lastname
                        }`
                      : "Để lại bình luận..."
                  }
                  onChange={(e) => setComment(e.target.value)}
                  onKeyPress={(e) => handleKeyPress(e)}
                  style={{ width: "100%", marginBottom: "10px" }}
                />
              </CenterFlexBox>

              <CenterFlexBox
                style={{
                  justifyContent: "space-between",
                }}
              >
                <CenterFlexBox>
                  <ActionButton style={{ width: "40px" }}>
                    <AiOutlineCamera onClick={ handleMediaComment} />
                  </ActionButton>
                  <ActionButton style={{ width: "40px" }} onClick={handleMediaComment}>
                    <MdInsertEmoticon />
                  </ActionButton>
                </CenterFlexBox>
                <ActionButton style={{ width: "100px", height: "30px" }} onClick={handleComment}>
                  <VscSend style={{ fontSize: "20px", color: "#198754" }} />
                </ActionButton>
              </CenterFlexBox>
            </ContainerInputCommentDiv>
          </LeftInfoDiv>
        }
      />
      <ContainerDiv className="doc-detail" onClick={onHind}>
        <LeftDiv className="left" >
          <LeftInfoDiv
            titleheader={
              <Box
                sx={{
                  color: "#198754",
                  fontSize: "18px",
                  fontWeight: "bold",
                  marginTop: "0px",
                }}
              >
                Thông tin học liệu
              </Box>
            }
            onExpand={onExpand}
          >
            {material ? (
              <DetailMaterialLeftSection
                material={material}
                detailMaterial={material?.resourceDTOResponse}
                owner={material?.owner}
                inputRef={inputRef}
                focusInput={focusInput}
                isF5={isF5}
              />
            ) : (
              <Box sx={{ width: "100%", height: "100%", marginLeft: "10px" }}>
                <SkeletonDetailMaterial />
              </Box>
            )}
            {!material ? (
              ""
            ) : material?.resourceDTOResponse?.approveType === "REJECT" ? (
              <CenterFlexBox style={{ color: "red" }}>
                Nội dung không được chấp thuận
              </CenterFlexBox>
            ) : material?.resourceDTOResponse?.approveType === "ACCEPTED" ? (
              <ContainerInputCommentDiv>
                <CenterFlexBox>
                  {isLoadingComment && (
                    <BeatLoader
                      color="#198754"
                      size={5}
                      style={{ marginLeft: "5px", marginTop: "5px" }}
                    />
                  )}
                </CenterFlexBox>
                {/* {replyUser !== null &&<Box sx={{color:"gray",height:"20px"}}>Trả lời: {replyUser.commentDTOResponse.commenterDTOResponse.userReply?.firstname+" "+replyUser.commentDTOResponse.commenterDTOResponse?.lastname}</Box>} */}

                <CenterFlexBox>
                  <InputCmt
                    ref={inputRef}
                    type="text"
                    name="comment"
                    id=""
                    placeholder={
                      replyUser !== null
                        ? `Trả lời: ${replyUser.commentDTOResponse.commenterDTOResponse?.firstname} ${replyUser.commentDTOResponse.commenterDTOResponse?.lastname}`
                        : "Để lại bình luận..."
                    }
                    onChange={(e) => setComment(e.target.value)}
                    onKeyPress={(e) => handleKeyPress(e)}
                    value={comment}
                    style={{
                      width: "100%",
                      backgroundColor: "#F2F2F2",
                      marginBottom: "10px",
                    }}
                  />
                </CenterFlexBox>
                <CenterFlexBox
                  style={{
                    justifyContent: "space-between",
                  }}
                >
                  <CenterFlexBox
                    style={
                      {
                        justifyContent: "start",
                      }
                    }
                  >
                    <ActionButton style={{ width: "40px" }}>
                      <AiOutlineCamera />
                    </ActionButton>
                    <ActionButton style={{ width: "40px" }}>
                      <MdInsertEmoticon />
                    </ActionButton>
                  </CenterFlexBox>
                  <ActionButton
                    onClick={handleComment}
                    style={{ width: "100px", height: "30px" }}
                  >
                    <VscSend style={{ fontSize: "20px", color: "#198754" }} />
                  </ActionButton>
                </CenterFlexBox>
              </ContainerInputCommentDiv>
            ) : !roleCurrent ? (
              ""
            ) : roleCurrent === "MODERATOR" ? (
              <CenterFlexBox style={{ marginTop: "5px", color: "red" }}>
                <FormDialog
                  title="Kiểm duyệt học liệu"
                  label="Lý do từ chối"
                  open={open}
                  setOpen={handleOpen}
                  setClose={handleClose}
                  handleSubmit={handleSubmitReject}
                  setContentReport={setMessage}
                  contentReport={message}
                  isLoading={isLoadingModerator}
                >
                  <ActionButton style={{ color: "red", marginRight: "15px" }}>
                    <CiCircleRemove style={{ marginRight: "5px" }} />
                    Từ chối
                  </ActionButton>
                </FormDialog>
                <FormDialog
                  title="Kiểm duyệt học liệu"
                  label="Phản hồi"
                  open={open2}
                  setOpen={handleOpen2}
                  setClose={handleClose2}
                  handleSubmit={handleSubmitApprove}
                  setContentReport={setMessage}
                  contentReport={message}
                  isLoading={isLoadingModerator}
                >
                  <ActionGreenButton>
                    <TiTick style={{ marginRight: "5px" }} /> Chấp nhận
                  </ActionGreenButton>
                </FormDialog>
              </CenterFlexBox>
            ) : (
              <CenterFlexBox style={{ color: "#198754" }}>
                Tài liệu đang trong quá trình kiểm duyệt
              </CenterFlexBox>
            )}
          </LeftInfoDiv>
        </LeftDiv>
        <RightDiv className="right" style={styleRightPage}>
          {isLoading ? (
            <Box>
              Đang tải dữ liệu<BeatLoader
                      color="#198754"
                      size={5}
                      style={{ marginLeft: "5px", marginTop: "5px" }}
                    />
              <Box sx={{ textAlign: "center" }}></Box>
            </Box>
          ) : (
            <CenterFlexBox
              style={{
                height: "50px",
                margin: "0 0 30px 0",
                paddingLeft: "10px",
                justifyContent: "flex-end",
              }}
            >
              {material?.resourceDTOResponse?.approveType === "ACCEPTED" ? (
                <>
                  <MdActionButton
                    title="Lưu tài liệu"
                    onClick={() => {
                      handleActive(
                        material?.resourceDTOResponse?.id,
                        ACTION_SAVED
                      );
                    }}
                  >
                    {isSave ? (
                      <BsBookmarkFill style={{ color: "#197854" }} />
                    ) : (
                      <BsBookmark />
                    )}
                  </MdActionButton>
                  <Clipboard
                    data-clipboard-text={`${window.location.protocol}//${window.location.host}${route.asPath}`}
                    button-title={route.asPath}
                  >
                    <MdActionButton
                      title="Lấy link"
                      onClick={() => {
                        handleActive(material?.resourceDTOResponse?.id, "COPY");
                      }}
                    >
                      <BsLink />
                    </MdActionButton>
                  </Clipboard>
                </>
              ) : (
                ""
              )}
              {material?.resourceDTOResponse?.approveType === "ACCEPTED" ||
              material?.owner?.username === user?.username ? (
                <MdActionButton
                  title=""
                  style={{
                    width: "90px",
                    height: "40px",
                    backgroundColor: "#198754",
                    color: "white",
                  }}
                  onClick={() =>
                    handleDownload(material?.resourceDTOResponse?.resourceSrc)
                  }
                >
                  {isDownLoading?<BeatLoader
                      color="white"
                      size={5}
                      style={{ marginLeft: "5px", marginTop: "5px" }}
                    />:<BsDownload />}
                  <Box
                    sx={{
                      fontSize: "14px",
                      marginLeft: "10px",
                      fontWeight: "bold",
                    }}
                  >
                    Tải về
                  </Box>
                </MdActionButton>
              ) : (
                ""
              )}
            </CenterFlexBox>
          )}
          {material ? (
            // material?.resourceDTOResponse?.resourceType === "PDF" ||
            material?.resourceDTOResponse?.resourceType === "PPTX" ||
            material?.resourceDTOResponse?.resourceType === "DOCX" ? (
              // <PDFReader file={material?.resourceDTOResponse?.resourceSrc} />
              <ReadALLFile file={material?.resourceDTOResponse?.resourceSrc} />
            ) 
            :material?.resourceDTOResponse?.resourceType === "PDF"
            // ||
            // material?.resourceDTOResponse?.resourceType === "PPTX" ||
            // material?.resourceDTOResponse?.resourceType === "DOCX" 
            ?
            <PDFReader file={material?.resourceDTOResponse?.resourceSrc}/>
            
            : material?.resourceDTOResponse?.resourceType === "PNG" ||
              material?.resourceDTOResponse?.resourceType === "JPEG" ||
              material?.resourceDTOResponse?.resourceType === "JPG" ? (
              // || material?.resourceDTOResponse.resourceType === "PDF"
              // || material?.resourceDTOResponse.resourceType === "DOCX"
              <ImageDetail
                
                src={material?.resourceDTOResponse?.resourceSrc}
              />
            ) : material?.resourceDTOResponse?.resourceType === "MP4" ? (
              <video
                key={material?.resourceDTOResponse?.id}
                controls
                width="900"
                height="600"
              >
                <source src={material?.resourceDTOResponse?.resourceSrc} />
              </video>
            ) : material?.resourceDTOResponse?.resourceType === "MP3" ? (
              <Player url={material?.resourceDTOResponse?.resourceSrc} />
            ) : (
              <ReadALLFile file={material?.resourceDTOResponse?.resourceSrc} />
            )
          ) : (
            // <PowerPointViewer file={material?.resourceDTOResponse?.resourceSrc}/>
            // <ALLFileReader type={material?.resourceDTOResponse?.resourceType} path={material?.resourceDTOResponse?.resourceSrc}/>
            // <PdfViewerComponent document={material?.resourceDTOResponse?.resourceSrc} />
            // <PowerPointReader/>
            <LoadingTab />
          )}

          <Box
            sx={{
              mt: 9,
            }}
          >
            Tài liệu liên quan
          </Box>
          {material ? (
            material?.resourceDTOResponse?.resourceType === "PPTX" ||
            material?.resourceDTOResponse?.resourceType === "PPT" ||
            material?.resourceDTOResponse?.resourceType === "PDF" ||
            material?.resourceDTOResponse?.resourceType === "DOC" ||

            material?.resourceDTOResponse?.resourceType === "DOCX"||
            material?.resourceDTOResponse?.resourceType === "MP3" || 
            material?.resourceDTOResponse?.resourceType === "MP4" ? (
              <>
                <CommonTableMaterial
                  resourceId={material?.resourceDTOResponse?.id}
                />
              </>
            )  : material?.resourceDTOResponse?.resourceType === "MP4" ? (
              <>
                {/* <Box sx={{ color: "#198754", display: "flex" }}>
                  {material?.listTagRelate?.map((tag, index) => (
                    <NormalButton key={index}>{tag.tagName}</NormalButton>
                  ))}
                </Box> */}
                <SuggestMedias
                  resourceId={material?.resourceDTOResponse?.id}
                  type="VIDEO"
                />
              </>
            ) :  <>
            {/* <Box sx={{ color: "#198754", display: "flex" }}>
              {material?.listTagRelate?.map((tag, index) => (
                <NormalButton
                  key={index}
                  onClick={() => handleClickSuggestTag(tag.tagName)}
                >
                  {tag.tagName}
                </NormalButton>
              ))}
            </Box> */}
            <SuggestMedias
                  resourceId={material?.resourceDTOResponse?.id}
              type="IMAGE"
            />
          </>
          ) : (
            <Grid container wrap="wrap">
              {Array.from(new Array(12)).map((item, index) => (
                <Box key={index} sx={{ width: 150, mr: 5, ml: 5, my: 5 }}>
                  <Skeleton variant="rectangular" width={150} height={118} />

                  <Box sx={{ pt: 0.5 }}>
                    <Skeleton />
                    <Skeleton width="60%" />
                  </Box>
                </Box>
              ))}
            </Grid>
          )}
        </RightDiv>
      </ContainerDiv>
      {/* <RoleBasedGuard hasContent role="TEACHER">
        Không thể truy cập
      </RoleBasedGuard> */}
    </LayoutPage>
  );
};



// export async function getServerSideProps(context) {
//   // Lấy thông tin cookie từ request headers
//   const { req, res } = context;
//   const cookies = new Cookies(req, res);

//   // Lấy giá trị cookie với tên 'token'
//   const token = cookies.get('token');

//   // Kiểm tra nếu không có token, thực hiện chuyển hướng
//   if (!token) {
//     // Thực hiện chuyển hướng đến trang khác (ví dụ: "/login")
//     return {
//       redirect: {
//         destination: '/not-auth', // Đường dẫn của trang muốn chuyển hướng đến
//         permanent: false, // Nếu bạn muốn chuyển hướng này là tạm thời hay vĩnh viễn
//       },
//     };
//   }

//   // Nếu có token, tiếp tục xử lý và trả về props
//   return {
//     props: {},
//   };
// }
