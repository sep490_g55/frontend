import LayoutPage from "@/layouts/LayoutPage";
import Header from "@/layouts/common/header/Header";
import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Col, Container, Row } from "react-bootstrap";
import {
  BookSeries,
  SearchSection,
  ViewClass,
  Subjects,
  BookVolume,
  CategoryMaterial,
  TitleCategory,
  HightTitleCategory,
  ClassCategoryMaterial,
  BookSeriesCategoryMaterial,
  SubjectBookSeriesCategoryMaterial,
  BookVolumeCategoryMaterial,
} from "@/components/Search/section";
import Catalogue from "@/components/Catalogue/catalogue";
import ShowMaterials from "@/components/ShowMaterials/ShowMaterials";
import { Material } from "@/components/ShowMaterials/styles";
import dynamic from "next/dynamic";
import { getListClass } from "@/utils/classesAxios";
import { getBookSeries } from "@/utils/bookSeriesAxios";
import { getSubjects } from "@/utils/subjectAxios";
import { Box, Grid } from "@mui/material";
import { getSlidesWithThumbnail, searchKhoHocLieu } from "@/dataProvider/agent";
import { getSlidesWithThumbnailOLD } from "@/utils/materialsAPI";
import { getMaterials, getNoAuthMaterials } from "@/utils/folderSrc";
import CatalogueResponsive from "@/components/Catalogue/catalogueResponsive";
import { useDispatch, useSelector } from "react-redux";
import { setMaterials } from "@/redux/slice/MaterialSlice";
import {
  setNewMaterial,
  setViewBookSeries,
  setViewBookVolume,
  setViewChapter,
  setViewClass,
  setViewLesson,
  setViewSubject,
} from "@/redux/slice/NewMaterialSlice";
import { BeatLoader } from "react-spinners";
import { ActiveGreenButton, ButtonCustomNextUI, CenterFlexBox, UnActiveGreenButton, WrapCenterFlexBox } from "@/components/common_sections";
import Footer from "@/layouts/common/footer/Footer";
import {
  MUIBookSeriesSelect,
  MUIBookVolumeSelect,
  MUIClassSelect,
  MUISubjectBookSeriesSelect,
} from "@/components/Select/AddressSelect/NEXUI/curriculums";
import { Button } from "@nextui-org/button";
import { StorePage } from "@/layouts/common/header/Store";

const home = () => {
  const dispatch = useDispatch();

  /// new call  api
  const [newBookSeriesList, setNewBookSeriesList] = useState([]);
  const [newSubjectList, setNewSubjectList] = useState([]);
  const [chapterList, setChapterList] = useState([]);
  const [slideList, setSlideList] = useState([]);

  const [classList, setClassList] = useState([]);
  const [bookSeriesList, setBookSeries] = useState([]);
  const [subjectBookSeriesList, setSubjectBookSeriesList] = useState([]);
  const [bookVolumeList, setBookVolumeList] = useState([]);

  // change when user click
  const [classID, setClassID] = useState("");
  const [bookSeriesID, setBookSeriesID] = useState("");
  const [subjectID, setSubjectID] = useState("");
  const [bookVolumeID, setBookVolumeID] = useState("");
  const [chapterID, setChapterID] = useState("");
  const [lessonID, setLessonID] = useState("");
  const [typeTab, setTypeTab] = useState("SLIDE");

  const [isShow, setIsShow] = useState(true);
  const [isClass, setIsClass] = useState(true);
  const [isBookSeries, setIsBookSeries] = useState(true);
  const [isSubject, setIsSubject] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const showBookSeries = isClass ? { display: "block" } : { display: "none" };
  const showSubject = isBookSeries ? { display: "block" } : { display: "none" };
  const showBookVolume = isSubject ? { display: "block" } : { display: "none" };

  const [resource, setResource] = useState(null);

  //
  const newMaterial = useSelector((state) => state.newMaterial.material);
  //console.log("newMaterial hien tai",newMaterial)

  const setBookSeriesListAfter = () => {
    setIsClass(true);
  };
  const setSubjectListAfter = () => {
    setIsBookSeries(true);
  };
  const setBookVolumeListAfter = () => {
    setIsSubject(true);
  };
  const onHind = () => {
    setIsBookSeries(false);
    setIsClass(false);
    setIsSubject(false);
  };

  // materials API
  useEffect(() => {
    getResource();
  }, [
    newMaterial?.class?.id,
    newMaterial?.bookSeries?.id,
    newMaterial?.subject?.id,
    newMaterial?.bookVolume?.id,
    newMaterial?.chapter?.id,
    newMaterial?.lesson?.id,
    newMaterial?.type,
    newMaterial?.pageIndex,
  ]);

  const getResource = async () => {
    setIsLoading(true);

    const api = `resource/materials?classId=${newMaterial?.class?.id}&subjectId=${newMaterial?.subject?.id}&bookVolumeId=${newMaterial?.bookVolume?.id}&chapterId=${newMaterial?.chapter?.id}&lessonId=${newMaterial?.lesson?.id}&bookSeriesId=${newMaterial?.bookSeries?.id}&tabResourceType=${newMaterial?.type}&pageIndex=${newMaterial?.pageIndex}&pageSize=${newMaterial?.pageSize}`;

    try {
      // const resource = await searchKhoHocLieu('/resource/materials',{classId: classID, bookVolumeId:bookVolumeID, chapterId: chapterID,lessonId: lessonID,bookSeriesId: bookSeriesID,tabResourceType:typeTab })
      const resource = await searchKhoHocLieu(api);

      //console.log("resource",resource)
      if (resource.status === 200) {
        //console.log("api",api)

        //console.log("resource: 110",resource.data)

        setResource(resource.data);
      } else {
        //console.log(`slides with thumbnail not found`);
      }
    } catch (error) {
      //console.log(`Can not get slides with thumbnail ${error}`);
    }

    setIsLoading(false);
  };

  // end materials API

  // tab responsive

  //
  return (
    <LayoutPage title="Home page">
      <Header
        isKhoHocLieu={true}
        tab={
          <CatalogueResponsive
            bookVolumeList={bookVolumeList}
            isLoading={isLoading}
            chapterList={chapterList}
            isShow={isShow}
            setIsShow={setIsShow}
          />
        }
      />
      <Box>
        <SearchSection />
        <Container sx={{ padding: "0 10px" }}>
          <StorePage/>
          <WrapCenterFlexBox style={{ justifyContent: "space-around", }}>
            <CenterFlexBox>
            <MUIClassSelect />
            <MUIBookSeriesSelect />
            </CenterFlexBox>
            <CenterFlexBox>
            <MUISubjectBookSeriesSelect />
            <MUIBookVolumeSelect />
            </CenterFlexBox>
            </WrapCenterFlexBox>
          {/* <ClassCategoryMaterial 
       classList={classList}
       setClassList={setClassList}
       />
        <BookSeriesCategoryMaterial 
       classID={newMaterial?.class?.id}
       bookSeriesList={bookSeriesList}
       setBookSeries={setBookSeries}
       classList={classList}
     />
        <SubjectBookSeriesCategoryMaterial 
       bookSeriesID={newMaterial?.bookSeries?.id}
       subjectBookSeriesList={subjectBookSeriesList}
       setSubjectBookSeriesList={setSubjectBookSeriesList}
       bookSeriesList={bookSeriesList}
       />
        <BookVolumeCategoryMaterial 
        subjectID={newMaterial?.subject?.id}
        bookVolumeList={bookVolumeList}
        setBookVolumeList={setBookVolumeList}
        subjectBookSeriesList={subjectBookSeriesList}
        /> */}
          {/* <BookSeries
          isLoading={isLoading}
          bookSeries={newBookSeriesList}
          setIsBookSeries={setSubjectListAfter}
          styleBookSeries={showBookSeries}
          setBookSeriesID={(e)=>setBookSeriesID(e.target.value)}
        />
        <Subjects subjects={newSubjectList} styleSubjects={showSubject} setSubjectID={(e)=>setSubjectID(e.target.value)} setIsSubject={setBookVolumeListAfter}/>
        <BookVolume bookVolumes={bookVolumeList} styleBookVolumes={showBookVolume} setBookVolumeID={(e)=>setBookVolumeID(e.target.value)} /> */}
          {isLoading ? (
            <CenterFlexBox >
              Đang cập nhật{" "}
              <BeatLoader
                color="#198754"
                size={5}
                style={{ marginLeft: "5px", marginTop: "5px" }}
              />
            </CenterFlexBox>
          ) : (
            <></>
          )}
        </Container>
        <Container sx={{ padding: "0 10px" }}><Material>
          <Catalogue
            bookVolumeList={bookVolumeList}
            isLoading={isLoading}
            chapterList={chapterList}
            isShow={isShow}
            setIsShow={setIsShow}
          />
          <ShowMaterials
            resource={resource}
            isLoading={isLoading}
            slideThumbnails={slideList}
            isShow={isShow}
            setIsShow={setIsShow}
            setTypeTab={setTypeTab}
          />
        </Material>
        </Container>
      </Box>
      <Footer />
    </LayoutPage>
  );
};

export default home;
