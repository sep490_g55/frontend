import { useRouter } from 'next/router';
import { useEffect } from 'react';
import LoadingScreen from '@/components/loading-screen/LoadingScreen';
import Head from 'next/head';

// ----------------------------------------------------------------------

// HomePage.getLayout = (page) => <MainLayout> {page} </MainLayout>;

// ----------------------------------------------------------------------

export default function HomePage() {

    const { push } = useRouter()

    useEffect(() => {
        setTimeout(() => {
            push("auth/login")
        }, 1000);
    }, [])

    return (
        <>
            <Head>
            <title>
                VISSDOC
            </title>
        </Head>
            <LoadingScreen />
        </>
    );
}
