import { useRouter } from 'next/router';
import { useEffect } from 'react';
import LoadingScreen from '@/components/loading-screen/LoadingScreen';
import Head from 'next/head';
import { Box, Button } from '@mui/material';
import { useAuthContext } from "@/auth/useAuthContext";

// ----------------------------------------------------------------------

// HomePage.getLayout = (page) => <MainLayout> {page} </MainLayout>;

// ----------------------------------------------------------------------

export default function NotOutTimePage() {

    const { push } = useRouter()
    const {  logout } = useAuthContext();
    const handleLogout= async()=>{
        await logout();
    }
    useEffect(() => {
        handleLogout();

    }, [])

    return (
        <>
            <Head>
            <title>
                VISSDOC - Hết quyền truy cập
            </title>
        </Head>
            <Box sx={{display:"flex",justifyContent:"center",alignItems:"center",height:"800px"}}>
                    Bạn đã hết hạn truy cập. Bạn cần đăng nhập lại, để truy cập vào trang này
                    <Button onClick={() => push("/auth/login")}>Đăng nhập ngay</Button>
            </Box>
        </>
    );
}
