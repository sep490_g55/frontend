
import LayoutPage from '@/layouts/LayoutPage'
import React, { useState } from 'react'

//components
import Header from "@/layouts/common/header/Header";
import {BodyProfile, BodyProfileUser, HeaderProfile} from '@/components/Profile/section'
import { useAuthContext } from "@/auth/useAuthContext";
import withAuth from "@/auth/withAuthenticated";
import {updateAvatar} from "@/dataProvider/agent";
import { ToastContainer, toast } from "react-toastify";
import { useRouter } from "next/router";

//mui
import { Box, Button, Container } from '@mui/material'
import { CenterFlexBox } from '@/components/common_sections';
import { IoIosArrowBack } from 'react-icons/io';
const Profile = () => {
  const router = useRouter()
  const {
    query: { uid },
  } = router;

  const { user,isAuthenticated } = useAuthContext();
  // if(!isAuthenticated){  return( <>kHÔNG CÓ QUYỀN TRUY CẬP</>);}
  
  return (
    <>
        <LayoutPage title='Thông tin tài khoản người dùng'>
                <Header isKhoHocLieu={false} />
                <ToastContainer />
                <Button onClick={() => router.push("/dashboards/admin")}>
          <CenterFlexBox>
            <IoIosArrowBack />
            <Box>Quay lại</Box>{" "}
          </CenterFlexBox>
        </Button>{" "}
                <HeaderProfile />
                <Box sx={{display: 'flex',
                justifyContent:'center',
                alignItems: 'center'}}>
                <BodyProfileUser uid={uid}/>
                </Box>
        </LayoutPage>
    </>
  )
}

export default Profile