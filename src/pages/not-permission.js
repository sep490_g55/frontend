import { useRouter } from 'next/router';
import { useEffect } from 'react';
import LoadingScreen from '@/components/loading-screen/LoadingScreen';
import Head from 'next/head';
import { Box, Button } from '@mui/material';

// ----------------------------------------------------------------------

// HomePage.getLayout = (page) => <MainLayout> {page} </MainLayout>;

// ----------------------------------------------------------------------

export default function NotPermissionPage() {

    const { push } = useRouter()

    // useEffect(() => {
    //     setTimeout(() => {
    //         push("/home")
    //     }, 5000);
    // }, [])

    return (
        <>
            <Head>
            <title>
                VISSDOC - Không có quyền truy cập
            </title>
        </Head>
            <Box sx={{display:"flex",justifyContent:"center",alignItems:"center",height:"800px"}}>
                    Bạn không có quyền truy cập vào trang quản trị viên, hãy liên lạc với quản trị viên adminvissdoc@vissdoc.edu.vn
                    <Button onClick={() => push("/home")}>Trang chủ</Button>
            </Box>
        </>
    );
}
