import React from "react";
import { useEffect } from "react";

import { useRouter } from "next/router";
import yup from "@/utils/yupGlobal";
import { yupResolver } from "@hookform/resolvers/yup";

// import Box from '@mui/material/Box';
import "bootstrap/dist/css/bootstrap.min.css";

import {
  Button,
  Col,
  Container,
  Form,
  FormGroup,
  Label,
  Row,
} from "reactstrap";

import { GoogleLogin, GoogleOAuthProvider } from "@react-oauth/google";
import {
  StyledDivContent,
  StyledSection,
  StyledLayout,
  StyledDivLeftContent,
  StyledDivRightContent,
  StyledButtonFull,
} from "../../components/auth/style";
import Link from "next/link";
import LoginLayout from "@/layouts/login/LoginLayout";
import ColInput from "@/layouts/login/ColInput";
// @mui/material/Box';
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Input from "@mui/material/Input";
import FilledInput from "@mui/material/FilledInput";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";

//@mui/icons-material
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Header from "@/layouts/common/header/Header";
import LayoutPage from "@/layouts/LayoutPage";
import { useAuthContext } from "@/auth/useAuthContext";
import { BeatLoader } from "react-spinners";
import { Alert } from "@mui/material";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from "react-toastify";
import {gotoForgetPassword,submitForgetPassword} from "@/dataProvider/agent"
import ConfirmEmail from "./confirm-email";
export default function ForgoPassword({responseData}) {
  const router = useRouter();

  const [showPassword, setShowPassword] = React.useState(false);
  console.log("responseData 61",responseData)
  if(responseData?.status !== 200){
    toast.warning("Đường link không hợp lệ, xác nhận lại email");
    return <ConfirmEmail/> 
  }
  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleLogin = () => {
    router.push("/auth/login");
  };
  
  const { registerUser } = useAuthContext();
  const [isLoading, setIsLoading] = React.useState(false);
  
  const { token } = router.query;

  

  const schema = yup
    .object({
  
      pass: yup
        .string()
        .required("Không để trống mật khẩu")
        .password("Mật khẩu không đúng định dạng Gợi ý: Thoconxinhxan@s2"),
      repass: yup
        .string()
        .required("Xác nhận lại mật khẩu")
        .test("passwords-match", "Mật khẩu không khớp", function (value) {
          return value === this.parent.pass;
        }),
    })
    .required();
  const {
    reset,
    setError,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const onSubmit = async (data) => {
    setIsLoading(true);
    try {
      console.log(data);
      const res= await submitForgetPassword({"token":token,"newPassword": data.pass,
      "confirmationPassword": data.repass});

      if (res.status === 200) {
        toast.success("Đổi mật khẩu thành công");
        
        setTimeout(() => {
          router.push("/auth/login");
      }, 2000);
      } else if (res.status === 202) {
        console.log("nothing");
        toast.warning("Kiểm tra lại thông tin");
      } else {
        console.log("nothing");
        toast.error("Đổi mật khẩu không thất bại");
      }
    } catch (error) {
      console.error(error);

      // reset();

      setError("afterSubmit", {
        ...error,
        message: error.message,
      });
    }

    setIsLoading(false);
  };
  return (
    <LayoutPage title="Quên mật khẩu">
      <Header />

      <LoginLayout title="Register Page">
        <ToastContainer />

        <StyledLayout>
          <div className="title">
            <h4>Đổi mật khẩu mới</h4>
          </div>
          {/* <GoogleOAuthProvider>

    <GoogleLogin
    // onSuccess={responseMessage} 
    // onError={errorMessage} 
    />
 </GoogleOAuthProvider>
 <StyledDivContent >
  -------------- Hoặc --------------
 </StyledDivContent> */}
          {!!errors.afterSubmit && (
            <Alert severity="error">{errors.afterSubmit.message}</Alert>
          )}

          <Form onSubmit={handleSubmit(onSubmit)}>
           

            
              
            <FormControl
              sx={{ width: "100%", maxWidth: "100%", marginTop: 2 }}
              variant="outlined"
            >
              <InputLabel htmlFor="password">Password</InputLabel>
              <OutlinedInput
                error={errors.pass}
                helperText={errors.pass && errors.pass.message}
                {...register("pass")}
                id="password"
                type={showPassword ? "text" : "password"}
                // onKeyUp={(e) => setData({...data,password: e.target.value})}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Mật khẩu"
              />
            </FormControl>

            <FormControl
              sx={{ width: "100%", maxWidth: "100%", marginTop: 2 }}
              variant="outlined"
            >
              <InputLabel htmlFor="confirm-password">
                Nhập lại mật khẩu
              </InputLabel>
              <OutlinedInput
                error={errors.repass}
                helperText={errors.repass && errors.repass.message}
                {...register("repass")}
                id="confirm-password"
                type={showPassword ? "text" : "password"}
                // onKeyUp={(e) => setData({...data,password: e.target.value})}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Xác nhận mật khẩu"
              />
            </FormControl>

            <StyledDivContent>
              <StyledButtonFull
                 style={{backgroundColor:"#197854"}}
                 className="login-page__btn-login"
                 type={isLoading ?"button":"submit"}
              >
                Tiếp tục &#10149;{" "}
                {isLoading ? (
                  <BeatLoader
                    color="white"
                    size={5}
                    style={{ marginLeft: "5px", marginTop: "5px" }}
                  />
                ) : (
                  ""
                )}
              </StyledButtonFull>
            </StyledDivContent>
          </Form>

          <StyledDivContent>
            Đã có tài khoản?<Link href="login"> Đăng nhập</Link>
          </StyledDivContent>
        </StyledLayout>
      </LoginLayout>
    </LayoutPage>
  );
}
export async function getServerSideProps(context) {
  try {
    const { query } = context;
    const { token } = query;

    const res = await gotoForgetPassword(token);
    console.log("Error in getServerSideProps:", res);
    // Extract the necessary information from the response
    const responseData = {
      status: res.status,
      // Add other relevant properties from the response if needed
    };

    return {
      props: {
        responseData,
      },
    };
  } catch (error) {
    console.error("Error in getServerSideProps:", error);

    return {
      props: {
        error: "An error occurred while fetching data",
      },
    };
  }
}
