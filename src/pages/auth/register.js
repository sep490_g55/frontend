import React from "react";
import { useRouter } from "next/router";
import yup from "@/utils/yupGlobal";
import { yupResolver } from "@hookform/resolvers/yup";

// import Box from '@mui/material/Box';
import "bootstrap/dist/css/bootstrap.min.css";

import {
  Button,
  Col,
  Container,
  Form,
  FormGroup,
  Label,
  Row,
} from "reactstrap";

import { GoogleLogin, GoogleOAuthProvider } from "@react-oauth/google";
import {
  StyledDivContent,
  StyledSection,
  StyledLayout,
  StyledDivLeftContent,
  StyledDivRightContent,
  StyledButtonFull,
} from "../../components/auth/style";
import Link from "next/link";
import LoginLayout from "@/layouts/login/LoginLayout";
import ColInput from "@/layouts/login/ColInput";
// @mui/material/Box';
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Input from "@mui/material/Input";
import FilledInput from "@mui/material/FilledInput";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";

//@mui/icons-material
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Header from "@/layouts/common/header/Header";
import LayoutPage from "@/layouts/LayoutPage";
import { useAuthContext } from "@/auth/useAuthContext";
import { BeatLoader } from "react-spinners";
import { Alert } from "@mui/material";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from "react-toastify";

export default function RegisterPage() {
  const route = useRouter();
  const [showPassword, setShowPassword] = React.useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleLogin = () => {
    route.push("/auth/login");
  };
  const { registerUser } = useAuthContext();
  const [isLoading, setIsLoading] = React.useState(false);

  const schema = yup
    .object({
      username: yup
        .string()
        .required("Không để trống tên tài khoản")
        .username("Tên tài khoản không hợp lệ. Gợi ý: thoconxinhxan"),
      email: yup
        .string()
        .required("Không để trống email")
        .email("Email không hợp lệ Gợi ý: thoconxinhxan@gmail.com"),
      pass: yup
        .string()
        .required("Không để trống mật khẩu")
        .password("Mật khẩu không đúng định dạng Gợi ý: Thoconxinhxan@s2"),
      repass: yup
        .string()
        .required("Xác nhận lại mật khẩu")
        .test("passwords-match", "Mật khẩu không khớp", function (value) {
          return value === this.parent.pass;
        }),
    })
    .required();
  const {
    reset,
    setError,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const onSubmit = async (data) => {
    setIsLoading(true);
    try {
      console.log(data);
      const status = await registerUser(data.username, data.email, data.pass);

      if (status === 200) {
        toast.success("Đăng ký thành công");
        route.push("/auth/get-information");
      } else if (status === 202) {
        console.log("nothing");
        toast.warning("Tên tài khoản hoặc email đã được đăng ký");
      } else {
        console.log("nothing");
        toast.error("Đăng ký không thất bại");
      }
    } catch (error) {
      console.error(error);

      // reset();

      setError("afterSubmit", {
        ...error,
        message: error.message,
      });
    }

    setIsLoading(false);
  };
  return (
    <LayoutPage title="Đăng ký">
      <Header />

      <LoginLayout title="Register Page">
        <ToastContainer />

        <StyledLayout>
          <div className="title">
            <h4>Đăng ký</h4>
          </div>
         
          {!!errors.afterSubmit && (
            <Alert severity="error">{errors.afterSubmit.message}</Alert>
          )}

          <Form onSubmit={handleSubmit(onSubmit)}>
            <Box
              sx={{
                width: "100%",
                maxWidth: "100%",
                marginTop: 2,
                marginBottom: 2,
              }}
            >
              <TextField
                fullWidth
                error={errors.username}
                helperText={errors.username && errors.username.message}
                {...register("username")}
                label="Tên đăng nhập"
                type="text"
                id="username"
                // onKeyUp={(e) => setData({...data,email: e.target.value})}
              />
              {errors.username && (
                <Alert severity="error">{errors.username.message}</Alert>
              )}
            </Box>

            <Box
              sx={{
                width: "100%",
                maxWidth: "100%",
                marginTop: 2,
                marginBottom: 2,
              }}
            >
              <TextField
                fullWidth
                error={errors.email}
                helperText={errors.email && errors.email.message}
                {...register("email")}
                label="Email"
                type="email"
                id="email"
              />
            </Box>
            <FormControl
              sx={{ width: "100%", maxWidth: "100%", marginTop: 2 }}
              variant="outlined"
            >
              <InputLabel htmlFor="password">Password</InputLabel>
              <OutlinedInput
                error={errors.pass}
                helperText={errors.pass && errors.pass.message}
                {...register("pass")}
                id="password"
                type={showPassword ? "text" : "password"}
                // onKeyUp={(e) => setData({...data,password: e.target.value})}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Mật khẩu"
              />
            </FormControl>

            <FormControl
              sx={{ width: "100%", maxWidth: "100%", marginTop: 2 }}
              variant="outlined"
            >
              <InputLabel htmlFor="confirm-password">
                Nhập lại mật khẩu
              </InputLabel>
              <OutlinedInput
                error={errors.repass}
                helperText={errors.repass && errors.repass.message}
                {...register("repass")}
                id="confirm-password"
                type={showPassword ? "text" : "password"}
                // onKeyUp={(e) => setData({...data,password: e.target.value})}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Xác nhận mật khẩu"
              />
            </FormControl>

            <StyledDivContent>
              <StyledButtonFull
                style={{backgroundColor:"#197854"}}
                className="login-page__btn-login"
                type={isLoading ?"button":"submit"}
              > 
                Tiếp tục &#10149;{" "}
                {isLoading ? (
                  <BeatLoader
                    color="white"
                    size={5}
                    style={{ marginLeft: "5px", marginTop: "5px" }}
                  />
                ) : (
                  ""
                )}
              </StyledButtonFull>
            </StyledDivContent>
          </Form>

          <StyledDivContent>
            Đã có tài khoản?<Link href="login"> Đăng nhập</Link>
          </StyledDivContent>
        </StyledLayout>
      </LoginLayout>
    </LayoutPage>
  );
}
