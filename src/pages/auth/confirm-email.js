import React, { useState,useEffect } from "react";
// @mui/material/Box';
import Box from "@mui/material/Box";

//@mui/icons-material

import "bootstrap/dist/css/bootstrap.min.css";


import {
  StyledLayout
 ,  StyledButtonFull,
 StyledDivContent, 
} from "../../components/auth/style";
import LoginLayout from "@/layouts/login/LoginLayout";
import { useRouter } from "next/router";
import { toast,ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import LayoutPage from "@/layouts/LayoutPage";
import { useDispatch } from "react-redux";
import { CenterFlexBox } from "@/components/common_sections";
import { Avatar } from "@mui/material";
const { default: axiosClient } = require("@/utils/axios");
import { getMaterials } from "@/utils/folderSrc";
import { BeatLoader } from "react-spinners";
import TextField from "@mui/material/TextField";
import Header from "@/layouts/common/header/Header";
import { useForm } from 'react-hook-form';
import  yup from "@/utils/yupGlobal"
import { yupResolver } from '@hookform/resolvers/yup';
import { Form } from "reactstrap";
import Link from "next/link";
import { forgetPassword } from "@/dataProvider/agent";
export default function ConfirmEmail() {
  const dispatch = useDispatch();
  const [showPassword, setShowPassword] = React.useState(false);
  const [roles, setRoles] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  const route = useRouter();
  const [data, setData] = useState("");
  const schema = yup
  .object({
    email: yup
    .string()
    .required('Không để trống tên tài khoản')
    .usernameEmail('Kiểm tra tồn tại số, ký tự đặc biệt')
    ,
  
  })
  .required()
  const {
    reset,
    setError,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  })
  const onSubmit = async(data) => {
    setIsLoading(true)
    try {
    console.log(data)
    const res = await forgetPassword(data);
 

    
        if (res.status === 200) {
        toast.success("Kiểm tra hộp thư email để xác thực");
        
      }else if (res.status === 202) {
        
        toast.warning("Email không tồn tại");
      }else{
        toast.error("Không thể xác thực");

      }
      // route.push("/home");
      // console.log("access_token login: ", access_token);
      // console.log("profile login: ", profile);
      
    
  
    } catch (error) {
      console.error(error);

      // reset();

      setError('afterSubmit', {
        ...error,
        message: error.message,
      });
    }
    
    setIsLoading(false)
  }
  
  // const googleClientId = process.env.GOOGLE_CLIENT_ID;
  return (
    <LayoutPage title="Xác thực email ">
      <Header/> 
      <LoginLayout title="Nhập email xác thực">
        <ToastContainer />
        
        <StyledLayout style={{height:"50%",}}>
          <Box sx={{w:"100%",fontSize:"24px",textAlign:"center",mb:10}}>Nhập email xác thực</Box>
          <Form onSubmit={handleSubmit(onSubmit)}>
          <TextField
           {...register("email")}
           type="email"
                  fullWidth
                  label="Email"
                  id="email"
                  onChange={(e) => setData({ ...data, email: e.target.value })}
                  sx={{mb:5}}
                />
                <StyledButtonFull
                style={{backgroundColor:"#197854"}}
                className="login-page__btn-login"
                type={isLoading ?"button":"submit"}
              >
                Xác nhận {isLoading ? <BeatLoader
          color="white"
          size={5}
          style={{marginLeft:"5px",marginTop:"5px"}}
        />:""}
              </StyledButtonFull>
          </Form>
          <StyledDivContent>
              Đã có tài khoản?<Link href="/auth/login"> Đăng nhập</Link>
            
            </StyledDivContent>
        </StyledLayout>
      </LoginLayout>
    </LayoutPage>
  );
}
