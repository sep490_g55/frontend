import LoginLayout from "@/layouts/login/LoginLayout";
import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

import { Button, Col, Container, FormGroup, Label, Row } from "reactstrap";

import { GoogleLogin, GoogleOAuthProvider } from "@react-oauth/google";
import {
  StyledDivContent,
  StyledSection,
  StyledLayout,
  StyledDivLeftContent,
  StyledDivRightContent,
  StyledButtonFull,
} from "../../components/auth/style";
import Link from "next/link";
import Form from "react-bootstrap/Form";
import FormInput from "@/layouts/login/FormInput";
import ColInput from "@/layouts/login/ColInput";
import ColSelectOption from "@/layouts/login/ColSelectOption";
import { getCities } from "@/utils/provinceAxios";
import { getDistricts } from "@/utils/districtAxios";
import { getWards } from "@/utils/wardAxios";

// @mui/material/Box';
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Input from "@mui/material/Input";
import FilledInput from "@mui/material/FilledInput";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";

//@mui/icons-material
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { getProvinces } from "@/utils/addressApi";

//date
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { DateTimePicker } from "@mui/x-date-pickers";
import dayjs from "dayjs";
import { FormGetInfo2 } from "../../components/auth/common";
import LayoutPage from "@/layouts/LayoutPage";
import Header from "@/layouts/common/header/Header";
import { useAuthContext } from "@/auth/useAuthContext";
import { getLocalStorage, getProfile } from "@/dataProvider/agent";
import { ToastContainer, toast } from 'react-toastify';
import { useRouter } from "next/router";
import { isValidToken, jwtDecode } from "@/auth/utils";

const GetInformation = () => {
  const route =useRouter();

  const accessToken = typeof window !== 'undefined' ? getLocalStorage('access_token') : '';

  
  const { getInformation,user,updateInformation } = useAuthContext();
  // console.log("accessToken",accessToken)
  if (isValidToken(accessToken)) {
    const decoded = jwtDecode(accessToken);
      if(decoded?.isValid){
        route.push("/home")
    }
    
  }else{}
  // const [user,setUser] = useState(null)
  const [isLoading, setIsLoading] = React.useState(false);
  const [age, setAge] = useState('');
  const [id, setId] = useState(user?.id);

  const [username, setUsername] = useState(null);
  const [firstName, setFirstName] = useState(null);
  const [lastName, setLastName] = useState(null);
  const [email, setEmail] = useState(null);

  const [gender, setGender] = useState(false );
  const [phone, setPhone] = useState(null);
  const [school, setSchool] = useState(null);
  const [dateOfBirth, setDateOfBirth] = useState(null);
  const [clas, setClas] = useState(null);
  const [province, setProvince] = useState("Hà Nội");
  const [district, setDistrict] = useState("Hoài Đức");
  const [ward, setWard] = useState("An Khánh");

  const [isChange, setIsChange] = useState(false);

  // useEffect(() => {
    
  // }, [isChange])
  const onSubmit=()=>{}
  const handleSubmitForm = async() => {
    getInformation(id,username,firstName, lastName,email,dateOfBirth,phone,gender,school,province,district,ward,clas)
    setIsChange(!isChange)
    updateUser()
    
  }
  const updateUser = async() => {
    setIsLoading(true)

    const res = await updateInformation(id,firstName, lastName,phone,gender,province,district,ward,dateOfBirth,school,clas)
    if(res?.status  ===  200){
      toast.success("Cập nhật thông tin thành công")
      route.push("/auth/login")
    }else if(res?.status  ===  202){
      if(res?.data.email){
        toast.warning(" email đã được đăng ký ")
      }else
      if(res?.data.dateOfBirth){
        toast.warning("Kiểm tra lại ngày sinh")
      }else
      if(res?.data.username){
        toast.warning("tên tài khoản đã được đăng ký")
      }else
      if(res?.data.firstname){
        toast.warning("Kiểm tra lại họ")
      }else
      if(res?.data.lastname){
        toast.warning("Kiểm tra lại tên")
      }else
      if(res?.data.message === "Phone is existed"){
        toast.warning("Sđt đã được đăng ký ")
      }else
      if(res?.data.province){
        toast.warning("Kiểm tra lại ngày tỉnh thành")
      }else
      if(res?.data.district){
        toast.warning("Kiểm tra lại quận huyện")
      }
      else
      if(res?.data.school){
        toast.warning("Kiểm tra lại trường")
      }
      else
      if(res?.data.classId){
        toast.warning("Kiểm tra lại trường")
      }else{
        toast.warning(" Thông tin chưa chính xác")

      }
    }else{
      toast.error("Lấy thông tin thất bại")

    }
    setIsLoading(false)
  }
  
  return (
    <LayoutPage title="Thông tin người dùng">
      {/* <Header/> */}
<ToastContainer />

    <LoginLayout >
      <StyledLayout>
        <Box className="title" sx={{fontSize:'24px',mb:5,mt:5}}>
          Thông tin về bạn
        </Box>

        <FormGetInfo2  titleBtn='Đăng ký' isLoadingSubmit={isLoading} profile={user} handleSubmitForm={handleSubmitForm} setGender={setGender} setUsername={setUsername} setFirstName={setFirstName} setLastName={setLastName}setEmail={setEmail} setPhone={setPhone}  setProvince={setProvince}   setDistrict={setDistrict} setSchool={setSchool} setDateOfBirth={setDateOfBirth} setClas ={setClas} setWard={setWard}/>

        <StyledDivContent>
          Đã có tài khoản?<Link href="login"> Đăng nhập</Link>
        </StyledDivContent>
      </StyledLayout>
    </LoginLayout>
    </LayoutPage>
  );
};

export default GetInformation;
