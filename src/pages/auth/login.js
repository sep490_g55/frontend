import React, { useState } from "react";
import  yup from "@/utils/yupGlobal"
import { yupResolver } from '@hookform/resolvers/yup';

// @mui/material/Box';
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Input from "@mui/material/Input";
import FilledInput from "@mui/material/FilledInput";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import { Alert } from '@mui/material';
//@mui/icons-material
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";

import "bootstrap/dist/css/bootstrap.min.css";

import {
  Button,
  Col,
  Container,
  Form,
  FormGroup,
  Label,
  Row,
} from "reactstrap";

import { GoogleLogin, GoogleOAuthProvider } from "@react-oauth/google";
import {
  StyledDivContent,
  StyledSection,
  StyledLayout,
  StyledDivLeftContent,
  StyledDivRightContent,
  StyledButtonFull,
} from "../../components/auth/style";
import Link from "next/link";
import LoginLayout from "@/layouts/login/LoginLayout";
import ColInput from "@/layouts/login/ColInput";
import { BsFillEyeFill, BsFillEyeSlashFill } from "react-icons/bs";
import ColInputPassword from "@/layouts/login/ColInputPassword";
import { loginUser } from "@/utils/userService";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import GoogleButton from "@/components/LoginGoogle/loginwgoogle";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import {
  getProfile,
  getSlidesWithThumbnail,
  loginAuth,
  setLocalStorage,
} from "@/dataProvider/agent";
import { BASE_URLL, KHOI_URL } from "@/constants/baseApi";
import LayoutPage from "@/layouts/LayoutPage";
import { getSlidesWithThumbnailOLD } from "@/utils/materialsAPI";
import { useDispatch } from "react-redux";
import { setLoggedInAccount } from "@/redux/slice/AccountSlice";
import { setProfile } from "@/redux/slice/ProfileSlice";
const { default: axiosClient } = require("@/utils/axios");
import { BeatLoader } from "react-spinners";
import { getMaterials } from "@/utils/folderSrc";
import { LoadingButton } from "@mui/lab";
import { useForm } from 'react-hook-form';
import Header from "@/layouts/common/header/Header";
import { useAuthContext } from "@/auth/useAuthContext";
import { setRole } from "@/auth/utils";
import withInfo from "@/auth/withInfo";


const LoginPage = ()=> {
  const route = useRouter();

  const { login,ROLES,user,setRoleCurrent,isAuthenticated } = useAuthContext();
  if(isAuthenticated){
    setRole(route)
  }
  

  const [showPassword, setShowPassword] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  
  const schema = yup
  .object({
    email: yup
    .string()
    .required('Không để trống tên tài khoản')
    .usernameEmail('Kiểm tra tồn tại số, ký tự đặc biệt')
    ,
  pass: yup
    .string()
    .required('Không để trống mật khẩu')
    .password('Mật khẩu không đúng định dạng'),
  })
  .required()
  const {
    reset,
    setError,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  })
  const onSubmit = async(data) => {
    setIsLoading(true)
    try {
    //console.log(data)
    const status = await login(data.email,data.pass);
    //console.log("status",status === -1);
    //console.log("roles",ROLES);

    if (status === -1) {
      toast.info("Cập nhật thông tin");
      route.push("/auth/get-information")
      
     
    }
       else if (status === 200) {
        toast.success("Đăng nhập thành công");
        setTimeout(() => {
          setRole(route)
      }, 3000);
        
      }else{
        toast.error("Đăng nhập không thành công");
      }

      
    
  
    } catch (error) {


      setError('afterSubmit', {
        ...error,
        // message: error.message,

        message: error.message === "Cannot read properties of undefined (reading 'split')"?"Thông tin tài khoản không chính xác":error.message === "Cannot read properties of undefined (reading 'access_token')"?"Kiểm tra kết nối mạng":"Có vấn đề của server,báo cáo với hệ thống",
      });
    }
    
    setIsLoading(false)
  }
  return (
    <LayoutPage title="Đăng nhập">
      <Header/>
      <LoginLayout title="Login Page">
        <ToastContainer />
        <StyledLayout>
         
            <Box
              className="title"
              sx={{ fontSize: "24px", marginBottom: "40px" }}
            >
              Đăng nhập
            </Box>
            {!!errors.afterSubmit && <Alert severity="error">{errors.afterSubmit.message}</Alert>}
            
 
            <Form onSubmit={handleSubmit(onSubmit)} className="login-page_form">
     
              <Box
                sx={{
                  width: "100%",
                  maxWidth: "100%",
                }}
              >
                <TextField
                {...register("email")}
                  fullWidth
                  label="Email/Tên đăng nhập"
                  id="email"
                  
                 

                />
                {errors?.email && <Alert severity="error">{errors?.email?.message}</Alert>}
              </Box>

              <FormControl
                sx={{
                  width: "100%",
                  maxWidth: "100%",

                  marginTop: 2,
                }}
                variant="outlined"
              >
                <InputLabel htmlFor="password">Password</InputLabel>
                <OutlinedInput
                {...register("pass")}

                  id="password"
                  type={showPassword ? "text" : "password"}
            
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  }
                  label="Password"
                />
                {errors.pass && <Alert severity="error">{errors.pass.message}</Alert>}

              </FormControl>

              <StyledDivRightContent>
                <Link href="confirm-email">Quên mật khẩu ? </Link>
              </StyledDivRightContent>
              <StyledDivContent>
                <StyledDivLeftContent>
                  <FormGroup check className="login-page__remember">
                    <Label check>
                      <Input
                        type="checkbox"
                        // onChange={(e) => {
                        //   handleDataChange("isRemember", e.target.checked);
                        // }}
                      />
                      Ghi nhớ đăng nhập
                    </Label>
                  </FormGroup>
                </StyledDivLeftContent>
              </StyledDivContent>
              <StyledButtonFull
                // outline
              
                style={{backgroundColor:"#197854"}}
                className="login-page__btn-login"
                type={isLoading ||isAuthenticated ?"button":"submit"}
              >
                Đăng nhập {isLoading ? <BeatLoader
          color="white"
          size={5}
          style={{marginLeft:"5px",marginTop:"5px"}}
        />:""}
              </StyledButtonFull>
              {/* <LoadingButton
        fullWidth
        color="inherit"
        size="large"
        type="submit"
        variant="contained"
        loading={isLoading}
        sx={{
          bgcolor: "#198754",
          color: 'white',
          '&:hover': {
            bgcolor: 'green',
          },
        }}
      >
        Đăng nhập
      </LoadingButton> */}
            </Form>

            <StyledDivContent>
              Chưa có tài khoản?<Link href="register"> Đăng ký</Link>
              {/* <NextLink href="register" passHref>
          <Link variant="body2" color="inherit" underline="always" >
          Đăng kýh
          </Link>
        </NextLink> */}
            </StyledDivContent>
          
        </StyledLayout>
      </LoginLayout>
    </LayoutPage>
  );
}
export default withInfo(LoginPage);

