import React, { useState,useEffect } from "react";
// @mui/material/Box';
import Box from "@mui/material/Box";

//@mui/icons-material

import "bootstrap/dist/css/bootstrap.min.css";


import {
  StyledLayout
} from "../../components/auth/style";
import LoginLayout from "@/layouts/login/LoginLayout";
import { useRouter } from "next/router";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import LayoutPage from "@/layouts/LayoutPage";
import { useDispatch } from "react-redux";
import { CenterFlexBox } from "@/components/common_sections";
import { Avatar } from "@mui/material";
const { default: axiosClient } = require("@/utils/axios");
import { getMaterials } from "@/utils/folderSrc";
import { BeatLoader } from "react-spinners";
import { useAuthContext } from "@/auth/useAuthContext";
import { setLocalStorage } from "@/dataProvider/agent";
import HomePage from "../home"
import { AdminSystem, ModeratorSystem, TeacherSystem } from "@/theme/system";
import Cookies from "js-cookie";
export default function SwitchRole() {
  const dispatch = useDispatch();
  const [showPassword, setShowPassword] = React.useState(false);
  const [roles, setRoles] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(false);
  const { ROLES ,setRoleCurrent} = useAuthContext();
  //console.log("ROLES",ROLES)
  const route = useRouter();
 if( ROLES.length === 1) {route.push("/home") }

  const handleSwitch=(uri,value)=>{
    Cookies.set("role", value);
    setLocalStorage("role",value);
    route.push(uri);
    
  }
  // const googleClientId = process.env.GOOGLE_CLIENT_ID;
  return (
    <LayoutPage title="Đối tượng truy cập">
      <LoginLayout title="Đối tượng truy cập">
        <ToastContainer />
        
        <StyledLayout style={{height:"100%",}}>
          <Box sx={{w:"100%",fontSize:"24px",textAlign:"center",mb:10}}>Đăng nhập với</Box>
          <CenterFlexBox >
         {/* {//console.log("ROLES2",ROLES)} */}
          {
        //   isLoading ? <BeatLoader
        //   color="#198754"
        //   size={5}
        //   style={{marginLeft:"5px",marginTop:"5px"}}
        // />: 
        
        ROLES?.map(role =>
            role === "ADMIN" ?
            <CenterFlexBox key={1} style={{height:"200px",flexDirection:"column",cursor:"pointer",borderRadius:10,margin:15,boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px'}} onClick={() =>handleSwitch("/dashboards/admin","ADMIN")}>
                    <AdminSystem/>
                    <Box>ADMIN</Box>
                </CenterFlexBox>
               :
                ""
          ) 
        }
        {ROLES?.map(role =>
            role === "TEACHER" ?
            <CenterFlexBox key={3} style={{height:"200px",flexDirection:"column",cursor:"pointer",borderRadius:10,margin:15,boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px'}} onClick={() => handleSwitch("/home","TEACHER")}>
                    <TeacherSystem/>
                    <Box>TEACHER</Box>
                </CenterFlexBox>
               :
                ""
          ) 
        }
        {ROLES?.map(role =>
            role === "MODERATOR" ?
            <CenterFlexBox key={2} style={{height:"200px",flexDirection:"column",cursor:"pointer",margin:15,borderRadius:10,boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px'}} onClick={() => handleSwitch("/materials/moderator/list","MODERATOR")}>
                    <ModeratorSystem/>
                    <Box>MODERATOR</Box>
                </CenterFlexBox>
               :
                ""
          ) 
        }
        
          </CenterFlexBox>
        </StyledLayout>
      </LoginLayout>
    </LayoutPage>
  );
}
