import { useSelector } from "react-redux";
import TabMaterialList from "@/components/DashBoard/TabManageMaterials/MaterialsList";


export default function TabPermission({api}) {
  

  return(
    // <TabFolderMaterial  nextTabIndex={nextTabIndex} setFolderID={setFolderID} api={api} handleView={handleView}  type="BOOK-VOLUME"></TabFolderMaterial>
    <TabMaterialList  uri={api} useFor="ADMIN-PERMISSION" ></TabMaterialList>
  
    )
}
