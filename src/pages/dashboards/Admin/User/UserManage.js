import React, { useState } from "react";
import {
  Table,
  TableHeader,
  TableColumn,
  TableBody,
  TableRow,
  TableCell,
  Input,
  Button,
  DropdownTrigger,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Chip,
  User,
  Pagination
} from "@nextui-org/react";
// import {PlusIcon} from "./PlusIcon";
import { VerticalDotsIcon } from "@/components/DashBoard/TabManageMaterials/VerticalDotsIcon";
// import {SearchIcon} from "./SearchIcon";
// import {ChevronDownIcon} from "./ChevronDownIcon";
import { statusOptions } from "@/components/DashBoard/TabManageMaterials/data";
import { capitalize } from "@/components/DashBoard/TabManageMaterials/utils";
import { Search } from "@mui/icons-material";
import { BiChevronDown } from "react-icons/bi";
import { PiPlusCircleDuotone } from "react-icons/pi";
import { useRouter } from "next/navigation";
import { NextUIDistrictSelect, NextUIProvinceSelect } from "@/components/Select/AddressSelect/NEXUI/sections";
import { useEffect } from "react";
import { getMaterials } from "@/utils/folderSrc";
import { CustomizedMenus } from "@/components/Option/menu";
import { format } from 'date-fns';
import { RingLoader } from "react-spinners";
import { AvatarAndInfoDiv } from "@/components/common_sections";

const statusColorMap = {
  active: "success",
  paused: "danger",
  vacation: "warning",
};
const columns = [
  { name: "STT", uid: "no", sortable: true },
  { name: "Tên tài khoản", uid: "username", sortable: true },

  { name: "Họ", uid: "firstname", sortable: true },

  { name: "Tên", uid: "lastname", sortable: true },
  { name: "Ngày sinh", uid: "dateOfBirth", sortable: true },
  { name: "Giới tính", uid: "gender", sortable: true },
  { name: "SĐT", uid: "phone", sortable: true },
  { name: "Tỉnh", uid: "district", sortable: true },
  { name: "Huyện", uid: "province", sortable: true },
  { name: "Trường", uid: "school", sortable: true },
  { name: "Lớp", uid: "classId", sortable: true },
  { name: "Trạng thái", uid: "active", sortable: true },
  { name: "", uid: "actions" },
];
const INITIAL_VISIBLE_COLUMNS = [
  "name",
  "username",
  "firstname",
  "lastname",
  "province",
  "school",
  "active",

  "actions",
];
const StyledUserImg = {
  opacity:"1 !important"
}; 

export default function TabUserList () {
  // const originalDate = detailMaterial.createdAt
  const formattedDate = format(new Date("2023-11-05T20:15:58"), 'dd/MM/yyyy');
 const [users, setUsers] = useState([{username:"hoangadma",firstname:"Nguyễn Việt",lastname:"Hoàng",dateOfBirth:"03/10/2001",gender:true,phone:"0988903540",district:"Nam Định",province:"Ý Yên",school:"THPT Phạm Văn Nghị",classId:"Lớp 12",active:true,avatar:"https://m.media-amazon.com/images/M/MV5BOTBhMTI1NDQtYmU4Mi00MjYyLTk5MjEtZjllMDkxOWY3ZGRhXkEyXkFqcGdeQXVyNzI1NzMxNzM@._V1_.jpg"},])

  const router = useRouter();


  const [nameFilter, setNameFilter] = React.useState("");
  const [selectedKeys, setSelectedKeys] = React.useState(new Set([]));
  const [visibleColumns, setVisibleColumns] = React.useState(
    new Set(INITIAL_VISIBLE_COLUMNS)
  );
  const [visualTypeFilter, setVisualTypeFilter] = React.useState(new Set([""]));

  const [statusFilter, setStatusFilter] = React.useState(new Set([""]));
  const [materialTypeFilter, setMaterialTypeFilter] = React.useState("");    
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [sortDescriptor, setSortDescriptor] = React.useState({
    column: "age",
    direction: "ascending",
  });
  const [totalElement, setTotalElement] = React.useState(0);

  const [page, setPage] = React.useState(1);
  const [pages, setPages] = React.useState(1);  // pages is totalPage within filtered


  // new handle

  const [isLoading, setIsLoading] = React.useState(false);

  const handleSetStatusFilter = (e) => {
    setIsLoading(true);
    setStatusFilter(e);
    setPage(1);

  };

  const handleSetMaterialTypeFilter = (e) => {
    setIsLoading(true);
    setMaterialTypeFilter(e);
    setPage(1);

  };

  const handleSetVisualTypeFilter = (e) => {
    setIsLoading(true);
    setVisualTypeFilter(e);
    setPage(1);

  };
  const handleSetPage = (index) => {
    setIsLoading(true);
    setPage(index);
  };
  
//   useEffect(() => {
//     getMyMaterial();
//   }, [page,rowsPerPage,nameFilter,statusFilter,visualTypeFilter]);
//   const getMyMaterial = async () => {
//     const statusFilterValue = Array.from(statusFilter)[0];
//     const visualTypeFilterValue = Array.from(visualTypeFilter)[0];

// const api=""

//     try {
//       const res = await getMaterials(api);
//       if (res) {
//         console.log("res", res);
       
//         setPages(res.totalPage)
//         setMyMaterial(res);
//         setTotalElement(res.totalElement)
//       }
//       setIsLoading(false);
//     } catch (e) {
//       console.log("e", e);
//     }
//   };
  ///
  const hasSearchFilter = Boolean(nameFilter);

  const headerColumns = React.useMemo(() => {
    if (visibleColumns === "all") return columns;

    return columns.filter((column) =>
      Array.from(visibleColumns).includes(column.uid)
    );
  }, [visibleColumns]);

  const filteredItems = React.useMemo(() => {
    if (users) {
      let filteredMaterial = [...users];
      return filteredMaterial;
    }
    return [];
  }, [users, statusFilter, materialTypeFilter]);

 
  const sortedItems = React.useMemo(() => {
    return [...filteredItems].sort((a, b) => {
      const first = a[sortDescriptor.column];
      const second = b[sortDescriptor.column];
      const cmp = first < second ? -1 : first > second ? 1 : 0;

      return sortDescriptor.direction === "descending" ? -cmp : cmp;
    });
  }, [sortDescriptor, filteredItems]);

  const renderCell = React.useCallback((user, columnKey) => {
    const cellValue = user[columnKey];

    switch (columnKey) {
      case "username":
        return (
        <AvatarAndInfoDiv
        avatarsrc="https://i.pravatar.cc/150?u=a04258114e29026702d"
        title1="hoangadma"
        title2="hoangnvliu@gmail.com"

        />)
      case "firstname":
        return cellValue;
        case "lastname":
        return cellValue;
        case "dateOfBirth":
          return cellValue;
      case "createdAt":
       const date = format(new Date(user.createdAt), 'MM/dd/yyyy hh:mm:ss')
        return <Box title="ngày/giờ">{date}</Box>;
     
      
      
      case "active":
        return (
          <Chip
            className="capitalize"
            color={user.active ? "success":"danger" }
            size="sm"
            variant="flat"
          >
            {user.active? "Hoạt động":"Không hoạt động"}
          </Chip>
        );
      case "actions":
        
        return (
          <div className="relative flex justify-end items-center gap-2">
          <CustomizedMenus
              type="ADMIN-USER"
              name={user.email}
            />
        </div>
        );
      default:
        return cellValue;
    }
  }, []);

  const onNextPage = React.useCallback(() => {
    if (page < pages) {
      handleSetPage(page + 1)
    }
  }, [page, pages]);

  const onPreviousPage = React.useCallback(() => {
    if (page > 1) {
      handleSetPage(page - 1)

    }
  }, [page]);

  const onRowsPerPageChange = React.useCallback((e) => {
    setRowsPerPage(Number(e.target.value));
    handleSetPage(1)

  }, []);

  const onSearchChange = React.useCallback((value) => {
    if (value) {
      setNameFilter(value);
      
    handleSetPage(1)

    } else {
      setNameFilter("");
    }
  }, []);

  const onClear = React.useCallback(() => {
    setNameFilter("");
    handleSetPage(1)

  }, []);

  const topContent = React.useMemo(() => {
    return (
      <>
        <div className="flex flex-col gap-4">
          <div className="flex justify-between gap-3 items-end">
           <div></div>
            <div className="flex gap-4">
            <Dropdown>
                <DropdownTrigger className="hidden sm:flex">
                  <Button
                    endContent={<BiChevronDown className="text-small" />}
                    variant="flat"
                  >
                    Vai trò
                  </Button>
                </DropdownTrigger>
                <DropdownMenu
                  aria-label="Table Columns"
                  closeOnSelect={true}
                  selectedKeys={materialTypeFilter}
                  selectionMode="single"
                  onSelectionChange={handleSetMaterialTypeFilter}
                >
                 
                    <DropdownItem key={1} textValue={1}  className="capitalize">
                     Admin
                    </DropdownItem>
                    <DropdownItem key={2} textValue={2}  className="capitalize">
                     Moderator
                    </DropdownItem>
                    <DropdownItem key={3} textValue={3}  className="capitalize">
                     Teacher
                    </DropdownItem>
                 
                </DropdownMenu>
              </Dropdown>
              
              <Dropdown>
                <DropdownTrigger className="hidden sm:flex">
                  <Button
                    endContent={<BiChevronDown className="text-small" />}
                    variant="flat"
                  >
                    Trạng thái
                  </Button>
                </DropdownTrigger>
                <DropdownMenu
                  disallowEmptySelection
                  aria-label="Table Columns"
                  closeOnSelect={true}
                  selectedKeys={statusFilter}
                  selectionMode="single"
                  onSelectionChange={handleSetStatusFilter}
                >
                  {statusOptions.map((status) => (
                    <DropdownItem key={status.uid} textValue={status.uid} className="capitalize">
                      {capitalize(status.name)}
                    </DropdownItem>
                  ))}
                </DropdownMenu>
              </Dropdown>
              <Dropdown>
                <DropdownTrigger className="hidden sm:flex">
                  <Button
                    endContent={<BiChevronDown className="text-small" />}
                    variant="flat"
                  >
                    Cột thông tin
                  </Button>
                </DropdownTrigger>
                <DropdownMenu
                  disallowEmptySelection
                  aria-label="Table Columns"
                  closeOnSelect={false}
                  selectedKeys={visibleColumns}
                  selectionMode="multiple"
                  onSelectionChange={setVisibleColumns}
                >
                  {columns.map((column) => (
                    <DropdownItem key={column.uid} className="capitalize">
                      {capitalize(column.name)}
                    </DropdownItem>
                  ))}
                </DropdownMenu>
              </Dropdown>
              <Button color="primary" endContent={<PiPlusCircleDuotone />}>
                Add New
              </Button>
            </div>
            
          </div>
          <div className="flex flex-col gap-4 items-end">
            
        <Input
              isClearable
              className="w-full sm:max-w-[50%]"
              placeholder="Tìm kiếm theo tên..."
              startContent={<Search />}
              value={nameFilter}
              onClear={() => onClear()}
              onValueChange={onSearchChange}
            />
        </div>
          <div className="flex justify-between items-center">
            <span className="text-default-400 text-small">
              Tổng có {totalElement} tài liệu
            </span>
            <label className="flex items-center text-default-400 text-small">
              Tài liệu hiển thị trên 1 trang:
              <select
                className="bg-transparent outline-none text-default-400 text-small"
                onChange={onRowsPerPageChange}
              >
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
              </select>
            </label>
          </div>
        </div>
        
      </>
    );
  }, [
    materialTypeFilter,
    statusFilter,
    visibleColumns,
    onRowsPerPageChange,
    nameFilter,
    onSearchChange,
    hasSearchFilter,
    visualTypeFilter,
    users,
  ]);

  const bottomContent = React.useMemo(() => {
    return (
      <div className="py-2 px-2 flex justify-between items-center">
        <span className="w-[30%] text-small text-default-400">
         
        </span>
        <Pagination
          isCompact
          showControls
          showShadow
          color="primary"
          page={page}
          total={pages}
          onChange={handleSetPage}
        />
        <div className="hidden sm:flex w-[30%] justify-end gap-2">
          <Button
            isDisabled={pages === 1}
            size="sm"
            variant="flat"
            onPress={onPreviousPage}
          >
            Previous
          </Button>
          <Button
            isDisabled={pages === 1}
            size="sm"
            variant="flat"
            onPress={onNextPage}
          >
            Next
          </Button>
        </div>
      </div>
    );
  }, [selectedKeys, page, pages, hasSearchFilter]);

  return (
    <>
    
     <Table
        aria-label="Example table with custom cells, pagination and sorting"
        isHeaderSticky
        bottomContent={bottomContent}
        bottomContentPlacement="outside"
        classNames={{
          wrapper: "max-h-[382px]",
        }}
        // selectedKeys={selectedKeys}
        // selectionMode="multiple"
        sortDescriptor={sortDescriptor}
        topContent={topContent}
        topContentPlacement="outside"
        onSelectionChange={setSelectedKeys}
        onSortChange={setSortDescriptor}
        color="success"
          selectionMode="single"
      >
        <TableHeader columns={headerColumns}>
          {(column) => (
            <TableColumn
              key={column.uid}
              align={column.uid === "actions" ? "center" : "start"}
              allowsSorting={column.sortable}
            >
              {column.name}
            </TableColumn>
          )}
        </TableHeader>
        <TableBody emptyContent={isLoading ?"Đang tải dữ liệu" : "Không thấy dữ liệu"} items={sortedItems} isLoading={isLoading}
            loadingContent={<RingLoader color="#198754" />}>
          {(item) => (
            <TableRow key={item.username}>
              {(columnKey) => (
                <TableCell>{renderCell(item, columnKey)}</TableCell>
              )}
            </TableRow>
          )}
        </TableBody>
      </Table>
    </>
  );
}
