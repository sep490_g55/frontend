
import TabMaterialList from "@/components/DashBoard/TabManageMaterials/MaterialsList";


export default function TabClass({handleView,api,setFolderID,nextTabIndex}) {
  return(
    // <TabMaterialList nextTabIndex={nextTabIndex} setFolderID={setFolderID} api={api} handleView={handleView} type="CLASS"></TabMaterialList>
    <TabMaterialList  uri={api} handleView={handleView} useFor="ADMIN" type="CLASS" setFolderID={setFolderID} nextTabIndex = {nextTabIndex}></TabMaterialList>
  
    )
}