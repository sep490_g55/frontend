import TabMaterialList from "@/components/DashBoard/TabManageMaterials/MaterialsList";


export default function TabChapters({handleView,api,setFolderID,nextTabIndex}) {
  return(
    // <TabFolderMaterial nextTabIndex={nextTabIndex} setFolderID={setFolderID} api={api} handleView={handleView}  type="CHAPTER"></TabFolderMaterial>
    <TabMaterialList  uri={api} handleView={handleView} useFor="ADMIN" type="CHAPTER" setFolderID={setFolderID} nextTabIndex = {nextTabIndex}></TabMaterialList>
  
    )
}