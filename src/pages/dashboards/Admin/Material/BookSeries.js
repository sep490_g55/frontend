import { useSelector } from "react-redux";
import TabMaterialList from "@/components/DashBoard/TabManageMaterials/MaterialsList";


export default function TabBookSeries({handleView,api,setFolderID,nextTabIndex}) {
  const objClass = useSelector((state) => state.viewFolder.class);
  return(
    // <TabFolderMaterial objClass={objClass} nextTabIndex={nextTabIndex} setFolderID={setFolderID} api={api} handleView={handleView}  type="BOOK-SERIES"></TabFolderMaterial>
    <TabMaterialList  uri={api} handleView={handleView}  useFor="ADMIN" type="BOOK-SERIES" setFolderID={setFolderID} nextTabIndex = {nextTabIndex}></TabMaterialList>
  
    )
}
