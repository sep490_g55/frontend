import TabMaterialList from "@/components/DashBoard/TabManageMaterials/MaterialsList";


export default function TabLesson({handleView,api,setFolderID,nextTabIndex}) {
  return(
    // <TabFolderMaterial nextTabIndex={nextTabIndex} setFolderID={setFolderID} api={api} handleView={handleView}  type="LESSON"></TabFolderMaterial>
    <TabMaterialList  uri={api} handleView={handleView} useFor="ADMIN" type="LESSON" setFolderID={setFolderID} nextTabIndex = {nextTabIndex}></TabMaterialList>
  
    )
}