import { useSelector } from "react-redux";
import TabMaterialList from "@/components/DashBoard/TabManageMaterials/MaterialsList";


export default function TabBookVolume({handleView,api,setFolderID,nextTabIndex}) {
  

  return(
    // <TabFolderMaterial  nextTabIndex={nextTabIndex} setFolderID={setFolderID} api={api} handleView={handleView}  type="BOOK-VOLUME"></TabFolderMaterial>
    <TabMaterialList  uri={api} handleView={handleView} useFor="ADMIN" type="BOOK-VOLUME" setFolderID={setFolderID} nextTabIndex = {nextTabIndex}></TabMaterialList>
  
    )
}
