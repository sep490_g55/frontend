import TabMaterialList from "@/components/DashBoard/TabManageMaterials/MaterialsList";

export default function TabSubjectBookSeries({handleView,api,setFolderID,nextTabIndex,type}) {

  return(
    // <TabFolderMaterial nextTabIndex={nextTabIndex} setFolderID={setFolderID} api={api} handleView={handleView}  type={type}></TabFolderMaterial>
    <TabMaterialList  uri={api} handleView={handleView} useFor="ADMIN" type={type} setFolderID={setFolderID} nextTabIndex = {nextTabIndex}></TabMaterialList>
  
    )
}