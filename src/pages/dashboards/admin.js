import LayoutPage from "@/layouts/LayoutPage";
import HeaderAdmin from "@/layouts/common/header/HeaderAdmin";
import React from "react";
import { ToastContainer } from "react-toastify";
import { AdminDashBoardContainer,NestedList } from "../../components/DashBoard/Admin/sections";
import Header from "@/layouts/common/header/Header";
import { useAuthContext } from "@/auth/useAuthContext";
import withAuth from "@/auth/withAuthAdmin";
import { useRouter } from "next/router";

 const Admin = () => {
  const [tabValue, setTabValue] = React.useState(0);
  const [open, setOpen] = React.useState(false);
  const [selectedTab, setSelectedTab] = React.useState(0);
  const [subSelectedTab, setSubSelectedTab] = React.useState(20);
  return (
    <LayoutPage title="DashBoard Admin">
      <ToastContainer />
      <Header tab={<NestedList  setTabValue={setTabValue} open={open} setOpen={setOpen} selectedTab={selectedTab} setSelectedTab={setSelectedTab} subSelectedTab={subSelectedTab} setSubSelectedTab={setSubSelectedTab}/>} />
      <AdminDashBoardContainer tabValue={tabValue} setTabValue={setTabValue} open={open} setOpen={setOpen} selectedTab={selectedTab} setSelectedTab={setSelectedTab} subSelectedTab={subSelectedTab} setSubSelectedTab={setSubSelectedTab}/>
    </LayoutPage>
  );
};
export default Admin