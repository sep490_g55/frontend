
import './globals.css'
import * as React from "react";
import { SessionProvider } from "next-auth/react"
import { Provider } from 'react-redux';
import { useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Head from "next/head";
import NextUIProviders from './providers';
import store from '@/redux/store'
import ProgressBar from '../components/progress-bar';
import { AuthContext, AuthProvider } from '@/auth/JwtContext';
export default function App({
  Component,
  // emotionCache = clientSideEmotionCache,
  pageProps: { session, ...pageProps },
}) {
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");
  }, []);
  return (
    <>
    <Head>
    <meta name="viewport" content="initial-scale=1, width=device-width" />
    </Head>
    <AuthProvider>
    <Provider store={store}>
    <NextUIProviders>
      <SessionProvider session={session}>
      <ProgressBar />
        <Component {...pageProps} />
      </SessionProvider>
    </NextUIProviders>
 
    </Provider>
    </AuthProvider>
    </>
  )
}