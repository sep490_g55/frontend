import { Html, Head, Main, NextScript } from 'next/document'
// utils
import createEmotionCache from '@/utils/createEmotionCache';
// theme
import palette from '../theme/palette';
export default function Document() {
  return (
    <Html lang="en">
      <Head >
      <meta charSet="utf-8" />
                    {/* <link rel="manifest" href="/manifest.json" /> */}

                    {/* PWA primary color */}
                    <meta name="theme-color" content={palette('light').primary.main} />

                    {/* Favicon */}
                    {/* <link rel="apple-touch-icon" sizes="180x180" href="./../assets/vissdoc_logo.png" />
                    <link rel="icon" type="image/png" sizes="32x32" href="./../assets/vissdoc_logo.png" />
                    <link rel="icon" type="image/png" sizes="16x16" href="./../assets/vissdoc_logo.png" /> */}

                    {/* Fonts */}
                    <link rel="preconnect" href="https://fonts.googleapis.com" />
                    <link rel="preconnect" href="https://fonts.gstatic.com" />
                    <link
                        href="https://fonts.googleapis.com/css2?family=Public+Sans:wght@400;500;600;700;800&display=swap"
                        rel="stylesheet"
                    />
                    <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@900&display=swap" rel="stylesheet" />

                    {/* Emotion */}
                    <meta name="emotion-insertion-point" content="" />
                    {/* {this.props.emotionStyleTags} */}

                    {/* Meta */}
                    <meta
                        name="description"
                        content="The starting point for your next project with Minimal UI Kit, built on the newest version of Material-UI ©, ready to be customized to your style"
                    />
                    <meta name="keywords" content="react,material,kit,application,dashboard,admin,template" />
                    <meta name="author" content="Minimal UI Kit" />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
