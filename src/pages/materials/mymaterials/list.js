
import React from "react";

//components
import LayoutPage from "@/layouts/LayoutPage";
import Header from "@/layouts/common/header/Header";
import { MaterialsContainer, MyMaterialLeftSection } from "../../../components/DashBoard/MyMaterial/sections";
import {ManageMaterialColumns, columns} from "@/components/Table/sections";
import { ToastContainer } from "react-toastify";


  
 const ListMaterial= () =>{
//handle tabs
// const [value, setValue] = React.useState(0);
const [tabValue, setTabValue] = React.useState(1);
const [selectedTab, setSelectedTab] = React.useState(1);
  const [subSelectedTab, setSubSelectedTab] = React.useState(21);
  const [open, setOpen] = React.useState(selectedTab === 2? true : false);
//end handle tabs
    const tab = () =>{
        
            <MyMaterialLeftSection  value={value} handleChangeView={setTabValue}/>
    
    }
    return(
        <>
            <LayoutPage title='Tài liệu của tôi'>
            <ToastContainer />
                <Header tab={<MyMaterialLeftSection  tabValue={tabValue} handleChangeView={setTabValue} selectedTab={selectedTab} setSelectedTab={setSelectedTab} subSelectedTab={subSelectedTab}setSubSelectedTab={setSubSelectedTab}open={open}setOpen={setOpen}/>} isKhoHocLieu={false} />
                <MaterialsContainer  tabValue={tabValue} handleChangeView={setTabValue} selectedTab={selectedTab} setSelectedTab={setSelectedTab} subSelectedTab={subSelectedTab}setSubSelectedTab={setSubSelectedTab}open={open}setOpen={setOpen}/>
            </LayoutPage>
        </>
    )
 }
 export default ListMaterial