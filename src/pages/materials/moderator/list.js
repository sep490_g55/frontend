
import React from "react";

//components
import LayoutPage from "@/layouts/LayoutPage";
import Header from "@/layouts/common/header/Header";
import { MaterialsContainer, MyMaterialLeftSection } from "../../../components/DashBoard/Moderator/sections";
import {ManageMaterialColumns, columns} from "@/components/Table/sections";
import { ToastContainer } from "react-toastify";
import HeaderAdmin from "@/layouts/common/header/HeaderAdmin";
import { useAuthContext } from "@/auth/useAuthContext";
import withAuth from "@/auth/withAuthModerator";

  
 const ListModerateMaterial= () =>{
    const { user,isAuthenticated } = useAuthContext();
    // const roleMod = user?.roleDTOResponses.find((role) => role === "MODERATOR")
    // if(!isAuthenticated || !roleMod){  return( <>kHÔNG CÓ QUYỀN TRUY CẬP</>);}
const [tabValue, setTabValue] = React.useState(1);
const [selectedTab, setSelectedTab] = React.useState(1);
const [subSelectedTab, setSubSelectedTab] = React.useState(311);  // default when go to moderator page
const [isSuperIndex, setIsSuperIndex] = React.useState(true);
const [isSuperIndex2, setIsSuperIndex2] = React.useState(false);



const [open, setOpen] = React.useState(selectedTab === 3? true : false);
const [subOpen, setSubOpen] = React.useState(false);
const [subOpen2, setSubOpen2] = React.useState(false);
    return(
        <>
            <LayoutPage title='Kiểm duyệt tài liệu'>
            <ToastContainer />
                <Header tab={<MyMaterialLeftSection  tabValue={tabValue} handleChangeView={setTabValue}selectedTab={selectedTab} setSelectedTab={setSelectedTab} subSelectedTab={subSelectedTab} setSubSelectedTab={setSubSelectedTab} isSuperIndex={isSuperIndex} setIsSuperIndex={setIsSuperIndex} isSuperIndex2={isSuperIndex2} setIsSuperIndex2={setIsSuperIndex2} open={open} setOpen={setOpen} subOpen={subOpen}setSubOpen={setSubOpen}subOpen2={subOpen2}setSubOpen2={setSubOpen2}/>} isKhoHocLieu={false} />
                <MaterialsContainer  tabValue={tabValue} handleChangeView={setTabValue}selectedTab={selectedTab} setSelectedTab={setSelectedTab} subSelectedTab={subSelectedTab} setSubSelectedTab={setSubSelectedTab} isSuperIndex={isSuperIndex} setIsSuperIndex={setIsSuperIndex} isSuperIndex2={isSuperIndex2} setIsSuperIndex2={setIsSuperIndex2}  open={open} setOpen={setOpen} subOpen={subOpen}setSubOpen={setSubOpen}subOpen2={subOpen2}setSubOpen2={setSubOpen2}/>
            </LayoutPage>
        </>
    ) 
 }
 export default ListModerateMaterial