import React, { useState } from "react";
import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import {
  ChooseFileStepper,
  DetailStepper,
  FailFinishStepper,
  FinishStepper,
  HeaderUploadPage,
  areAllFilesImagesOrVideos,
  convertFilesToArrayFile,
  convertFilesToFormData,
} from "@/components/Upload/stepper_sections";
import {
  BrGreenButtonAction,
  ButtonAction,
  GreenButtonAction,
} from "@/components/common_sections";
import LayoutPage from "@/layouts/LayoutPage";
import Header from "@/layouts/common/header/Header";
import { useDispatch, useSelector } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import { uploadFile } from "@/dataProvider/agent";
import { FullChangeStoreDialog } from "@/components/Dialog/section";

const steps = ["Chọn file", "Thông tin chi tiết về file", "Hoàn thành"];

export default function HorizontalLinearStepper() {
  const [activeStep, setActiveStep] = React.useState(0);
  const [skipped, setSkipped] = React.useState(new Set());
  const newMaterial = useSelector((state) => state.newMaterial.material);
  const [fileInput, setFileInput] = useState(null);

  // //console.log("newMaterial",newMaterial);
  const isStepOptional = (step) => {
    return step === 1;
  };

  const isStepSkipped = (step) => {
    return skipped.has(step);
  };

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  };
  const handleNextStep2 = () => {
    handleNext();
    // const dispatch = useDispatch();
    // dispatch(setIsDetail(valueText));
  };
  //handle  submit
  const [isLoading, setIsLoading] = useState(true);
  const [isSuccess, setIsSuccess] = useState(false);

  const UploadFile = async () => {
    // const newMaterial = useSelector(state => state.newMaterial?.material)
    //console.log("newMaterial first upload",newMaterial)
    // console.log("fileInput",fileInput)

    const formData = new FormData();

    for (const file of fileInput) {
      formData.append("files", file);
    }

    formData.append("subjectId", newMaterial?.subject?.subjectId + "");
    formData.append("name", newMaterial?.name);
    formData.append("description", newMaterial?.description);
    formData.append("visualType", newMaterial?.mode.id);
    if (newMaterial.isDetail) {
      formData.append("LessonId", newMaterial?.lesson.id + "");
    } else {
      formData.append("tagList", newMaterial?.tags);
    }
    //console.log("formData",formData)

    const res = await uploadFile(formData);
    //console.log("res upload",res)
    if (res.status === 200) {
      toast.success("Đã đưa tài liệu lên hệ thống ");
      //console.log("Đã đưa tài liệu lên hệ thống ");

      setIsSuccess(true);
    } else {
      toast.error("Không thể đưa tài liệu lên hệ thống ");
      //console.log("Không thể đưa tài liệu lên hệ thống ");
    }
    setIsLoading(false);
  };
  //
  const handleDone = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
    UploadFile();
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      // You probably want to guard against something like this,
      // it should never occur unless someone's actively trying to break something.
      throw new Error("Không thể bỏ qua các bước.");
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped((prevSkipped) => {
      const newSkipped = new Set(prevSkipped.values());
      newSkipped.add(activeStep);
      return newSkipped;
    });
  };

  const handleReset = () => {
    setFileInput(null);
    setActiveStep(0);
  };
  // const arrayFile = convertFilesToArrayFile(fileInput)
  // console.log("newMaterial?.subject?.id === 0",newMaterial?.subject?.id )
  const checkToNext = () => {
    if (activeStep !== 1) {
      return !fileInput || fileInput?.length === 0;
    } else if (newMaterial.isDetail && activeStep === 1) {
      return newMaterial?.subject?.subjectId === "" || newMaterial?.lesson?.id === "";
    } else if (!newMaterial.isDetail && activeStep === 1) {
      return newMaterial?.subject?.subjectId === "" || newMaterial?.tags?.length < 3;
    }
  };
  const isCheck = checkToNext();
  return (
    <LayoutPage title="Upload tài liệu">
      <ToastContainer />

      <Header />
      <Box sx={{ width: "100%", padding: "5px 50px", marginBottom: "50px" }}>
        <HeaderUploadPage />
        <Stepper activeStep={activeStep}>
          {steps.map((label, index) => {
            const stepProps = {};
            const labelProps = {};
            if (isStepOptional(index)) {
              labelProps.optional = <Typography variant="caption"></Typography>;
            }
            if (isStepSkipped(index)) {
              stepProps.completed = false;
            }
            return (
              <Step key={label} {...stepProps}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            );
          })}
        </Stepper>
        {activeStep === steps.length ? (
          <React.Fragment>
            <Typography sx={{ mt: 2, mb: 1 }}>
              Các bước đã xong
            </Typography>
            <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
              <Box sx={{ flex: "1 1 auto" }} />
              <Button onClick={handleReset}>Đưa thêm tài liệu lên</Button>
            </Box>
          </React.Fragment>
        ) : (
          <React.Fragment>
            {activeStep === 0 ? (
              <ChooseFileStepper
                fileInput={fileInput}
                setFileInput={setFileInput}
              />
            ) : activeStep === 1 ? (
              <DetailStepper
                type="add"
                newMaterial={newMaterial}
                fileInput={fileInput}
              />
            ) : // "hello"
            activeStep === 2 ? (
              <FinishStepper
                fileInput={fileInput}
                newMaterial={newMaterial}
                isSuccess={isSuccess}
                isLoading={isLoading}
              />
            ) : (
              <FailFinishStepper fileInput={fileInput} />
            )}
            {/* <DetailStepper/> */}
            {/* {activeStep + 1} */}
            <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
              {activeStep !== 0 &&<ButtonAction
                color="inherit"
                disabled={activeStep === 0 || !fileInput || fileInput?.length === 0}
                style={{
                  cursor:
                    activeStep === 0 || !fileInput || fileInput?.length === 0 ? "not-allowed" : "pointer",
                }}
                onClick={handleBack}
                sx={{ mr: 1 }}
              >
                Trở lại
              </ButtonAction>}
              <Box sx={{ flex: "1 1 auto" }} />
              {/* {isStepOptional(activeStep) && (
              <ButtonAction color="inherit" onClick={handleSkip} sx={{ mr: 1 }}>
                Bỏ qua
              </ButtonAction>
            )} */}
              {activeStep !== 1 && activeStep !== 2? (
                areAllFilesImagesOrVideos(fileInput ? fileInput : []) ? (
                  <FullChangeStoreDialog handleNext={handleNext}>
                    <BrGreenButtonAction
                      type="button"
                      //  onClick={handleNext}
                      disabled={isCheck}
                      style={{
                        backgroundColor: isCheck ? "gray" : "#197854",
                        "&:hover": { fontWeight: isCheck ? "none" : "" },
                        cursor: isCheck ? "not-allowed" : "pointer",
                      }}
                    >
                      {activeStep === steps.length - 1 ? "Xong" : "Tiếp"}
                    </BrGreenButtonAction>
                  </FullChangeStoreDialog>
                ) : (
                  <BrGreenButtonAction
                    type="button"
                    onClick={handleNextStep2}
                    disabled={isCheck}
                    style={{
                      backgroundColor: isCheck ? "gray" : "#197854",
                      "&:hover": { fontWeight: isCheck ? "none" : "" },
                      cursor: isCheck ? "not-allowed" : "pointer",
                    }}
                  >
                    {activeStep === steps.length - 1 ? "Xong" : "Tiếp"}
                  </BrGreenButtonAction>
                )
              ) : newMaterial.isDetail && activeStep === 1 ? (
                <BrGreenButtonAction
                  type="button"
                  onClick={handleDone}
                  disabled={isCheck}
                  style={{
                    backgroundColor: isCheck ? "gray" : "#197854",
                    "&:hover": { fontWeight: isCheck ? "none" : "" },
                    cursor: isCheck ? "not-allowed" : "pointer",
                  }}
                >
                  {activeStep === steps.length - 1 ? "Xong" : "Tiếp"}
                </BrGreenButtonAction>
              ) : !newMaterial.isDetail && activeStep === 1 ? (
                <BrGreenButtonAction
                  type="button"
                  onClick={handleDone}
                  disabled={isCheck}
                  style={{
                    backgroundColor: isCheck ? "gray" : "#197854",
                    "&:hover": { fontWeight: isCheck ? "none" : "" },
                    cursor: isCheck ? "not-allowed" : "pointer",
                  }}
                >
                  {activeStep === steps.length - 1 ? "Xong" : "Tiếp"}
                </BrGreenButtonAction>
              ) : (
                <BrGreenButtonAction
                  type="button"
                  onClick={handleNextStep2}
                  disabled={isCheck}
                  style={{
                    backgroundColor: isCheck ? "gray" : "#197854",
                    "&:hover": { fontWeight: isCheck ? "none" : "" },
                    cursor: isCheck ? "not-allowed" : "pointer",
                  }}
                >
                  {activeStep === steps.length - 1 ? "Xong" : "Tiếp"}
                </BrGreenButtonAction>
              )}
            </Box>
          </React.Fragment>
        )}
      </Box>
    </LayoutPage>
  );
}
