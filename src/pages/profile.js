
import LayoutPage from '@/layouts/LayoutPage'
import React, { useState } from 'react'

//components
import Header from "@/layouts/common/header/Header";
import {BodyProfile, HeaderProfile} from '@/components/Profile/section'
import { useAuthContext } from "@/auth/useAuthContext";
import withAuth from "@/auth/withAuthenticated";
import {updateAvatar} from "@/dataProvider/agent";
import { ToastContainer, toast } from "react-toastify";

//mui
import { Box, Container } from '@mui/material'
const Profile = () => {
  const { user,isAuthenticated,resetUser } = useAuthContext();
 
  //set avatar
  const [previewImage, setPreviewImage] = useState(null);

  const handleFileChange = async(e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (e) => {
        setPreviewImage(e.target.result);
      };
      reader.readAsDataURL(file);
      const formData = new FormData();
      formData.append('avatar', file);
      const res = await updateAvatar(formData)
      console.log(res)
      if(res.status === 200){
        toast.success("Thay đổi ảnh đại diện thành công");
        resetUser(res.data)
      }else{
        toast.error("Thay đổi ảnh đại diện thất bại");

      }
    }
  };
  //end set avatar
  return (
    <>
        <LayoutPage title='Thông tin tài khoản'>
                <Header isKhoHocLieu={false} />
                <ToastContainer />

                <HeaderProfile />
                <Box sx={{display: 'flex',
                justifyContent:'center',
                alignItems: 'center'}}>
                <BodyProfile previewImage={previewImage} handleFileChange={handleFileChange}/>
                </Box>
        </LayoutPage>
    </>
  )
}

export default Profile