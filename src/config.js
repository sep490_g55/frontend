export const HOST_API_KEY = process.env.HOST_API_KEY || '';

export const ACTION_DOWNLOAD = 'DOWNLOAD';
export const ACTION_LIKE = 'LIKE';
export const ACTION_UNLIKE = 'UNLIKE';
export const ACTION_SAVED = 'SAVED';
export const ACTION_UNSAVED = 'UNSAVED';
export const ACTION_UNSHARE = 'UNSHARE';
