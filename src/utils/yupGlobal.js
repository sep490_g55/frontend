import * as yup from 'yup'

const REGEX_PASSWORD= /^(?=.*\d)(?=.*[a-zA-Z])[\da-zA-Z_.\-@]{8,}$/
const REGEX_ONLY_NUMBER= /^\d+$/
const REGEX_USERNAME_OR_EMAIL= /^(?:(?![@_\.])[a-zA-Z0-9@._]){3,20}$|^[\w\.-]+@[a-zA-Z\d\.-]+\.[a-zA-Z]{2,}$/
const REGEX_USERNAME = /^[a-zA-Z0-9_]{3,20}$/;
const REGEX_EMAIL = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/
const REGEX_PHONE = /^(0|\+84)(\s|\.)?((3[2-9])|(5[689])|(7[06-9])|(8[1-689])|(9[0-46-9]))(\d)(\s|\.)?(\d{3})(\s|\.)?(\d{3})$/
const REGEX_NAME = /^[^\d!@#$%^&*()_+={}\[\]:;<>,.?/"\\|\-=~`]+$/;
const REGEX_SCHOOL = /^(?![\d])[a-zđàáảạãăắằẳẵặâấầẩẫậèéẻẽẹêếềểễệìíỉĩịòóỏọõôốồổỗộơớờởỡợùúủụũưứừửữựỳýỷỹỵ\s\d-]+$/i;


yup.addMethod(yup.string, 'password', function (
  message,
) {
  return this.matches(REGEX_PASSWORD, {
    message,
    excludeEmptyString: true,
  })
})
yup.addMethod(yup.string, 'usernameEmail', function (
    message,
  ) {
    return this.matches(REGEX_USERNAME_OR_EMAIL, {
      message,
      excludeEmptyString: true,
    })
  })
  yup.addMethod(yup.string, 'rePassword', function (
    message,
  ) {
    return this.matches(REGEX_USERNAME_OR_EMAIL, {
      message,
      excludeEmptyString: true,
    })
  })

yup.addMethod(yup.string, 'onlyNumber', function (
  message,
) {
  return this.matches(REGEX_ONLY_NUMBER, {
    message,
    excludeEmptyString: true,
  })
})

yup.addMethod(yup.string, 'username', function (
  message,
) {
  return this.matches(REGEX_USERNAME, {
    message,
    excludeEmptyString: true,
  })
})

yup.addMethod(yup.string, 'email', function (
  message,
) {
  return this.matches(REGEX_EMAIL, {
    message,
    excludeEmptyString: true,
  })
})
yup.addMethod(yup.string, 'name', function (
  message,
) {
  return this.matches(REGEX_NAME, {
    message,
    excludeEmptyString: true,
  })
})
yup.addMethod(yup.string, 'phone', function (
  message,
) {
  return this.matches(REGEX_PHONE, {
    message,
    excludeEmptyString: true,
  })
})

yup.addMethod(yup.string, 'school', function (
  message,
) {
  return this.matches(REGEX_SCHOOL, {
    message,
    excludeEmptyString: true,
  })
})

export default yup
