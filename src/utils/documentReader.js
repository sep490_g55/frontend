// // utils/documentReader.js

// const mammoth = require("mammoth");

// export const readDocxPages = async (filePath) => {
//   const result = await mammoth.extractRawText({ path: filePath });

//   // Split the content into pages (you may need to adjust this logic)
//   const pages = result.value.split("\n\n"); 

//   return pages;
// };

// const pptxParser = require("pptx-parser");
// const fs = require("fs");

// export const readPptxPages = (filePath) => {
//   const content = fs.readFileSync(filePath, "binary");
//   const parsed = pptxParser.parse(content);

//   return parsed.slides.map((slide) => slide.content);
// };