import React, { useRef } from 'react';
import ClipboardJS from 'clipboard';
import { BASE_URL, instance } from '@/dataProvider/agent';

  
    export const handleCopyClick = () => {
      const clipboard = new ClipboardJS(buttonRef.current, {
        text: () => textToCopy,
      });
  
      // Kích hoạt sự kiện sao chép
      clipboard.on('success', (e) => {
        console.log(`Đã sao chép: ${e.text}`);
        clipboard.destroy();
      });
  
      // Kích hoạt sự kiện lỗi
      clipboard.on('error', (e) => {
        console.error('Sao chép thất bại', e);
        clipboard.destroy();
      });
  
      // Kích hoạt thủ công sự kiện sao chép
      clipboard.onClick({ delegateTarget: buttonRef.current });
    };

    export const getFileNameFromUrl = (url) =>{
      // Sử dụng split để tách chuỗi bằng dấu '/'
      let segments = url.split('/');
  
      // Lấy phần tử cuối cùng của mảng segments (đó là tên file)
      let fileName = segments[segments.length - 1];
  
      // Nếu URL chứa các tham số (ví dụ: ?X-Amz-Algorithm=...), cắt bỏ phần đuôi
      let index = fileName.indexOf('?');
      if (index !== -1) {
          fileName = fileName.substring(0, index);
      }
  
      return fileName;
  }

  export const downloadFile = (apiEndpoint, authToken) => {
    // Tạo header chứa token xác thực
    const headers = new Headers({
      'Authorization': `Bearer ${authToken}`,
    });
  
    // Thực hiện yêu cầu GET đến API endpoint
    fetch(BASE_URL+"/resource/download/" +apiEndpoint, {
      method: 'GET',
      headers: headers,
    })
    .then(response => {
      // Kiểm tra xem yêu cầu có thành công không
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
  
      // Trích xuất tên tệp từ header hoặc có thể là một phần khác của dữ liệu trả về
      const contentDisposition = response.headers.get('Content-Disposition');
      const fileNameMatch = contentDisposition && contentDisposition.match(/filename="(.+)"/);
      const fileName = fileNameMatch ? fileNameMatch[1] : 'downloaded_file';
  
      // Trích xuất dữ liệu tải xuống
      return response.blob().then(blob => {
        // Tạo một đường link ẩn để kích thích việc tải xuống
        const link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = apiEndpoint;
  
        // Thêm link vào trang web và kích thích việc tải xuống
        document.body.appendChild(link);
        link.click();
  
        // Xóa link sau khi tải xuống hoặc nếu có lỗi
        link.parentNode.removeChild(link);
      });
    })
    .catch(error => {
      console.error('Error downloading file:', error);
    });
  }
  
  // Sử dụng hàm
  const apiEndpoint = 'URL_API_DOWNLOAD';  // Thay thế bằng URL API download thực tế
  const authToken = 'TOKEN_XAC_THUC';      // Thay thế bằng token xác thực thực tế
  // downloadFile(apiEndpoint, authToken);
  