import { BASE_URL } from "@/constants/baseApi"

import axios from 'axios';
//http://103.161.178.66:8182//api/ https://localhost:7270/
const instance = axios.create({
    baseURL: BASE_URL,
    timeout: 60000,
});


import { PATH_AUTH } from '../routes/paths';

// INTERCEPTORS CONFIG START
instance.interceptors.response.use(responseOnSuccessMiddleware, responseOnErrorMiddleware);

function responseOnSuccessMiddleware(res) {
    return res;
}

function responseOnErrorMiddleware(error) {
    const { status } = error.response;
    if (status === 401) {
        localStorage.clear();
        window.location.href = PATH_AUTH.login;
    }
    return error;
}

// INTERCEPTORS CONFIG END

export const getApi = async(url) =>{
   
    try {
    // console.log("paramObj",paramObj);

        const res = await instance.get(url);
        return res;
    } catch (err) {
        return err;
    }
}


export const getAddress = async (url) =>{
    return getApi(BASE_URL+url);
}
export const getProvinces = async (url) =>{
    return axiosClient.get(BASE_URL+url);
}

export const getDistricts = async (url) =>{
    return axiosClient.get(BASE_URL+url);
}
export const getWards = async (url) =>{
    return axiosClient.get(BASE_URL+url);
}