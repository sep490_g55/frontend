import { BASE_URL,BASE_URLL, KHOI_URL } from "@/constants/baseApi"
// import axiosInstance from "./axios"
import axiosClient from "./axios";

export const getSlidesWithThumbnailOLD = async (url) =>{
    const token = localStorage.getItem('access_token');

    return axiosClient.get(KHOI_URL+url, {
        headers: {
            Authorization:  `Bearer ${token}`,
            'Content-Type': 'application/json; charset=utf-8',
            'Access-Control-Allow-Headers':
                'Content-Type, X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Date, X-Api-Version, X-File-Name',
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Origin': '*',
        },
    });
}

export const getSlide = async (url) =>{
    return axiosClient.get(BASE_URLL+url);
}
export const getMedias = async (url) =>{
    return axiosClient.get(BASE_URLL+url);
}