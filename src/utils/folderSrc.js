import {
  BASE_URL,
  BASE_URL3000,
  BASE_URLL,
  KHOI_URL,
} from "@/constants/baseApi";
import axios from "axios";
import axiosClient from "./axios";

export const getFolderSrc = async (url) => {
  return axiosClient.get(BASE_URL3000 + url);
};
export const setClass = async (name) => {
  const token = localStorage.getItem("access_token");
  return axiosClient.post(KHOI_URL + "class",
  {"name": name}
  , {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
  
};
export const updateClass = async (name,id) => {
  const token = localStorage.getItem("access_token");
  return axiosClient.put(KHOI_URL + "class/"+id,
  {"name": name}
  , {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
  
};
export const updateFolder = async (url,name,id) => {
  const token = localStorage.getItem("access_token");
  return axiosClient.put(KHOI_URL + url+id,
  {"name": name}
  , {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }).then(function (response) {
    console.log(response)
  })
  
};
export const getMaterials = async (url) => {
    const token = localStorage.getItem("access_token");
  return axiosClient.get(KHOI_URL + url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const getNoAuthMaterials = async (url) => {
  const token = localStorage.getItem("access_token");
return axiosClient.get(KHOI_URL + url);
};

export const getSuggestTagList= async (url,tag) => {
  const token = localStorage.getItem("access_token");
return axiosClient.post(KHOI_URL + url,
  { "tagSuggest": tag}
  , {
  headers: {
    Authorization: `Bearer ${token}`,
  },
});
};
// export const getBookSeries = () =>{

//         const response = axios.get('http://localhost:3000/bookseries')
//         return response.data;

// }
// export const getSubjects = () =>{

//         const response = axios.get('http://localhost:3000/subjects')
//         return response.data;

// }
// export {getClasses,getBookSeries,getSubjects};
