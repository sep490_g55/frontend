import { useRouter } from 'next/router'
import React from 'react'
import { SiGoogleclassroom } from 'react-icons/si'
import logo from '../assets/image_logo.png'
import admin from '../assets/image_avatar_admin.png'
import moderator from '../assets/image_avartar_mod.png'
import teacher from '../assets/image_avartar_teacher.png'
import { Avatar, Box } from '@mui/material'
export const NameSystem = () => {
  return (
    <Box sx={{w:200,m:"5px"}}>VISSDOC</Box>
  )
}

export const LogoSystem = () => {
    const router = useRouter();
    return (
        // <SiGoogleclassroom  style={{fontSize:'32px',color: "#198754",cursor:'pointer'}} onClick={() => router.push("/home")} title="Trang chủ"/>
      <Avatar
      onClick={() => router.push("/home")} title="Trang chủ"
      style={{width:"70px",height:"70px",cursor:'pointer'}}
        src={logo.src}
      />
    )
  }
  
  export const AdminSystem = () => {
    const router = useRouter();
    return (
        // <SiGoogleclassroom  style={{fontSize:'32px',color: "#198754",cursor:'pointer'}} onClick={() => router.push("/home")} title="Trang chủ"/>
      <Avatar
      style={{width:"70px",height:"70px"}}
        src={admin.src}
      />
    )
  }
  export const ModeratorSystem = () => {
    const router = useRouter();
    return (
        // <SiGoogleclassroom  style={{fontSize:'32px',color: "#198754",cursor:'pointer'}} onClick={() => router.push("/home")} title="Trang chủ"/>
      <Avatar
      style={{width:"70px",height:"70px"}}
        src={moderator.src}
      />
    )
  }
  export const TeacherSystem = () => {
    const router = useRouter();
    return (
        // <SiGoogleclassroom  style={{fontSize:'32px',color: "#198754",cursor:'pointer'}} onClick={() => router.push("/home")} title="Trang chủ"/>
      <Avatar
      style={{width:"70px",height:"70px"}}
        src={teacher.src}
      />
    )
  }