import { createSlice } from '@reduxjs/toolkit'

export const SearchTagSlice = createSlice({
  name: 'tags',
  initialState: {
    tags: [
        {
            "tagId": 0,
            "tagName": ""
        },
    ]
  },
  reducers: {
    setTagList(state,action){
        state.tags =action.payload;
    }
  }
})

// Action creators are generated for each case reducer function
export const { setTagList} = SearchTagSlice.actions

export default SearchTagSlice.reducer