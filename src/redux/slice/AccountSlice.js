import { createSlice } from '@reduxjs/toolkit'

export const AccountSlice = createSlice({
  name: 'account',
  initialState: {
    access_token: ''
  },
  reducers: {
    setLoggedInAccount(state,action){
        state.access_token =action.payload;
    }
  }
})

// Action creators are generated for each case reducer function
export const { setLoggedInAccount} = AccountSlice.actions

export default AccountSlice.reducer