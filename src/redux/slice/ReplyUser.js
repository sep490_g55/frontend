import { createSlice } from '@reduxjs/toolkit'

export const ReplyUserSlice = createSlice({
  name: 'replyUser',
  initialState: {
    info: null
  },
  reducers: {
    setInfoUserReply(state,action){
        state.info =action.payload;
    }
  }
})

// Action creators are generated for each case reducer function
export const { setInfoUserReply} = ReplyUserSlice.actions

export default ReplyUserSlice.reducer