import { createSlice } from '@reduxjs/toolkit'

export const SearchInputTextSlice = createSlice({
  name: 'searchInputText',
  initialState: {
    text:""
  },
  reducers: {
    setSearchInputText(state,action){
        state.text =action.payload;
    }
  }
})

// Action creators are generated for each case reducer function
export const { setSearchInputText} = SearchInputTextSlice.actions

export default SearchInputTextSlice.reducer