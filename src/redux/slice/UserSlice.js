import { createSlice } from '@reduxjs/toolkit'

export const UserSlice = createSlice({
  name: 'user',
  initialState: {
    user: null
  },
  reducers: {
    setInfoUser(state,action){
        state.user =action.payload;
    }
  }
})

// Action creators are generated for each case reducer function
export const { setInfoUser} = UserSlice.actions

export default UserSlice.reducer