import { createSlice } from '@reduxjs/toolkit'

export const NewMaterialSlice = createSlice({
  name: 'newMaterial',
  initialState: {
    material: {

      bookSeries: "",
      bookVolume: "",
      chapter: "",
      class: "",
      lesson: "",
      subject:"" ,
      mode:"",
      name:"",
      pageIndex:"1",
      pageSize:"12",
      visualType:"",
    }
  },
  reducers: {
   
    setFilterClass(state, action) {
      state.material.class = action.payload;
      // console.log("action.payload",initialState.material)
    },
    setFilterBookSeries(state, action) {
      state.material.bookSeries = action.payload;
    },
    setFilterSubject(state, action) {
      state.material.subject = action.payload;
    },
    setFilterBookVolume(state, action) {
      state.material.bookVolume = action.payload;
    },
    setFilterChapter(state, action) {
      state.material.chapter = action.payload;
    },
    setFilterLesson(state, action) {
      state.material.lesson = action.payload;
    },
    
    setFilterMode(state, action) {
      state.material.mode = action.payload;
    },
    
    setFilterName(state, action) {
      state.material.name = action.payload;
    },
    setTags(state, action) {
      state.material.tags = action.payload;
    },
    setIsDetail(state, action) {
      state.material.isDetail = action.payload;
    },
    setType(state, action) {
      state.material.type = action.payload;
    },
    setPageIndex(state, action) {
      state.material.pageIndex = action.payload;
    },
    
  }
})

// Action creators are generated for each case reducer function
export const { setNewMaterial,setViewClass,setViewBookSeries,setViewSubject,setViewBookVolume,setViewChapter,setViewLesson,setFile,setMode,setDescription,setName,setTags,setIsDetail,setType,setPageIndex} = NewMaterialSlice.actions

export default NewMaterialSlice.reducer