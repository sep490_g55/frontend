import { createSlice } from '@reduxjs/toolkit'

export const MaterialSlice = createSlice({
  name: 'materials',
  initialState: {
    material:null
  },
  reducers: {
    setMaterials(state,action){
        state.material =action.payload;
    }
  }
})

// Action creators are generated for each case reducer function
export const { setMaterials} = MaterialSlice.actions

export default MaterialSlice.reducer