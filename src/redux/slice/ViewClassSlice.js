import { createSlice } from '@reduxjs/toolkit'

export const ViewClassSlice = createSlice({
  name: 'viewClass',
  initialState: {
    detailClass: null
  },
  reducers: {
    setViewClass(state,action){
        state.detailClass =action.payload;
    }
  }
})

// Action creators are generated for each case reducer function
export const { setViewClass} = ViewClassSlice.actions

export default ViewClassSlice.reducer