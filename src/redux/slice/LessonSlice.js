import { createSlice } from '@reduxjs/toolkit'

export const LessonSlice = createSlice({
  name: 'lesson',
  initialState: {
    lessonID: 0
  },
  reducers: {
    setLesson(state,action){
        state.lessonID =action.payload;
    }
  }
})

// Action creators are generated for each case reducer function
export const { setLesson} = LessonSlice.actions

export default LessonSlice.reducer