import { createSlice } from '@reduxjs/toolkit'

export const ReLoading = createSlice({
  name: 'reloading',
  initialState: {
    isReLoad: false,
    isSearch: false


  },
  reducers: {
    setIsReload(state,action){
        state.isReLoad =action.payload;
    },
    setIsSearch(state,action){
      state.isSearch =action.payload;
  }
  }
})

// Action creators are generated for each case reducer function
export const { setIsReload,setIsSearch} = ReLoading.actions

export default ReLoading.reducer