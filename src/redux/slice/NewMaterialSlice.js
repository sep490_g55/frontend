import { createSlice } from '@reduxjs/toolkit'

export const NewMaterialSlice = createSlice({
  name: 'newMaterial',
  initialState: {
    material: {

      bookSeries: {id: "", name: ''},
      bookVolume: {id: "", name: ''},
      chapter: {id: "", name: ''},
      class: {id: "", name: ''},
      lesson: {id: "", name: ''},
      subject: {id: "",subjectId:"", name: ''},
      mode:{id:"PRIVATE",name:"Chỉ mình tôi",},
      description:"",
      file: null,
      name:"",
      tags:[],
      isDetail:true,
      type:"SLIDE",
      pageIndex:"1",
      pageSize:"12",
      bookSeriesList:null,
      bookVolumeList:null,
      chapterList: null,
      classList: null,
      lessonList: null,
      subjectList: null,
    }
  },
  reducers: {
    setNewMaterial(state,action){
        state.material =action.payload;
    },
    setViewClass(state, action) {
      state.material.class = action.payload;
      // console.log("action.payload",initialState.material)
    },
    setViewBookSeries(state, action) {
      state.material.bookSeries = action.payload;
    },
    setViewSubject(state, action) {
      state.material.subject = action.payload;
    },
    setViewBookVolume(state, action) {
      state.material.bookVolume = action.payload;
    },
    setViewChapter(state, action) {
      state.material.chapter = action.payload;
    },
    setViewLesson(state, action) {
      state.material.lesson = action.payload;
    },
    setFile(state, action) {
      state.material.file = action.payload;
    },
    setMode(state, action) {
      state.material.mode = action.payload;
    },
    setDescription(state, action) {
      state.material.description = action.payload;
    },
    setName(state, action) {
      state.material.name = action.payload;
    },
    setTags(state, action) {
      state.material.tags = action.payload;
    },
    setIsDetail(state, action) {
      state.material.isDetail = action.payload;
    },
    setType(state, action) {
      state.material.type = action.payload;
    },
    setPageIndex(state, action) {
      state.material.pageIndex = action.payload;
    },

    setViewClassList(state, action) {
      state.material.classList = action.payload;
      // console.log("action.payload",initialState.material)
    },
    setViewBookSeriesList(state, action) {
      state.material.bookSeriesList = action.payload;
    },
    setViewSubjectList(state, action) {
      state.material.subjectList = action.payload;
    },
    setViewBookVolumeList(state, action) {
      state.material.bookVolumeList = action.payload;
    },
    setViewChapterList(state, action) {
      state.material.chapterList = action.payload;
    },
    setViewLessonList(state, action) {
      state.material.lessonList = action.payload;
    },
  }
})

// Action creators are generated for each case reducer function
export const { setNewMaterial,setViewClass,setViewBookSeries,setViewSubject,setViewBookVolume,setViewChapter,setViewLesson,setFile,setMode,setDescription,setName,setTags,setIsDetail,setType,setPageIndex,setViewClassList,setViewBookSeriesList,setViewSubjectList,setViewBookVolumeList,setViewChapterList,setViewLessonList,} = NewMaterialSlice.actions

export default NewMaterialSlice.reducer