import { createSlice } from '@reduxjs/toolkit'

export const ProfileSlice = createSlice({
  name: 'profile',
  initialState: {
    info: {
      "id": 0,
    "firstname": "",
    "lastname": "",
    "username": "",
    "email": "",
    "active": true,
    "avatar": "",
    "gender": true,
    "dateOfBirth": "",
    "phone": "",
    "district": "",
    "province": "",
    "village": "",
    "school": "",
    "createdAt": "",
    "classId": 0,
    "roleDTOResponses": []

    }
  },
  reducers: {
    setProfile(state,action){
        state.info =action.payload;
    }
  }
})

// Action creators are generated for each case reducer function
export const { setProfile} = ProfileSlice.actions

export default ProfileSlice.reducer