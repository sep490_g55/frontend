import { createSlice } from "@reduxjs/toolkit";

export const ViewFolderSRCSlice = createSlice({
  name: "viewFolder",
  initialState: {
    class: null,
    bookSeries: null,
    bookVolume: null,
    subject: null,
    chapter: null,
    lesson: null,
    isUpdate:false,
  },
  reducers: {
    setViewClass(state, action) {
      state.class = action.payload;
    },
    setViewBookSeries(state, action) {
      state.bookSeries = action.payload;
    },
    setViewSubjectBookSeries(state, action) {
      state.subject = action.payload;
    },
    setViewBookVolume(state, action) {
      state.bookVolume = action.payload;
    },
    setViewChapter(state, action) {
      state.chapter = action.payload;
    },
    setViewLesson(state, action) {
      state.lesson = action.payload;
    },
    setIsUpdate(state, action) {
      state.isUpdate = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setViewClass,setViewBookSeries,setViewSubjectBookSeries,setViewBookVolume,setViewChapter,setViewLesson,setIsUpdate } = ViewFolderSRCSlice.actions;

export default ViewFolderSRCSlice.reducer;
