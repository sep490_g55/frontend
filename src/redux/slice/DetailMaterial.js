import { createSlice } from '@reduxjs/toolkit'

export const DetailMaterialSlice = createSlice({
  name: 'material',
  initialState: {
    material: {
      "id": 114,
    "name": "present abc",
    "description": "description",
    "approveType": "UNACCEPTED",
    "visualType": "PUBLIC",
    "classId": 1,
    "bookSeriesId": 1,
    "subjectId": 1,
    "bookVolumeId": 1,
    "chapterId": 1,
    "lessonId": 1,
    "resourceType": "JPG",
    "resourceSrc": "present-abc-1701679360081.jpg",
    "viewCount": 2,
    "tagList": []

    }
  },
  reducers: {
    setMyMaterial(state,action){
        state.material =action.payload;
    },
    setDescriptionDetailMaterial(state, action) {
      state.material.description = action.payload;
      // console.log("action.payload",initialState.material)
    },
    setApproveDetailMaterial(state, action) {
      state.material.approveType = action.payload;
    },
    setVisualTypeDetailMaterial(state, action) {
      state.material.visualType = action.payload;
    },
    setClassIdDetailMaterial(state, action) {
      state.material.classId = action.payload;
    },
    setBookSeriesIdDetailMaterial(state, action) {
      state.material.bookSeriesId = action.payload;
    },
    setChapterIdDetailMaterial(state, action) {
      state.material.chapterId = action.payload;
    },
    setLessonIdDetailMaterial(state, action) {
      state.material.lessonId = action.payload;
    },
    setResourceTypeDetailMaterial(state, action) {
      state.material.resourceType = action.payload;
    },
  
    setTagList(state, action) {
      state.material.tagList = action.payload;
    },
   
    
  }
})

// Action creators are generated for each case reducer function
export const {setMyMaterial,setDescriptionDetailMaterial,setVisualTypeDetailMaterial,setClassIdDetailMaterial,setBookSeriesIdDetailMaterial,setChapterIdDetailMaterial,setLessonIdDetailMaterial,setTagList} = NewMaterialSlice.actions

export default NewMaterialSlice.reducer