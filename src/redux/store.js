import { configureStore } from '@reduxjs/toolkit'
import accountReducer from '@/redux/slice/AccountSlice'
import profileReducer from '@/redux/slice/ProfileSlice'
import materialReducer from '@/redux/slice/MaterialSlice'
import tagReducer from '@/redux/slice/SearchTagSlice'
import lessonReducer from "@/redux/slice/LessonSlice"
import newMaterialReducer from "@/redux/slice/NewMaterialSlice"
import searchInputTextReducer from "@/redux/slice/SearchInputTextSlice"
import viewClassReducer from "@/redux/slice/ViewClassSlice"
import viewFolderReducer from "@/redux/slice/ViewFolderSRCSlice"
import userReducer from "@/redux/slice/UserSlice"
import reloadingReducer from "@/redux/slice/IsReLoad"
import replyUserReducer from "@/redux/slice/ReplyUser"


export default configureStore({
  reducer: {
    account: accountReducer,
    profile: profileReducer,
    materials: materialReducer,
    tags: tagReducer,
    lesson: lessonReducer,
    newMaterial: newMaterialReducer,
    searchInputText : searchInputTextReducer,
    viewClass : viewClassReducer,
    viewFolder:viewFolderReducer,
    user:userReducer,
    reloading:reloadingReducer,
    replyUser:replyUserReducer,
  }
})