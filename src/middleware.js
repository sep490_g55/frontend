import { NextResponse } from 'next/server'
import  { NextRequest } from 'next/server'
import {getLocalStorage} from "@/dataProvider/agent"
import { isValidToken, jwtDecode, setupLocalStorage } from '@/auth/utils';
const jwt = require('jsonwebtoken');
// import { NextResponse } from 'next/server'
export function middleware(request) {

     request.cookies.get('token')?.value;
    // const token = getLocalStorage("access_token")
    const token = request.cookies.get('token')?.value;
    const roles = request.cookies.get('roles')?.value;

    // const g_state = request.cookies.get('g_state')?.value

    // console.log("token2",token)
  if(!token) {
     return NextResponse.redirect(new URL('/not-auth',request.url))
  }else  if(token !== undefined) {


    if(!roles.includes("ADMIN")&& request.url.includes('/dashboards/admin') ){
    // console.log("admin")

      return NextResponse.redirect(new URL("/not-permission",request.url))
    }
    if(!roles.includes("ADMIN")&& request.url.includes('/profile-user') ){
      // console.log("admin2")
   
      return NextResponse.redirect(new URL("/not-permission",request.url))
    }

    // if(!roles.includes("TEACHER")&& request.url.includes('/materials/upload/stepper') ){
    // // console.log("TEACHER")
     
    //   return NextResponse.redirect(new URL("/not-is-teacher",request.url))
    // }
    if((!roles.includes("TEACHER")&&!roles.includes("MODERATOR"))&& request.url.includes('/materials/upload/stepper') ){
      // console.log("MODERATOR")
       
        return NextResponse.redirect(new URL("/not-permission",request.url))
      }

    if(!roles.includes("MODERATOR")&& request.url.includes('/materials/moderator/list') ){
      return NextResponse.redirect(new URL("/not-permission",request.url))
    }

    if(!roles.includes("TEACHER")&& request.url.includes('/materials/mymaterials/list') ){
      return NextResponse.redirect(new URL("/not-permission",request.url))
    }
    
  }
}

export const config = {
  matcher: ['/dashboards/admin',
  '/profile-user',
  '/auth/switch-role',
  '/profile',
  '/materials/moderator/list',
  '/materials/mymaterials/list',
  '/materials/upload/stepper',
  // '/detail_material'

]
}