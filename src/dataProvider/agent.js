import axios from 'axios';
//http://103.161.178.66:8182//api/ https://localhost:7270/
// http://209.145.51.33:8080/api/v1
// export const BASE_URL = "http://localhost:8080/api/v1"
export const BASE_URL = "http://209.145.51.33:8080/api/v1"
// export const BASE_URL = "http://emss-env.eba-dfvtidac.ap-southeast-1.elasticbeanstalk.com/api/v1"

 const instance = axios.create({
    baseURL: BASE_URL,
    // baseURL: `http://209.145.51.33:8080/api/v1`,


    timeout: 60000,
});


import { PATH_AUTH } from '../routes/paths';

// INTERCEPTORS CONFIG START
instance.interceptors.response.use(responseOnSuccessMiddleware, responseOnErrorMiddleware);

function responseOnSuccessMiddleware(res) {
    return res;
}

function responseOnErrorMiddleware(error) {
    const { status } = error.response;
    if (status === 401) {
        localStorage.clear();
        window.location.href = PATH_AUTH.login;
    }
    return error;
}

// INTERCEPTORS CONFIG END

export const setLocalStorage = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
};
export const getLocalStorage = (key) => {
    if (typeof window !== 'undefined') {
        return JSON.parse(localStorage.getItem(key));
    }
};

export const clearLocalStorage = () => {
    localStorage.clear();
};

// GET API AREA ============================>
export const getApiV2 = async(url) => {
    const token = getLocalStorage('access_token');
    try {
        const res = await instance.get(url, {
            headers: {
                Authorization: token ? `Bearer ${token}` : 'no auth',
            }
        });
        return res;
    } catch (err) {
        return err;
    }
}
export const getApi2 = async(url) => {
    try {
        const res = await instance.get(url);
        return res;
    } catch (err) {
        return err;
    }
}

export const getApi = async(url, params) =>{
    // ////console.log("params58",params);
    // delete all params fail
    const paramObj = {};
    if (params && Object.keys(params).length) {
        Object.keys(params).forEach(function (key) {
            if (params[key]) {
                paramObj[key] = params[key];
            }
        });
    }

    const token = getLocalStorage('access_token');
    try {
    // ////console.log("paramObj",paramObj);

        const res = await instance.get(url, {
            headers: {
                Authorization: token ? `Bearer ${token}` : 'no auth',
            },
            params: paramObj,
        });
        return res;
    } catch (err) {
        return err;
    }
}


//USER
export const getLogOut = () => {
    return getApi('/auth/logout');
};

//USER
export const getProfile = () => {
    return getApi('/profile');
};

//MATERIAL
export const searchKhoHocLieu = (url) => {
    return getApi2(url);
};

export const searchKhoMedia = (params) => {
    return getApi('/resource/medias',params);
};

export const getDetailMaterial = (id) => {
    return getApi('/resource/detail/'+id);
};
export const getFolderSRC = (url) => {
    return getApi(url);
};

//FOLDER
export const getClass = (classID) => {
    return getApi('/class/'+classID);
};
export const getBookSeries = (bookSeriesID) => {
    return getApi('/book-series/'+bookSeriesID);
};
export const getSubject = (subjectID) => {
    return getApi('/subject/'+subjectID);
};
export const getBookVolume = (bookVolumeId) => {
    return getApi('/book-volume/'+bookVolumeId);
};
export const getChapter = (chapterId) => {
    return getApi('/chapter/'+chapterId);
};
export const getLesson = (lessonId) => {
    return getApi('/lesson/'+lessonId);
};

//FOLDER FOR UPLOAD
export const getClassUPLOAD = (subjectId) => {
    return getApi('/class/list-by-subject?subjectId='+subjectId);
};
export const getBookSeriesUPLOAD = (classId,subjectId) => {
    return getApi(`/book-series/list-by-subject-class?subjectId=${subjectId}&classId=${classId}`);
};
export const getBookVolumeUPLOAD = (bookSeriesId,subjectId) => {
    return getApi(`/book-volume/list-by-subject-book-series?subjectId=${subjectId}&bookSeriesId=${bookSeriesId}`);
};


//GET ALL
export const getAllClass = () => {
    return getApi2('/class/list');
};
export const getALLBookSeriesByClassID = (classID) => {
    return getApi2(`/book-series/list-by-class?classId=${classID}`);
};
export const getALLSubject = () => {
    return getApi(`/subject/list`);
};
export const getALLSubjectBookSeriesByBookSeriesID = (subjectBookSeriesID) => {
    return getApi2(`/subject/list-by-book-series?bookSeriesId=${subjectBookSeriesID}`);
};
export const getALLBookVolumeBySubjectBookSeriesID = (subjectBookSeriesID) => {
    return getApi2(`/book-volume/list-by-book-series-subject?bookSeriesSubjectId=${subjectBookSeriesID}`);
};
export const getALLChapterByBookVolumeID = (bookVolumeId) => {
    return getApi2(`/chapter/list-by-book-volume?bookVolumeId=${bookVolumeId}`);
};
export const getALLLessonByChapterID = (chapterId) => {
    return getApi2(`/lesson/list-by-chapter?chapterId=${chapterId}`);
};

export const getALLTag = () => {
    return getApi2(`/resource/tags-global`,);
};


/// 
export const gotoForgetPassword = (payload) => {
    ////console.log(`/auth/confirm?token=`+payload)
    return getApi2(`/auth/confirm?token=`+payload);
};

export const getUserPreview = () => {
    ////console.log(`/users/preview`)
    return getApi(`/users/preview`);
};

//GET DETAIL MY MATERIAL
export const getMyMaterialByID = (id) => {
    // ////console.log(`/users/preview`)
    return getApi(`/resource/${id}`);
};


/// GET ADMIN
export const getUserById = (id) => {
    // ////console.log(`/users/preview`)
    return getApi(`/users/${id}`);
};

export const resetPassword = (id) => {
    // ////console.log(`/users/preview`)
    return getApi(`/users/reset/${id}`);
};
export const getAllRole = () => {
    // ////console.log(`/users/preview`)
    return getApi(`/role/display`);
};

export const getAllRoleActive = () => {
    // ////console.log(`/users/preview`)
    return getApi(`/role/list`);
};

export const getAllPermission = () => {
    // ////console.log(`/users/preview`)
    return getApi(`/system-permission/list`);
};

export const getVisualTypes = () => {
    // ////console.log(`/users/preview`)
    return getApi2(`/helper/list-visual-type`);
};

export const getActions = () => {
    // ////console.log(`/users/preview`)
    return getApi2(`/helper/list-action-type`);
};

export const getApproveTypes = () => {
    // ////console.log(`/users/preview`)
    return getApi2(`/helper/list-approve-type`);
};

export const getMethodTypes = () => {
    // ////console.log(`/users/preview`)
    return getApi2(`/helper/list-method-type`);
};

export const getTabResourceTypes = () => {
    // ////console.log(`/users/preview`)
    return getApi2(`/helper/list-tab-resource-type`);
};

export const getResourceTypes = () => {
    // ////console.log(`/users/preview`)
    return getApi2(`/helper/list-resource-type`);
};

export const changeVisualTypeResource = (id,type) => {
    // ////console.log(`/users/preview`)
    return getApi(`/resource/visual/${id}?type=${type}`);
};

export const getReportComment = () => {
    // ////console.log(`/users/preview`)
    return getApi(`/resource/visual/${id}?type=${type}`);
};

export const downloadResource = (file) => {
    // ////console.log(`/users/preview`)
    return getApi(`/resource/download/${file}`);
};


export const showMoreComment = (id) => {
    // ////console.log(`/users/preview`)
    return getApi(`/comment/more/${id}`);
};

export const getResourceSharedUsers = (id) => {
    // ////console.log(`/users/preview`)
    return getApi(`/resource/share/${id}`);
};

export const getSuggestResourcesByResourceId = (id) => {
    return getApi(`/resource/relate/${id}`);
};

export const getCommentResourcesByResourceId = (id) => {
    return getApi(`/resource/comment-list/${id}`);
};


// POST API AREA ============================>
async function postApi(url, payload, file) {
    const token = getLocalStorage('access_token');
    ////console.log("token",token?"true":"false")
    ////console.log("payload last",payload)

    try {
        const res = await instance.post(`/${url}`, payload, {
            headers: {
                Authorization: token ? `Bearer ${token}` : 'no auth',
                'Content-Type': file ? 'multipart/form-data' : 'application/json; charset=utf-8',
                'Access-Control-Allow-Headers':
                    'Content-Type, X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Date, X-Api-Version, X-File-Name',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Origin': '*',
            },
        });
        return res;
    } catch (err) {
        return err;
    }
}
async function postApi2(url, payload) {
   
    try {
        const res = await instance.post(`/${url}`, payload);
        ////console.log("res",res)
        return res;
    } catch (err) {
        return "Kiểm tra kết nối mạng";
    }
}


// LOGIN
export const loginAuth = (payload) => {
    return postApi2('auth/login', payload);
};
export const register = (payload) => {
    return postApi2('register', payload);
};




// MATERIAL
export const getSuggestTagList = (payload) => {
    return postApi('resource/tags', payload);
};


// FOLDER 
export const setClass = (payload) => {
    return postApi('class', payload);
};

export const setBookSeries = (classID,payload) => {
    return postApi(`book-series?classId=${classID}`, payload);
};

export const setSubject = (payload) => {
    return postApi('subject', payload);
};

export const setSubjectBookSeries = (bookSeriesID,payload) => {
    ////console.log("bookSeriesID",bookSeriesID)
    return postApi(`book-series-subject/change-subject?bookSeriesId=${bookSeriesID}`, payload);
};

export const setBookVolume = (bookSeriesSubjectID,payload) => {
    return postApi('book-volume?bookSeriesSubjectId='+bookSeriesSubjectID, payload);
};

export const setChapter = (bookVolumeId,payload) => {
    return postApi('chapter?bookVolumeId='+bookVolumeId, payload);
};
export const setLesson = (chapterId,payload) => {
    return postApi('lesson?chapterId='+chapterId, payload);
};

export const uploadFile = (payload) => {   
    return postApi('user-resource/upload',payload,true);
};
export const getALLTagGlobal = () => {
    return postApi(`resource/tags-global`,{
        "tagSuggest": ""
    });
};
export const addNewTag = (payload) => {   
    return postApi('tags',payload);
};
export const forgetPassword = (payload) => {
    return postApi2(`auth/forgot-password`,payload);
};


export const submitForgetPassword = (payload) => {
 
    return postApi2(`auth/submit-forgot-password`,payload);
};

export const actionResource = (payload) => {
    
    return postApi(`user-resource`,payload);
};


//SHARE RESOURCE

export const getUserToShare = () => {
    
    return postApi(`resource/share/suggest`,{"text": ""});
};

export const ShareMaterial = (id,payload) => {
    
    return postApi(`resource/share/${id}`,payload);
};

///UPDATE MATERIAL
export const updateMaterial = (id,payload) => {
    
    return postApi(`resource/${id}`,payload,true);
};



//ADMIN
export const addNewRole = (payload) => {
    
    return postApi(`role`,payload);
};
export const addNewPermission = (payload) => {
    
    return postApi(`system-permission`,payload);
};


//REPORT  MATERIAL
///
export const reportMaterial = (payload) => {
    
    return postApi(`report-resource`,payload);
};


//MODERATOR
export const approveResource = (payload) => {
    
    return postApi(`review/approve`,payload);
};


export const rejectResource = (payload) => {
    
    return postApi(`review/reject`,payload);
};

export const getResourceTag = (payload) => {
    
    return postApi(`resource-tags`,payload);
};


//COMMENT
export const createComment = (payload) => {
    
    return postApi(`comment`,payload);
};

export const createReportComment = (payload) => {
    
    return postApi(`report-comment`,payload);
};


// PUT API AREA ============================>
async function putApi(url, payload) {
    const token = getLocalStorage('access_token');
    try {
        const res = await instance.put(`/${url}`, payload, {
            headers: {
                Authorization: token ? `Bearer ${token}` : 'no-author',
            },
        });
        return res;
    } catch (err) {
        return err;
    }
}
async function putApi2(url, payload) {
    try {
        const res = await instance.put(`/${url}`, payload);
        return res;
    } catch (err) {
        return err;
    }
}
export const updateClass = (classID,payload) => {
    return putApi('class/'+classID, payload);
};
export const updateBookSeries = (bookSeriesID,payload) => {
    return putApi('book-series/'+bookSeriesID, payload);
};
export const updateSubject = (subjectID,payload) => {
    return putApi('subject/'+subjectID, payload);
};
export const updateBookVolume = (bookVolumeId,payload) => {
    return putApi('book-volume/'+bookVolumeId, payload);
};
export const updateChapter = (chapterId,payload) => {
    return putApi('chapter/'+chapterId, payload);
};
export const updateLesson = (lessonId,payload) => {
    return putApi('lesson/'+lessonId, payload);
};

// UPDATE-REGISTER
export const updateRegister = (payload) => {
    return putApi2('register', payload);
};

// UPDATE-REGISTER
export const updateUser = (payload) => {
    ////console.log("payload update user",payload)
    return putApi('profile', payload);
};

export const updateAvatar = (payload) => {
    return putApi('profile/avatar', payload);
};


//ADMIN
export const setRoleUser = (id,payload) => {
    return putApi(`users/${id}`, payload);
};


export const updatePermission = (id,payload) => {
    return putApi(`system-permission/${id}`, payload);
};

export const setPermissionRole = (id,payload) => {
    return putApi(`role/${id}`, payload);
};


export const setResourceTags = (payload) => {
    return putApi(`resource-tags`, payload);
};

export const deleteTag = (id) => {
    return putApi(`tags/${id}`);
};



// DELETE API AREA ============================>
async function deleteApi(url, payload) {
    const token = getLocalStorage('access_token');

    try {
        const res = await instance.delete(`/${url}`, {
            headers: {
                Authorization: token ? `Bearer ${token}` : 'no-author',
            },
        });
        return res;
    } catch (err) {
        return err;
    }
}
export const deleteComment = (id) => {
    return deleteApi(`comment/${id}`);
};
export const changeStatusUser = (id) => {
    return deleteApi(`users/${id}`);
};
export const changeStatusBookSeriesSubject = (id) => {
    return deleteApi(`book-series-subject/${id}`);
};

export const changeStatusPermission = (id,active) => {
    return getApi(`system-permission/status/${id}?active=${active}`);
};
export const changeStatusRole = (id,active) => {
    return getApi(`role/status/${id}?active=${active}`);
};

export const changeStatusLesson = (id) => {
    return deleteApi(`lesson/${id}`);
};
export const changeStatusChapter = (id) => {
    return deleteApi(`chapter/${id}`);
};
export const changeStatusBookVolume = (id) => {
    return deleteApi(`book-volume/${id}`);
};
export const changeStatusSubject = (id) => {
    return deleteApi(`subject/${id}`);
};export const changeStatusBookSeries = (id) => {
    return deleteApi(`book-series/${id}`);
};
export const changeStatusClass = (id) => {
    return deleteApi(`class/${id}`);
};

export const changeStatusResource = (id) => {
    return deleteApi(`user-resource/my-resource/${id}`);
};

export const unShareResource = (id) => {
    return deleteApi(`user-resource/shared/${id}`);
};
// PATCH API AREA ============================>
async function patchApi(url, payload) {
    const token = getLocalStorage('access_token');
    try {
        const res = await instance.patch(`/${url}`, payload, {
            headers: {
                Authorization: token ? `Bearer ${token}` : 'no-author',
            },
        });
        return res;
    } catch (err) {
        return err;
    }
}

export const changePassword = (payload) => {
    return patchApi('users/change-password', payload);
};