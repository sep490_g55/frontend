import * as React from 'react';
import Skeleton from '@mui/material/Skeleton';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';

export default function SkeletonDetailMaterial() {
  return (
    <Stack spacing={2}>
      {/* For variant="text", adjust the height via font-size */}
      <Skeleton variant="text" sx={{ fontSize: '1rem' }} />
      {/* For other variants, adjust the size with `width` and `height` */}
      <Skeleton variant="circular" width={40} height={40} />
      <Skeleton variant="rectangular" width={210} height={60} />
      <Skeleton variant="rounded" width={210} height={60} />
      <Box
      sx={{
        display: "flex",
        justifyContent: "start",
        alignItems: "flex-start",
        width: "100%",
        marginTop:"50px",
        marginLeft:"50px"
      }}
      >
      <Skeleton variant="circular" width={40} height={40} />
      <Skeleton variant="rectangular" width={210} height={60} />
      </Box>
      <Box
      sx={{
        display: "flex",
        justifyContent: "start",
        alignItems: "flex-start",
        width: "100%",
        marginLeft:"50px"

      }}
      >
      <Skeleton variant="circular" width={40} height={40} />
      <Skeleton variant="rectangular" width={210} height={60} />
      </Box>
    </Stack>
  );
}