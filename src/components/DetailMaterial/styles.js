"use client";
import * as React from "react";
import { styled, alpha } from "@mui/material/styles";
import Button from "@mui/material/Button";
import bgsearch from "@/assets/bgsearch2.jpg";
export const ContainerDiv = styled("div")(({ theme }) => ({
  width: "100%",
  height: "auto",
  marginTop: "50px",
  marginLeft: "-2%",
  marginRight: "-2%",
  // padding: '10px'
  color: "#050505",
  flex: 1,
  display: "flex",
  position: "relative",
  justifyContent: "space-between",
  alignItems: "flex-start",
  zIndex: 8,
  [theme.breakpoints.up("xs")]: {
    justifyContent: "center",
  },
  [theme.breakpoints.up("md")]: {
    justifyContent: "center",
  },
  [theme.breakpoints.up("lg")]: {
    justifyContent: "space-between",
  },
  [theme.breakpoints.up("xl")]: {
    justifyContent: "space-between",
  },
}));

export const LeftDiv = styled("div")(({ theme }) => ({
  width: "420px",
  minWidth: "380px",
  flex: "0 0 280px",
  zIndex: 11,
  height: "85vh",
  display: "flex",
  position: "fixed",
  flexDirection: "column",
  borderTopRightRadius: "14px",
  // marginLeft:  "40px",
  webkitTransition: "width 2s" /* Safari */,
  transition: "width 2s",
  // boxShadow: '0 -3px 3px rgb(0 0 0 / 3%), 3px 0 0 rgb(0 0 0 / 3%)',
  // boxShadow: 'rgba(136, 165, 191, 0.48) 6px 2px 16px 0px, rgba(255, 255, 255, 0.8) -6px -2px 16px 0px',
  boxShadow:
    "rgba(0, 0, 0, 0.16) 0px 10px 36px 0px, rgba(0, 0, 0, 0.06) 0px 0px 0px 1px",
  padding: "9px",
  overflow: "hidden",
  [theme.breakpoints.down("xs")]: {
    display: "none !important",
    // height: "20vh",
  },
  [theme.breakpoints.up("xs")]: {
    display: "none !important",
    // height: "20vh",
  },
  [theme.breakpoints.up("md")]: {
    // height: "50vh",
    display: "none !important",


  },
  [theme.breakpoints.up("lg")]: {
    height: "82vh",
    display: "flex !important",
    paddingLeft:"50px",

  },
  [theme.breakpoints.up("xl")]: {
    height: "88vh",
    display: "flex !important",
    paddingLeft:"60px",

  },
}));
export const RightDiv = styled("div")(({ theme }) => ({
  height: "auto",
  zIndex: 9,
  marginLeft: "380px",
  webkitTransition: "width 2s" /* Safari */,
  transition: "width 2s",
  display: "flex",
  flexDirection: "column",
  alignContent: "center",
  justifyContent: "center",
  alignItems: "center",

  [theme.breakpoints.up("xs")]: {
    marginLeft: 0,
    width: "90% !important",
  },
  [theme.breakpoints.up("md")]: {
    marginLeft: 0,
    width: "90% !important",
  },
  [theme.breakpoints.up("lg")]: {
    marginLeft: "450px",
    width: "70% !important",
  },
  [theme.breakpoints.up("xl")]: {
    marginLeft: "450px",
    width: "100% !important",
  },
}));
export const ContainerInputCommentDiv = styled("div")(({ theme }) => ({
  width: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "column",
  padding: "10px 20px",
  position: "absolute",
  bottom: 0,
  left: 0,
  backgroundColor: "inherit",

  [theme.breakpoints.up("xs")]: {
    padding: "25px 20px 15px 40px",
  },
  [theme.breakpoints.up("md")]: {
    padding: "25px 20px 15px 40px",
  },
  [theme.breakpoints.up("lg")]: {
    padding: "25px 25px 10px 40px",
  },
  [theme.breakpoints.up("xl")]: {
    padding: "25px 20px 15px 40px",
  },
}));
export const ContainerDetailMaterialLeftDiv = styled("div")(({ theme }) => ({
  color: "#191b26",
  position: "relative",
  height: "73vh",

  [theme.breakpoints.up("xs")]: {
    height: "30vh",
  },
  [theme.breakpoints.up("md")]: {
     height: "30vh",
  },
  [theme.breakpoints.up("lg")]: {
    height: "61vh",
  },
  [theme.breakpoints.up("xl")]: {
    height: "73vh",
  },
}));
export const InfoMaterialDiv = styled("div")(({ theme }) => ({
  color: "#191b26",
  position: "relative",
  height: "50vh",
  overflowX:"auto",

  [theme.breakpoints.up("xs")]: {
    height: "93vh",
  },
  [theme.breakpoints.up("md")]: {
    height: "93vh",

  },

  [theme.breakpoints.up("md")]: {
    height: "93vh",
  },
  [theme.breakpoints.up("lg")]: {
    height: "53vh",
  },
  [theme.breakpoints.up("xl")]: {
    height: "68vh",
  },
}));
export const MaterialDetailDiv = styled("div")(({ theme }) => ({
  height: 500,
  width: 800,

  [theme.breakpoints.up("xs")]: {
    height: 300,
    width: 400,
  },
  [theme.breakpoints.up("md")]: {
    height: 500,
    width: 800,
  },
  [theme.breakpoints.up("lg")]: {
    height: 500,
    width: 800,
  },
  [theme.breakpoints.up("xl")]: {
    height: 500,
    width: 800,
  },
}));
export const HeaderLeftLeft = styled("div")(({ theme }) => ({
  width: "75%",
  float: "left",
  backgroundColor: "#37581B",
  color: "white",
  borderRadius: "20px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  padding: "5px",
}));
export const HeaderRightLeft = styled("div")(({ theme }) => ({
  width: "20%",
  float: "right",
  fontSize: "26px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  marginLeft: "5%",
  cursor: "pointer",
}));

export const ActionLabel = styled("label")(({ theme }) => ({
  width: "auto",
  float: "right",
  fontSize: "16px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  marginLeft: "2%",
  cursor: "pointer",
  background: "gray",
  color: "white",
  borderRadius: "15px",
  padding: "5px",
}));

export const InputCmt = styled("input")(({ theme }) => ({
  width: "100%",
  height: "40px",
  borderRadius: "15px",
  padding: "0 15px",
}));
export const SelectAction = styled("input")({
  color: "greenyellow !important",
  cursor: "pointer",
  width: "100%",
  visibility: "hidden",
  position: "absolute",
  "&:checked + .title-action": {
    background: "#14606C",
    color: "white !important",
    fontWeight: "bold",
  },
});

export const GroupComment = styled("div")(({ theme }) => ({
  display: "block",
  width: "100%",
  height: "auto",
  padding: "10px 0",
  overflowX:"auto",
  [theme.breakpoints.up("xs")]: {
    height: "77vh",
  },
  [theme.breakpoints.up("md")]: {
    height: "77vh",

  },

  [theme.breakpoints.up("md")]: {
    height: "52vh",

  },
  [theme.breakpoints.up("lg")]: {
    height: "26vh",
  },
  [theme.breakpoints.up("xl")]: {
    height: "52vh",
  },
}));
