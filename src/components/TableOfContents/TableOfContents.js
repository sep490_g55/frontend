import React, { useEffect, useState } from 'react';
import Chapter from '@/components/Chapter/Chapter';
import { Container } from 'reactstrap';
import { Box, Skeleton } from '@mui/material';
import {ContentDiv} from './styles'
import { useDispatch, useSelector } from 'react-redux';
import { BeatLoader } from 'react-spinners';
import { getALLLessonByChapterID } from '@/dataProvider/agent';
import { CenterFlexBox } from '../common_sections';
const TableOfContents = ({isLoading, chapters, openChapter, onChapterClick }) => 

{
  const newMaterial = useSelector(state => state.newMaterial.material)

const [selectedChapter, setSelectedChapter] = useState(newMaterial?.chapter?.id)
const [chapterID, setChapterID] = useState("")
const [lessonList, setLessonList] = useState([])
const [isLoading2, setIsLoading2] = useState(true);  
useEffect(() => {
  GetAll()
}, [newMaterial?.chapter?.id,chapters])
const GetAll = async() =>{
  setIsLoading2(true)
  const res = await  getALLLessonByChapterID(newMaterial?.chapter?.id === ""|| newMaterial?.chapter?.id ===  undefined ? chapters[0]?.id === null||chapters[0]?.id===undefined ? "0":chapters[0]?.id:newMaterial?.chapter?.id);
  setLessonList(res.data)
  setIsLoading2(false)

}

  return(
  
  <ContentDiv>
    {chapters.length === 0 && !isLoading ? 
    <Box sx={{color:"#050505"}}>Không có dữ liệu</Box>: 
    chapters.length === 0 && isLoading ? 
     <Box sx={{ width: "100%" }}>
      <Skeleton width="50%" />
      <Skeleton animation="wave" width="50%" sx={{marginLeft:"20px"}}/>
      <Skeleton animation="wave" width="50%" sx={{marginLeft:"20px"}}/>
      <Skeleton animation="wave" width="50%" sx={{marginLeft:"20px"}}/>
      <Skeleton width="50%" />
      <Skeleton animation="wave" width="50%" sx={{marginLeft:"20px"}}/>
      <Skeleton animation="wave" width="50%" sx={{marginLeft:"20px"}}/>
      <Skeleton animation="wave" width="50%" sx={{marginLeft:"20px"}}/>
      <Skeleton width="50%" />
      <Skeleton animation="wave" width="50%" sx={{marginLeft:"20px"}}/>
      <Skeleton animation="wave" width="50%" sx={{marginLeft:"20px"}}/>
      <Skeleton animation="wave" width="50%" sx={{marginLeft:"20px"}}/>
    </Box>  : 
    chapters.length > 0 && isLoading ?
    <> <CenterFlexBox>
          <BeatLoader
              color="#198754"
              size={5}
              style={{marginLeft:"5px",marginTop:"5px"}}
            />
      </CenterFlexBox>
        {
          chapters.map((chapter, index) => (
            <Chapter
        isLoading={isLoading2}
            
            key={index}
            id={chapter.id}
              title={chapter.name}
              lessons={lessonList}
              isOpen={ newMaterial?.chapter?.id === chapter.id}
              onChapterClick={() => onChapterClick(index)}
              selectedChapter={selectedChapter}
              setSelectedChapter={setSelectedChapter}
            />
        ))
        }
      </>
    :chapters.map((chapter, index) => (
        <Chapter
        chapterID={chapter.id}
        isLoading={isLoading2}
        
        key={index}
        id={chapter.id}
          title={chapter.name}
          
          isOpen={ newMaterial?.chapter?.id === chapter.id}
          onChapterClick={() => onChapterClick(index)}
          selectedChapter={selectedChapter}
          setSelectedChapter={setSelectedChapter}
          lessons={lessonList}
        />
    ))}
  </ContentDiv>
);
}

export default TableOfContents;
