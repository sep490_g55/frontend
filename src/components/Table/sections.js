import { Box } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import Paper from "@mui/material/Paper";
import Pagination from "@mui/material/Pagination";
import PaginationItem from "@mui/material/PaginationItem";
import Stack from "@mui/material/Stack";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import Typography from "@mui/material/Typography";
import { styled, alpha } from "@mui/material/styles";

// import {useLocation } from 'react-router-dom';

import React, { useState } from "react";
import { AiFillDelete } from "react-icons/ai";
import { BsShareFill } from "react-icons/bs";
import { BiSolidEditAlt } from "react-icons/bi";

import { Button } from "reactstrap";
import { GreenButtonAction, IconButtonAction } from "../FolderImage/styles";
import FormDialog, { FullSaveMaterialDialog } from "../Dialog/section";
import { ToastContainer, toast } from "react-toastify";
import { CustomizedMenus } from "../Option/menu";
import { SaveButton } from "../common_sections";

export const columns = [
  { field: "id", headerName: "ID", width: 70 },
  { field: "firstName", headerName: "First name", width: 130 },
  { field: "lastName", headerName: "Last name", width: 130 },
  {
    field: "age",
    headerName: "Age",
    type: "number",
    width: 90,
  },
  {
    field: "fullName",
    headerName: "Full name",
    description: "This column has a value getter and is not sortable.",
    sortable: false,
    width: 160,
    valueGetter: (params) =>
      `${params.row.firstName || ""} ${params.row.lastName || ""}`,
  },
];

export const ManageMaterialColumns = [
  { field: "id", headerName: "ID", width: 50 },
  { field: "name", headerName: "Tên", width: 250, renderCell: (params) => {} },
  { field: "views", headerName: "Lượt xem", width: 110 },
  { field: "likes", headerName: "Lượt thích", width: 100 },
  { field: "mode", headerName: "Chế độ", width: 100 },
  {
    field: "status",
    headerName: "Trạng thái",
    width: 160,
    renderCell: (params) => {
      if (params.row.status == "Đã kiểm duyệt") {
        return <Box sx={{ color: "#44642C" }}>Đã kiểm duyệt</Box>;
      } else if (params.row.status == "Bị từ chối") {
        return <Box sx={{ color: "red" }}>Bị từ chối</Box>;
      } else if (params.row.status == "Bị báo cáo") {
        return <Box sx={{ color: "#D7BB00" }}>Bị báo cáo</Box>;
      } else {
        return <Box sx={{ color: "blue" }}>Chưa kiểm duyệt</Box>;
      }
    },
  },
  {
    field: "actions",
    headerName: "Hành động",
    width: 120,
    renderCell: (params) => {
      if (params.row.status == "Đã kiểm duyệt") {
        return (
          // <IconButtonAction>
          //   <FullShareMaterialDialog name={params.row.name} />
          // </IconButtonAction>
          <CustomizedMenus name={params.row.name} type="SHARE" />
        );
      } else if (params.row.status == "Bị báo cáo") {
        return (
          <CustomizedMenus name={params.row.name} type="EDIT-SHARE-DELETE" />
        );
      } else if (params.row.status == "Chưa kiểm duyệt") {
        return <CustomizedMenus name={params.row.name} type="EDIT-DELETE" />;
      } else {
        return <CustomizedMenus name={params.row.name} type="DELETE" />;
      }
    },
  },
];
const CustomDataGrid = styled((props) => <DataGrid {...props} />)(
  ({ theme }) => ({
    "& .css-wop1k0-MuiDataGrid-footerContainer": {
      display: "none",
    },
  })
);

export const ShareMaterialsColumns = [
  { field: "name", headerName: "Tên", width: 250 },
  { field: "owner", headerName: "Chủ sở hữu", width: 200 },
  { field: "createOn", headerName: "Ngày đăng", width: 190 },
];

export const SaveMaterialsColumns = [
  { field: "name", headerName: "Tên", width: 250 },
  { field: "owner", headerName: "Chủ sở hữu", width: 200 },
  { field: "createOn", headerName: "Ngày đăng", width: 190 },
  {
    field: "action",
    headerName: "Hành động",
    width: 90,
    renderCell: (params) => {
      return (
        <FullSaveMaterialDialog name={params.row.name} isSave={params.row.isSave}/>
      );
    },
  },
];

export const CourseraManageColumns = [
  { field: "id", headerName: "ID", width: 0 },
  { field: "name", headerName: "Tên", width: 250 },
  { field: "owner", headerName: "Chủ sở hữu", width: 200 },
  { field: "createOn", headerName: "Sửa gần đây", width: 250 },
  {
    field: "action",
    headerName: "",
    width: 90,
    renderCell: (params) => {
      return (
        <CustomizedMenus name={params.row.name} type="MANAGE-CLASS" id={params.row.id} />
        // <FullSaveMaterialDialog name={params.row.name} isSave={params.row.isSave}/>
      );
    },
  },
];
export const TableMaterial = ({ rows, columns, totalPage }) => {
  const [page, setPage] = React.useState(1);
  const handleChangePage = (event, value) => {
    setPage(value);
  };

  return (
    <Box sx={{ height: 600, width:{xs:"450px",md:'100%'}, marginBottom: "150px"  }}>
      <CustomDataGrid rows={rows} columns={columns} />
      <Stack
        spacing={2}
        sx={{
          border: "1px solid rgba(224, 224, 224, 1)",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          padding: "20px ",
        }}
      >
        <Typography>Trang hiện tại: {page}</Typography>
        <Pagination
          count={totalPage}
          showFirstButton
          showLastButton
          onChange={handleChangePage}
        />
      </Stack>
    </Box>
  );
};

