import LayoutPage from "@/layouts/LayoutPage";
import Header from "@/layouts/common/header/Header";
import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Col, Container, Row } from "react-bootstrap";
import {
  BookSeries,
  SearchSection,
  ViewClass,
  Subjects,
} from "@/components/Search/section";

import dynamic from "next/dynamic";
import { getListClass } from "@/utils/classesAxios";
import { getBookSeries } from "@/utils/bookSeriesAxios";
import { getSubjects } from "@/utils/subjectAxios";
import { Box } from "@mui/material";

const SearchWithFolder = ({ classesList, bookSeries, subjects }) => {
  const bookSeriesAPI = bookSeries;
  const subjectsAPI = subjects;
  const classesAPI = classesList;

  // console.log(classesAPI);
  
  const [subjectList, setSubjectList] = useState(subjectsAPI);
  const [isShow, setIsShow] = useState(true);
  const [isClass, setIsClass] = useState(false);
  const [isBookSeries, setIsBookSeries] = useState(false);
  const [classID, setClassID] = useState(classesAPI[0].class_id);
  const [bookSeriesList, setBookSeriesList] = useState(bookSeriesAPI);
  const [bookSeriesID, setBookSeriesID] = useState(bookSeriesList[0].book_series_id);
  
  const showBookSeries = isClass ? { display: "block" } : { display: "none" };
  const showSubject = isBookSeries ? { display: "block" } : { display: "none" };
  const setBookSeriesListAfter = () => {
    
    setIsClass(true);
    setBookSeriesList(bookSeries.filter(bs => bs.class_id == classID))
    // alert('value: ',classID);
  };
  const setSubjectListAfter = () => {
    setIsBookSeries(true);
    setSubjectList(subjects.filter(sl => sl.book_series_id == bookSeriesID))
  };
  const onHind = () => {
    setIsBookSeries(false);
    setIsClass(false);
  };
  return (
   
        <Box
          
        >
        <ViewClass classes={classesAPI} setIsClass={setBookSeriesListAfter} setClassID={(e) => setClassID(e.target.value) }/>
        <BookSeries
          bookSeries={bookSeriesList}
          setIsBookSeries={setSubjectListAfter}
          styleBookSeries={showBookSeries}
          setBookSeriesID={(e)=>setBookSeriesID(e.target.value)}
        />
        <Subjects subjects={subjectList} styleSubjects={showSubject} />
        </Box>
       
  );
};

export default SearchWithFolder;
