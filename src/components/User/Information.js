"use client";
import React from "react";
import { useRouter, useState,useEffect } from "next/router";
import yup from "@/utils/yupGlobal";
import { yupResolver } from "@hookform/resolvers/yup";
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
// import Box from '@mui/material/Box';
import "bootstrap/dist/css/bootstrap.min.css";
import { getAllClass } from "@/dataProvider/agent";


import {
  Button,
  Col,
  Container,
  Form,
  FormGroup,
  Label,
  Row,
} from "reactstrap";

import { GoogleLogin, GoogleOAuthProvider } from "@react-oauth/google";
import {
  StyledDivContent,
  StyledSection,
  StyledLayout,
  StyledDivLeftContent,
  StyledDivRightContent,
  StyledButtonFull,
  CustomTextField,
  ColInformation,
} from "@/components/auth/style";
import Link from "next/link";
import LoginLayout from "@/layouts/login/LoginLayout";
import ColInput from "@/layouts/login/ColInput";
// @mui/material/Box';
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Input from "@mui/material/Input";
import FilledInput from "@mui/material/FilledInput";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";

//@mui/icons-material
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Header from "@/layouts/common/header/Header";
import LayoutPage from "@/layouts/LayoutPage";
import { useAuthContext } from "@/auth/useAuthContext";
import { BeatLoader } from "react-spinners";
import { Alert } from "@mui/material";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from "react-toastify";
import {
  MUIDistrictSelect,
  MUIProvinceSelect,
} from "@/components/Select/AddressSelect/MUI/address";

export const InformationUser = ({ profile }) =>{
  const [provinceID, setProvinceID] = useState("");
  const [districtID, setDistrictID] = useState("");
  const [province, setProvince] = useState({
    name: profile ? profile?.province : "",
    code: 0,
    division_type: "",
    codename: "",
    phone_code: 0,
    districts: [],
  });
  const [district, setDistrict] = useState({
    name: profile ? profile?.district : "",
    code: 0,
    division_type: "",
    codename: "",
    province_code: 0,
    wards: [],
  });

  const route = useRouter();
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleLogin = () => {
    route.push("/auth/login");
  };
  const [age, setAge] = useState('');

  const handleChangeAge = (event) => {
    setAge(event.target.value);
  };
  const { registerUser } = useAuthContext();
  const [isLoading, setIsLoading] = useState(false);

  const schema = yup
    .object({
      username: yup
        .string()
        .required("Không để trống tên tài khoản")
        .username("Tên tài khoản không hợp lệ. Gợi ý: thoconxinhxan"),
      email: yup
        .string()
        .required("Không để trống email")
        .email("Email không hợp lệ Gợi ý: thoconxinhxan@gmail.com"),
      firstName: yup
        .string()
        .required("Không để trống ")
        .name("Mật khẩu không đúng định dạng Gợi ý: Thocon"),
      lastName: yup
        .string()
        .required("Không để trống ")

        .name("Mật khẩu không đúng định dạng Gợi ý: xinhxan"),
        phone: yup
        .string()
        .required("Không để trống ")
        .phone("SĐT không đúng định dạng Gợi ý: 0988909999"),
        school: yup
        .string()
        .required("Không để trống ")
        .school("Tên trường không đúng định dạng Gợi ý: THPT Phạm Văn Nghị"),
    })
    .required();
  const {
    reset,
    setError,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const onSubmit = async (data) => {
    setIsLoading(true);
    try {
      console.log(data);
      // const status = await registerUser(data.username,data.email,data.pass);

      //   if (status === 200) {
      //     toast.success("Đăng ký thành công");
      //     route.push('/auth/get-information');

      //   } else if (status === 202) {
      //     console.log("nothing");
      //     toast.warning("Tên tài khoản hoặc email đã được đăng ký");
      //   }else{
      //     console.log("nothing");
      //     toast.error("Đăng ký không thất bại");
      //   }
    } catch (error) {
      console.error(error);

      // reset();

      setError("afterSubmit", {
        ...error,
        message: error.message,
      });
    }

    setIsLoading(false);
  };

  const getClassList = async() =>{
    setIsLoading(true)
    try{
      const classList = await getAllClass();
      setClasses(classList.data)
    }catch(e){
      console.log(e)
    }
    setIsLoading(false)
  };
  getClassList();
  return (
   
          

          <Form onSubmit={handleSubmit(onSubmit)}>
            {!!errors.afterSubmit && (
            <Alert severity="error">{errors.afterSubmit.message}</Alert>
          )}
            <ColInformation>
              <CustomTextField
                error={errors.username}
                defaultValue="Hello World"
                helperText={errors.username && errors.username.message}
                fullWidth
                {...register("username")}
                label="Tên đăng nhập"
                type="text"
                id="username"
                // onKeyUp={(e) => setData({...data,email: e.target.value})}
              />
            </ColInformation>
            <ColInformation>
              <CustomTextField
                error={errors.firstName}
                defaultValue="Hello World"
                helperText={errors.firstName && errors.firstName.message}
                fullWidth
                {...register("firstName")}
                label="Họ"
                type="text"
                id="firstName"
                // onKeyUp={(e) => setData({...data,email: e.target.value})}
              />
              <CustomTextField
                error={errors.lastName}
                defaultValue="Hello World"
                helperText={errors.lastName && errors.lastName.message}
                fullWidth
                {...register("lastName")}
                label="Tên"
                type="text"
                id="lastName"
                // onKeyUp={(e) => setData({...data,email: e.target.value})}
              />
            </ColInformation>

            <ColInformation>
              <CustomTextField
                error={errors.phone}
                defaultValue="Hello World"
                helperText={errors.phone && errors.phone.message}
                fullWidth
                {...register("phone")}
                label="Số điện thoại"
                type="text"
                id="phone"
                // onKeyUp={(e) => setData({...data,email: e.target.value})}
              />
              <FormControl sx={{ m: 1, minWidth: 120 }} >
                <InputLabel id="demo-simple-select-error-label">Giới tính</InputLabel>
                <Select
                  labelId="demo-simple-select-error-label"
                  id="demo-simple-select-error"
                  value={age? "Nam":"Nữ"}
                  label="Giới tính"
                  onChange={handleChangeAge}
                  renderValue={(value) => `${value}`}
                >
                  
                  <MenuItem value={true}>Nam</MenuItem>
                  <MenuItem value={false}>Nữ</MenuItem>
                </Select>
              </FormControl>
            </ColInformation>

            <ColInformation>
              <MUIProvinceSelect
                errors={errors}
                register={register}
                province={province}
                setProvinceID={setProvinceID}
              />
              <MUIDistrictSelect
                errors={errors}
                register={register}
                district={district}
                provinceID={provinceID}
                districtID={districtID}
                setDistrictID={setDistrictID}
              />
            </ColInformation>
            <ColInformation>
              <CustomTextField
                error={errors.school}
                defaultValue="Hello World"
                helperText={errors.school && errors.school.message}
                fullWidth
                {...register("phone")}
                label="Tên trường"
                type="text"
                id="school"
                // onKeyUp={(e) => setData({...data,email: e.target.value})}
              />
              <FormControl sx={{ m: 1, minWidth: 120 }} >
                <InputLabel id="demo-simple-select-error-label">Lớp</InputLabel>
                <Select
                  labelId="demo-simple-select-error-label"
                  id="demo-simple-select-error"
                  value={age? "Nam":"Nữ"}
                  label="Giới tính"
                  onChange={handleChangeAge}
                  renderValue={(value) => `${value}`}
                >
                  
                  <MenuItem value={true}>Nam</MenuItem>
                  <MenuItem value={false}>Nữ</MenuItem>
                </Select>
              </FormControl>
            </ColInformation>

            <StyledDivContent>
              <StyledButtonFull
                outline
                className="login-page__btn-login"
                type="submit"
              >
                Tiếp tục &#10149;{" "}
                {isLoading ? (
                  <BeatLoader
                    color="white"
                    size={5}
                    style={{ marginLeft: "5px", marginTop: "5px" }}
                  />
                ) : (
                  ""
                )}
              </StyledButtonFull>
            </StyledDivContent>
          </Form>

          
  );
}
