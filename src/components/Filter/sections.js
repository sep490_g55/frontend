import * as React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import { useState } from 'react';
import { Autocomplete, TextField } from '@mui/material';

const Item = styled(Paper)(({ theme }) => ({
//   backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
//   ...theme.typography.body2,
//   padding: theme.spacing(1),
//   textAlign: 'center',
//   color: theme.palette.text.secondary,
}));

export const FullFolderFilter = () => {
    const [classes, setClasses] = useState([]);
    const [bookSeries, setBookSeries] = useState([]);
    const [subjects, setSubjects] = useState([]);
    const [bookVolumes, setBookVolumes] = useState([]);
    const [chapters, setChapters] = useState([]);
    const [lessons, setLessons] = useState([]);

    React.useEffect(() => {
        getClasses();
        getSubjects();
    
      }, []);
      const getClasses = async () => {
        const api = "classes/";
    
        try {
          const res = await getFolderSrc(api);
    
          if (res) {
            setClasses(res);
            
          } else {
            console.log(`classes not found`);
          }
        } catch (error) {
          console.log(`Can not get classes ${error}`);
        }
      };
      const getSubjects = async () => {
        const api = "subjects/";
    
        try {
          const res = await getFolderSrc(api);
    
          if (res) {
            setSubjects(res);
            
          } else {
            console.log(`subjects not found`);
          }
        } catch (error) {
          console.log(`Can not get subjects ${error}`);
        }
      };
  return (
    <Box sx={{ width: '100%' }}>
      <Stack direction="row" spacing={2}>
        <SelectInList label="Lớp" list={classes} width={110}/>
        <SelectInList label="Bộ sách" list={bookSeries} width={110}/>
        <SelectInList label="Môn" list={subjects} width={110}/>
        <SelectInList label="Đầu sách" list={bookVolumes} width={110}/>
        <SelectInList label="Chương" list={chapters} width={110}/>
        <SelectInList label="Bài học" list={lessons} width={110}/>

      </Stack>
    </Box>
  )
}

export const SelectInList = ({label,list,width}) =>{
    const listProps = {
        options: list,
    getOptionLabel: (option) => option.name,
    }
    return(
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {/* <Box>Môn học* :</Box> */}
          <Autocomplete
            {...listProps}
            disablePortal
            // id="combo-box-demo"
            options={list}
            sx={{ width: {width}, marginLeft: "20px" }}
            renderInput={(params) => (
              <TextField {...params} label={label} />
            )}
          />
        </Box>
    )
}

