import * as React from "react";
import { styled, alpha } from "@mui/material/styles";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import EditIcon from "@mui/icons-material/Edit";
import Divider from "@mui/material/Divider";
import ArchiveIcon from "@mui/icons-material/Archive";
import FileCopyIcon from "@mui/icons-material/FileCopy";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import IconButton from "@mui/material/IconButton";
import { BeatLoader } from "react-spinners";

import FormDialog, {
  FullAddBookSeriesDialog,
  FullAddBookVolumeDialog,
  FullAddChapterDialog,
  FullAddClassDialog,
  FullAddLessonDialog,
  FullAddRoleDialog,
  FullAddSubjectDialog,
  FullAddTagDialog,
  FullDeleteMaterialDialog,
  FullEditMaterialDialog,
  FullSetPermissionDialog,
  FullShareMaterialDialog,
  FullUpdatePermissionDialog,
  FullViewCommentDialog,
  FullAddFolderTagDialog,
  FullAddSubjectBookSeriesDialog,
  FullUpdateRoleDialog,
  FullViewMaterialReportedDialog,
  FullUnSaveMaterialDialog,
  FullUnShareMaterialDialog,
} from "../Dialog/section";
import { Box } from "@mui/material";
import { useState } from "react";
import { BsShareFill } from "react-icons/bs";
import Dialog from "@mui/material/Dialog";
import { FaTags } from "react-icons/fa";
import { GrView } from "react-icons/gr";

import { IconAndText } from "../common_sections";
import { createStore } from "redux";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { setClass, } from "@/redux/slice/ViewClassSlice";
import { setViewBookSeries, setViewBookVolume, setViewChapter, setViewClass, setViewLesson, setViewSubjectBookSeries } from "@/redux/slice/ViewFolderSRCSlice";
import {changeStatusBookSeries, changeStatusBookVolume, changeStatusChapter, changeStatusClass, changeStatusLesson, changeStatusPermission, changeStatusRole, changeStatusSubject, changeStatusUser, changeVisualTypeResource, getVisualTypes, resetPassword,changeStatusBookSeriesSubject} from "@/dataProvider/agent"
import { toast } from "react-toastify";
import { IoMdArrowDropdown } from "react-icons/io";
import { useEffect } from "react";
import { setIsReload } from "@/redux/slice/IsReLoad";
const StyledMenu = styled((props) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "right",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "right",
    }}
    {...props}
  />
))(({ theme }) => ({
  "& .MuiPaper-root": {
    borderRadius: 6,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color:
      theme.palette.mode === "light"
        ? "rgb(55, 65, 81)"
        : theme.palette.grey[300],
    boxShadow:
      "rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px",
    "& .MuiMenu-list": {
      padding: "4px 0",
    },
    "& .MuiMenuItem-root": {
      "& .MuiSvgIcon-root": {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      "&:active": {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity
        ),
      },
    },
  },
}));

export const CustomizedMenus = ({viewType, isIcon,icon,onDetailView,value,id,type, name,classID,bookSeriesID,subjectID,chapterID,bookVolumeID,lessonID ,folderID}) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Box>
      <IconButton
        aria-label="more"
        id="long-button"
        aria-controls={open ? "long-menu" : undefined}
        aria-expanded={open ? "true" : undefined}
        aria-haspopup="true"
        onClick={handleClick}
      >
        {
          isIcon?
          <IoMdArrowDropdown />
          :<MoreVertIcon />}
      </IconButton>
      <StyledMenu
        id="demo-customized-menu"
        MenuListProps={{
          "aria-labelledby": "demo-customized-button",
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >
        {type == "SHARE-FOR-ME" ? (
          <MenuItemUnShare  handleClose={handleClose} value={value} />
        ) :type == "MY-SAVED" ? (
          <MenuItemUnSave  handleClose={handleClose} value={value} />
        ) :type == "SET-VISUAL-TYPE" ? (
          <MenuItemSetVisualType  handleClose={handleClose} id={id} />
        ) :type == "MODERATOR-FOLDER-TAG" ? (
          <MenuItemModerateFolderTag viewType={viewType} value={value} handleClose={handleClose} folderID={folderID} onDetailView={onDetailView}/>
        ) :type == "ADMIN-SET-PERMISSION" ? (
          <MenuItemAdminSetPermission  />
        ) :type == "ADMIN-UPDATE-PERMISSION" ? (
          <MenuItemAdminUpdatePermission  value={value} handleClose={handleClose}/>
        ) :type == "MODERATOR-VIEW-COMMENT" ? (
          <MenuItemAdminViewComment value ={value} handleClose={handleClose}/>
        ) :type == "MODERATOR-VIEW-MATERIAL-REPORT" ? (
          <MenuItemModeratorViewMaterialReported value={value}  handleClose={handleClose}/>
        ) :type == "ADMIN-ROLE" ? (
          <MenuItemAdminRole value={value}  id={id} name={name} handleClose={handleClose}/>
        ) :type == "ADMIN-LESSON" ? (
          <MenuItemAdminLesson onDetailView={onDetailView}  name={name} handleClose={handleClose}  lessonID={lessonID} value={value}/>
        ) :type == "ADMIN-BOOK-VOLUME" ? (
          <MenuItemAdminBookVolume onDetailView={onDetailView}  name={name} handleClose={handleClose} bookVolumeID={bookVolumeID} value={value}/>
        ) :type == "ADMIN-CHAPTER" ? (
          <MenuItemAdminChapter onDetailView={onDetailView}  name={name} handleClose={handleClose}  chapterID={chapterID}  value={value}  />
        ) :type == "ADMIN-SUBJECT" ? (
          <MenuItemAdminSubject onDetailView={onDetailView}  name={name} handleClose={handleClose} subjectID={subjectID} value={value} />
        ) :type == "ADMIN-SUBJECT-BOOK-SERIES" ? (
      <MenuItemAdminSubjectBookSeries value={value} onDetailView={onDetailView}  name={name} handleClose={handleClose} subjectID={subjectID}  />
        ) :type == "ADMIN-CLASS" ? (
          <MenuItemAdminClass onDetailView={onDetailView}  name={name} handleClose={handleClose} classID={classID}value={value} />
        ) :type == "ADMIN-BOOK-SERIES" ? (
          <MenuItemAdminBookSeries onDetailView={onDetailView}  name={name} handleClose={handleClose} bookSeriesName={name}value={value} bookSeriesID={bookSeriesID} classID={classID} />
        ):type == "ADMIN-USER" ? (
          <MenuItemAdminUser id={id}  name={name}  value={value} handleClose={handleClose} />
        ) :
        type == "MANAGE-CLASS" ? (
          <MenuItemViewClass  id={id} name={name} handleClose={handleClose} />
        ) : type == "SHARE" ? (
          <MenuItemShareMaterial value={value} id={id} name={name} handleClose={handleClose} />
        ) : type == "UPDATE-SHARE-DELETE" ? (
          <MenuItemEditShareDeleteMaterial
          value={value}
          id={id}
          name={name}
            handleClose={handleClose}
          />
        )  : type == "DELETE" ? (
          <MenuItemDeleteMaterial 
          value={value}
          id={id} name={name} handleClose={handleClose} />
        ) : 
        type == "SHARE-DELETE" ? (
          <MenuItemShareDeleteMaterial
          value={value}
          id={id} name={name} handleClose={handleClose} />
        ) : 
        type == "UPDATE-DELETE" ? (
          <MenuItemUpdateDeleteMaterial
          value={value}
          id={id} name={name} handleClose={handleClose} />
        ) : (
          ""
        )}
        
        {/* <MenuItemShareMaterial name={name} handleClose={handleClose}/> */}
      </StyledMenu>
    </Box>
  );
};

export const MenuItemSetVisualType = ({handleClose,id }) => {
  const dispatch = useDispatch();
  const [visualTypeList,setVisualTypeList] = useState([])
  
  const [isLoading,setIssLoading] = useState(false)
  useEffect(()=>{
    getVisualTypeList()
  },[])
  const  getVisualTypeList = async() =>{
    setIssLoading(true)
    const res= await getVisualTypes()
    if(res.status === 200){
      setVisualTypeList(res.data)
    }
    setIssLoading(false)
   
  }

  const handleClick = async(type) =>{
    setIssLoading(true)
    const res= await changeVisualTypeResource(id,type)
    if(res.status === 200){
      dispatch(setIsReload((pre) => !pre));
      toast.success("Thay đổi thành công")
    }else{
      toast.warning("Thay đổi thất bại")

    }
    setIssLoading(false)
    handleClose()
  }
  return (
    <>
   
      {visualTypeList.length === 0 ?
      <MenuItem disableRipple>
        Đang tải ...
      </MenuItem>
      :visualTypeList.map((type) =>
          <MenuItem disableRipple onClick={()=>handleClick(type)}>
            {type === "PUBLIC"?"Công Khai" 
            : type === "RESTRICT"?"Hạn chế"
            :"Chỉ mình tôi"}
          </MenuItem>
        )
    }
    </>
  );
};
export const MenuItemAdminClass = ({onDetailView, name, handleClose,classID,value }) => {
  const dispatch = useDispatch();
  const handleClick = () =>{
    onDetailView()
    dispatch(setViewClass({id:classID,name:name}))
  }  
  const [isLoading,setIssLoading] = useState(false)
  const handleClick2 = async() =>{
    setIssLoading(true)
    const res= await changeStatusClass(classID)
    if(res.status === 200){
      dispatch(setIsReload((pre) => !pre));
      {value?.active ? toast.success( `Đã chặn ${name} ` ):toast.success(`Đã bỏ chặn ${name}`)}
    }else{
      {value?.active ? toast.warning( `Không thể chặn ${name} ` ):toast.warning( `Không bỏ thể chặn  ${name}`)}

    }
    setIssLoading(false)

  }
  return (
    <>
    <MenuItem disableRipple onClick={() =>handleClick()}>Xem chi tiết</MenuItem>
      <MenuItem disableRipple>
        <FullAddClassDialog
          name={name}
          nameButton="Cập nhật"
          handleCloseMenu={handleClose}
          classID={classID}

          type="UPDATE"
          addOrUpdate="UPDATE"
        />
      </MenuItem>
      <MenuItem disableRipple onClick={()=>handleClick2()}>{value?.active ? "Chặn" :"Bỏ chặn"} {isLoading&&<BeatLoader
            color="#197854"
            size={5}
            style={{ marginLeft: "5px", marginTop: "5px" }}
          />}</MenuItem>
    </>
  );
};

export const MenuItemAdminBookSeries = ({onDetailView, name, handleClose,classID,bookSeriesID,value }) => {
  const dispatch = useDispatch();
  const handleClick = () =>{
    onDetailView()
    dispatch(setViewBookSeries({id:bookSeriesID,name:name}))
  }  
  const [isLoading,setIssLoading] = useState(false)
  const handleClick2 = async() =>{
    setIssLoading(true)
    const res= await changeStatusBookSeries(bookSeriesID)
    if(res.status === 200){
      dispatch(setIsReload((pre) => !pre));
      {value?.active ? toast.success( `Đã chặn ${name} ` ):toast.success(`Đã bỏ chặn ${name}`)}
    }else{
      {value?.active ? toast.warning( `Không thể chặn ${name} ` ):toast.warning( `Không bỏ thể chặn  ${name}`)}

    }
    setIssLoading(false)

  }
  return (
    <>
    <MenuItem disableRipple onClick={handleClick}>Xem chi tiết</MenuItem>
      <MenuItem disableRipple>
        <FullAddBookSeriesDialog
          name={name}
          nameButton="Cập nhật"
          handleCloseMenu={handleClose}
          bookSeriesID={bookSeriesID}
          type="UPDATE"
          addOrUpdate="UPDATE"
        />
      </MenuItem>
      <MenuItem disableRipple onClick={()=>handleClick2()}>{value?.active ? "Chặn" :"Bỏ chặn"} {isLoading&&<BeatLoader
            color="#197854"
            size={5}
            style={{ marginLeft: "5px", marginTop: "5px" }}
          />}</MenuItem>
    </>
  );
};
export const MenuItemAdminSubject = ({onDetailView, name, handleClose,classID ,bookSeriesID,subjectID,value}) => {
  const dispatch = useDispatch();
  const handleClick = () =>{
    onDetailView()
    // dispatch(setViewSubjectBookSeries({id:bookSeriesID,name:name}))
  }
  const [isLoading,setIssLoading] = useState(false)
  const handleClick2 = async() =>{
    setIssLoading(true)
    const res= await changeStatusSubject(subjectID)
    if(res.status === 200){
      dispatch(setIsReload((pre) => !pre));
      {value?.active ? toast.success( `Đã chặn ${name} ` ):toast.success(`Đã bỏ chặn ${name}`)}
    }else{
      {value?.active ? toast.warning( `Không thể chặn ${name} ` ):toast.warning( `Không bỏ thể chặn  ${name}`)}

    }
    setIssLoading(false)

  }
  return (
    <>
    {/* <MenuItem disableRipple onClick={handleClick}>Xem chi tiết</MenuItem> */}
      <MenuItem disableRipple>
        <FullAddSubjectDialog
          name={name}
          nameButton="Cập nhật"
          handleCloseMenu={handleClose}
          subjectID={subjectID}
          type="UPDATE"
          addOrUpdate="UPDATE"
        />
      </MenuItem>
      <MenuItem disableRipple onClick={()=>handleClick2()}>{value?.active ? "Chặn" :"Bỏ chặn"} {isLoading&&<BeatLoader
            color="#197854"
            size={5}
            style={{ marginLeft: "5px", marginTop: "5px" }}
          />}</MenuItem>
    </>
  );
};
export const MenuItemAdminSubjectBookSeries = ({onDetailView, name, handleClose,bookSeriesID,subjectID,value}) => {
  const dispatch = useDispatch();
  const handleClick = () =>{
    onDetailView()
    dispatch(setViewSubjectBookSeries({id:subjectID,name:name}))
  }
//  console.log("value",value)
  const [isLoading,setIssLoading] = useState(false)
  const handleClick2 = async() =>{
    setIssLoading(true)
    const res= await changeStatusBookSeriesSubject(subjectID)
    if(res.status === 200){
      dispatch(setIsReload((pre) => !pre));
      {value?.active ? toast.success( `Đã chặn ${name} ` ):toast.success(`Đã bỏ chặn ${name}`)}
    }else{
      {value?.active ? toast.warning( `Không thể chặn ${name} ` ):toast.warning( `Không bỏ thể chặn  ${name}`)}

    }
    setIssLoading(false)

  }
  return (
    <>
    <MenuItem disableRipple onClick={handleClick}>Xem chi tiết</MenuItem>
    <MenuItem disableRipple onClick={()=>handleClick2()}>{value?.active ? "Chặn" :"Bỏ chặn"} {isLoading&&<BeatLoader
            color="#197854"
            size={5}
            style={{ marginLeft: "5px", marginTop: "5px" }}
          />}</MenuItem>
    
    </>
  );
};
export const MenuItemAdminBookVolume = ({onDetailView, name, handleClose,bookVolumeID,value}) => {
  const dispatch = useDispatch();
  const handleClick = () =>{
    onDetailView()
    dispatch(setViewBookVolume({id:bookVolumeID,name:name}))
  }
  const [isLoading,setIssLoading] = useState(false)
  const handleClick2 = async() =>{
    setIssLoading(true)
    const res= await changeStatusBookVolume(bookVolumeID)
    if(res.status === 200){
      dispatch(setIsReload((pre) => !pre));
      {value?.active ? toast.success( `Đã chặn ${name} ` ):toast.success(`Đã bỏ chặn ${name}`)}
    }else{
      {value?.active ? toast.warning( `Không thể chặn ${name} ` ):toast.warning( `Không bỏ thể chặn  ${name}`)}

    }
    setIssLoading(false)

  }
  return (
    <>
    <MenuItem disableRipple onClick={handleClick}>Xem chi tiết</MenuItem>
      <MenuItem disableRipple>
        <FullAddBookVolumeDialog
          name={name}
          nameButton="Cập nhật"
          handleCloseMenu={handleClose}
          bookVolumeID={bookVolumeID}

          type="UPDATE"
          addOrUpdate="UPDATE"
        />
      </MenuItem>
      <MenuItem disableRipple onClick={()=>handleClick2()}>{value?.active ? "Chặn" :"Bỏ chặn"} {isLoading&&<BeatLoader
            color="#197854"
            size={5}
            style={{ marginLeft: "5px", marginTop: "5px" }}
          />}</MenuItem>
    </>
  );
};

export const MenuItemAdminChapter = ({onDetailView, name, handleClose,chapterID,value}) => {
  const dispatch = useDispatch();
  const handleClick = () =>{
    onDetailView()
    dispatch(setViewChapter({id:chapterID,name:name}))
  }
  const [isLoading,setIssLoading] = useState(false)
  const handleClick2 = async() =>{
    setIssLoading(true)
    const res= await changeStatusChapter(chapterID)
    if(res.status === 200){
      dispatch(setIsReload((pre) => !pre));
      {value?.active ? toast.success( `Đã chặn ${name} ` ):toast.success(`Đã bỏ chặn ${name}`)}
    }else{
      {value?.active ? toast.warning( `Không thể chặn ${name} ` ):toast.warning( `Không bỏ thể chặn  ${name}`)}

    }
    setIssLoading(false)

  }
  return (
    <>
    <MenuItem disableRipple onClick={handleClick}>Xem chi tiết</MenuItem>
      <MenuItem disableRipple>
        <FullAddChapterDialog
          name={name}
          nameButton="Cập nhật"
          handleCloseMenu={handleClose}
          chapterID={chapterID}
          type="UPDATE"
          addOrUpdate="UPDATE"
        />
      </MenuItem>
      <MenuItem disableRipple onClick={()=>handleClick2()}>{value?.active ? "Chặn" :"Bỏ chặn"} {isLoading&&<BeatLoader
            color="#197854"
            size={5}
            style={{ marginLeft: "5px", marginTop: "5px" }}
          />}</MenuItem>
    </>
  );
};
export const MenuItemAdminLesson = ({onDetailView, name, handleClose,lessonID,value}) => {
  const dispatch = useDispatch();
  const handleClick = () =>{
    onDetailView()
    dispatch(setViewLesson({id:lessonID,name:name}))
  }
  const [isLoading,setIssLoading] = useState(false)
  const handleClick2 = async() =>{
    setIssLoading(true)
    const res= await changeStatusLesson(lessonID)
    if(res.status === 200){
      dispatch(setIsReload((pre) => !pre));
      {value?.active ? toast.success( `Đã chặn ${name} ` ):toast.success(`Đã bỏ chặn ${name}`)}
    }else{
      {value?.active ? toast.warning( `Không thể chặn ${name} ` ):toast.warning( `Không bỏ thể chặn  ${name}`)}

    }
    setIssLoading(false)

  }
  return (
    <>
    {/* <MenuItem disableRipple onClick={handleClick}>Xem chi tiết</MenuItem> */}
      <MenuItem disableRipple>
        <FullAddLessonDialog
          name={name}
          nameButton="Cập nhật"
          handleCloseMenu={handleClose}
          lessonID={lessonID}
          type="UPDATE"
          addOrUpdate="UPDATE"
        />
      </MenuItem>
      <MenuItem disableRipple onClick={()=>handleClick2()}>{value?.active ? "Chặn" :"Bỏ chặn"} {isLoading&&<BeatLoader
            color="#197854"
            size={5}
            style={{ marginLeft: "5px", marginTop: "5px" }}
          />}</MenuItem>
    </>
  );
};
export const MenuItemAdminRole = ({id,name,handleClose,value}) => {
  const [isLoading,setIssLoading] = useState(false)
  const dispatch = useDispatch();
  const handleClick = async() =>{
    setIssLoading(true)
    const res= await changeStatusRole(id,!value?.active)
  
    if(res.status === 200){
        dispatch(setIsReload((pre) => !pre));
        {value?.active ? toast.success( `Đã chặn ${name} ` ):toast.success(`Đã bỏ chặn ${name}`)}
    }
    setIssLoading(false)

  }
  return (
    <>
    <MenuItem disableRipple >
    <FullSetPermissionDialog
          value={value}
          id={id}
          name={name}
          handleCloseMenu={handleClose}
        />
    </MenuItem>

    <MenuItem disableRipple onClick={()=>handleClick()}>{value?.active ? "Chặn" :"Bỏ chặn"} {isLoading&&<BeatLoader
            color="#197854"
            size={5}
            style={{ marginLeft: "5px", marginTop: "5px" }}
          />}</MenuItem>
      <MenuItem disableRipple>
        
      </MenuItem>
      
    </>
  );
};

export const MenuItemAdminViewComment = ({value,handleClose}) => {
  return (
    <>
    <MenuItem disableRipple >
      <FullViewCommentDialog value={value} handleCloseMenu={handleClose}/>
    </MenuItem>

    
      
    </>
  );
};
export const MenuItemModeratorViewMaterialReported = ({value,handleClose}) => {
  return (
    <>
    <MenuItem disableRipple >
      <FullViewMaterialReportedDialog value={value} handleCloseMenu={handleClose}/>
    </MenuItem>

    {/* <MenuItem disableRipple >
      Chặn</MenuItem> */}
      
      
    </>
  );
};
export const MenuItemAdminSetPermission = () => {
  return (
    <>
    <MenuItem disableRipple >
      <FullSetPermissionDialog/>
    </MenuItem>

    <MenuItem disableRipple ><Box sx={{width:'110px',height:'auto',display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'}}>Chặn</Box></MenuItem>
      
      
    </>
  );
};


export const MenuItemAdminUpdatePermission = ({value}) => {
  const [isLoading,setIssLoading] = useState(false)
  const dispatch = useDispatch();
  const handleClick2 = async() =>{
    setIssLoading(true)
    const res= await changeStatusPermission(value?.systemPermissionId,!value?.active)
    if(res.status === 200){
      dispatch(setIsReload((pre) => !pre));
      {value?.active ? toast.success( `Đã chặn ${value.permissionName} ` ):toast.success(`Đã bỏ chặn ${value.permissionName}`)}
    }else{
      {value?.active ? toast.warning( `Không thể chặn ${value.permissionName} ` ):toast.warning( `Không bỏ thể chặn  ${value.permissionName}`)}

    }
    setIssLoading(false)

  }
  return (
    <>
    <MenuItem disableRipple >
      <FullUpdatePermissionDialog value={value}/>
    </MenuItem>

    <MenuItem disableRipple onClick={()=>handleClick2()}>{value?.active ? "Chặn" :"Bỏ chặn"} {isLoading&&<BeatLoader
            color="#197854"
            size={5}
            style={{ marginLeft: "5px", marginTop: "5px" }}
          />}</MenuItem>
      
      
    </>
  );
};
export const MenuItemAdminUser = ({handleClose,id,value}) => {
  const handleClick = () =>{
    router.push("../../profile-user?uid="+id)
    handleClose()
  }
  const handleResetPassword= async()=>{
    const res = await resetPassword(id)
    if(res.status === 200){
      toast.success("Đã reset password")
    }else{
      toast.error("Reset password thất bại")
    }
  }
  const dispatch = useDispatch();
  const [isLoading,setIssLoading] = useState(false)
  const handleClick2 = async() =>{
    setIssLoading(true)
    const res= await changeStatusUser(id)
    if(res.status === 200){
      dispatch(setIsReload((pre) => !pre));
      {value?.active ? toast.success( `Đã chặn ${value.username} ` ):toast.success(`Đã bỏ chặn ${value.username}`)}
    }else{
      {value?.active ? toast.warning( `Không thể chặn ` ):toast.warning( `Không bỏ thể chặn  `)}

    }
    setIssLoading(false)

  }
  const router = useRouter()
  return (
    <>
    <MenuItem disableRipple onClick={() => handleClick()}>
        Cập nhật
    </MenuItem>
    <MenuItem disableRipple onClick={() => handleResetPassword()}>
        Reset mật khẩu
    </MenuItem>
    <MenuItem disableRipple onClick={()=>handleClick2()}>{value?.active ? "Chặn" :"Bỏ chặn"} {isLoading&&<BeatLoader
            color="#197854"
            size={5}
            style={{ marginLeft: "5px", marginTop: "5px" }}
          />}</MenuItem>
      
      
    </>
  );
};

export const MenuItemEditMaterial = ({ value,name, handleClose,id }) => {
  return (
    <>
      <MenuItem disableRipple>
        <FullEditMaterialDialog
          value={value}
          id={id}
          name={name}
          nameButton="Chỉnh sửa"
          handleCloseMenu={handleClose}
        />
      </MenuItem>
    </>
  );
};

export const MenuItemDeleteMaterial = ({value, name, handleClose }) => {
  return (
    <>
      <MenuItem disableRipple>
        <FullDeleteMaterialDialog
          value={value}
          handleCloseMenu={handleClose}
          nameButton="Xóa"
          name={name}
        />
      </MenuItem>
    </>
  );
};
const CustomMenuItem = ({ children, handleClose, text }) => {
  return (
    <MenuItem disableRipple>
      {children}
      <Box sx={{ marginLeft: "10px" }}>{text}</Box>
    </MenuItem>
  );
};
export const MenuItemEditShareDeleteMaterial = ({value, name, handleClose,id }) => {
  return (
    <>
      <MenuItem disableRipple>
        <FullEditMaterialDialog
          value={value}
          id={id}
          name={name}
          nameButton="Chỉnh sửa"
          handleCloseMenu={handleClose}
        />
      </MenuItem>

      <MenuItem disableRipple>
        <FullShareMaterialDialog
          id={id}
          name={name}
          nameButton="Chia sẻ"
          handleCloseMenu={handleClose}
        />
      </MenuItem>

      <MenuItem disableRipple>
        <FullDeleteMaterialDialog
         value={value}
         handleCloseMenu={handleClose}
          nameButton="Xóa"
          name={name}
        />
      </MenuItem>
    </>
  );
};
export const MenuItemShareMaterial = ({value, name, handleClose,id }) => {
  return (
    <>
      

      <MenuItem disableRipple>
        <FullShareMaterialDialog
          id={id}
          name={name}
          nameButton="Chia sẻ"
          handleCloseMenu={handleClose}
        />
      </MenuItem>

     
    </>
  );
};
export const MenuItemShareDeleteMaterial = ({value, id,name, handleClose }) => {
  return (
    <>
      <MenuItem disableRipple>
        <FullShareMaterialDialog
          value={value}
          id={id}
          name={name}
          nameButton="Chia sẻ"
          handleCloseMenu={handleClose}
        />
      </MenuItem>

      <MenuItem disableRipple>
        <FullDeleteMaterialDialog
         value={value}
         handleCloseMenu={handleClose}
          nameButton="Xóa"
          name={name}
        />
      </MenuItem>
    </>
  );
};
export const MenuItemUpdateDeleteMaterial = ({value, name, handleClose,id }) => {
  return (
    <>
      <MenuItem disableRipple>
        <FullEditMaterialDialog
          value={value}
          id={id}
          name={name}
          nameButton="Chỉnh sửa"
          handleCloseMenu={handleClose}
        />
      </MenuItem>

      <MenuItem disableRipple>
        <FullDeleteMaterialDialog
          value={value}
          handleCloseMenu={handleClose}
          nameButton="Xóa"
          name={name}
        />
      </MenuItem>
    </>
  );
};
export const MenuItemEditDeleteMaterial = ({ name, handleClose,id }) => {
  return (
    <>
      <MenuItem disableRipple>
        <FullEditMaterialDialog
        value={value}
        id={id}
          name={name}
          nameButton="Chỉnh sửa"
          handleCloseMenu={handleClose}
        />
      </MenuItem>

      <MenuItem disableRipple>
        <FullDeleteMaterialDialog
          value={value}
          handleCloseMenu={handleClose}
          nameButton="Xóa"
          name={name}
          id={id}
        />
      </MenuItem>
    </>
  );
};

export const MenuItemViewClass = ({ id,name, handleClose }) => {
  const handleView = () =>{
    handleClose();
    
  }
  return (
    <>
      <MenuItem disableRipple onClick={handleView}>
        <IconAndText text="Xem" >
          <GrView />
        </IconAndText>
      </MenuItem>
      <MenuItem disableRipple >
        <FullAddTagDialog
        name={name}
        handleCloseMenu={handleClose}
        />
      </MenuItem>
    </>
  );
};

export const MenuItemModerateFolderTag = ({viewType,onDetailView, handleClose,folderID,value ,type}) => {
  const dispatch = useDispatch();
  const handleClick = () =>{
    onDetailView()
    if(viewType === "CLASS"){
      dispatch(setViewClass({id:value.id,name:value.name}))
    }else if(viewType === "BOOK-SERIES"){
      dispatch(setViewBookSeries({id:value.id,name:value.name}))
    }else if(viewType === "SUBJECT-BOOK-SERIES"){
      dispatch(setViewSubjectBookSeries({id:value.id,name:value.name}))
    }else if(viewType === "BOOK-VOLUME"){
      dispatch(setViewBookVolume({id:value.id,name:value.name}))
    }else if(viewType === "CHAPTER"){
      dispatch(setViewChapter({id:value.id,name:value.name}))
    }else if(viewType === "LESSON"){
      dispatch(setViewLesson({id:value.id,name:value.name}))
    }
  }  
  return (
    <>
    {viewType !== "LESSON" &&<MenuItem disableRipple onClick={handleClick}>Xem chi tiết</MenuItem>}
      <MenuItem disableRipple>
        <FullAddFolderTagDialog
       
          nameButton="Thêm tag"
          handleCloseMenu={handleClose}
          folderID={folderID}
          type="UPDATE"
          addOrUpdate="UPDATE"
          viewType={viewType}
          value={value}
        />
      </MenuItem>
      
    </>
  );
};

export const MenuItemUnSave = ({value, handleClose }) => {
  return (
    <>
      

      <MenuItem disableRipple>
        <FullUnSaveMaterialDialog
          value={value}      
          handleCloseMenu={handleClose}
        />
      </MenuItem>

     
    </>
  );
};

export const MenuItemUnShare = ({value, handleClose }) => {
  return (
    <>
      

      <MenuItem disableRipple>
        <FullUnShareMaterialDialog
          value={value}      
          handleCloseMenu={handleClose}
        />
      </MenuItem>

     
    </>
  );
};