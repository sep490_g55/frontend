import React, { Component } from 'react'; 
import "react-responsive-carousel/lib/styles/carousel.min.css"; 
import { Carousel } from 'react-responsive-carousel'; 

export default class NextJsCarousel extends Component { 
	render() { 
		return ( 
			<div> 
			<h2>NextJs Carousel - GeeksforGeeks</h2> 
			<Carousel> 
				<div> 
					<img src="https://res.klook.com/image/upload/Mobile/City/swox6wjsl5ndvkv5jvum.jpg" alt="image1"/> 
					<p className="legend">Image 1</p> 

				</div> 
				<div> 
					<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjcI6DuppUCb6vqQQMgViacHb8vvjZaxybSpy5Oy3R8blw1TyW1lRpCYTdmL9uAOvzHgs&usqp=CAU" alt="image2" /> 
					<p className="legend">Image 2</p> 

				</div> 
				<div> 
					<img src="https://a.travel-assets.com/findyours-php/viewfinder/images/res70/506000/506662-eiffel-tower.jpg" alt="image3"/> 
					<p className="legend">Image 3</p> 

				</div> 
				<div> 
					<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSpcIG9r6D1i4pn-hJuLvesGrI4L3wXsic0wC-AHFzmPR_YPytm9KFL1oZVU8K2_2B-k58&usqp=CAU" alt="image4"/> 
					<p className="legend">Image 4</p> 

				</div> 
				<div> 
					<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSZxZv4XDWoCZTqw8HUiEB5r3T5ylIuOSqtnKqGx6AmjzEQtvzTPaIPdKsrRr6FWfdOuQI&usqp=CAU" alt="image5"/> 
					<p className="legend">Image 5</p> 

				</div> 
			</Carousel> 
			</div> 
		); 
	} 
};
