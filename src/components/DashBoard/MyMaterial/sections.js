import React, { useEffect } from "react";

//mui
import { Box } from "@mui/material";
import TextField from "@mui/material/TextField";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import {
  ManageMaterialColumns,
  SaveMaterialsColumns,
  ShareMaterialsColumns,
} from "@/components/Table/sections";

import {
  ContainerDiv,
  LeftDiv,
  RightDiv,
} from "@/components/DetailMaterial/styles";

//icon
import { MdCloudUpload } from "react-icons/md";
import { BsBookmarksFill } from "react-icons/bs";
import { FaShareSquare } from "react-icons/fa";
import { AiFillHome } from "react-icons/ai";
import { VscFileMedia } from "react-icons/vsc";

//
import { useState } from "react";
import { LeftInfoDiv } from "@/components/InfoMaterial/section";
import {
  AvatarAndInfoDiv, CenterFlexBox, Style2LeftInfoDiv
} from "@/components/common_sections";
import {
  BrGreenButtonAction, GreenButtonAction, HoverBrGreenButtonAction
} from "@/components/FolderImage/styles";

//import of tabs component
import ListSubheader from "@mui/material/ListSubheader";
import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Collapse from "@mui/material/Collapse";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import { TableMaterial } from "@/components/Table/sections";
import { useRouter } from "next/router";
import { StyleLeftInfoDiv } from "@/components/InfoMaterial/styles";
import { useSelector } from "react-redux";
import TabMaterialList from "../TabManageMaterials/MaterialsList";
import { useAuthContext } from "@/auth/useAuthContext";
import { getLocalStorage, getUserPreview } from "@/dataProvider/agent";
import { AdminHomeDiv } from "../Admin/style";
import { NameSystem, TeacherSystem } from "@/theme/system";
export const MaterialsContainer = ({ tabValue, handleChangeView,selectedTab, setSelectedTab,subSelectedTab, setSubSelectedTab,open, setOpen }) => {
  const route = useRouter();
  const handleToUploadPage = () => {
    route.push({
      pathname: "/materials/upload/stepper",
    });
  };

  ///handle expand tab container
  const [isExpand, setIsExpand] = useState(true);
  const onExpand = () => {
    isExpand ? setIsExpand(false) : setIsExpand(true);
  };
  // end handle expand tab container


  return (
    <ContainerDiv>
      <LeftDiv style={{ paddingLeft: "40px", paddingRight:"30px",overflowX:"hidden" }}>
       <Style2LeftInfoDiv> <LeftInfoDiv >
          <MyMaterialLeftSection tabValue={tabValue} handleChangeView ={handleChangeView} selectedTab={selectedTab} setSelectedTab={setSelectedTab} subSelectedTab={subSelectedTab}setSubSelectedTab={setSubSelectedTab}open={open}setOpen={setOpen}/>
        </LeftInfoDiv>
        </Style2LeftInfoDiv>
      </LeftDiv>
      <RightDiv>
        {tabValue === 1? <TeacherHome/>: tabValue === 21? <TabMaterialList amMine={true} uri="/user-resource/display-my-resource?"/>: tabValue === 22? <TabMaterialList amMine={true} useFor="MY-REPORT-RESOURCE" uri="/report-resource/display?"/> : tabValue === 3? <TabMaterialList useFor="MY-SAVED"  uri="/user-resource/display-saved?"/>: tabValue === 4? <TabMaterialList useFor="SHARE-FOR-ME" uri="/user-resource/display-shared?"/>:""}
      </RightDiv>
    </ContainerDiv>
  );
};

const TeacherHome= ()=>{
  const { user, isAuthenticated, logout,ROLES } = useAuthContext();
    const fullName = user?.firstname + " " + user?.lastname;
  return(
  <AdminHomeDiv
   
  >
    
    <AdminHomeDiv
   
   >
     <CenterFlexBox><TeacherSystem/>
     <CenterFlexBox style={{width:"fit-content",}}> Chào mừng <Box sx={{fontWeight:"500",padding:"10px"}}>{fullName}</Box>đã quay trở lại</CenterFlexBox></CenterFlexBox>
     <CenterFlexBox>Trang quản lý học liệu  của tôi</CenterFlexBox>
   </AdminHomeDiv>
  </AdminHomeDiv>
    // <Box sx={{width:"300px",backgroundImage: "url('https://microbiomology.org/wp-content/uploads/2019/12/Project-Management-freewallpaperimage.xyz-Copy-Copy-1.jpg')"}}></Box>
  )
}

const NestedList = ({ setTabValue,selectedTab, setSelectedTab,subSelectedTab, setSubSelectedTab,open, setOpen }) => {
  

  const handleClick = () => {
    setOpen(!open);
  };

  
  const handleSelectedTab = (newValue) => {  //action for super tab
    setSubSelectedTab(20) 
    setOpen(false);
    setSelectedTab(newValue);
    setTabValue(newValue);
    if (newValue === 2) {
      handleClick();
      setSubSelectedTab(21);
      setTabValue(21) 

    }
  };

  const handleSelectedSubTab = (newValue) => { //action for sub tab
    
    setSubSelectedTab(newValue);
    setTabValue(newValue);
   
  };

  return (
    <>
      <List
        sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            CHUNG
          </ListSubheader>
        }
      >
        <ItemTabList
          selectedTab={selectedTab}
          index={1}
          handleSelectedTab={handleSelectedTab}
          icon={<AiFillHome />}
          text="Trang chủ"
          isCollapse={false}
        />
      </List>

      <List
        sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
        component="nav"
        aria-labelledby="nested-list-subheader2"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader2">
            DANH MỤC QUẢN LÝ
          </ListSubheader>
        }
      >
        <ItemTabList
          selectedTab={selectedTab}
          index={2}
          handleSelectedTab={handleSelectedTab}
          icon={<VscFileMedia />}
          text="Học liệu của tôi"
          isCollapse={true}
          open={open}
        />
        <CollapseTabItem index={21} open={open} text="Danh sách học liệu" subSelectedTab={subSelectedTab} handleSelectedTab={()=>handleSelectedSubTab(21)} />
        <CollapseTabItem index={22} open={open} text="Học liệu bị báo cáo" subSelectedTab={subSelectedTab} handleSelectedTab={()=>handleSelectedSubTab(22)}/>
        <ItemTabList
          selectedTab={selectedTab}
          index={3}
          handleSelectedTab={handleSelectedTab}
          icon={<BsBookmarksFill />}
          text="Học liệu đã lưu"
          isCollapse={false}
        />
        <ItemTabList
          selectedTab={selectedTab}
          index={4}
          handleSelectedTab={handleSelectedTab}
          icon={<FaShareSquare />}
          text="Được chia sẻ với tôi"
          isCollapse={false}
        />

      </List>
    </>
  );
};

export const ItemTabList = ({
  selectedTab,
  index,
  handleSelectedTab,
  icon,
  text,
  isCollapse,
  open,
}) => {
  return (
    <>
      <ListItemButton
        sx={{
          borderRadius: "15px",
          backgroundColor: selectedTab === index ? "#198754" : "inherit",
          color: selectedTab === index ? "white" : "",
          "&:hover": {
            backgroundColor: selectedTab === index ? "#198754" : "#EEEEEF",
          },
        }}
        onClick={() => handleSelectedTab(index)}
      >
        <ListItemIcon
          sx={{
            color: selectedTab === index ? "white" : "",
          }}
        >
          {icon}
        </ListItemIcon>
        <ListItemText primary={text} />
        {isCollapse ? open ? <ExpandLess /> : <ExpandMore /> : ""}
      </ListItemButton>
    </>
  );
};
export const CollapseTabItem = ({index, open, icon, text,subSelectedTab,handleSelectedTab }) => {
  return (
    <>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItemButton sx={{ pl: 4,borderRadius: "15px", color: subSelectedTab === index ? "#198754" : "", }}
          onClick={() => handleSelectedTab(index)}
          >
            <ListItemIcon>
              {/* <StarBorder /> */}
              {icon}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItemButton>
        </List>
      </Collapse>
    </>
  );
};

export const MyMaterialLeftSection = ({ tabValue, handleChangeView,selectedTab, setSelectedTab,subSelectedTab, setSubSelectedTab,open, setOpen }) => {
  const {user,isAuthenticated, logout } = useAuthContext();
  const fullName = user?.firstname+" "+user?.lastname;
  const avatar = user?.avatar
  const route = useRouter();
  const [preview, setPreview] = useState(null)
  const handleToUploadPage = () => {
    route.push({
      pathname: "/materials/upload/stepper",
    });
  };
  const reloading = useSelector((state) => state.reloading.isReLoad);

  useEffect(() => {
    const previewFunction = async() =>{
    const res = await getUserPreview()
    setPreview(res.data)
    }
    previewFunction()
  }, [reloading]);
  
  return (
    <>
      <AvatarAndInfoDiv
            avatarsrc={avatar}
            title1={fullName}
            title2={user?.school}
          />
          <Box
            sx={{
              width: "100%",
              hight: "50px",
              display: "flex",
              justifyContent: "space-evenly",
              margin: "20px 0",
            }}
          >
            <NumAndTitleDiv title="Lưu" number={preview?.savedCount} />
            <NumAndTitleDiv title="Tải lên" number={preview?.uploadCount} />
            <NumAndTitleDiv title="Thích" number={preview?.likeCount} />
          </Box>

          <Box
            sx={{
              display: "flex",
              textAlign: "center",
              justifyContent: "center",
            }}
          >
            <HoverBrGreenButtonAction
              // style={{ width: "100%", height: "48px", borderRadius: "15px",backgroundColor:"white",border:"1px solid #198754",color:"#198754",fontWeight:"bold" }}
              onClick={handleToUploadPage}
            >
              <MdCloudUpload /> Tải lên
            </HoverBrGreenButtonAction>
          </Box>

          <NestedList setTabValue={handleChangeView} selectedTab={selectedTab} setSelectedTab={setSelectedTab} subSelectedTab={subSelectedTab}setSubSelectedTab={setSubSelectedTab}open={open}setOpen={setOpen}/> 
    </>
  );
};
export const NumAndTitleDiv = ({ number, title }) => {
  return (
    <>
      <Box sx={{ textAlign: "center" }}>
        <Box>{number}</Box>
        <Box>{title}</Box>
      </Box>
    </>
  );
};

export const IconAndTitleDiv = ({ icon, title, onClick }) => {
  return (
    <>
      <Box
        sx={{
          width: "100%",
          display: "flex",
          margin: "20px 0 20px 30px",
          justifyContent: "flex-start",
          alignItems: "baseline",
        }}
        onClick={onClick}
      >
        <Box sx={{ fontSize: "20px", marginRight: "15px" }}>{icon}</Box>
        <Box>{title}</Box>
      </Box>
    </>
  );
};



// end component for tabs

//Filter for list marterials
export const FilterMaterial = ({ type }) => {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "flex-end",
        alignItems: { xs: "center", md: "none" },
        flexDirection: { xs: "column", md: "row" },
      }}
    >
      <Box>
        <FormControl sx={{ m: 1, minWidth: 120 }}>
          <InputLabel id="material-type">Loại tài liệu</InputLabel>
          <Select
            labelId="material-type"
            id="select-material-type"
            //   value={age}
            label="Loại tài liệu"
            //   onChange={handleChange}
          >
            <MenuItem value={10}>.pdf</MenuItem>
            <MenuItem value={20}>.pptx</MenuItem>
            <MenuItem value={30}>.docs</MenuItem>
            <MenuItem value={30}>media</MenuItem>
            <MenuItem value={30}>khác</MenuItem>
          </Select>
        </FormControl>
        {type === "mymaterials" ? (
          <>
            <FormControl sx={{ m: 1, minWidth: 120 }}>
              <InputLabel id="mode">Chế độ</InputLabel>
              <Select
                labelId="mode"
                id="select-mode"
                //   value={age}
                label="Chế độ"
                //   onChange={handleChange}
              >
                <MenuItem value={10}>công khai</MenuItem>
                <MenuItem value={20}>không công khai</MenuItem>
                <MenuItem value={30}>hạn chế</MenuItem>
              </Select>
            </FormControl>
            <FormControl sx={{ m: 1, minWidth: 120 }}>
              <InputLabel id="status">Trạng thái</InputLabel>
              <Select
                labelId="status"
                id="select-status"
                //   value={age}
                label="Trạng thái"
                //   onChange={handleChange}
              >
                <MenuItem value={10}>đã kiểm duyệt</MenuItem>
                <MenuItem value={20}>chưa kiểm duyệt</MenuItem>
                <MenuItem value={30}>bị từ chối</MenuItem>
              </Select>
            </FormControl>{" "}
          </>
        ) : (
          ""
        )}
      </Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginLeft: { xs: "0", md: "40px" },
        }}
      >
        <Box
          component="form"
          sx={{
            "& > :not(style)": { m: 1, width: "25ch" },
          }}
          noValidate
          autoComplete="off"
        >
          <TextField
            id="outlined-basic"
            label="Tên của file"
            variant="outlined"
          />
          <FormHelperText></FormHelperText>
        </Box>
        <GreenButtonAction> Tìm kiếm</GreenButtonAction>
      </Box>
    </Box>
  );
};
// end Filter for list marterials

//my list marterials
export const MyMaterials = () => {
  const ManageMaterialRows = [
    {
      id: 1,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 1,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 2,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 5,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 3,
      name: "Tai-lieu-toan-dai-so-lop2.docs",
      views: 822,
      likes: 2,
      mode: "công khai",
      status: "Chưa kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 4,
      name: "Tai-lieu-toan-dai-so-lop3.docs",
      views: 102,
      likes: 2,
      mode: "công khai",
      status: "Bị báo cáo",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 5,
      name: "Tai-lieu-toan-dai-so-lop4.docs",
      views: 152,
      likes: 2,
      mode: "công khai",
      status: "Bị từ chối",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 6,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 112,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 7,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 1,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 8,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 5,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 9,
      name: "Tai-lieu-toan-dai-so-lop2.docs",
      views: 822,
      likes: 2,
      mode: "công khai",
      status: "Chưa kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 10,
      name: "Tai-lieu-toan-dai-so-lop3.docs",
      views: 102,
      likes: 2,
      mode: "công khai",
      status: "Bị báo cáo",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
  ];
  return (
    <>
      <FilterMaterial type="mymaterials" />
      <TableMaterial
        rows={ManageMaterialRows}
        columns={ManageMaterialColumns}
        totalPage="11"
      />
    </>
  );
};
// end my list marterials

export const SaveMaterials = () => {
  const SaveMaterialRows = [
    {
      id: 1,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 1,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 2,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 5,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 3,
      name: "Tai-lieu-toan-dai-so-lop2.docs",
      views: 822,
      likes: 2,
      mode: "công khai",
      status: "Chưa kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 4,
      name: "Tai-lieu-toan-dai-so-lop3.docs",
      views: 102,
      likes: 2,
      mode: "công khai",
      status: "Bị báo cáo",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 5,
      name: "Tai-lieu-toan-dai-so-lop4.docs",
      views: 152,
      likes: 2,
      mode: "công khai",
      status: "Bị từ chối",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 6,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 112,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 7,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 1,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 8,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 5,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 9,
      name: "Tai-lieu-toan-dai-so-lop2.docs",
      views: 822,
      likes: 2,
      mode: "công khai",
      status: "Chưa kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 10,
      name: "Tai-lieu-toan-dai-so-lop3.docs",
      views: 102,
      likes: 2,
      mode: "công khai",
      status: "Bị báo cáo",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
  ];
  return (
    <>
      <FilterMaterial />
      <TableMaterial
        rows={SaveMaterialRows}
        columns={SaveMaterialsColumns}
        totalPage="11"
      />
    </>
  );
};

export const ShareMaterials = () => {
  const ShareMaterialRows = [
    {
      id: 1,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 1,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 2,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 5,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 3,
      name: "Tai-lieu-toan-dai-so-lop2.docs",
      views: 822,
      likes: 2,
      mode: "công khai",
      status: "Chưa kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 4,
      name: "Tai-lieu-toan-dai-so-lop3.docs",
      views: 102,
      likes: 2,
      mode: "công khai",
      status: "Bị báo cáo",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 5,
      name: "Tai-lieu-toan-dai-so-lop4.docs",
      views: 152,
      likes: 2,
      mode: "công khai",
      status: "Bị từ chối",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 6,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 112,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 7,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 1,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 8,
      name: "Tai-lieu-toan-dai-so-lop1.docs",
      views: 5,
      likes: 2,
      mode: "công khai",
      status: "Đã kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 9,
      name: "Tai-lieu-toan-dai-so-lop2.docs",
      views: 822,
      likes: 2,
      mode: "công khai",
      status: "Chưa kiểm duyệt",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
    {
      id: 10,
      name: "Tai-lieu-toan-dai-so-lop3.docs",
      views: 102,
      likes: 2,
      mode: "công khai",
      status: "Bị báo cáo",
      createOn: "2023-10-20 15:18:50",
      owner: "Viet Hoang",
      isSave: "true",
    },
  ];
  return (
    <>
      <FilterMaterial />
      <TableMaterial
        rows={ShareMaterialRows}
        columns={ShareMaterialsColumns}
        totalPage="10"
      />
    </>
  );
};
