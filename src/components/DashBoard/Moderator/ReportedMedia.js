import { useSelector } from "react-redux";
import TabMaterialList from "@/components/DashBoard/TabManageMaterials/MaterialsList";


export default function ReportedMedia({uri,useFor,isShowFilter}) {
//   const objClass = useSelector((state) => state.viewFolder.class);
  return(
    // <TabFolderMaterial objClass={objClass} nextTabIndex={nextTabIndex} setFolderID={setFolderID} api={api} handleView={handleView}  type="BOOK-SERIES"></TabFolderMaterial>
    <TabMaterialList  uri={uri} useFor={useFor} isShowFilter={isShowFilter}></TabMaterialList>
  
    )
}
