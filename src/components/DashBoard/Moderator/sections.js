import React from "react";
import {Button, Dropdown, DropdownItem, DropdownMenu, DropdownTrigger, Input, Pagination} from "@nextui-org/react";
//mui
import { Box, Grid, Stack, TextField } from "@mui/material";
import { RingLoader } from "react-spinners";

import {
  ContainerDiv,
  LeftDiv,
  RightDiv,
} from "@/components/DetailMaterial/styles";
import { addNewTag, deleteTag, getFolderSRC, getLocalStorage } from "@/dataProvider/agent";

//icon
import { MdAddModerator, MdCloudUpload } from "react-icons/md";
import { BsBookmarksFill } from "react-icons/bs";
import { FaShareSquare,FaHashtag } from "react-icons/fa";
import { AiFillHome, AiOutlineComment } from "react-icons/ai";
import { VscFileMedia } from "react-icons/vsc";

//
import { useState } from "react";
import { LeftInfoDiv } from "@/components/InfoMaterial/section";
import {
  AvatarAndInfoDiv, CenterFlexBox, GreenButtonAction, Item, Style2LeftInfoDiv
} from "@/components/common_sections";
import {
  BrGreenButtonAction,HoverBrGreenButtonAction
} from "@/components/FolderImage/styles";

//import of tabs component
import ListSubheader from "@mui/material/ListSubheader";
import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Collapse from "@mui/material/Collapse";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import { useRouter } from "next/router";
import { StyleLeftInfoDiv } from "@/components/InfoMaterial/styles";
import { useDispatch, useSelector } from "react-redux";
import TabMaterialList from "@/components/DashBoard/TabManageMaterials/MaterialsList";
import {MUIBookSeriesSelect, MUIBookVolumeSelect, MUIChapterSelect, MUIClassSelect, MUIFolderSelect, MUILessonSelect, MUISubjectSelect} from "@/components/Select/AddressSelect/NEXUI/folder";
import { useEffect } from "react";
import {Chip} from "@nextui-org/react";
import { Search } from "@mui/icons-material";
import TabLesson from "@/pages/dashboards/Admin/Material/Lesson";
import TabChapters from "@/pages/dashboards/Admin/Material/Chapter";
import TabBookVolume from "@/pages/dashboards/Admin/Material/BookVolume";
import TabBookSeries from "@/pages/dashboards/Admin/Material/BookSeries";
import TabClass from "@/pages/dashboards/Admin/Material/Class";
import TabSubjects from "@/pages/dashboards/Admin/Material/Subject";
import { ToastContainer, toast } from "react-toastify";

import TabComment from "../../../pages/materials/moderator/Comment/CommentManage";
import { AdminHomeDiv } from "../Admin/style";
import { useAuthContext } from "@/auth/useAuthContext";
import { AdminSystem, ModeratorSystem, NameSystem } from "@/theme/system";
import LoadingTab from "@/components/loading-screen/LoadingTab";
import { BeatLoader } from "react-spinners";
import TabSubjectBookSeries from "@/pages/dashboards/Admin/Material/SubjectBookSeries";
import ReportedMaterial from "./ReportedMaterial"
import ReportedComment from "./ReportedComment"
import ReportedMedia from "./ReportedMedia"
import { setIsReload } from "@/redux/slice/IsReLoad";
import { BiChevronDown } from "react-icons/bi";
import { capitalize } from "../TabManageMaterials/utils";

export const MaterialsContainer = ({ tabValue, handleChangeView,selectedTab, setSelectedTab,subSelectedTab, setSubSelectedTab,isSuperIndex, setIsSuperIndex,isSuperIndex2, setIsSuperIndex2 ,open, setOpen,subOpen, setSubOpen,subOpen2, setSubOpen2 }) => {
  const route = useRouter();
  const handleToUploadPage = () => {
    route.push({
      pathname: "/materials/upload/stepper",
    });
  };

  ///handle expand tab container
  const [isExpand, setIsExpand] = useState(true);
  const onExpand = () => {
    isExpand ? setIsExpand(false) : setIsExpand(true);
  };
  // end handle expand tab container
  const [classID, setClassID] = useState(0);
  const [bookSeriesID, setBookSeriesID] = useState(0);
  const [subjectID, setSubjectID] = useState(0);
  const [bookVolumeID, setBookVolumeID] = useState(0);
  const [chapterID, setChapterID] = useState(0);
  const [lessonID, setLessonID] = useState(0);

  
  return (
    <ContainerDiv>
      <LeftDiv style={{ paddingLeft: "40px", paddingRight:"30px",overflowX:"hidden" }}>
       <Style2LeftInfoDiv> <LeftInfoDiv >
       <Box
            sx={{
              display: "flex",
              textAlign: "center",
              justifyContent: "center",
            }}
          >
            <HoverBrGreenButtonAction
              // style={{ width: "100%", height: "48px", borderRadius: "15px",backgroundColor:"white",border:"1px solid #198754",color:"#198754",fontWeight:"bold" }}
              onClick={handleToUploadPage}
            >
              <MdCloudUpload /> Tải lên
            </HoverBrGreenButtonAction>
          </Box>
          <MyMaterialLeftSection tabValue={tabValue} handleChangeView ={handleChangeView} selectedTab={selectedTab} setSelectedTab={setSelectedTab} subSelectedTab={subSelectedTab} setSubSelectedTab={setSubSelectedTab} isSuperIndex={isSuperIndex} setIsSuperIndex={setIsSuperIndex} isSuperIndex2={isSuperIndex2} setIsSuperIndex2={setIsSuperIndex2}  open={open} setOpen={setOpen} subOpen={subOpen}setSubOpen={setSubOpen}subOpen2={subOpen2}setSubOpen2={setSubOpen2}/>
        </LeftInfoDiv>
        </Style2LeftInfoDiv>
      </LeftDiv>
      <RightDiv>
        {tabValue === 1 ? <ModeratorHome />  : tabValue === 2 ? <TabMaterialList amMine={true} useFor="MODERATOR" uri="/user-resource/display-my-resource?" />: tabValue === 311 ? <TabMaterialList useFor="MODERATOR" uri="review/medias?" />:tabValue === 312 ? <ReportedMaterial useFor="MODERATOR-REPORT" uri="/review/report-medias?"/>:tabValue === 321 ? <TabMaterialList useFor="MODERATOR" uri="/review/materials?"isShowFilter={true}/>: tabValue === 322 ? <ReportedMedia useFor="MODERATOR-REPORT-MATERIAL" uri="review/report-materials?" isShowFilter={true}/>:tabValue === 4 ? <ManageTags/>:tabValue === 5 ? <TabClass nextTabIndex={21} setFolderID={setClassID} api={`/class/display?`} handleView={handleChangeView}/>:tabValue == "21"? <TabBookSeries nextTabIndex={22} setFolderID={setBookSeriesID} api={`/book-series/display/${classID}?`} handleView={handleChangeView}/>:tabValue == "22"? <TabSubjectBookSeries type="SUBJECT-BOOK-SERIES" nextTabIndex={23} setFolderID={setSubjectID} api={`/book-series-subject/display/${bookSeriesID}?`} handleView={handleChangeView}/>:tabValue == "23"? <TabBookVolume nextTabIndex={24} setFolderID={setBookVolumeID} api={`/book-volume/display/${subjectID}?`} handleView={handleChangeView}/>:tabValue == "24"? <TabChapters nextTabIndex={25} setFolderID={setChapterID} api={`/chapter/display/${bookVolumeID}?`} handleView={handleChangeView}/>:tabValue == "25"? <TabLesson nextTabIndex={26} setFolderID={setLessonID} api={`/lesson/display/${chapterID}?`} handleView={handleChangeView}/>:<ReportedComment useFor="REPORT-COMMENT" uri=""/> }
      </RightDiv>
    </ContainerDiv>
  );
};
const ModeratorHome= ()=>{
  const { user, isAuthenticated, logout,ROLES } = useAuthContext();
    const fullName = user?.firstname + " " + user?.lastname;
  return(
  <AdminHomeDiv
   
  >
    <CenterFlexBox><ModeratorSystem/>
    <CenterFlexBox style={{width:"fit-content",}}>Chào mừng <Box sx={{fontWeight:"500",padding:"10px"}}>{fullName}</Box> đã quay trở lại</CenterFlexBox></CenterFlexBox>
    <CenterFlexBox>Trang kiểm duyệt của <NameSystem /></CenterFlexBox>
  </AdminHomeDiv>
    // <Box sx={{width:"300px",backgroundImage: "url('https://microbiomology.org/wp-content/uploads/2019/12/Project-Management-freewallpaperimage.xyz-Copy-Copy-1.jpg')"}}></Box>
  )
}
const NestedList = ({ setTabValue,selectedTab, setSelectedTab,subSelectedTab, setSubSelectedTab,isSuperIndex, setIsSuperIndex,isSuperIndex2, setIsSuperIndex2 ,open, setOpen,subOpen, setSubOpen,subOpen2, setSubOpen2}) => {
  

  const handleClick = () => {
    setOpen(!open);
  };

  

  const handleSelectedTab = (newValue) => {  //action for super tab
    setSubSelectedTab(20) 
    setOpen(false);
    setSubOpen(false);
    setSubOpen2(false);

    setSelectedTab(newValue);
    setTabValue(newValue);

    // default when user click vào ông
    if (newValue === 3) { 
      setTabValue(311)
      handleClick();
    }
  };

  const handleSelectedSubTab = (newValue) => { //action for sub tab


    setSubSelectedTab(newValue);
    setTabValue(newValue);

    // still keeping active of ông và bố 
    if(newValue === 311 || newValue === 312){
      setIsSuperIndex(true)
      setIsSuperIndex2(false)

    }else if(newValue === 321 || newValue === 322){
      setIsSuperIndex2(true)
      setIsSuperIndex(false)

    }
  };

  return (
    <>
      <List
        sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            CHUNG
          </ListSubheader>
        }
      >
        <ItemTabList
          selectedTab={selectedTab}
          index={1}
          handleSelectedTab={handleSelectedTab}
          icon={<AiFillHome />}
          text="Trang chủ"
          isCollapse={false}
        />
      </List>

      <List
        sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
        component="nav"
        aria-labelledby="nested-list-subheader2"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader2">
            DANH MỤC QUẢN LÝ
          </ListSubheader>
        }
      >
        <ItemTabList
          selectedTab={selectedTab}
          index={2}
          handleSelectedTab={handleSelectedTab}
          icon={<VscFileMedia />}
          text="Học liệu của tôi"
          isCollapse={false}
        />
        <ItemTabList
          selectedTab={selectedTab}
          index={3}
          handleSelectedTab={handleSelectedTab}
          icon={<MdAddModerator />}
          text="Học liệu kiểm duyệt"
          isCollapse={true}
          open={open}
        />
        {/* handleSelectedTab={()=>handleSelectedSubTab(311)}  set to that default show first their tab*/}
        <CollapseTabItem index={31} open={open} text="Kho media " subSelectedTab={subSelectedTab} handleSelectedTab={()=>handleSelectedSubTab(311)} isCollapse={true} subOpen ={subOpen} setSubOpen={setSubOpen} isSuperIndex={isSuperIndex}/>
            <CollapseTabItem index={311} open={subOpen} text="Danh sách học liệu " subSelectedTab={subSelectedTab} handleSelectedTab={()=>handleSelectedSubTab(311)}  />
            <CollapseTabItem index={312} open={subOpen} text="Học liệu bị báo cáo" subSelectedTab={subSelectedTab} handleSelectedTab={()=>handleSelectedSubTab(312)} />
        <CollapseTabItem index={32} open={open} text="Kho học liệu" subSelectedTab={subSelectedTab} handleSelectedTab={()=>handleSelectedSubTab(321)} isCollapse={true} subOpen ={subOpen2} setSubOpen={setSubOpen2} isSuperIndex={isSuperIndex2}/>
            <CollapseTabItem index={321} open={subOpen2} text="Danh sách học liệu " subSelectedTab={subSelectedTab} handleSelectedTab={()=>handleSelectedSubTab(321)} />
            <CollapseTabItem index={322}  open={subOpen2} text="Học liệu bị báo cáo" subSelectedTab={subSelectedTab} handleSelectedTab={()=>handleSelectedSubTab(322)}/>
        <ItemTabList
          selectedTab={selectedTab}
          index={4}
          handleSelectedTab={handleSelectedTab}
          icon={<FaHashtag />}
          text="Danh sách tag"
          isCollapse={false}
        />
        <ItemTabList
          selectedTab={selectedTab}
          index={5}
          handleSelectedTab={handleSelectedTab}
          icon={<FaShareSquare />}
          text="Chương trình"
          isCollapse={false}
        />
<ItemTabList
          selectedTab={selectedTab}
          index={6}
          handleSelectedTab={handleSelectedTab}
          icon={ <AiOutlineComment />}
          text="Bình luận"
          isCollapse={false}
        />
      </List>
    </>
  );
};

export const ItemTabList = ({
  selectedTab,
  index,
  handleSelectedTab,
  icon,
  text,
  isCollapse,
  open,
}) => {
  return (
    <>
      <ListItemButton
        sx={{
          borderRadius: "15px",
          backgroundColor: selectedTab === index ? "#198754" : "inherit",
          color: selectedTab === index ? "white" : "",
          "&:hover": {
            backgroundColor: selectedTab === index ? "#198754" : "#EEEEEF",
          },
        }}
        onClick={() => handleSelectedTab(index)}
      >
        <ListItemIcon
          sx={{
            color: selectedTab === index ? "white" : "",
          }}
        >
          {icon}
        </ListItemIcon>
        <ListItemText primary={text} />
        {isCollapse ? open ? <ExpandLess /> : <ExpandMore /> : ""}
      </ListItemButton>
    </>
  );
};
export const CollapseTabItem = ({index, open, icon, text,subSelectedTab,handleSelectedTab,subOpen, setSubOpen,isCollapse,isSuperIndex}) => {
  const handleClick =() =>{
    handleSelectedTab(index);
    setSubOpen? setSubOpen(!subOpen) :"";
  }
  return (
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItemButton sx={{ pl: isCollapse ? 4:8,borderRadius: "15px", color: subSelectedTab === index || isSuperIndex ? "#198754" : "", }}
          onClick={() => handleClick()}
          >
            <ListItemIcon>
              {/* <StarBorder /> */}
              {icon}
            </ListItemIcon>
            <ListItemText primary={text} />
            {isCollapse ? subOpen ? <ExpandLess /> : <ExpandMore /> : ""}
          </ListItemButton>
        </List>
      </Collapse>
  );
};

export const 
MyMaterialLeftSection = ({ tabValue, handleChangeView,selectedTab, setSelectedTab,subSelectedTab, setSubSelectedTab,isSuperIndex, setIsSuperIndex,isSuperIndex2, setIsSuperIndex2 ,open, setOpen,subOpen, setSubOpen,subOpen2, setSubOpen2 }) => {
  const profile = useSelector(state => state.profile.info)
   const fullName = profile ? profile.firstname + " "+profile.lastname :  ""
  const token = useSelector(state => state.account.access_token)
  const route = useRouter();
  const handleToUploadPage = () => {
    route.push({
      pathname: "/materials/upload/stepper",
    });
  };
  return (
    <>
          <NestedList setTabValue={handleChangeView} selectedTab={selectedTab} setSelectedTab={setSelectedTab} subSelectedTab={subSelectedTab} setSubSelectedTab={setSubSelectedTab} isSuperIndex={isSuperIndex} setIsSuperIndex={setIsSuperIndex} isSuperIndex2={isSuperIndex2} setIsSuperIndex2={setIsSuperIndex2}  open={open} setOpen={setOpen} subOpen={subOpen}setSubOpen={setSubOpen}subOpen2={subOpen2}setSubOpen2={setSubOpen2}/>
    </>
  );
};
export const NumAndTitleDiv = ({ number, title }) => {
  return (
    <>
      <Box sx={{ textAlign: "center" }}>
        <Box>{number}</Box>
        <Box>{title}</Box>
      </Box>
    </>
  );
};

 export const FilterModerate = ({classID,bookSeriesID,subjectID,bookVolumeID,chapterID,lessonID, handleChangeClass,handleChangeBookSeries,handleChangeSubject,handleChangeBookVolume,handleChangeChapter,handleChangeLesson})=>{
  const [classList, setClassList] = useState([])
  const [bookSeriesList, setBookSeriesList] = useState([])
  const [subjectList, setSubjectList] = useState([])
  const [bookVolumeList, setBookVolumeList] = useState([])
  const [chapterList, setChapterList] = useState([])
  const [lessonList, setLessonList] = useState([])

  // useEffect(() => {
  
    
  // }, [classID,bookSeriesID,subjectID,bookVolumeID,chapterID,lessonID])

  return(
    <div className="flex w-full gap-4 mb-4">
      <MUIClassSelect  handleChange={handleChangeClass}/>
      <MUIBookSeriesSelect  classID={classID} handleChange={handleChangeBookSeries}/>
      <MUISubjectSelect bookSeriesID={bookSeriesID} handleChange={handleChangeSubject}/>
      <MUIBookVolumeSelect subjectID={subjectID} handleChange={handleChangeBookVolume}/>
      <MUIChapterSelect bookVolumeID={bookVolumeID} handleChange={handleChangeChapter}/>
      <MUILessonSelect chapterID={chapterID} handleChange={handleChangeLesson}/>
    </div>
  )
}

const ManageTags = () => {
  const reloading = useSelector((state) => state.reloading.isReLoad);
  const dispatch = useDispatch();
 
  const [tags, setTags] = React.useState([]);
  const [page, setPage] = React.useState(1);
  const [totalPage, setTotalPage] = React.useState(1);
  const [name, setName] = React.useState("");
  const [isLoading, setIsLoading] = React.useState(true);
  const [newTag, setNewTag] = React.useState("");
  const [isLoading2, setIsLoading2] = React.useState(false);
  const [deletingTagId, setDeletingTagId] = React.useState("");
  const [statusFilter, setStatusFilter] = React.useState(new Set([""]));

  useEffect(() => {
    getListTag()
  },[name,page,reloading,statusFilter])
  const getListTag = async()=>{
    setIsLoading(true)
    const statusFilterValue = Array.from(statusFilter)[0];
    try{
      const res = await getFolderSRC("/tags/search?name="+name+"&pageSize=50&pageIndex="+page+ `&active=${statusFilterValue}`);
      //console.log("res",res)

      if(res.status === 200){

        setTags(res.data)
        setTotalPage(res.data.totalPage)
      }else{
        //console.log("Khoong tim thay tag")
      }

    }catch(e){
      //console.log(e)
    }

    setIsLoading(false)

  }
  const handleSetPage = (index) => {
    // setIsLoading(true);
    setPage(index);
  };
  
  const onNextPage = React.useCallback(() => {
    if (page < totalPage) {
      handleSetPage(page + 1);
    }
  }, [page, totalPage]);

  const onPreviousPage = React.useCallback(() => {
    if (page > 1) {
      handleSetPage(page - 1);
    }
  }, [page]);

  const handleChangePage = (event, value) => {
    setPage(value);
  };
  const handleChangeValue = (e) =>{
    setName(e.target.value)
  }
  const handleAddNameTag= (e) =>{
    setNewTag(e.target.value)
  }
  const handleAddTag= async() =>{
    setIsLoading2(true)
    try{
      const res = await addNewTag({name:newTag})
    if(res.status === 200){
     
      dispatch(setIsReload((pre) => !pre));
      toast.success("Thêm tag thành công")
    setNewTag("")

    }else{
      toast.warning("Thêm tag thất bại")

    }
    }catch(e){
      toast.error("Thêm tag thất bại")

    }
    setIsLoading2(false)

  }
  const handleClose = async(tag) =>{
    setDeletingTagId(tag.id)
    const res = await deleteTag(tag.id);
    if(res.status === 200){
      dispatch(setIsReload((pre) => !pre));

      tag.active ? toast.success("Đã ẩn thẻ tag "+tag.name):toast.success("Đã mở thẻ tag "+tag.name)
    }else{
      toast.warning("Không thể xóa tag này")

    }
    setDeletingTagId("")

  }
  
  const handleSetStatusFilter = (e) => {

    setStatusFilter(e);

  };
  return (
    <Box
      sx={{
        width: "100%",
        height:'auto'
      }}
    >
        <ToastContainer />

      <div className="flex flex-row gap-4 items-end mb-4">
      <Dropdown>
                    <DropdownTrigger className="hidden sm:flex">
                      <Button
                        endContent={<BiChevronDown className="text-small" />}
                        variant="flat"
                      >
                        Trạng thái
                      </Button>
                    </DropdownTrigger>
                    <DropdownMenu
                      disallowEmptySelection
                      aria-label="Table Columns"
                      closeOnSelect={true}
                      selectedKeys={statusFilter}
                      selectionMode="single"
                      onSelectionChange={handleSetStatusFilter}
                    >
                      {[{name:"Tất cả",uid:""},{name:"Thẻ mở",uid:true},{name:"Thẻ đóng",uid:false}].map((status) => (
                        <DropdownItem
                          key={status.uid}
                          textValue={status.uid}
                          className="capitalize"
                        >
                          {capitalize(status.name)}
                        </DropdownItem>
                      ))}
                    </DropdownMenu>
                  </Dropdown>
            <Input
                  isClearable
                  className="w-full sm:max-w-[50%]"
                  placeholder="Tìm kiếm theo tên ..."
                  startContent={<Search />}
                  value={name}
                  // onClear={() => onClear()}
                  onChange={(e)=>handleChangeValue(e)}
                />
                <Input
              key="outside"
              type="text"
              label="Thẻ tag mới"
              labelPlacement="outside"
              // description="outside"
              value={newTag}
              onChange={(e)=>handleAddNameTag(e)}

            />
            <Button color="primary" disabled={isLoading2} onClick={handleAddTag}>
       {isLoading2 ? <BeatLoader
          color="white"
          size={5}
          style={{marginLeft:"5px",marginTop:"5px"}}
        />:"Thêm mới"}
    </Button>
            </div>
      {/* <ItemBox > */}
      
      <div className="flex flex-wrap gap-3 pt-9 pl-9 pb-9 rounded-xl" style={{boxShadow: 'rgba(0, 0, 0, 0.16) 0px 10px 36px 0px, rgba(0, 0, 0, 0.06) 0px 0px 0px 1px'}}>
   
      {isLoading && tags?.data?.length === 0? <LoadingTab/>:isLoading && tags?.data?.length > 0
      ?<>
        {tags?.data?.map((tag, index) => (
      tag?.active 
        ? <Chip key={index} onClick={() => handleClose(tag)} color="success" variant="flat" style={{cursor:"pointer"}} title="Bấm ẩn thẻ">
        {deletingTagId == tag.id? <BeatLoader
          color="black"
          size={5}
          style={{marginLeft:"5px",marginTop:"5px"}}
        />:tag.name }
      </Chip>
      :<Chip key={index}  onClick={() => handleClose(tag)} color="warning" variant="flat" style={{cursor:"pointer"}} title="Bấm mở thẻ">
        {deletingTagId == tag.id? <BeatLoader
          color="black"
          size={5}
          style={{marginLeft:"5px",marginTop:"5px"}}
        />:tag.name }
      </Chip>       
    ))}
    <CenterFlexBox>
    <BeatLoader
          color="#197854"
          size={5}
          style={{marginLeft:"5px",marginTop:"5px"}}
        />
    </CenterFlexBox>
      </>
       : tags?.data?.map((tag, index) => (
        tag?.active 
          ? <Chip key={index} onClick={() => handleClose(tag)} color="success" variant="flat" style={{cursor:"pointer"}} title="Bấm ẩn thẻ">
          {deletingTagId == tag.id? <BeatLoader
          color="black"
          size={5}
          style={{marginLeft:"5px",marginTop:"5px"}}
        />:tag.name }
        </Chip>
        :<Chip key={index}  onClick={() => handleClose(tag)} color="warning" variant="flat" style={{cursor:"pointer"}} title="Bấm mở thẻ">
          {deletingTagId == tag.id? <BeatLoader
          color="black"
          size={5}
          style={{marginLeft:"5px",marginTop:"5px"}}
        />:tag.name }
        </Chip>
        
      ))}
       
    </div>
    <div className="py-2 px-2 flex justify-between items-center">
        <span className="w-[30%] text-small text-default-400"></span>
        <Pagination
          isCompact
          showControls
          showShadow
          color="primary"
          page={page}
          total={totalPage}
          onChange={handleSetPage}
        />
        <div className="hidden sm:flex w-[30%] justify-end gap-2">
          <Button
            isDisabled={totalPage === 1}
            size="sm"
            variant="flat"
            onPress={onPreviousPage}
          >
            Trước
          </Button>
          <Button
            isDisabled={totalPage === 1}
            size="sm"
            variant="flat"
            onPress={onNextPage}
          >
            Sau
          </Button>
        </div>
      </div>
    </Box>
  );
};