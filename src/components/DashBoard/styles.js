import * as React from "react";
import { styled, alpha } from "@mui/material/styles";
import Button from "@mui/material/Button";

export const AdminDashBoardContainerDiv = styled("div")(({ theme }) => ({
  marginTop:'50px',
  
  [theme.breakpoints.down("xs")]: {
    
  },
  [theme.breakpoints.up("xs")]: {
    
  }
  ,
  [theme.breakpoints.down("md")]: {
    
  },
  [theme.breakpoints.up("md")]: {
    
  },
  [theme.breakpoints.up("lg")]: {
    
  },
  [theme.breakpoints.up("xl")]: {
   
  },
}));