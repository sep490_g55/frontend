
import { ContainerDiv, LeftDiv, RightDiv } from '@/components/DetailMaterial/styles';
import { LeftInfoDiv } from '@/components/InfoMaterial/section';

import * as React from 'react';
import ListSubheader from '@mui/material/ListSubheader';
import List from '@mui/material/List';
import { AiFillHome } from 'react-icons/ai';
import { MdManageAccounts, MdOutlineSettingsAccessibility, MdPermIdentity, MdVideoSettings } from 'react-icons/md';


import TabBookSeries from '../../../pages/dashboards/Admin/Material/BookSeries';
import TabSubjects from '../../../pages/dashboards/Admin/Material/Subject';
import TabUser from '../../../pages/dashboards/Admin/User/UserList';
import TabClass from '../../../pages/dashboards/Admin/Material/Class';
import TabChapters from '../../../pages/dashboards/Admin/Material/Chapter';
import TabBookVolume from '../../../pages/dashboards/Admin/Material/BookVolume';
import TabLesson from '../../../pages/dashboards/Admin/Material/Lesson';
import TabRole from '../../../pages/dashboards/Admin/Role/RoleManage';
import TabPermission from '../../../pages/dashboards/Admin/Permission/PermissionManage';
import { CollapseTabItem, ItemTabList } from '@/components/DashBoard/MyMaterial/sections';
import { useState } from 'react';
import TabSubjectBookSeries from '@/pages/dashboards/Admin/Material/SubjectBookSeries';
import { Box } from '@mui/material';
import { Image } from 'react-bootstrap';
import { AdminHomeDiv } from './style';
import { useAuthContext } from '@/auth/useAuthContext';
import { AdminSystem, NameSystem } from '@/theme/system';
import { CenterFlexBox } from '@/components/common_sections';
export const AdminDashBoardContainer = ({tabValue, setTabValue,open, setOpen,selectedTab, setSelectedTab,subSelectedTab, setSubSelectedTab}) => {
    // const [tabValue, setTabValue] = React.useState(0);
    const [classID, setClassID] = useState(0);
    const [bookSeriesID, setBookSeriesID] = useState(0);
    const [subjectID, setSubjectID] = useState(0);
    const [bookVolumeID, setBookVolumeID] = useState(0);
    const [chapterID, setChapterID] = useState(0);
    const [lessonID, setLessonID] = useState(0);
    
  return (
    <ContainerDiv>
        <LeftDiv style={{ paddingLeft: "40px", paddingRight:"30px",overflowX:"hidden" }}>
            <LeftInfoDiv>
                <NestedList setTabValue={setTabValue} open={open} setOpen={setOpen} selectedTab={selectedTab} setSelectedTab={setSelectedTab} subSelectedTab={subSelectedTab} setSubSelectedTab={setSubSelectedTab}/>
            </LeftInfoDiv>
        </LeftDiv>
        <RightDiv>
            {tabValue === 0? <AdminHome/> :tabValue === 1? <TabUser api={`/users/display?`} /> :  tabValue === 20? <TabClass nextTabIndex={21} setFolderID={setClassID} api={`/class/display?`} handleView={setTabValue}/>:tabValue == "21"? <TabBookSeries nextTabIndex={22} setFolderID={setBookSeriesID} api={`/book-series/display/${classID}?`} handleView={setTabValue}/>:tabValue == "22"? <TabSubjectBookSeries type="SUBJECT-BOOK-SERIES" nextTabIndex={23} setFolderID={setSubjectID} api={`/book-series-subject/display/${bookSeriesID}?`} handleView={setTabValue}/>:tabValue == "23"? <TabBookVolume nextTabIndex={24} setFolderID={setBookVolumeID} api={`/book-volume/display/${subjectID}?`} handleView={setTabValue}/>:tabValue == "24"? <TabChapters nextTabIndex={25} setFolderID={setChapterID} api={`/chapter/display/${bookVolumeID}?`} handleView={setTabValue}/>:tabValue == "25"? <TabLesson nextTabIndex={26} setFolderID={setLessonID} api={`/lesson/display/${chapterID}?`} handleView={setTabValue}/>:tabValue === 4? <TabRole api="/role/display?" />:tabValue === 5? <TabPermission  api="/system-permission/display?"/>:<TabSubjects type="SUBJECT" setFolderID={setSubjectID} api={`/subject/display?`}/>}
            {/* {tabValue === 1? <TabUser  handleView={setTabValue}/> :  tabValue === 20? <TabClass nextTabIndex={21} setFolderID={setClassID} api="class/list" handleView={setTabValue}/>:tabValue == "21"? <TabBookSeries nextTabIndex={22} setFolderID={setBookSeriesID} api={`book-series/list-by-class?classId=${classID}`} handleView={setTabValue}/>:tabValue == "22"? <TabSubjects type="SUBJECT-BOOK-SERIES" nextTabIndex={23} setFolderID={setSubjectID} api={`subject/list-by-book-series?bookSeriesId=${bookSeriesID}`} handleView={setTabValue}/>:tabValue == "23"? <TabBookVolume nextTabIndex={24} setFolderID={setBookVolumeID} api={`book-volume/list-by-book-series-subject?bookSeriesSubjectId=${subjectID}`} handleView={setTabValue}/>:tabValue == "24"? <TabChapters nextTabIndex={25} setFolderID={setChapterID} api={`chapter/list-by-book-volume?bookVolumeId=${bookVolumeID}`} handleView={setTabValue}/>:tabValue == "25"? <TabLesson nextTabIndex={26} setFolderID={setLessonID} api={`lesson/list-by-chapter?chapterId=${chapterID}`} handleView={setTabValue}/>:tabValue === 4? <TabRole handleView={setTabValue}/>:tabValue === 5? <TabPermission handleView={setTabValue}/>:<TabSubjects type="SUBJECT" setFolderID={setSubjectID} api={`subject/list-by-book-series`}/>} */}
        </RightDiv>
    </ContainerDiv>
  )
}
const AdminHome= ()=>{
  const { user, isAuthenticated, logout,ROLES } = useAuthContext();
    const fullName = user?.firstname + " " + user?.lastname;
  return(
  <AdminHomeDiv
   
  >
    
    <AdminHomeDiv
   
   >
     <CenterFlexBox><AdminSystem/>
     <CenterFlexBox style={{width:"fit-content",}}>Chào mừng <Box sx={{fontWeight:"500",padding:"10px"}}>{fullName}</Box>đã quay trở lại</CenterFlexBox></CenterFlexBox>
     <CenterFlexBox>Trang quản lý hệ thống của <NameSystem /></CenterFlexBox>
   </AdminHomeDiv>
  </AdminHomeDiv>
    // <Box sx={{width:"300px",backgroundImage: "url('https://microbiomology.org/wp-content/uploads/2019/12/Project-Management-freewallpaperimage.xyz-Copy-Copy-1.jpg')"}}></Box>
  )
}

export const NestedList= ({setTabValue,open, setOpen,selectedTab, setSelectedTab,subSelectedTab, setSubSelectedTab}) => {
    const handleClick = () => {
      setOpen(!open);
    };
    
 
    const handleSelectedTab =(newValue)  =>{
      setOpen(false);
      setSelectedTab(newValue);
      // setTabValue(newValue);
      if (newValue === 2) {
        handleClick();
      setTabValue(20);

      }else{
      setTabValue(newValue);

      }
    }
    const handleSelectedSubTab = (newValue) => { //action for sub tab
    
      setSubSelectedTab(newValue);
      setTabValue(newValue);
     
    };
    
    return (
        <>
      <List
        sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            CHUNG
          </ListSubheader>
        }
      >
      
        <ItemTabList
          selectedTab={selectedTab}
          index={0}
          handleSelectedTab={handleSelectedTab}
          icon={<AiFillHome />}
          text="Trang chủ"
          isCollapse={false}
        />
      </List>

      <List
        sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}
        component="nav"
        aria-labelledby="nested-list-subheader2"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader2">
            DANH MỤC QUẢN LÝ
          </ListSubheader>
        }
      >
       
        <ItemTabList
          selectedTab={selectedTab}
          index={1}
          handleSelectedTab={handleSelectedTab}
          icon={ <MdManageAccounts />}
          text="Tài khoản"
          isCollapse={false}
        />
        <ItemTabList
          selectedTab={selectedTab}
          index={2}
          handleSelectedTab={handleSelectedTab}
          icon={  <MdVideoSettings
            />}
          text="Chương trình"
          isCollapse={true}
          open={open}

        />
        {/* handleSelectedSubTab  is set value tab */}
        <CollapseTabItem  index={20} open={open} text="Quản lý lớp học" subSelectedTab={subSelectedTab} handleSelectedTab={() =>handleSelectedSubTab(20)} />
        <CollapseTabItem index={26} open={open} text="Quản lý môn học" subSelectedTab={subSelectedTab} handleSelectedTab={() =>handleSelectedSubTab(26)} />
        

<ItemTabList
          selectedTab={selectedTab}
          index={4}
          handleSelectedTab={handleSelectedTab}
          icon={ <MdPermIdentity />}
          text="Đối tượng truy cập"
          isCollapse={false}
        />
       <ItemTabList
          selectedTab={selectedTab}
          index={5}
          handleSelectedTab={handleSelectedTab}
          icon={<MdOutlineSettingsAccessibility />}
          text="Quyền truy cập"
          isCollapse={false}
        />
        
        
      </List>
      </>
    );
  }