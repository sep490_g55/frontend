import * as React from 'react';
import styled from 'styled-components';
import Button from '@mui/material/Button';
import bgSearch from '@/assets/bgHome.png'
import admin from '@/assets/image_avatar_admin.png'
import moderator from '@/assets/image_avartar_mod.png'
import teacher from '@/assets/image_avartar_teacher.png'
export const AdminHomeDiv = styled('div')(({ theme }) => ({
   width:"100%",
   height:"500px",
    backgroundImage:  `url(${bgSearch.src})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
    textAlign:"end",
    color:"#198754",
    fontSize:"20px"
  }));

  export const ModeratorHomeDiv = styled('div')(({ theme }) => ({
    width:"650px",
    height:"500px",
     backgroundImage:  `url(${bgSearch.src})`,
     backgroundRepeat: 'no-repeat',
     backgroundSize: 'cover',
     backgroundPosition: 'center center',
     textAlign:"end",
     color:"#198754",
     fontSize:"24px"
   }));