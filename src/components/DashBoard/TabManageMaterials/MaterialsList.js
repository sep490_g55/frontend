import React, { useState } from "react";
import {
  Table,
  TableHeader,
  TableColumn,
  TableBody,
  TableRow,
  TableCell,
  Input,
  Button,
  DropdownTrigger,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Chip,
  Pagination,
} from "@nextui-org/react";
// import {PlusIcon} from "./PlusIcon";
// import {SearchIcon} from "./SearchIcon";
// import {ChevronDownIcon} from "./ChevronDownIcon";
// import { materialTypes } from "./data";
import { capitalize } from "./utils";
import { Search } from "@mui/icons-material";
import { PiPlusCircleDuotone } from "react-icons/pi";
import {
  AvatarAndInfoDiv,
  CenterFlexBox,
  RouterLink,
} from "@/components/common_sections";
import { useRouter } from "next/navigation";
import { useEffect } from "react";
import { RingLoader } from "react-spinners";
import { CustomizedMenus } from "@/components/Option/menu";

import { format } from "date-fns";
import { Box } from "@mui/material";
import { FilterModerate } from "@/components/DashBoard/Moderator/sections";
import {
  getFolderSRC,
  getLocalStorage,
  getResourceTypes,
} from "@/dataProvider/agent";
import {
  FullAddBookSeriesDialog,
  FullAddBookVolumeDialog,
  FullAddPermissionDialog,
  FullAddChapterDialog,
  FullAddClassDialog,
  FullAddLessonDialog,
  FullAddSubjectBookSeriesDialog,
  FullAddSubjectDialog,
  FullAddRoleDialog,
  FullSetPermissionDialog,
} from "@/components/Dialog/section";
import { useSelector } from "react-redux";

//icon
import { AiOutlineCamera, AiOutlineVideoCamera } from "react-icons/ai";
import { BsDownload, BsBookmark, BsShareFill } from "react-icons/bs";
import { BiInfoCircle } from "react-icons/bi";
import { MdPublic } from "react-icons/md";
import { IoMdPeople } from "react-icons/io";
import { RiGitRepositoryPrivateLine } from "react-icons/ri";
import { TbPointFilled } from "react-icons/tb";
import { RxDividerVertical } from "react-icons/rx";
import { FaRegComment } from "react-icons/fa";
import { AiFillFileImage, AiOutlineFile } from "react-icons/ai";
import { BiChevronDown, BiSolidFilePdf } from "react-icons/bi";
import { SiMicrosoftpowerpoint } from "react-icons/si";
import { FcDocument, FcVideoFile } from "react-icons/fc";
import { BsFiletypeSvg } from "react-icons/bs";
import { MdOutlineAudioFile } from "react-icons/md";

const typeFileMap = {
  PPTX: (
    <SiMicrosoftpowerpoint
      style={{ color: "#FF7700", fontSize: "24px" }}
      title="Định dạng file PPTX"
    />
  ),
  PDF: (
    <BiSolidFilePdf
      style={{ color: "#FF7700", fontSize: "24px" }}
      title="Định dạng file PDF"
    />
  ),
  PNG: (
    <AiFillFileImage
      style={{ color: "#050505", fontSize: "24px" }}
      title="Định dạng file PNG"
    />
  ),
  JPG: (
    <AiFillFileImage
      style={{ color: "#050505", fontSize: "24px" }}
      title="Định dạng file JPG"
    />
  ),
  JPEG: (
    <AiFillFileImage
      style={{ color: "#050505", fontSize: "24px" }}
      title="Định dạng file JPEG"
    />
  ),
  GIF: (
    <AiFillFileImage
      style={{ color: "#050505", fontSize: "24px" }}
      title="Định dạng file GIF"
    />
  ),
  SVG: (
    <BsFiletypeSvg
      style={{ color: "#050505", fontSize: "24px" }}
      title="Định dạng file SVG"
    />
  ),

  MP3: (
    <MdOutlineAudioFile
      style={{ color: "#050505", fontSize: "24px" }}
      title="Định dạng file MP3"
    />
  ),
  PCM: (
    <MdOutlineAudioFile
      style={{ color: "#050505", fontSize: "24px" }}
      title="Định dạng file PCM"
    />
  ),
  WAV: (
    <MdOutlineAudioFile
      style={{ color: "#050505", fontSize: "24px" }}
      title="Định dạng file WAV"
    />
  ),
  AIFF: (
    <MdOutlineAudioFile
      style={{ color: "#050505", fontSize: "24px" }}
      title="Định dạng file AIFF"
    />
  ),
  FLAC: (
    <MdOutlineAudioFile
      style={{ color: "#050505", fontSize: "24px" }}
      title="Định dạng file FLAC"
    />
  ),
  ALAC: (
    <MdOutlineAudioFile
      style={{ color: "#050505", fontSize: "24px" }}
      title="Định dạng file ALAC"
    />
  ),
  WMA: (
    <MdOutlineAudioFile
      style={{ color: "#050505", fontSize: "24px" }}
      title="Định dạng file WMA"
    />
  ),
  AAC: (
    <MdOutlineAudioFile
      style={{ color: "#050505", fontSize: "24px" }}
      title="Định dạng file AAC"
    />
  ),
  OGG: (
    <MdOutlineAudioFile
      style={{ color: "#050505", fontSize: "24px" }}
      title="Định dạng file OGG"
    />
  ),

  MP4: <FcVideoFile style={{ fontSize: "24px" }} title="Định dạng file MP4" />,
  WMV: <FcVideoFile style={{ fontSize: "24px" }} title="Định dạng file WMV" />,
  MKV: <FcVideoFile style={{ fontSize: "24px" }} title="Định dạng file MKV" />,
  VOB: <FcVideoFile style={{ fontSize: "24px" }} title="Định dạng file VOB" />,
  FLV: <FcVideoFile style={{ fontSize: "24px" }} title="Định dạng file FLV" />,
  WMV9: (
    <FcVideoFile style={{ fontSize: "24px" }} title="Định dạng file WMV9" />
  ),
  MPEG: (
    <FcVideoFile style={{ fontSize: "24px" }} title="Định dạng file MPEG" />
  ),

  DOCX: <FcDocument style={{ fontSize: "24px" }} title="Định dạng file DOC" />,
  OTHER: <AiOutlineFile style={{ fontSize: "24px" }} title="Định dạng file" />,
};

export default function TabMaterialList({
  uri,
  useFor,
  type,
  setFolderID,
  handleView,
  nextTabIndex,
  isShowFilter,
  amMine,
}) {

  const materialTypes = uri?.includes("media")?[
    {name: "Tất cả", uid: ""},
    {name: "JPG", uid: "JPG"},
    {name: "PNG", uid: "PNG"},
    {name: "JPEG", uid: "JPEG"},
    {name: "MP3", uid: "MP3"},
    {name: "MP4", uid: "MP4"},
  
   
  ]:[
    {name: "Tất cả", uid: ""},
    {name: "PPTX", uid: "PPTX"},
    {name: "PDF", uid: "PDF"},
    {name: "DOCX", uid: "DOCX"},
    {name: "JPG", uid: "JPG"},
    {name: "PNG", uid: "PNG"},
    {name: "JPEG", uid: "JPEG"},
    {name: "MP3", uid: "MP3"},
    {name: "MP4", uid: "MP4"},
  
   
  ];
  // const[materialTypes,setMaterialTypes] = useState([])
  const reloading = useSelector((state) => state.reloading.isReLoad);
  const statusColorMap =
    useFor === "ADMIN"
      ? { true: "success", false: "danger" }
      : {
          ACCEPTED: "success",
          REJECT: "danger",
          vacation: "warning",
        };
  const accessMap = {
    PUBLIC: <MdPublic title="Công khai" />,
    RESTRICT: <IoMdPeople title="Hạn chế" />,
    PRIVATE: <RiGitRepositoryPrivateLine title="Chỉ mình tôi" />,
  };
  const columns =
    useFor === "ADMIN"
      ? [
          { name: "Tên", uid: "name", sortable: true },
          { name: "Người tạo", uid: "creator", sortable: true },
          { name: "Ngày tạo", uid: "createdAt", sortable: true },
          { name: "Trạng thái", uid: "active", sortable: true },
          { name: "Hành động", uid: "actions" },
        ]
      : useFor === "ADMIN-USER"
      ? [
          { name: "STT", uid: "no", sortable: true },
          { name: "Tên tài khoản", uid: "username", sortable: true },
          { name: "Họ", uid: "firstname", sortable: true },
          { name: "Tên", uid: "lastname", sortable: true },
          { name: "Ngày sinh", uid: "dateOfBirth", sortable: true },
          { name: "Giới tính", uid: "gender", sortable: true },
          { name: "SĐT", uid: "phone", sortable: true },
          { name: "Tỉnh", uid: "district", sortable: true },
          { name: "Huyện", uid: "province", sortable: true },
          { name: "Trường", uid: "school", sortable: true },
          { name: "Lớp", uid: "classId", sortable: true },
          { name: "Vai trò", uid: "roleDTOResponses", sortable: false },
          { name: "Trạng thái", uid: "active", sortable: true },
          { name: "Hành động", uid: "actions" },
        ]
      : useFor === "ADMIN-ROLE"
      ? [
          { name: "Tên role", uid: "roleName", sortable: true },
          { name: "Mô tả", uid: "description", sortable: true },
          { name: "Tạo ngày", uid: "createdAt", sortable: true },
          { name: "Người tạo", uid: "creator", sortable: true },
          { name: "Trạng thái", uid: "active", sortable: true },
          { name: "Hành động", uid: "actions" },
        ]
      : useFor === "ADMIN-PERMISSION"
      ? [
          { name: "Tên quyền", uid: "permissionName", sortable: true },
          { name: "Mô tả", uid: "description", sortable: true },
          { name: "PATH", uid: "path", sortable: true },
          { name: "METHOD", uid: "methodType", sortable: true },
          { name: "Tạo ngày", uid: "createdAt", sortable: true },
          { name: "Người tạo", uid: "creator", sortable: true },
          { name: "Trạng thái", uid: "active", sortable: true },
          { name: "Hành động", uid: "actions" },
        ]
      : useFor === "REPORT-COMMENT"
      ? [
          { name: "Nội dung bình luận", uid: "content", sortable: true },
          { name: "Người bình luận", uid: "commenter", sortable: true },
          { name: "Tạo ngày", uid: "createdAt", sortable: true },
          { name: "Hành động", uid: "actions" },
        ]
        :useFor === "MODERATOR" && uri.includes("review")?
        [
          { name: "Tên file", uid: "name", sortable: true },
          { name: "Mô tả", uid: "description", sortable: true },
          { name: "Định dạng file", uid: "resourceType", sortable: true },
          { name: "Ngày tạo", uid: "createdAt", sortable: true },
          { name: "Quyền truy cập", uid: "visualType", sortable: true },
          { name: "Trạng thái", uid: "approveType", sortable: true },
        ]
      :[
          { name: "Tên file", uid: "name", sortable: true },
          { name: "Mô tả", uid: "description", sortable: true },
          { name: "Định dạng file", uid: "resourceType", sortable: true },
          { name: "Ngày tạo", uid: "createdAt", sortable: true },
          { name: "Quyền truy cập", uid: "visualType", sortable: true },
          { name: "Trạng thái", uid: "approveType", sortable: true },
          { name: "Hành động", uid: "actions" },
        ];
  const INITIAL_VISIBLE_COLUMNS =
    useFor === "ADMIN"
      ? ["name", "creator", "createdAt", "active", "actions"]
      : useFor === "ADMIN-USER"
      ? [
          "name",
          "username",
          "firstname",
          "lastname",
          "province",

          "active",
          "roleDTOResponses",
          "actions",
        ]
      : useFor === "ADMIN-ROLE"
      ? ["roleName", "description", "createdAt", "active", "creator", "actions"]
      : useFor === "ADMIN-PERMISSION"
      ? [
          "permissionName",
          "createdAt",
          "path",
          "dependencyPermissionId",
          "creator",
          "actions",
        ]
      :
      useFor === "REPORT-COMMENT"
      ? [
          "content",
          "createdAt",
          "commenter",
          "actions",
        ]
        :useFor === "MODERATOR" && uri.includes("review") 
        ? [
          "name",
          "resourceType",
          "createdAt",
          "approveType",
          "visualType",
        ]: [
          "name",
          "resourceType",
          "createdAt",
          "approveType",
          "visualType",
          "actions",
        ];
 
  const visualTypes =
    useFor === "ADMIN-ROLE" || useFor === "ADMIN-PERMISSION"||useFor === "ADMIN"
      ? [
          { name: "Tất cả", uid: "" },
          { name: "Hoạt động", uid: true },
          { name: "Không hoạt động", uid: false },
        ]
      : (useFor?.includes("MODERATOR") && uri.includes("review") ) || useFor === "SHARE-FOR-ME" ?
      [
        { name: "Tất cả", uid: "" },
        { name: "Công khai", uid: "PUBLIC" },
        { name: "Hạn chế", uid: "RESTRICT" },
      ]
      
      :[
          { name: "Tất cả", uid: "" },
          { name: "Công khai", uid: "PUBLIC" },
          { name: "Hạn chế", uid: "RESTRICT" },
          { name: "Chỉ mình tôi", uid: "PRIVATE" },
        ];

  const statusOptions =
    useFor === "ADMIN-ROLE" || useFor === "ADMIN-PERMISSION"||useFor === "ADMIN"
      ? [
          { name: "Tất cả", uid: "" },
          { name: "Hoạt động", uid: true },
          { name: "Không hoạt động", uid: false },
        ]
      : 
      //  useFor?.includes("MODERATOR")
      // ?[
      //   { name: "Tất cả", uid: "" },
      //   { name: "Đã kiểm duyệt", uid: "ACCEPTED" },
      //   { name: "Chưa kiểm duyệt", uid: "UNACCEPTED" },
      //   { name: "Bị từ chối", uid: "REJECT" },
      // ]
      // :
       [
          { name: "Tất cả", uid: "" },
          { name: "Đã kiểm duyệt", uid: "ACCEPTED" },
          { name: "Chưa kiểm duyệt", uid: "UNACCEPTED" },
          { name: "Bị từ chối", uid: "REJECT" },
        ];
  // const originalDate = detailMaterial.createdAt
  const formattedDate = format(new Date("2023-11-05T20:15:58"), "dd/MM/yyyy");
  const viewFolder = useSelector((state) => state.viewFolder);

  const roleCurrent = getLocalStorage("role");
  const router = useRouter();

  const [nameFilter, setNameFilter] = React.useState("");
  const [selectedKeys, setSelectedKeys] = React.useState(new Set([]));
  const [visibleColumns, setVisibleColumns] = React.useState(
    new Set(INITIAL_VISIBLE_COLUMNS)
  );
  const [visualTypeFilter, setVisualTypeFilter] = React.useState(new Set([""]));

  const [statusFilter, setStatusFilter] = React.useState(new Set([""]));
  const [materialTypeFilter, setMaterialTypeFilter] = React.useState("");
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [sortDescriptor, setSortDescriptor] = React.useState({
    column: "age",
    direction: "ascending",
  });
  const [totalElement, setTotalElement] = React.useState(0);

  const [page, setPage] = React.useState(1);
  const [pages, setPages] = React.useState(1); // pages is totalPage within filtered

  // new handle
  const [isLoading, setIsLoading] = React.useState(true);
  const [myMaterial, setMyMaterial] = React.useState(null);

  // for manage user list
  const [roleId, setRoleId] = useState(new Set([""]));
  const [classId, setClassId] = useState(new Set([""]));
  const [roles, setRoles] = useState([]);
  const [classes, setClasses] = useState([]);

  // folder
  const [classID, setClassID] = useState("");
  const [bookSeriesID, setBookSeriesID] = useState("");
  const [subjectID, setSubjectID] = useState("");
  const [bookVolumeID, setBookVolumeID] = useState("");
  const [chapterID, setChapterID] = useState("");
  const [lessonID, setLessonID] = useState("");

  const handleSetStatusFilter = (e) => {
    setIsLoading(true);
    setStatusFilter(e);
    setPage(1);
  };

  const handleSetMaterialTypeFilter = (e) => {
    setIsLoading(true);
    setMaterialTypeFilter(e);
    setPage(1);
  };

  const handleSetVisualTypeFilter = (e) => {
    setIsLoading(true);
    setVisualTypeFilter(e);
    setPage(1);
  };
  const handleSetPage = (index) => {
    setIsLoading(true);
    setPage(index);
  };
  // //.log("isUpdate",viewFolder?.isUpdate)
  useEffect(() => {
    if (useFor === "ADMIN-USER") {
      getRoles();
      getClasses();
    }
    getMyMaterial();
  }, [
    reloading,
    page,
    rowsPerPage,
    nameFilter,
    statusFilter,
    visualTypeFilter,
    uri,
    viewFolder?.isUpdate,
    classId,
    roleId,
    useFor,
    materialTypeFilter,
    classID,
    bookSeriesID,
    subjectID,
    bookVolumeID,
    chapterID,
    lessonID
  ]);

  const getClasses = async () => {
    try {
      const res = await getFolderSRC("/class/list");
      if (res) {
        setClasses(res.data);
      }
    } catch (e) {
      //.log("e", e);
    }

    /// get resource type: PTTX, PDF,..
    // try {
    //   const res = await getResourceTypes();
    //   if (res.status ===200) {
    //     setMaterialTypes(res.data);
    //   }
    // } catch (e) {
    //   //.log("e", e);
    // }
  };
  const getRoles = async () => {
    try {
      const res = await getFolderSRC("/role/list");
      if (res) {
        setRoles(res.data);
      }
    } catch (e) {
      //.log("e", e);
    }
  };
  const getMyMaterial = async () => {
    setIsLoading(true);
    const statusFilterValue = Array.from(statusFilter)[0];
    const visualTypeFilterValue = Array.from(visualTypeFilter)[0];
    const classIdFilterValue = Array.from(classId)[0];
    const roleIdFilterValue = Array.from(roleId)[0];
    const materialTypeFilterValue = Array.from(materialTypeFilter)[0]
      ? Array.from(materialTypeFilter)[0]
      : "";

    // //.log("uri",uri)
    const isExistName = nameFilter === "" ? "" : "&name=" + nameFilter;
    const api = `${uri}name=${nameFilter}&visualType=${visualTypeFilterValue}&approveType=${statusFilterValue}&pageSize=${rowsPerPage}&pageIndex=${page}&classId=${classIdFilterValue}&roleId=${roleIdFilterValue}&resourceType=${materialTypeFilterValue}&active=${statusFilterValue}`;
    const apiMOD = `${uri}name=${nameFilter}&visualType=${visualTypeFilterValue}&approveType=${statusFilterValue}&pageSize=${rowsPerPage}&pageIndex=${page}&active=${statusFilterValue}&classId=${classID}&bookSeriesId=${bookSeriesID}&subjectId=${subjectID}&bookVolumeId=${bookVolumeID}&chapterId=${chapterID}&lessonId=${lessonID}&resourceType=${materialTypeFilterValue}`;
    const apiREMA = `review/report-materials?name=${nameFilter}&classId=${classID}&bookSeriesId=${bookSeriesID}&subjectId=${subjectID}&bookVolumeId=${bookVolumeID}&chapterId=${chapterID}&lessonId=${lessonID}&approveType=${statusFilterValue}&visualType=${visualTypeFilterValue}&pageIndex=${page}&pageSize=${rowsPerPage}&resourceType=${materialTypeFilterValue}`;
    // //.log("amMine1",amMine)
    const apiReportComment = `/comment/display?content=${nameFilter}&pageSize=${rowsPerPage}&pageIndex=${page}`;
    // //.log("api",api)
    try {
      const res = await getFolderSRC(
        useFor === "MODERATOR-REPORT" ||
          useFor === "MODERATOR" ||
          useFor === "ADMIN-ROLE" ||
          useFor === "ADMIN-PERMISSION"
          ? apiMOD
          : useFor === "MODERATOR-REPORT-MATERIAL"
          ? apiREMA
          : useFor === "REPORT-COMMENT"
          ? apiReportComment
          : api
      );
      if (res.status ===200) {
        // //.log("res", res.data);

        setPages(res.data.totalPage);
        setMyMaterial(res.data);
        setTotalElement(res.data.totalElement);
      }
      setIsLoading(false);
    } catch (e) {
      //.log("e", e);
    }
  };
  ///
  const hasSearchFilter = Boolean(nameFilter);

  const headerColumns = React.useMemo(() => {
    if (visibleColumns === "all") return columns;

    return columns.filter((column) =>
      Array.from(visibleColumns).includes(column.uid)
    );
  }, [visibleColumns]);

  const filteredItems = React.useMemo(() => {
    if (myMaterial) {
      let filteredMaterial = [...myMaterial.data];
      return filteredMaterial;
    }
    return [];
  }, [myMaterial, statusFilter, materialTypeFilter]);

  const sortedItems = React.useMemo(() => {
    return [...filteredItems].sort((a, b) => {
      const first = a[sortDescriptor.column];
      const second = b[sortDescriptor.column];
      const cmp = first < second ? -1 : first > second ? 1 : 0;

      return sortDescriptor.direction === "descending" ? -cmp : cmp;
    });
  }, [sortDescriptor, filteredItems]);

  const renderCell =
    useFor === "ADMIN"
      ? React.useCallback((item, columnKey) => {
          const cellValue = item[columnKey];

          switch (columnKey) {
            case "name":
              return cellValue;
            case "creator":
              return cellValue ? cellValue : "Không rõ";
            case "createdAt":
              const date = format(
                new Date(
                  item?.createdAt ? item?.createdAt : "2023-10-09T21:30:30"
                ),
                "MM/dd/yyyy hh:mm:ss"
              );
              return cellValue ? (
                <Box title="ngày/giờ">{date}</Box>
              ) : (
                "Không rõ"
              );

            case "active":
              return (
                <Chip
                  className="capitalize"
                  color={statusColorMap[item.active]}
                  size="sm"
                  variant="flat"
                >
                  {item.active ? "Hoạt động" : "Không hoạt động"}
                </Chip>
              );
            case "actions":
              const handleDetailView = (itemID) => {
                handleView(nextTabIndex);
                setFolderID(itemID);
              };

              return (
                <div className="relative flex justify-end items-center gap-2">
                  {roleCurrent === "ADMIN" && type === "CLASS" ? (
                    <CustomizedMenus
                      value={item}
                      onDetailView={() => handleDetailView(item.id)}
                      type="ADMIN-CLASS"
                      name={item.name}
                      classID={item.id}
                    />
                  ) : roleCurrent === "ADMIN" && type === "BOOK-SERIES" ? (
                    <CustomizedMenus
                      value={item}
                      onDetailView={() => handleDetailView(item.id)}
                      type="ADMIN-BOOK-SERIES"
                      name={item.name}
                      bookSeriesID={item.id}
                    />
                  ) : roleCurrent === "ADMIN" &&
                    type === "SUBJECT-BOOK-SERIES" ? (
                    <CustomizedMenus
                      value={item}
                      onDetailView={() => handleDetailView(item.id)}
                      type="ADMIN-SUBJECT-BOOK-SERIES"
                      name={item.name}
                      subjectID={item.id}
                    />
                  ) : roleCurrent === "ADMIN" && type === "SUBJECT" ? (
                    <CustomizedMenus
                      value={item}
                      onDetailView={() => handleDetailView(item.id)}
                      type="ADMIN-SUBJECT"
                      name={item.name}
                      subjectID={item.id}
                    />
                  ) : roleCurrent === "ADMIN" && type === "BOOK-VOLUME" ? (
                    <CustomizedMenus
                      value={item}
                      onDetailView={() => handleDetailView(item.id)}
                      type="ADMIN-BOOK-VOLUME"
                      name={item.name}
                      bookVolumeID={item.id}
                    />
                  ) : roleCurrent === "ADMIN" && type === "CHAPTER" ? (
                    <CustomizedMenus
                      value={item}
                      onDetailView={() => handleDetailView(item.id)}
                      type="ADMIN-CHAPTER"
                      name={item.name}
                      chapterID={item.id}
                    />
                  ) : roleCurrent === "ADMIN" && type === "LESSON" ? (
                    <CustomizedMenus
                      value={item}
                      onDetailView={() => handleDetailView(item.id)}
                      type="ADMIN-LESSON"
                      name={item.name}
                      lessonID={item.id}
                    />
                  ) : (
                    <CustomizedMenus
                      value={item}
                      viewType={type}
                      onDetailView={() => handleDetailView(item.id)}
                      type="MODERATOR-FOLDER-TAG"
                      folderID={item.id}
                    />
                  )}
                </div>
              );
            default:
              return cellValue;
          }
        }, [])
      : useFor === "ADMIN-USER"
      ? React.useCallback((user, columnKey) => {
          const cellValue = user[columnKey];

          switch (columnKey) {
            case "username":
              return (
                <AvatarAndInfoDiv
                  avatarsrc={user.avatar}
                  title1={user.username}
                  title2={user.email}
                />
              );
            case "firstname":
              return cellValue;
            case "lastname":
              return cellValue;
            case "dateOfBirth":
              return cellValue;
            case "createdAt":
              const date = format(
                new Date(user.createdAt),
                "MM/dd/yyyy hh:mm:ss"
              );
              return <Box title="ngày/giờ">{date}</Box>;
            case "roleDTOResponses":
              return user.roleDTOResponses?.map((role, index) =>
                index === user.roleDTOResponses?.length - 1
                  ? role.roleName + ""
                  : role.roleName + " / "
              );

            case "active":
              return (
                <Chip
                  className="capitalize"
                  color={user.active ? "success" : "danger"}
                  size="sm"
                  variant="flat"
                >
                  {user.active ? (
                    <TbPointFilled style={{ color: "green" }} />
                  ) : (
                    <TbPointFilled style={{ color: "red" }} />
                  )}
                </Chip>
              );
            case "actions":
              return (
                <div className="relative flex justify-end items-center gap-2">
                  <CustomizedMenus
                    id={user.id}
                    type="ADMIN-USER"
                    name={user.email}
                    value={user}
                  />
                </div>
              );
            default:
              return cellValue;
          }
        }, [])
      : useFor === "ADMIN-ROLE"
      ? React.useCallback((role, columnKey) => {
          const cellValue = role[columnKey];

          switch (columnKey) {
            case "roleName":
              return cellValue;
            case "description":
              return cellValue;

            case "createdAt":
              const date = format(
                new Date(role.createdAt),
                "MM/dd/yyyy hh:mm:ss"
              );
              return <Box title="ngày/giờ">{date}</Box>;

            case "creator":
              return cellValue;

            case "active":
              return (
                <Chip
                  className="capitalize"
                  color={role.active ? "success" : "danger"}
                  size="sm"
                  variant="flat"
                >
                  {role.active ? "Hoạt động" : "Không hoạt động"}
                </Chip>
              );
            case "actions":
              return (
                <div className="relative flex justify-end items-center gap-2">
                  <CustomizedMenus
                    value={role}
                    type="ADMIN-ROLE"
                    id={role.roleId}
                    name={role.roleName}
                  />
                </div>
              );
            default:
              return cellValue;
          }
        }, [])
      : useFor === "ADMIN-PERMISSION"
      ? React.useCallback((permission, columnKey) => {
          const cellValue = permission[columnKey];

          switch (columnKey) {
            case "permissionName":
              return cellValue;
            case "path":
              return cellValue;
            case "methodType":
              return cellValue;
            case "description":
              return cellValue;

            case "createdAt":
              const date = format(
                new Date(permission.createdAt),
                "MM/dd/yyyy hh:mm:ss"
              );
              return <Box title="ngày/giờ">{date}</Box>;

            case "creator":
              return cellValue;

            case "active":
              return (
                <Chip
                  className="capitalize"
                  color={permission.active ? "success" : "danger"}
                  size="sm"
                  variant="flat"
                >
                  {permission.active ? "Hoạt động" : "Không hoạt động"}
                </Chip>
              );
            case "actions":
              return (
                <div className="relative flex justify-end items-center gap-2">
                  <CustomizedMenus
                    value={permission}
                    type="ADMIN-UPDATE-PERMISSION"
                  />
                </div>
              );
            default:
              return cellValue;
          }
        }, [])
      :useFor === "REPORT-COMMENT"
      ? React.useCallback((comment, columnKey) => {
          const cellValue = comment[columnKey];

          switch (columnKey) {
            case "content":
              return cellValue;
            case "commenter":
              return cellValue;
            case "createdAt":
              const date = format(
                new Date(comment.createdAt),
                "MM/dd/yyyy hh:mm:ss"
              );
              return <Box title="ngày/giờ">{date}</Box>;

            // case "active":
            //   return (
            //     <Chip
            //       className="capitalize"
            //       color={permission.active ? "success" : "danger"}
            //       size="sm"
            //       variant="flat"
            //     >
            //       {permission.active ? "Hoạt động" : "Không hoạt động"}
            //     </Chip>
            //   );
            case "actions":
              return (
                <div className="relative flex justify-end items-center gap-2">
                  <CustomizedMenus
                    value={comment}
                    type="MODERATOR-VIEW-COMMENT"
                  />
                </div>
              );
            default:
              return cellValue;
          }
        }, [])
      : React.useCallback((material, columnKey) => {
          const cellValue = material[columnKey];

          switch (columnKey) {
            case "name":
              return (
                <RouterLink
                  router={router}
                  url={`../../../detail_material?id=${
                    material.id ? material.id : material.resourceId
                  }`}
                  title={material.name.length > 15 ?  material.name.slice(0, 19)+"...": material.name } 
                  description={material.description}
                />
              );

            case "resourceType":
              return (
                <CenterFlexBox>
                  {typeFileMap[material.resourceType]
                    ? typeFileMap[material.resourceType]
                    : typeFileMap["OTHER"]}
                </CenterFlexBox>
              );
            case "createdAt":
              const date = format(
                new Date(material.createdAt),
                "MM/dd/yyyy hh:mm:ss"
              );
              return <Box title="ngày/giờ">{date}</Box>;

            case "visualType":
              // //.log("amMine2",amMine)
              return amMine ? (
                <CenterFlexBox>
                  {accessMap[cellValue]}
                  <CustomizedMenus
                    type="SET-VISUAL-TYPE"
                    isIcon={true}
                    id={material.id ? material.id : material.resourceId}
                  />
                </CenterFlexBox>
              ) : (
                <CenterFlexBox>{accessMap[cellValue]}</CenterFlexBox>
              );

            case "approveType":
              return cellValue ? (
                <Chip
                  className="capitalize"
                  color={statusColorMap[material.approveType]}
                  size="sm"
                  variant="flat"
                >
                  {cellValue === "ACCEPTED"
                    ? "Đã kiểm duyệt"
                    : cellValue === "UNACCEPTED"
                    ? "Chưa kiểm duyệt"
                    : cellValue === "REJECT"
                    ? "Bị từ chối"
                    : ""}
                </Chip>
              ) : (
                ""
              );
            case "actions":
              //.log("type",useFor)
              return (
                <>
                  {useFor === "MY-SAVED"   ? (
                    <>
                       <CustomizedMenus
                          type="MY-SAVED"
                          value={material}
                        />
                    </>
                  )  :useFor === "SHARE-FOR-ME"   ? (
                    <>
                       <CustomizedMenus
                          type="SHARE-FOR-ME"
                          value={material}
                        />
                    </>
                  )  :useFor?.includes("MODERATOR-REPORT")  ? (
                    <>
                       <CustomizedMenus
                          type="MODERATOR-VIEW-MATERIAL-REPORT"
                          value={material}
                        />
                    </>
                  )  :useFor === "MODERATOR" && uri?.includes("review")  ? (
                    <>
                       
                    </>
                  )  : (
                    <div className="relative flex justify-end items-center gap-2">
                      {material.approveType === "REJECT" && useFor !== "MODERATOR-REPORT" ? (
                        <CustomizedMenus
                          type="UPDATE-DELETE"
                          value={material}
                          name={material.name}
                        />
                      ) : material.approveType === "UNACCEPTED" && !useFor?.includes("MODERATOR-REPORT")? (
                        <CustomizedMenus
                          value={material}
                          id={material?.resourceId}
                          type="UPDATE-DELETE"
                          name={material.name}
                        />
                        ) : material.approveType === "ACCEPTED" &&  useFor!=="MODERATOR" ? (

                        <CustomizedMenus
                          value={material}
                          id={material?.resourceId}
                          type="SHARE"
                          name={material.name}
                        />
                      ):material.approveType === "ACCEPTED" && useFor==="MODERATOR"?<CustomizedMenus
                      value={material}
                      id={material?.resourceId}
                      type="UPDATE-SHARE-DELETE"
                      name={material.name}
                    />:""}
                    </div>
                  )}
                </>
              );
            default:
              return cellValue;
          }
        }, [amMine,uri,useFor]);

  const onNextPage = React.useCallback(() => {
    if (page < pages) {
      handleSetPage(page + 1);
    }
  }, [page, pages]);

  const onPreviousPage = React.useCallback(() => {
    if (page > 1) {
      handleSetPage(page - 1);
    }
  }, [page]);

  const onRowsPerPageChange = React.useCallback((e) => {
    setRowsPerPage(Number(e.target.value));
    handleSetPage(1);
  }, []);

  const onSearchChange = React.useCallback((value) => {
    if (value) {
      setNameFilter(value);

      handleSetPage(1);
    } else {
      setNameFilter("");
    }
  }, []);

  const onClear = React.useCallback(() => {
    setNameFilter("");
    handleSetPage(1);
  }, []);

  const topContent = React.useMemo(() => {
    return (
      <>
        <div className="flex flex-col gap-4">
          {useFor === "TEACHER" ? (
            ""
          ) : (
            <div className="flex gap-2">
              {nextTabIndex === 22 ||
              nextTabIndex === 23 ||
              nextTabIndex === 24 ||
              nextTabIndex === 25 ||
              nextTabIndex === 26 ? (
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => handleView(roleCurrent === "ADMIN" ? 20 : 5)}
                >
                  Quản lý lớp học &#8250;{" "}
                </div>
              ) : (
                ""
              )}
              {nextTabIndex === 22 ||
              nextTabIndex === 23 ||
              nextTabIndex === 24 ||
              nextTabIndex === 25 ||
              nextTabIndex === 26 ? (
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => handleView(21)}
                >
                  {viewFolder?.class.name} &#8250;
                </div>
              ) : (
                ""
              )}
              {nextTabIndex === 23 ||
              nextTabIndex === 24 ||
              nextTabIndex === 25 ||
              nextTabIndex === 26 ? (
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => handleView(22)}
                >
                  {viewFolder?.bookSeries?.name} &#8250;
                </div>
              ) : (
                ""
              )}
              {nextTabIndex === 24 ||
              nextTabIndex === 25 ||
              nextTabIndex === 26 ? (
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => handleView(23)}
                >
                  {viewFolder?.subject?.name} &#8250;
                </div>
              ) : (
                ""
              )}
              {nextTabIndex === 25 || nextTabIndex === 26 ? (
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => handleView(24)}
                >
                  {viewFolder?.bookVolume?.name} &#8250;
                </div>
              ) : (
                ""
              )}
              {nextTabIndex === 26 ? (
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => handleView(24)}
                >
                  {viewFolder?.chapter?.name} &#8250;
                </div>
              ) : (
                ""
              )}
              {nextTabIndex === 26 ? <div>{viewFolder?.lesson?.name}</div> : ""}
            </div>
          )}
          <div className="flex justify-between gap-3 items-end">
            <div></div>
            <div className="flex gap-4">
              {useFor === "ADMIN-USER" ? (
                <>
                  <Dropdown>
                    <DropdownTrigger className="hidden sm:flex">
                      <Button
                        endContent={<BiChevronDown className="text-small" />}
                        variant="flat"
                      >
                        {Array.from(roleId)[0] === "" ||
                        Array.from(roleId)[0] === ""
                          ? "Vai Trò"
                          : roles.find(
                              (role) => role.roleId == Array.from(roleId)[0]
                            )?.roleName}
                      </Button>
                    </DropdownTrigger>
                    <DropdownMenu
                      aria-label="Table Columns"
                      closeOnSelect={true}
                      selectedKeys={roleId}
                      selectionMode="single"
                      onSelectionChange={setRoleId}
                    >
                      <DropdownItem key="" textValue="" className="capitalize">
                        {capitalize("Tất cả")}
                      </DropdownItem>
                      {roles.length === 0
                        ? ""
                        : roles.map((role) => (
                            <DropdownItem
                              key={role.roleId}
                              textValue={role.roleId}
                              className="capitalize"
                            >
                              {capitalize(role.roleName)}
                            </DropdownItem>
                          ))}
                    </DropdownMenu>
                  </Dropdown>
                  <Dropdown>
                    <DropdownTrigger className="hidden sm:flex">
                      <Button
                        endContent={<BiChevronDown className="text-small" />}
                        variant="flat"
                      >
                        {Array.from(classId)[0] === "" ||
                        Array.from(classId)[0] === ""
                          ? "Lớp"
                          : classes.find(
                              (lop) => lop.id == Array.from(classId)[0]
                            )?.name}
                      </Button>
                    </DropdownTrigger>
                    <DropdownMenu
                      aria-label="Table Columns"
                      closeOnSelect={true}
                      selectedKeys={classId}
                      selectionMode="single"
                      onSelectionChange={setClassId}
                    >
                      <DropdownItem key="" textValue="" className="capitalize">
                        {capitalize("Tất cả")}
                      </DropdownItem>
                      {classes?.length === 0
                        ? ""
                        : classes?.map((lop) => (
                            <DropdownItem
                              key={lop.id}
                              textValue={lop.id}
                              className="capitalize"
                            >
                              {capitalize(lop.name)}
                            </DropdownItem>
                          ))}
                    </DropdownMenu>
                  </Dropdown>
                </>
              ) : useFor === "ADMIN-ROLE" ||
                useFor === "ADMIN-PERMISSION" ||
                useFor === "ADMIN"? (
                <>
                  <Dropdown>
                    <DropdownTrigger className="hidden sm:flex">
                      <Button
                        endContent={<BiChevronDown className="text-small" />}
                        variant="flat"
                      >
                        Trạng thái
                      </Button>
                    </DropdownTrigger>
                    <DropdownMenu
                      disallowEmptySelection
                      aria-label="Table Columns"
                      closeOnSelect={true}
                      selectedKeys={statusFilter}
                      selectionMode="single"
                      onSelectionChange={handleSetStatusFilter}
                    >
                      {statusOptions.map((status) => (
                        <DropdownItem
                          key={status.uid}
                          textValue={status.uid}
                          className="capitalize"
                        >
                          {capitalize(status.name)}
                        </DropdownItem>
                      ))}
                    </DropdownMenu>
                  </Dropdown>
                </>
              ): useFor === "REPORT-COMMENT" ?"" : (
                <>
                  <Dropdown>
                    <DropdownTrigger className="hidden sm:flex">
                      <Button
                        endContent={<BiChevronDown className="text-small" />}
                        variant="flat"
                      >
                        Định dạng file
                      </Button>
                    </DropdownTrigger>
                    <DropdownMenu
                      aria-label="Table Columns"
                      closeOnSelect={true}
                      selectedKeys={materialTypeFilter}
                      selectionMode="single"
                      onSelectionChange={handleSetMaterialTypeFilter}
                    >
                      {materialTypes.map((type) => (
                        <DropdownItem
                          key={type.uid}
                          textValue={type.uid}
                          className="capitalize"
                        >
                          {capitalize(type.name)}
                        </DropdownItem>
                      ))}
                    </DropdownMenu>
                  </Dropdown>
                  {useFor!=="MY-SAVED"&&<Dropdown>
                    <DropdownTrigger className="hidden sm:flex">
                      <Button
                        endContent={<BiChevronDown className="text-small" />}
                        variant="flat"
                      >
                        Quyền truy cập
                      </Button>
                    </DropdownTrigger>
                    <DropdownMenu
                      aria-label="Table Columns"
                      closeOnSelect={true}
                      selectedKeys={visualTypeFilter}
                      selectionMode="single"
                      onSelectionChange={handleSetVisualTypeFilter}
                    >
                      {visualTypes.map((type) => (
                        <DropdownItem
                          key={type.uid}
                          textValue={type.uid}
                          className="capitalize"
                        >
                          {capitalize(type.name)}
                        </DropdownItem>
                      ))}
                    </DropdownMenu>
                  </Dropdown>
                  }
                 {(!useFor?.includes("MODERATOR") && !uri.includes("review") ) && useFor!=="MY-SAVED"&& useFor!=="SHARE-FOR-ME" &&<Dropdown>
                    <DropdownTrigger className="hidden sm:flex">
                      <Button
                        endContent={<BiChevronDown className="text-small" />}
                        variant="flat"
                      >
                        Trạng thái
                      </Button>
                    </DropdownTrigger>
                    <DropdownMenu
                      disallowEmptySelection
                      aria-label="Table Columns"
                      closeOnSelect={true}
                      selectedKeys={statusFilter}
                      selectionMode="single"
                      onSelectionChange={handleSetStatusFilter}
                    >
                      {statusOptions.map((status) => (
                        <DropdownItem
                          key={status.uid}
                          textValue={status.uid}
                          className="capitalize"
                        >
                          {capitalize(status.name)}
                        </DropdownItem>
                      ))}
                    </DropdownMenu>
                  </Dropdown>}
                </>
              )}

              <Dropdown>
                <DropdownTrigger className="hidden sm:flex">
                  <Button
                    endContent={<BiChevronDown className="text-small" />}
                    variant="flat"
                  >
                    Cột thông tin
                  </Button>
                </DropdownTrigger>
                <DropdownMenu
                  disallowEmptySelection
                  aria-label="Table Columns"
                  closeOnSelect={false}
                  selectedKeys={visibleColumns}
                  selectionMode="multiple"
                  onSelectionChange={setVisibleColumns}
                >
                  {columns.map((column) => (
                    <DropdownItem key={column.uid} className="capitalize">
                      {capitalize(column.name)}
                    </DropdownItem>
                  ))}
                </DropdownMenu>
              </Dropdown>
              {roleCurrent === "ADMIN" && useFor === "ADMIN-PERMISSION" ? (
                <Button color="primary" endContent={<PiPlusCircleDuotone />}>
                  <FullAddPermissionDialog
                    style={{ color: "black" }}
                    addOrUpdate="ADD"
                  />
                </Button>
              ) : roleCurrent === "ADMIN" && useFor === "ADMIN-ROLE" ? (
                <Button color="primary" endContent={<PiPlusCircleDuotone />}>
                  <FullAddRoleDialog
                    style={{ color: "black" }}
                    addOrUpdate="ADD"
                  />
                </Button>
              ) : roleCurrent === "ADMIN" && type === "CLASS" ? (
                <Button color="primary" endContent={<PiPlusCircleDuotone />}>
                  <FullAddClassDialog
                    style={{ color: "black" }}
                    addOrUpdate="ADD"
                  />
                </Button>
              ) : roleCurrent === "ADMIN" && type === "BOOK-SERIES" ? (
                <Button color="primary" endContent={<PiPlusCircleDuotone />}>
                  <FullAddBookSeriesDialog
                    classID
                    style={{ color: "black" }}
                    addOrUpdate="ADD"
                  />
                </Button>
              ) : roleCurrent === "ADMIN" && type === "SUBJECT-BOOK-SERIES" ? (
                <Button color="primary" endContent={<PiPlusCircleDuotone />}>
                  <FullAddSubjectBookSeriesDialog
                    classID={viewFolder?.class.id}
                    bookSeriesID={viewFolder?.bookSeries.id}
                    name="Thêm môn học vào bộ sách"
                    style={{ color: "black" }}
                    addOrUpdate="ADD"
                  />
                </Button>
              ) : roleCurrent === "ADMIN" && type === "SUBJECT" ? (
                <Button color="primary" endContent={<PiPlusCircleDuotone />}>
                  <FullAddSubjectDialog
                    name="Thêm môn học"
                    style={{ color: "black" }}
                    addOrUpdate="ADD"
                  />
                </Button>
              ) : roleCurrent === "ADMIN" && type === "BOOK-VOLUME" ? (
                <Button color="primary" endContent={<PiPlusCircleDuotone />}>
                  <FullAddBookVolumeDialog
                    name="Thêm đầu sách"
                    style={{ color: "black" }}
                    addOrUpdate="ADD"
                  />
                </Button>
              ) : roleCurrent === "ADMIN" && type === "CHAPTER" ? (
                <Button color="primary" endContent={<PiPlusCircleDuotone />}>
                  <FullAddChapterDialog
                    name="Thêm chương"
                    style={{ color: "black" }}
                    addOrUpdate="ADD"
                  />
                </Button>
              ) : roleCurrent === "ADMIN" && type === "LESSON" ? (
                <Button color="primary" endContent={<PiPlusCircleDuotone />}>
                  <FullAddLessonDialog
                    name="Thêm bài học"
                    style={{ color: "black" }}
                    addOrUpdate="ADD"
                  />
                </Button>
              ) : roleCurrent === "ADMIN" && type === "ADMIN-PERMISSION" ? (
                <Button color="primary" endContent={<PiPlusCircleDuotone />}>
                  <FullSetPermissionDialog
                    name="Thêm quyền"
                    style={{ color: "black" }}
                    addOrUpdate="ADD"
                  />
                </Button>
              ) : (
                ""
              )}
            </div>
          </div>
          <div className="flex flex-col gap-4 items-end">
            <Input
              isClearable
              className="w-full sm:max-w-[50%]"
              placeholder="Tìm kiếm theo tên, nội dung..."
              startContent={<Search />}
              value={nameFilter}
              onClear={() => onClear()}
              onValueChange={onSearchChange}
            />
          </div>
          <div className="flex justify-between items-center">
            <span className="text-default-400 text-small">
              Tổng có {totalElement} hàng dữ liệu
            </span>
            <label className="flex items-center text-default-400 text-small">
              Số hàng dữ liệu trên 1 trang:
              <select
                className="bg-transparent outline-none text-default-400 text-small"
                onChange={onRowsPerPageChange}
              >
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
              </select>
            </label>
          </div>
        </div>
      </>
    );
  }, [
    materialTypeFilter,
    statusFilter,
    visibleColumns,
    onRowsPerPageChange,
    nameFilter,
    onSearchChange,
    hasSearchFilter,
    visualTypeFilter,
    myMaterial,
  ]);

  const bottomContent = React.useMemo(() => {
    return (
      <div className="py-2 px-2 flex justify-between items-center">
        <span className="w-[30%] text-small text-default-400"></span>
        <Pagination
          isCompact
          showControls
          showShadow
          color="primary"
          page={page}
          total={pages}
          onChange={handleSetPage}
        />
        <div className="hidden sm:flex w-[30%] justify-end gap-2">
          <Button
            isDisabled={pages === 1}
            size="sm"
            variant="flat"
            onPress={onPreviousPage}
          >
            Trước
          </Button>
          <Button
            isDisabled={pages === 1}
            size="sm"
            variant="flat"
            onPress={onNextPage}
          >
            Sau
          </Button>
        </div>
      </div>
    );
  }, [selectedKeys, page, pages, hasSearchFilter]);

  return (
    <>
      {isShowFilter ? (
        <FilterModerate
          classID={classID}
          bookSeriesID={bookSeriesID}
          subjectID={subjectID}
          bookVolumeID={bookVolumeID}
          chapterID={chapterID}
          lessonID={lessonID}
          handleChangeClass={setClassID}
          handleChangeBookSeries={setBookSeriesID}
          handleChangeSubject={setSubjectID}
          handleChangeBookVolume={setBookVolumeID}
          handleChangeChapter={setChapterID}
          handleChangeLesson={setLessonID}
        />
      ) : (
        ""
      )}

      <Table
        aria-label="Example table with custom cells, pagination and sorting"
        isHeaderSticky
        bottomContent={bottomContent}
        bottomContentPlacement="outside"
        classNames={{
          wrapper: "max-h-[382px]",
        }}
        // selectedKeys={selectedKeys}
        // selectionMode="multiple"
        sortDescriptor={sortDescriptor}
        topContent={topContent}
        topContentPlacement="outside"
        onSelectionChange={setSelectedKeys}
        onSortChange={setSortDescriptor}
        color="success"
        selectionMode="single"
      >
        <TableHeader columns={headerColumns}>
          {(column) => (
            <TableColumn
              key={column.uid}
              align={column.uid === "actions" ? "center" : "start"}
              allowsSorting={column.sortable}
            >
              {column.name}
            </TableColumn>
          )}
        </TableHeader>
        <TableBody
          emptyContent={isLoading ? "Đang tải dữ liệu" : "Không thấy dữ liệu"}
          items={sortedItems}
          isLoading={isLoading}
          loadingContent={<RingLoader color="#198754" />}
        >
          {(item) => (
            <TableRow
              key={
                useFor === "MODERATOR-REPORT-MATERIAL"
                  ? item.id
                  : useFor === "ADMIN-ROLE"
                  ? item.roleId
                  : useFor === "ADMIN-PERMISSION"
                  ? item.systemPermissionId
                  : useFor === "REPORT-COMMENT"
                  ? item.commentId
                  : item.resourceId
              }
            >
              {(columnKey) => (
                <TableCell>{renderCell(item, columnKey)}</TableCell>
              )}
            </TableRow>
          )}
        </TableBody>
      </Table>
    </>
  );
}
