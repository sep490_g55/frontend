import React from "react";
const columns = [
  { name: "STT", uid: "no", sortable: true },
  { name: "Mô tả", uid: "description", sortable: true },
  { name: "Tên file", uid: "resourceName", sortable: true },
  { name: "Tên", uid: "name", sortable: true },
  { name: "Định dạng file", uid: "resourceType", sortable: true },
  { name: "Ngày tạo", uid: "createdAt", sortable: true },
  { name: "Quyền truy cập", uid: "visualType", sortable: true },
  { name: "Trạng thái", uid: "approveType", sortable: true },
  { name: "", uid: "actions" },
];

const statusOptions = [
  {name: "Tất cả", uid: ""},
  {name: "ACCEPTED", uid: "ACCEPTED"},
  {name: "UNACCEPTED", uid: "UNACCEPTED"},
  {name: "REJECT", uid: "REJECT"},
];
const materialTypes = [
  {name: "Tất cả", uid: ""},
  {name: "PPTX", uid: "PPTX"},
  {name: "PDF", uid: "PDF"},
  {name: "DOCX", uid: "DOCX"},
  {name: "JPG", uid: "JPG"},
  {name: "PNG", uid: "PNG"},
  {name: "JPEG", uid: "JPEG"},
  {name: "MP3", uid: "MP3"},
  {name: "MP4", uid: "MP4"},

 
];
const visualTypes = [
  {name: "Tất cả", uid: ""},
  {name: "PUBLIC", uid: "PUBLIC"},
  {name: "RESTRICT", uid: "RESTRICT"},
  {name: "PRIVATE", uid: "PRIVATE"},


 
];


export {columns,materialTypes, statusOptions,visualTypes};
