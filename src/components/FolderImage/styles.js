import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import Button from '@mui/material/Button';
export const ContainerButtonFilterMedia = styled('div')(({ theme }) => ({
  width: '100%',
  
}));

export const ButtonFilterMedia = styled('button')(({ theme }) => ({
    width: '100px',
    margin: '10px',
    padding: '5px',
    borderRadius: '15px',
    border: 'none',
display:'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection:"row",
  }));
  export const SelButtonFilterMedia = styled('button')(({ theme }) => ({
    width: '150px',
    margin: '10px',
    padding: '5px',
    borderRadius: '15px',
    border: 'none',
    backgroundColor:'#14606C'
  }));

  export const InfoImageDiv = styled('div')(({ theme }) => ({
    width: '100%',
    height: 'auto',
    marginBottom: '30px',
  display:'flex',
  justifyContent: 'center',
  alignItems: 'center',
  }));

  export const ActionDiv = styled('div')(({ theme }) => ({
    float:'clear',
    width: '100%',
    height: 'auto',
    margin: '20px ',
  display:'flex',
  justifyContent: 'space-between',

  }));

  export const ButtonAction = styled('button')(({ theme }) => ({
   backgroundColor:'#f0f0f0',
    border: 'none',
    width: '90px',
    height: '35px',
    borderRadius: '17px',
    color: 'black',
    marginLeft: '5px',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    '&:hover':{
      backgroundColor: '#F2F2F2',
      fontWeight: 'bold'
    },
    '&:focus':{
      fontWeight: 'none'
    }
  }));
  export const NormalButton = styled('button')(({ theme }) => ({
    backgroundColor:'inherit',
     border: '1px solid #EEEEEF',
     width: 'fit-content',
     padding:"5px",
     borderRadius: '9px',
     color: 'black',
     margin: '15px',
     display: 'flex',
     justifyContent: 'center',
     alignItems: 'center',
     '&:hover':{
      border: '1px solid #050505',
     },
     
   }));

  export const GreenButtonAction = styled('button')(({ theme }) => ({
    backgroundColor:'#198754',
     border: 'none',
     width: '100px',
     height: '35px',
     borderRadius: '8px',
     color: 'white',
     marginLeft: '15px',
     cursor: 'pointer',
     display:'flex',
  justifyContent:"space-evenly",
  alignItems:"center",
     '&:hover':{
      //  backgroundColor: '#ceced8',
       fontWeight: 'bold'
     },
     '&:focus':{
       fontWeight: 'none'
     }
   }));

   export const BrGreenButtonAction = styled('button')(({ theme }) => ({
    backgroundColor:'#198754',
     border: 'none',
     color: 'white',
     width: '100px',
     height: '35px',
     borderRadius: '25px',
    display:"flex",
    justifyContent:"center",
    alignItems:"center",
     cursor: 'pointer',
     '&:hover':{
       backgroundColor: '#198754',
       color: 'white',
     },
     '&:focus':{
       fontWeight: 'none'
     }
   }));
   export const HoverBrGreenButtonAction = styled('button')(({ theme }) => ({
    
    width: "100%", 
    height: "48px",
     borderRadius: "15px"
     ,backgroundColor:"white",
     border:"1px solid #198754",
     color:"#198754",fontWeight:"bold",
    display:"flex",
    justifyContent:"center",
    alignItems:"center",
     cursor: 'pointer',
     '&:hover':{
       backgroundColor: '#198754',
       color: 'white',
     },
     '&:focus':{
       fontWeight: 'none'
     }
   }));
   export const IconButtonAction = styled('button')(({ theme }) => ({
    backgroundColor:'inherit',
     border: 'none',
     width: '50px',
     height: '50px',
     borderRadius: '10px',
     color: 'black',
    position: 'relative',
    zIndex: 222,
     cursor: 'pointer',
     '&:hover':{
      border: '1px solid #DEDEDE',
       fontWeight: 'bold',
       fontSize: '18px'
     },
     '&:focus':{
       fontWeight: 'none'
     }
   }));

   export const ImgItem = styled('img')(({ theme }) => ({
   
     borderRadius: '10px',
     
     '&:hover':{
      // transition : scale(1.1);
     },
     '&:focus':{
      
     }
   }));