import { Box, Container, ImageList, ImageListItem } from "@mui/material";
import { useEffect, useState } from "react";

import { useRouter } from "next/router";
//icon
import { AiOutlineCamera, AiOutlineVideoCamera } from "react-icons/ai";
import { BsDownload, BsBookmark, BsShareFill } from "react-icons/bs";
import { BiInfoCircle } from "react-icons/bi";

// react-bootstrap
import { Image } from "react-bootstrap";

// style component
import { ImgItem, InfoImageDiv, NormalButton } from "@/components/FolderImage/styles";
import { ActionDiv, ButtonAction } from "./styles";
import { useSelector } from "react-redux";
import SpeedDialTooltipOpen from "../action/SpeedDial";
import LoadingTab from "../loading-screen/LoadingTab";
import { searchKhoMedia,getSuggestResourcesByResourceId } from "@/dataProvider/agent";
import React from "react";
import { Button, Pagination } from "@nextui-org/react";
import { BeatLoader } from "react-spinners";
import { CenterFlexBox } from "../common_sections";

export const FolderImage = ({type}) => {
  const route = useRouter();
  
  const [medias, setMedias] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  const {
    query: { search },
  } = route;
  const tagList = useSelector(state => state.tags.tags);
  const searchInputText = useSelector(state => state.searchInputText.text);
  const isSearch = useSelector(state => state.reloading.isSearch);

  // const [inputText, setInputText] = useState(search?search:searchInputText)
  // console.log("searchInputTex 333",searchInputText);
  const [page, setPage] = React.useState(1);
  const [pages, setPages] = React.useState(1); 
  useEffect(() => {
    getMedia()
    
  }, [tagList,type,isSearch,page]);
  
  const getMedia = async () =>{
    setIsLoading(true)

  let arrTag = "";
  if(tagList){
    
    tagList.map(tag => arrTag += tag.tagId+"," )
    // console.log("arrTag",arrTag);
  }
  console.log("searchInputText",searchInputText);

    // const api  = `resource/medias?listTags=${arrTag}&tabResourceType=${type}&name=${searchInputText}`
    try{
      const res = await searchKhoMedia(arrTag === undefined? { tabResourceType: type, name: searchInputText,pageIndex:page,pageSize:50}: {listTags:arrTag, tabResourceType: type, name: searchInputText,pageIndex:page,pageSize:50});
      if(res.status ===200){
      // console.log("get img: ",res.data)
      setMedias(res.data.data)
      setPages(res.data.totalPage)
      // setTags(res);
      }
    }catch(e){
      console.log("e",e)
    }
    setIsLoading(false)
  };

// page
const handleSetPage = (index) => {
  setPage(index);
};
const onNextPage = React.useCallback(() => {
  if (page < pages) {
    handleSetPage(page + 1)

  }
}, [page, pages]);

const onPreviousPage = React.useCallback(() => {
  if (page > 1) {
    handleSetPage(page - 1)
    // dispatch(setPageIndex(page - 1))

  }
},[page, pages])

  return (
    <>
    <Container>
      <Box sx={{ color:"gray",marginBottom:"10px" }}>Thông tin liên quan tới: 
      <Box sx={{ color:"#198754",  display:"flex" }}>
      {/* {searchInputText} */}
      {/* {tagList.length ===  0 ? "":tagList.map((tag,index) => <NormalButton key={index}>{tag.tagName}</NormalButton> )} */}
      </Box>
      </Box>
      {/* <Masonry data={medias} column={columns} /> */}
      <Box sx={{ width: "100%", height: "auto",mb:5 }}>
        {/* {isLoading&&<LoadingTab/>} */}
        {
          medias.length > 0 && isLoading 
          ? <><CenterFlexBox>
                <BeatLoader
              color="#198754"
              size={5}
              style={{ marginLeft: "5px", marginTop: "5px",marginBottom:"10px" }}
            />
            </CenterFlexBox> <ListMedia list={medias} cols={5} gap={8} type={type}/></>
          : medias.length === 0 && isLoading ?<LoadingTab/>
          : medias.length > 0 && !isLoading ? <ListMedia list={medias} cols={5} gap={8} type={type}/> :""
        }
    
     </Box>
     
    </Container>
    <div className="w-[100%] my-8 py-2 px-2 flex justify-between items-center  ">
    <span className="w-[30%] text-small text-default-400">
     
    </span>
    <Pagination
      isCompact
      showControls
      showShadow
      color="primary"
      page={page}
      total={pages}
      onChange={handleSetPage}
    />
    <div className="hidden sm:flex w-[30%] justify-end gap-2">
      <Button
        size="sm"
        variant="flat"
        onClick={onPreviousPage}
        disabled={page ===1}
      >
        Trước
      </Button>
      <Button
        size="sm"
        variant="flat"
        onClick={onNextPage}
        disabled={page ===pages}

      >
        Sau
      </Button>
    </div>
  </div>
  </>
  );
};
export const ListMedia = ({list,cols,gap,type})=>{
  const route = useRouter();

  return(
    <ImageList variant="masonry" cols={cols} gap={gap}>
       
         {list?.map((item) => (
          <ImageListItem key={item.id} onClick={()=> route.push(`/detail_material?id=${item.id}`)} sx={{cursor:"pointer"}}>
           {type === "IMAGE" ? <ImgItem
              srcSet={`${item.thumbnailSrc}`}
              src={`${item.thumbnailSrc}`}
              alt={item.thumbnailSrc}
              loading="lazy"
              title={item.name}
            />
            :
              <video key={item.id} controls width="600" height="400" poster={item.thumbnailSrc}>
                <source src={item.resourceSrc}/>
              </video>
            
          } 
          </ImageListItem>
        ))}
      </ImageList>
  )

}
export const SuggestMedias = ({resourceId,type}) =>{
  const [materials, setMaterial] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const getSuggestMaterial = async()=>{
    setIsLoading(true)
    const res = await getSuggestResourcesByResourceId(resourceId);
    if(res.status === 200){
        setMaterial(res?.data)
    }
    setIsLoading(false)

  }
  useEffect(() => {
    getSuggestMaterial();
  }, [resourceId]);
  return(
    <>
      {isLoading?<BeatLoader
          color="#198754"
          size={5}
          style={{marginLeft:"5px",marginTop:"5px"}}
        />:
          <ListMedia
          list={materials}
          cols={5}
          gap={8}
          type={type}
        />
      }
    </>
  )
}

export const FilterMedia = ({setType,type}) => {
  return (
    <Box sx={{display:'flex',
    justifyContent: 'center',
    alignItems: 'center',}}>
      <NormalButton onClick={() =>setType("IMAGE")}>
        <AiOutlineCamera /> <Box sx={{marginLeft:"10px",fontWeight:"bold", color: type === "IMAGE" ? "#198754" :"gray"}}>Hình ảnh</Box>
      </NormalButton>
      <NormalButton onClick={() =>setType("VIDEO")}>
        <AiOutlineVideoCamera /> <Box sx={{marginLeft:"10px",fontWeight:"bold",color: type === "VIDEO" ? "#198754" :"gray"}}>Video</Box>
      </NormalButton>
    </Box>
  );
};

export const InfoImage = ({ media }) => {
  const actions = [
    { icon: <BsDownload />, name: 'Tải về' },
    { icon: <BsBookmark />, name: 'Save' },    
    { icon: <BsShareFill />, name: 'Chia sẻ' },
  ];
  return (
    <InfoImageDiv>
      <Box sx={{ width: "50%" }}>
        <SpeedDialTooltipOpen actions={actions}/>
        <Image width="100%" src={media.resourceSrc} />
      </Box>
    </InfoImageDiv>
  );
};

export const ActionImage = ({ isExpand, onExpand }) => {
  const styleButton = !isExpand
    ? { backgroundColor: "#2196f3", color: "white" }
    : { display: "none" };

    //dialog
    const [open, setOpen] = useState(false);
    const handleOpen = () => {
      setOpen(true);
    };
    const handleClose = () => {
      setOpen(false);
    };

    const handleSubmit = async () => {
      // nếu thành công thì setOpent false
      setOpen(false);
      toast.success("Báo cáo thành công");
      // toast.error("Đăng nhập không thành công")
    };
    const title = "Chia sẻ tài liệu: "

    //end dialog
  return (
    <ActionDiv>
      <ButtonAction
        type="button"
        title="Hiển thị thông tin chi tiết"
        style={styleButton}
        onClick={onExpand}
      >
        <BiInfoCircle /> Chi tiết
      </ButtonAction>
      
    
    </ActionDiv>
  );
};
