import React from "react";
import {
  Table,
  TableHeader,
  TableColumn,
  TableBody,
  TableRow,
  TableCell,
  Input,
  Button,
  DropdownTrigger,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Chip,
  User,
  Pagination,
} from "@nextui-org/react";
import { PiPlusCircleDuotone } from "react-icons/pi";

export const NextUISelectSection = ({label,list}) =>{
    return (
      <div className="flex w-full flex-wrap md:flex-nowrap gap-4">
        <Select 
          label={label}
          className="max-w-xs" 
        >
          {list.map((elm,index) => (
            <SelectItem key={index} value={elm.name}>
              {elm.name}
            </SelectItem>
          ))}
        </Select>     
      </div>
    )
  }
  
  export const NextUIButton = ({title}) =>{
    return(
      <Button color="primary" endContent={<PiPlusCircleDuotone />}>
                {title}
              </Button>
    )
  }