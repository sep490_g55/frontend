import { useState, useEffect } from 'react';
import { m } from 'framer-motion';
// @mui
import { alpha, styled } from '@mui/material/styles';
import { Box, CircularProgress } from '@mui/material';
//
import Logo from '@/components/logo/Logo';
import { RingLoader } from 'react-spinners';

// ----------------------------------------------------------------------

const StyledRoot = styled('div')(({ theme }) => ({
    
  width: '100%',
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection:"column",
  backgroundColor: theme.palette.background.default,
}));

// ----------------------------------------------------------------------

export default function LoadingTab() {
  const [mounted, setMounted] = useState(false);

  useEffect(() => setMounted(true), []);

  if (!mounted) {
    return null;
  }

  return (
    <StyledRoot>
      <Box sx={{ display: 'flex',color:"#198754" }}>
      <RingLoader color="#198754" />
    </Box>
    <Box sx={{ display: 'flex',color:"#198754" }}>
      Vui lòng chờ...
    </Box>
    </StyledRoot>
  );
}
