import React, { useState,useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BsTrophy } from "react-icons/bs";
import { BsBookmark } from "react-icons/bs";
import { FaFilePowerpoint } from "react-icons/fa";
import {
  TableMaterialDiv,
  ItemMaterial,
  ImgItemMaterial,
  SectionItemMaterial,
  SupperItemMaterial,
  SkeletonImgItemMaterial,
  SkeletonItemMaterial,
  ContainerPagingDiv,
} from "./styles";
import { Row, Col } from "react-bootstrap";
import { Box, Button, Grid, Skeleton, Typography } from "@mui/material";
import Link from "next/link";
import { useRouter } from "next/router";
import {useDispatch, useSelector } from "react-redux";
import {
Pagination
} from "@nextui-org/react";
import { BeatLoader } from "react-spinners";
import { MdRemoveRedEye } from "react-icons/md";
import { CenterFlexBox } from "../common_sections";
import { setPageIndex, setViewClass, setViewLesson, setViewChapter } from '@/redux/slice/NewMaterialSlice';
import { ListMedia } from "../FolderImage/section";
export const TableMaterial = ({isLoading,slideThumbnails,styleForDetailMaterial}) => {
  const materialData = useSelector(state => state.materials.material)
  const [materials, setMaterial] = useState(slideThumbnails);
  const newMaterial = useSelector(state => state.newMaterial.material)

  const [page, setPage] = React.useState(1);
  const [pages, setPages] = React.useState(isLoading ? 1 : (slideThumbnails === null ? 1: slideThumbnails.totalPage));  // pages is totalPage within filtered
    useEffect(()=>{
      // console.log("slideThumbnails",slideThumbnails)
      // console.log("materialData",materialData)

      setPages(slideThumbnails?.totalPage);
      // setPage(1);

    },[slideThumbnails,])
 
  const dispatch = useDispatch();

  // page
  const handleSetPage = (index) => {
    setPage(index);
    dispatch(setPageIndex(index))
  };
  const onNextPage = React.useCallback(() => {
    if (page < pages) {
      handleSetPage(page + 1)
      dispatch(setPageIndex(page + 1))

    }
  }, [page, pages,newMaterial?.pageIndex]);

  const onPreviousPage = React.useCallback(() => {
    if (page > 1) {
      handleSetPage(page - 1)
      dispatch(setPageIndex(page - 1))

    }
  },[page, pages,newMaterial?.pageIndex])
  // end page
  // route
    const route = useRouter();
    const handleViewDetail = (id) =>{
      route.push({
        pathname: "/detail_material",
        query: { id: id },
        // hash: "detail-image",
      });
    }
  //end route
    // console.log("newMaterial?.type",newMaterial?.type === "IMAGE")

  // default-pdf-thumbnail
  //https://www.1access.com/wp-content/uploads/2019/10/GettyImages-1180389186.jpg 
  // default-ppt-thumbnail
  //https://s3.amazonaws.com/cms-assets.tutsplus.com/uploads/users/23/syllabuses/1207/image/how-to-use-microsoft-powerpoint-tutorials.jpg
  return (
    <>
    <TableMaterialDiv className="table-material" style={styleForDetailMaterial}>
      
      {materialData === null && isLoading ?
      
      <Grid container wrap="wrap">
      {Array.from(new Array(12)).map((item, index) => (
        <Box key={index} sx={{ width: 150, mr: 5,ml:5, my: 5 }}>
          
            <Skeleton variant="rectangular" width={150} height={118} />
          

       
            <Box sx={{ pt: 0.5 }}>
              <Skeleton />
              <Skeleton width="60%" />
            </Box>
          
        </Box>
      ))}
    </Grid>
    :materialData !== null && isLoading ? <> <Box sx={{textAlign:"center"}}><BeatLoader 
    color="#198754"
    size={5}
    style={{marginLeft:"5px",marginTop:"5px"}}
  /></Box>
  
  {newMaterial?.type === "IMAGE"?
    <ListMedia list={materialData.data} cols={5} gap={8} type="IMAGE"/>
    :materialData.data.map((material, index) => (
      <SupperItemMaterial key={index}>
      <ItemMaterial key={index} className="item" title={material.name} >
        <ImgItemMaterial src={material.thumbnailSrc} alt={material.name} onClick={() => handleViewDetail(material.id)}></ImgItemMaterial>
        <SectionItemMaterial className="title2-item">
            <Box
              sx={{
                
                color:'#198754',
                cursor: 'pointer',
                marginRight:"5px",
                fontSize:"14px",
              }}
              onClick={() => handleViewDetail(material.id)}
            >
              {material.name.length > 15 ?  material.name.slice(0, 19)+"...": material.name } 
            </Box>
            <Box
            
            >
              {/* <FaFilePowerpoint className="faBookmark" /> */}
            </Box>

            {/* BiSolidFileDoc
                      BiSolidFilePdf
                  */}
          </SectionItemMaterial>
        
            <SectionItemMaterial className="title2-item">
              {/* BsTrophyFill */}
              <CenterFlexBox
                style={{
                  width: "50px",
                  height: "50px",
                  cursor:  'pointer',
                  fontSize:"10px",
                  // margin: '10px',
                }}
                // onClick={onSave}
                title={`Đã có ${material.viewCount} lượt xem `}
              >
                <MdRemoveRedEye /><Box sx={{ml:"2px"}}>{material.viewCount}</Box>
              </CenterFlexBox>
              <CenterFlexBox
                style={{
                  padding: "10px",
                }}
              >
                {material.point === 0 ? <Box sx={{color:"blue",fontSize:"10px"}}>Miễn phí</Box> : <><Box sx={{marginRight:"5px"}}>{material.point}</Box> <BsTrophy className="faTrophy" /></>}
                
              </CenterFlexBox>
              
            </SectionItemMaterial>
      </ItemMaterial>
      </SupperItemMaterial>
    ))
  }
  </>
    : slideThumbnails === null && !isLoading || slideThumbnails.data.length === 0?
    "Không có tài liệu nào"
       :newMaterial?.type === "IMAGE"?
       <ListMedia list={slideThumbnails.data} cols={5} gap={8} type="IMAGE"/>
       :
       slideThumbnails.data.map((material, index) => (
        <SupperItemMaterial key={index}>
        <ItemMaterial key={index} className="item" title={material.name} >
          <ImgItemMaterial src={material.thumbnailSrc} alt={material.name} onClick={() => handleViewDetail(material.id)}></ImgItemMaterial>
            <SectionItemMaterial className="title2-item">
              <Box
                sx={{
                  
                  color:'#198754',
                  cursor: 'pointer',
                  marginRight:"5px",
                  fontSize:"14px",
                }}
                onClick={() => handleViewDetail(material.id)}
              >
                {material.name.length > 15 ?  material.name.slice(0, 19)+"...": material.name } 
              </Box>
              <Box
              
              >
                {/* <FaFilePowerpoint className="faBookmark" /> */}
              </Box>

              {/* BiSolidFileDoc
                        BiSolidFilePdf
                    */}
            </SectionItemMaterial>
            <SectionItemMaterial className="title2-item">
              {/* BsTrophyFill */}
              <CenterFlexBox
                style={{
                  width: "50px",
                  height: "50px",
                  cursor:  'pointer',
                  fontSize:"10px",
                  // margin: '10px',
                }}
                // onClick={onSave}
                title={`Đã có ${material.viewCount} lượt xem `}
              >
                <MdRemoveRedEye /><Box sx={{ml:"2px"}}>{material.viewCount}</Box>
              </CenterFlexBox>
              <CenterFlexBox
                style={{
                  padding: "10px",
                }}
              >
                {material.point === 0 ? <Box sx={{color:"blue",fontSize:"10px"}}>Miễn phí</Box> : <><Box sx={{marginRight:"5px"}}>{material.point}</Box> <BsTrophy className="faTrophy" /></>}
                
              </CenterFlexBox>
              
            </SectionItemMaterial>
        </ItemMaterial>
        </SupperItemMaterial>
      ))}
      
    </TableMaterialDiv>
    <ContainerPagingDiv >
    {/* <span className="w-[30%] text-small text-default-400">
     
    </span> */}
    <Pagination
      isCompact
      showControls
      showShadow
      color="primary"
      page={page}
      total={pages}
      onChange={handleSetPage}
      style={{margin:0}}
    />
    <div className="hidden sm:flex w-[30%] justify-end gap-2">
      <Button
        size="sm"
        variant="flat"
        onClick={onPreviousPage}
        disabled={page ===1}
      >
        Trước
      </Button>
      <Button
        size="sm"
        variant="flat"
        onClick={onNextPage}
        disabled={page ===pages}

      >
        Sau
      </Button>
    </div>
  </ContainerPagingDiv>
  </>
  );
};
export default TableMaterial;
