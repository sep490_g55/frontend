import React, { useState,useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BsTrophy } from "react-icons/bs";
import { BsBookmark } from "react-icons/bs";
import { FaFilePowerpoint } from "react-icons/fa";
import {
  TableMaterialDiv,
  ItemMaterial,
  ImgItemMaterial,
  SectionItemMaterial,
  SupperItemMaterial,
  SkeletonImgItemMaterial,
  SkeletonItemMaterial,
} from "./styles";
import { Row, Col } from "react-bootstrap";
import { Box, Button, Grid, Skeleton, Typography } from "@mui/material";
import Link from "next/link";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import {
Pagination
} from "@nextui-org/react";
import { BeatLoader } from "react-spinners";
import { CenterFlexBox } from "../common_sections";
import { MdRemoveRedEye } from "react-icons/md";
import { searchKhoMedia,getSuggestResourcesByResourceId } from "@/dataProvider/agent";
// export const TableMaterial = ({isLoading,slideThumbnails,styleForDetailMaterial,materialData}) => {
  export const TableMaterial = ({resourceId}) => {

  // const materialData = suggestMaterials ? suggestMaterials: useSelector(state => state.materials.material)
  const [materials, setMaterial] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const getSuggestMaterial = async()=>{
    setIsLoading(true)
    const res = await getSuggestResourcesByResourceId(resourceId);
    if(res.status === 200){
        setMaterial(res?.data)
    }
    setIsLoading(false)

  }
  useEffect(() => {
    getSuggestMaterial();
  }, [resourceId]);
  
    const route = useRouter();
    const handleViewDetail = (id) =>{
      route.push({
        pathname: "/detail_material",
        query: { id: id },
        // hash: "detail-image",
      });
    }
  //end route


  return (
    <>
    <TableMaterialDiv className="table-material">
      
      {materials === null && isLoading ?
      
      <Grid container wrap="wrap">
      {Array.from(new Array(12)).map((item, index) => (
        <Box key={index} sx={{ width: 150, mr: 5,ml:5, my: 5 }}>
          
            <Skeleton variant="rectangular" width={150} height={118} />
          

       
            <Box sx={{ pt: 0.5 }}>
              <Skeleton />
              <Skeleton width="60%" />
            </Box>
          
        </Box>
      ))}
    </Grid>
    :materials !== null && isLoading ? <> <Box sx={{textAlign:"center"}}><BeatLoader 
    color="#198754"
    size={5}
    style={{marginLeft:"5px",marginTop:"5px"}}
  /></Box>{
    materials.map((material, index) => (
      <SupperItemMaterial key={index}>
      <ItemMaterial key={index} className="item" title={material.name} >
        <ImgItemMaterial src={material.thumbnailSrc} alt={material.name} onClick={() => handleViewDetail(material.id)}></ImgItemMaterial>
        <SectionItemMaterial className="title2-item">
            <Box
              sx={{
                
                color:'#198754',
                cursor: 'pointer',
                marginRight:"5px",
                fontSize:"14px",
              }}
              onClick={() => handleViewDetail(material.id)}
            >
              {material.name.length > 15 ?  material.name.slice(0, 19)+"...": material.name } 
            </Box>
            <Box
            
            >
              {/* <FaFilePowerpoint className="faBookmark" /> */}
            </Box>

            {/* BiSolidFileDoc
                      BiSolidFilePdf
                  */}
          </SectionItemMaterial>
        {/* <SectionItemMaterial className="title2-item">
              <Box
                sx={{
                  
                  color:'#198754',
                  cursor: 'pointer',
                  marginRight:"5px",
                  fontSize:"14px",
                }}
                onClick={() => handleViewDetail(material.id)}
              >
                {material.name.length > 15 ?  material.name.slice(0, 19)+"...": material.name } 
              </Box>
              <Box
              
              >
                <FaFilePowerpoint className="faBookmark" />
              </Box>

            </SectionItemMaterial> */}
            <SectionItemMaterial className="title2-item">
              {/* BsTrophyFill */}
              <CenterFlexBox
                style={{
                  width: "50px",
                  height: "50px",
                  cursor:  'pointer',
                  fontSize:"10px",
                  // margin: '10px',
                }}
                // onClick={onSave}
                title={`Đã có ${material.viewCount} lượt xem `}
              >
                <MdRemoveRedEye /><Box sx={{ml:"2px"}}>{material.viewCount}</Box>
              </CenterFlexBox>
              <CenterFlexBox
                style={{
                  padding: "10px",
                }}
              >
                {material.point === 0 ? <Box sx={{color:"blue",fontSize:"10px"}}>Miễn phí</Box> : <><Box sx={{marginRight:"5px"}}>{material.point}</Box> <BsTrophy className="faTrophy" /></>}
                
              </CenterFlexBox>
              
            </SectionItemMaterial>
      </ItemMaterial>
      </SupperItemMaterial>
    ))
  }
  </>
    : materials === null && !isLoading || materials.length === 0?
    "Không có tài liệu nào"
       :
       materials.map((material, index) => (
        <SupperItemMaterial key={index}>
        <ItemMaterial key={index} className="item" title={material.name} >
          <ImgItemMaterial src={material.thumbnailSrc} alt={material.name} onClick={() => handleViewDetail(material.id)}></ImgItemMaterial>
          <SectionItemMaterial className="title2-item">
            <Box
              sx={{
                
                color:'#198754',
                cursor: 'pointer',
                marginRight:"5px",
                fontSize:"14px",
              }}
              onClick={() => handleViewDetail(material.id)}
            >
              {material.name.length > 15 ?  material.name.slice(0, 19)+"...": material.name } 
            </Box>
            <Box
            
            >
              {/* <FaFilePowerpoint className="faBookmark" /> */}
            </Box>

            {/* BiSolidFileDoc
                      BiSolidFilePdf
                  */}
          </SectionItemMaterial>
        
            <SectionItemMaterial className="title2-item">
              {/* BsTrophyFill */}
              <CenterFlexBox
                style={{
                  width: "50px",
                  height: "50px",
                  cursor:  'pointer',
                  fontSize:"10px",
                  // margin: '10px',
                }}
                // onClick={onSave}
                title={`Đã có ${material.viewCount} lượt xem `}
              >
                <MdRemoveRedEye /><Box sx={{ml:"2px"}}>{material.viewCount}</Box>
              </CenterFlexBox>
              <CenterFlexBox
                style={{
                  padding: "10px",
                }}
              >
                {material.point === 0 ? <Box sx={{color:"blue",fontSize:"10px"}}>Miễn phí</Box> : <><Box sx={{marginRight:"5px"}}>{material.point}</Box> <BsTrophy className="faTrophy" /></>}
                
              </CenterFlexBox>
              
            </SectionItemMaterial>
        </ItemMaterial>
        </SupperItemMaterial>
      ))}
      
    </TableMaterialDiv>
    {/* <div className="w-[100%] my-8 py-2 px-2 flex justify-between items-center">
    <span className="w-[30%] text-small text-default-400">
     
    </span>
    <Pagination
      isCompact
      showControls
      showShadow
      color="primary"
      page={page}
      total={pages}
      onChange={handleSetPage}
    />
    <div className="hidden sm:flex w-[30%] justify-end gap-2">
      <Button
        size="sm"
        variant="flat"
        onClick={onPreviousPage}
      >
        Trước
      </Button>
      <Button
        size="sm"
        variant="flat"
        onClick={onNextPage}
      >
        Sau
      </Button>
    </div>
  </div> */}
  </>
  );
};
export default TableMaterial;
