import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import Button from '@mui/material/Button';
import bgsearch from '@/assets/bgsearch2.jpg'
import { Skeleton } from '@mui/material';
export const ContainerPagingDiv = styled('div')(({ theme }) => ({
  width:"100%",
  display:"flex",
  justifyContent:"justify-between",
  
  [theme.breakpoints.up("xs")]: {
    justifyContent:"center",

  },
  [theme.breakpoints.up("sm")]: {
 
    justifyContent:"center",


  },
  [theme.breakpoints.up("md")]: {
    justifyContent:"center",


  },
  [theme.breakpoints.up("lg")]: {
    justifyContent:"justify-between",

  },
  [theme.breakpoints.up("xl")]: {
    justifyContent:"justify-between",


  }

  
}));
export const TableMaterialDiv = styled('div')(({ theme }) => ({
    width: '100%',
    height: '500px',
    // marginTop: '20px' ,
    marginBottom: '55px'  ,
    paddingLeft: '50px',
    
    [theme.breakpoints.up("xs")]: {
      paddingLeft: '10px',
    },
    [theme.breakpoints.up("sm")]: {
   
      paddingLeft: '30px',

    },
    [theme.breakpoints.up("md")]: {
      paddingLeft: '50px',

    },
    [theme.breakpoints.up("lg")]: {
      paddingLeft: '50px',
    },
    [theme.breakpoints.up("xl")]: {
      paddingLeft: '50px',

    }

    
  }));
  export const SupperItemMaterial = styled('div')(({ theme }) => ({
    width: '155px',
    height: '205px',
    margin: '32px',
    borderRadius: '15px',
    float: 'left',
    
    [theme.breakpoints.up("xs")]: {
      width: '110px'
    },
    [theme.breakpoints.up("sm")]: {
      width: '155px'
    },
    [theme.breakpoints.up("md")]: {
      width: '175px'
    },
    [theme.breakpoints.up("lg")]: {
      width: '125px'
    },
    [theme.breakpoints.up("xl")]: {
      width: '165px'
    },
    
    
  }));  
  export const SkeletonSupperItemMaterial = styled(Skeleton)(({ theme }) => ({
    width: '155px',
    height: '205px',
    margin: '32px',
    borderRadius: '15px',
    float: 'left',
    
    [theme.breakpoints.up("xs")]: {
      width: '145px'
    },
    [theme.breakpoints.up("sm")]: {
      width: '155px'
    },
    [theme.breakpoints.up("md")]: {
      width: '175px'
    },
    [theme.breakpoints.up("lg")]: {
      width: '165px'
    },
    [theme.breakpoints.up("xl")]: {
      width: '165px'
    },
    
    
  }));  
  export const ItemMaterial = styled('div')(({ theme }) => ({
    width: '150px',
    height: '200px',
    // margin: '32px',
    borderRadius: '15px',
    boxShadow: 'rgba(0, 0, 0, 0.2) 0px 20px 30px',
    display:"flex",
    justifyContent:"center",
    alignItems: "center",
    flexDirection: "column",
    transition: 'all 0.3s ease',
    [theme.breakpoints.up("xs")]: {
      width: '140px'
    },
    [theme.breakpoints.up("sm")]: {
      width: '150px'
    },
    [theme.breakpoints.up("md")]: {
      width: '170px'
    },
    [theme.breakpoints.up("lg")]: {
      width: '160px'
    },
    [theme.breakpoints.up("xl")]: {
      width: '160px'
    },
    "&:hover":{
      
      transform: 'scale(1.1)'
    }
    
  }));  
  export const SkeletonItemMaterial = styled(Skeleton)(({ theme }) => ({
    width: '150px',
    height: '200px',
    // margin: '32px',
    borderRadius: '15px',
    boxShadow: 'rgba(0, 0, 0, 0.2) 0px 20px 30px',
    display:"flex",
    justifyContent:"center",
    alignItems: "center",
    flexDirection: "column",
    transition: 'all 0.3s ease',
    [theme.breakpoints.up("xs")]: {
      width: '140px'
    },
    [theme.breakpoints.up("sm")]: {
      width: '150px'
    },
    [theme.breakpoints.up("md")]: {
      width: '170px'
    },
    [theme.breakpoints.up("lg")]: {
      width: '160px'
    },
    [theme.breakpoints.up("xl")]: {
      width: '160px'
    },
    "&:hover":{
      
      transform: 'scale(1.1)'
    }
    
  }));  
  export const ImgItemMaterial = styled('img')(({ theme }) => ({
    width: '115px',
    height: '105px',
    borderRadius: '15px',
    // borderBottom: '1px solid grey',
    cursor: 'pointer',
    // [theme.breakpoints.up("xs")]: {
    //   width: '125px'
    // },
    // [theme.breakpoints.up("sm")]: {
    //   width: '125px'
    // },
    // [theme.breakpoints.up("md")]: {
    //   width: '125px'
    // },
    // [theme.breakpoints.up("lg")]: {
    //   width: '125px'
    // },

    // [theme.breakpoints.up("xl")]: {
    //   width: '125px'
    // }
  }));  
  export const SkeletonImgItemMaterial = styled(Skeleton)(({ theme }) => ({
    width: '155px',
    height: '105px',
    // borderBottom: '1px solid grey',
    [theme.breakpoints.up("xs")]: {
      width: '145px'
    },
    [theme.breakpoints.up("sm")]: {
      width: '155px'
    },
    [theme.breakpoints.up("md")]: {
      width: '175px'
    },
    [theme.breakpoints.up("lg")]: {
      width: '165px'
    },

    [theme.breakpoints.up("xl")]: {
      width: '165px'
    }
  }));  
  export const SectionItemMaterial = styled('div')(({ theme }) => ({
    height: 50,
    display: 'flex',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  }));  