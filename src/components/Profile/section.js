import React from "react";
import {
  AvatarProfileDiv,
  BodyProfileDiv,
  FormProfileDiv,
  HeaderProfileDiv,
} from "./styles";
import { GreenButtonAction } from "../FolderImage/styles";
import { Avatar, Box, Button, TextField, Input } from "@mui/material";
import { RxAvatar } from "react-icons/rx";
import { FormGetInfo, FormGetInfoUser } from "@/components/auth/common";
import { Col, Row } from "reactstrap";
import FormDialog, { FullChangePasswordDialog } from "../Dialog/section";
import { useState, useCallback } from "react";
import { ToastContainer, toast } from "react-toastify";
import { BeatLoader } from "react-spinners";

//icon
import { AiOutlineCamera } from "react-icons/ai";
import { RiLockPasswordLine } from "react-icons/ri";
//component
import { CenterFlexBox } from "../common_sections";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { getMaterials } from "@/utils/folderSrc";
import { useRouter } from "next/router";
import LoadingTab from "../loading-screen/LoadingTab";
import { getProfile, getUserById, getAllRole, getAllRoleActive } from "@/dataProvider/agent";
import { useAuthContext } from "@/auth/useAuthContext";
import FormLabel from "@mui/material/FormLabel";
import FormControl from "@mui/material/FormControl";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormHelperText from "@mui/material/FormHelperText";
import Checkbox from "@mui/material/Checkbox";
import {
  StyledDivContent,
  StyledSection,
  StyledLayout,
  StyledDivLeftContent,
  StyledDivRightContent,
  StyledButtonFull,
  ColInformation,
  CustomTextField,
} from "@/components/auth/style";
import { getAllClass, setRoleUser } from "@/dataProvider/agent";
import { IoIosArrowBack } from "react-icons/io";

export const HeaderProfile = () => {
  const router = useRouter();
  
  return (
    <HeaderProfileDiv>
      <CenterFlexBox
        style={{
          fontSize: "26px",
          textTransform: "uppercase",
          justifyContent: "flex-start",
          paddingLeft: "100px",
          marginBottom: "30px",
        }}
      >
        
        <RxAvatar /> <Box>Thông tin tài khoản người dùng</Box>
      </CenterFlexBox>
    </HeaderProfileDiv>
  );
};

export const BodyProfile = ({ previewImage, handleFileChange }) => {
  // const profile = useSelector(state => state.profile.info)
  {
    /* // show toast */
  }
  {
    /* <ToastContainer /> */
  }
  const { logout, user, updateProfile,resetUser } = useAuthContext();

  //change password
  const route = useRouter();
  const avatar =
    typeof window !== "undefined" ? localStorage.getItem("avatar") : "";
  // avatar === "" ? router.push("../pages/auth/login"):"";
  const [open, setOpen] = useState(false);
  const [profile, setProfile] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isLoading2, setIsLoading2] = useState(false);

  const [id, setId] = useState(profile?.id);

  const [username, setUsername] = useState(null);
  const [firstName, setFirstName] = useState(user?.firstname);
  const [lastName, setLastName] = useState(user?.lastname);
  const [email, setEmail] = useState(null);

  const [gender, setGender] = useState(user?.gender);
  const [phone, setPhone] = useState(user?.phone);
  const [school, setSchool] = useState(user?.school);
  const [dateOfBirth, setDateOfBirth] = useState(user?.dateOfBirth);
  const [clas, setClas] = useState(user?.classId);
  const [province, setProvince] = useState(user?.province);
  const [district, setDistrict] = useState(user?.district);
  const [ward, setWard] = useState(user?.village);

  const newUser = {
    firstname: firstName,
    lastname: lastName,
    phone: phone,
    school: school,
    gender: gender,
    dateOfBirth: dateOfBirth,
    classId: clas,
    province: province,
    district: district,
  };
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  useEffect(() => {
    getProfileUser();
  }, []);
  const getProfileUser = async () => {
    try {
      !isLoading && setIsLoading(true);
      const profile = await getProfile();
      if (profile.status === 200) {
        setProfile(profile.data);
      }
    } catch (e) {
      console.log(e);
    }
    setIsLoading(false);
  };

  const handleChangePass = async () => {
    // nếu thành công thì setOpen false
    setOpen(false);
    toast.success("Thay đổi thành công");
    // toast.error("Đăng nhập không thành công")
  };
  //end change password

  const handleChangeProfile = async () => {
    setIsLoading2(true)
     const res = await updateProfile(
      id,
      firstName,
      lastName,
      phone,
      gender,
      province,
      district,
      ward,
      dateOfBirth,
      school,
      clas
    );
    if (res?.status === 200) {
      toast.success("Cập nhật thông tin thành công");

     resetUser(res?.data)
    } else if (res?.status === 202) {
      if (res?.data.email) {
        toast.warning(" email đã được đăng ký ");
      } else if (res?.data.dateOfBirth) {
        toast.warning("Kiểm tra lại ngày sinh");
      } else if (res?.data.username) {
        toast.warning("tên tài khoản đã được đăng ký");
      } else if (res?.data.firstname) {
        toast.warning("Kiểm tra lại họ");
      } else if (res?.data.lastname) {
        toast.warning("Kiểm tra lại tên");
      } else if (res?.data.message === "Phone is existed") {
        toast.warning("Sđt đã được đăng ký ");
      } else if (res?.data.province) {
        toast.warning("Kiểm tra lại ngày tỉnh thành");
      } else if (res?.data.district) {
        toast.warning("Kiểm tra lại quận huyện");
      } else if (res?.data.school) {
        toast.warning("Kiểm tra lại trường");
      } else if (res?.data.classId) {
        toast.warning("Kiểm tra lại lớp");
      } else {
        toast.warning(" Thông tin chưa chính xác");
      }
    } else {
      toast.error("Cập nhật thông tin thất bại");
    }
    setIsLoading2(false);
  };

  return (
    <>
      {" "}
      {!isLoading ? (
        <BodyProfileDiv>
          <FormProfileDiv>
            <FormGetInfo
              titleBtn="Cập nhật thông tin"
              isLoadingSubmit={isLoading2}
              newUser={newUser}
              profile={profile}
              handleSubmitForm={handleChangeProfile}
              setProvince={setProvince}
              setDistrict={setDistrict}
              setClas={setClas}
              setGender={setGender}
              setUsername={setUsername}
              setFirstName={setFirstName}
              setLastName={setLastName}
              setEmail={setEmail}
              setPhone={setPhone}
              setSchool={setSchool}
              setDateOfBirth={setDateOfBirth}
            ></FormGetInfo>
          </FormProfileDiv>
          <CenterFlexBox style={{ flexDirection: "column", width: 450 }}>
            <CenterFlexBox>
              {/* <FormDialog
            title="Đổi mật khẩu"
            label="Đổi mật khẩu"
            open={open}
            setOpen={handleOpen}
            setClose={handleClose}
            handleSubmit={handleChangePass}
            type="changepassword"
          >
            <GreenButtonAction sx={{ width: 180 }}>
              <RiLockPasswordLine /> <Box>Đổi mật khẩu</Box>
            </GreenButtonAction>
            
          </FormDialog> */}
              <FullChangePasswordDialog handleChangePass={handleChangePass} />
              <Button
                variant="contained"
                component="label"
                sx={{
                  width: "180px",
                  height: "35px",
                  borderRadius: "8px",
                  backgroundColor: "#198754",
                  marginLeft: "50px",
                  textTransform: "none",
                  display: "flex",
                  justifyContent: "space-evenly",
                  alignItems: "center",
                  fontSize: "15px",
                }}
              >
                <CenterFlexBox>
                  <AiOutlineCamera />
                  <Box sx={{ ml: "3px" }}>Đổi ảnh đại diện</Box>{" "}
                </CenterFlexBox>

                <Input
                  id="avatar"
                  type="file"
                  hidden
                  onChange={handleFileChange}
                />
              </Button>
            </CenterFlexBox>
            <AvatarProfileDiv
              sx={{
                width: "180px",
                height: "186px",
                display: "flex",
                mt: "50px",
                justifyContent: "center",
                alignItems: "center",
                // border: '2px solid gray',
              }}
            >
              <Avatar
                sx={{ width: "180px", height: "180px" }}
                src={
                  previewImage
                    ? previewImage
                    : profile !== null
                    ? profile.avatar
                    : ""
                }
              />
            </AvatarProfileDiv>
          </CenterFlexBox>
        </BodyProfileDiv>
      ) : (
        <LoadingTab />
      )}
    </>
  );
};

export const BodyProfileUser = ({ uid }) => {
  // const profile = useSelector(state => state.profile.info)
  {
    /* // show toast */
  }
  {
    /* <ToastContainer /> */
  }
  const { logout, user, updateProfile } = useAuthContext();

  //change password
  const route = useRouter();
  const avatar =
    typeof window !== "undefined" ? localStorage.getItem("avatar") : "";
  // avatar === "" ? router.push("../pages/auth/login"):"";
  const [open, setOpen] = useState(false);
  const [profile, setProfile] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingSubmit, setIsLoadingSubmit] = useState(false);

  const [id, setId] = useState(profile?.id);

  const [username, setUsername] = useState(null);
  const [firstName, setFirstName] = useState(user?.firstname);
  const [lastName, setLastName] = useState(user?.lastname);
  const [email, setEmail] = useState(null);

  const [gender, setGender] = useState(user?.gender);
  const [phone, setPhone] = useState(user?.phone);
  const [school, setSchool] = useState(user?.school);
  const [dateOfBirth, setDateOfBirth] = useState(user?.dateOfBirth);
  const [clas, setClas] = useState(user?.classId);
  const [province, setProvince] = useState(user?.province);
  const [district, setDistrict] = useState(user?.district);
  const [ward, setWard] = useState(user?.village);

  const [roles, setRoles] = useState([]);
  const [changedRoles, setChangedRoles] = useState([]);
  const [isChange, setIsChange] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const profileResponse = await getUserById(uid);
        if (profileResponse.status === 200) {
          setProfile(profileResponse.data);
          setChangedRoles(profileResponse.data.roles);
        }

        const rolesResponse = await getAllRoleActive();
        if (rolesResponse.status === 200) {
          setRoles(rolesResponse.data);
        }
        kiemTraQuyen(rolesResponse.data, profileResponse.data.roles);
      } catch (error) {
        console.error(error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();
  }, [uid]);


  function kiemTraQuyen(arrayA, arrayB) {
    setState((prev) => {
      const result = { ...prev };
      for (const a of arrayA) {
        const { roleId, roleName } = a;
        console.log("roleId", roleId);
        console.log("arrayB", arrayB);

        const isRoleInB = arrayB?.includes(roleId);
        result[roleName] = isRoleInB;
      }
      return result;
    });
  }

  // trạng  thái phân quyền role user
  const [state, setState] = React.useState(null);

  const [error, setError] = React.useState(false);

  const handleChange = useCallback(
    (event) => {
      setState((prevState) => ({
        ...prevState,
        [event.target.name]: event.target.checked,
      }));
      setError(
        checkExitRole({
          ...state,
          [event.target.name]: event.target.checked,
        })
      );
      // console.log("state",{...state,
      //   [event.target.name]: event.target.checked,})
      const stateChange = {
        ...state,
        [event.target.name]: event.target.checked,
      };
      let arrayRole = [];
      for (const a of roles) {
        const { roleId, roleName } = a;
        stateChange[roleName] && arrayRole.push(roleId);
      }
      setChangedRoles(arrayRole);

      
    },
    [state, checkExitRole]
  );

  function checkExitRole(input) {
    // Chuyển đối tượng thành mảng các giá trị
    const giaTriArray = Object.values(input);
    console.log("giaTriArray", giaTriArray);
    // Kiểm tra xem có ít nhất một giá trị true hay không
    return !giaTriArray.includes(true);
  }
  // const error = checkExitRole(state);
  function isDuplicate(arr1, arr2) {
    const arr = arr1.filter((value) => arr2.includes(value));
    return arr.length > 0;
  }
  const handleChangeRoleUser = async () => {
    setIsLoadingSubmit(true);
    let arrayRole = [];
    for (const a of roles) {
      const { roleId, roleName } = a;
      state[roleName] && arrayRole.push(roleId);
    }

    const res = await setRoleUser(uid, { roles: arrayRole });
    if (res.status === 200) {
      toast.success("Cập nhật thành công");
    } else if (res.status === 202) {
      toast.warning("Kiểm tra lại");
    } else {
      toast.error("Cập nhật thất bại");
    }
    setIsLoadingSubmit(false);
  };
  function arraysHaveSameElements(arr1, arr2) {
    // Kiểm tra xem cả hai mảng có đều là mảng không
    if (!Array.isArray(arr1) || !Array.isArray(arr2)) {
      return false;
    }
  
    // Kiểm tra xem cả hai mảng có cùng độ dài không
    if (arr1.length !== arr2.length) {
      return false;
    }
  
    // Sắp xếp mảng để so sánh phần tử
    const sortedArr1 = arr1.slice().sort();
    const sortedArr2 = arr2.slice().sort();
  
    // So sánh từng phần tử của mảng
    for (let i = 0; i < arr1.length; i++) {
      if (sortedArr1[i] !== sortedArr2[i]) {
        return false;
      }
    }
  
    return true;
  }
  return (
    <>
      {" "}
      {!isLoading ? (
        <BodyProfileDiv>
          <FormProfileDiv>
            <FormGetInfoUser
              titleBtn="Cập nhật thông tin"
              profile={profile}
              setProvince={setProvince}
              setDistrict={setDistrict}
              setClas={setClas}
              setGender={setGender}
              setUsername={setUsername}
              setFirstName={setFirstName}
              setLastName={setLastName}
              setEmail={setEmail}
              setPhone={setPhone}
              setSchool={setSchool}
              setDateOfBirth={setDateOfBirth}
            ></FormGetInfoUser>
          </FormProfileDiv>
          <CenterFlexBox style={{ flexDirection: "column", width: 450 }}>
            <AvatarProfileDiv
              sx={{
                width: "180px",
                height: "186px",
                display: "flex",
                mt: "50px",
                justifyContent: "center",
                alignItems: "center",
                // border: '2px solid gray',
              }}
            >
              <Avatar
                sx={{ width: "180px", height: "180px" }}
                src={profile?.avatar}
              />
            </AvatarProfileDiv>
            {console.log("STETA", state)}
            {state !== null && (
              <FormControl
                required
                error={error}
                component="fieldset"
                sx={{ m: 3 }}
                variant="standard"
              >
                <FormLabel component="legend">Chọn ít nhất 1</FormLabel>
                <FormGroup>
                  {roles?.map((role, index) => (
                    <FormControlLabel
                    key={index}
                      control={
                        <Checkbox
                          checked={state[role.roleName]}
                          onChange={handleChange}
                          name={role.roleName}
                        />
                      }
                      label={role.roleName}
                    />
                  ))}
                </FormGroup>
                <FormHelperText>Phân quyền cho tài khoản</FormHelperText>
              </FormControl>
            )}
            <StyledDivContent style={{ width: "100%" }}>
              <StyledButtonFull
                disabled={error}
                outline
                className="login-page__btn-login"
                type="button"
                onClick={
                  isLoadingSubmit
                    ? () => console.log("")
                    : () => handleChangeRoleUser()
                }
                style={{
                  backgroundColor:
                    error || arraysHaveSameElements(profile?.roles, changedRoles)
                      ? "gray"
                      : "#197854",
                  width: "80%",
                }}
              >
                Xác nhận
                {isLoadingSubmit ? (
                  <BeatLoader
                    color="white"
                    size={5}
                    style={{ marginLeft: "5px", marginTop: "5px" }}
                  />
                ) : (
                  ""
                )}
              </StyledButtonFull>
            </StyledDivContent>
          </CenterFlexBox>
        </BodyProfileDiv>
      ) : (
        <LoadingTab />
      )}
    </>
  );
};
