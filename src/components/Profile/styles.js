import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import Button from '@mui/material/Button';

export const HeaderProfileDiv = styled('div')(({ theme }) => ({
    width: '100%',
    height: '50px',
    
    // [theme.breakpoints.down('md')]: {
    //   flexWrap: 'wrap',
    //   position: 'relative',
    //   zIndex: 3
    // },
    // [theme.breakpoints.up('md')]: {
    //   flexWrap: 'wrap'

    // },
    // [theme.breakpoints.up('lg')]: {
    //   // backgroundColor: 'green',
    // // width: '700px',
  
    // },
    // [theme.breakpoints.up('xl')]: {
    //   // backgroundColor: 'green',
    // // width: '900px',
  
    // },
    
  }));

  
export const BodyProfileDiv = styled('div')(({ theme }) => ({
    width: '100%',
    // height: '100px',
    display: 'flex',
    justifyContent:'space-evenly',
    alignItems: 'flex-start',
    padding:"40px 90px",
    
    [theme.breakpoints.up('xs')]: {
      flexDirection:"column-reverse",
      justifyContent:'center',
    alignItems: 'center',
    padding:0,

    },
    [theme.breakpoints.up('md')]: {
      flexDirection:"row",
      justifyContent:'space-evenly',
    alignItems: 'flex-start',
    padding:"0 90px",
    },
    [theme.breakpoints.up('lg')]: {
      flexDirection:"row",

      justifyContent:'space-evenly',
    alignItems: 'flex-start',
    padding:"40px 90px",
  
    },
    [theme.breakpoints.up('xl')]: {
      flexDirection:"row",

      justifyContent:'space-evenly',
      alignItems: 'flex-start',
      padding:"40px 90px",
  
    },
    
  }));

    
export const FormProfileDiv = styled('div')(({ theme }) => ({
  width: '100%',
 
  [theme.breakpoints.up('xs')]: {
    width: '450px',
  },
  [theme.breakpoints.up('lg')]: {
    // backgroundColor: 'green',
  width: '100%',

  },
  [theme.breakpoints.up('xl')]: {
    // backgroundColor: 'green',
  width: '100%',

  },

  
}));

export const AvatarProfileDiv = styled('div')(({ theme }) => ({
  width:150, 
  display: 'flex',
  justifyContent:'center',
  alignItems: 'center',
  [theme.breakpoints.down('md')]: {
    
  },
  [theme.breakpoints.up('md')]: {
    
  },
  [theme.breakpoints.up('lg')]: {
    // backgroundColor: 'green',


  },
  [theme.breakpoints.up('xl')]: {


  },

  
}));