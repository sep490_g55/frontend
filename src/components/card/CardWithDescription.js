import React from 'react'
import { Card, CardTitle, GoArrow, GoCorner, SmallDesc } from './style'

const CardWithDescription = ({title,description,color,backgroundColor}) => {
  return (
    <Card class="card" style={{background:backgroundColor}}>
      <CardTitle class="card-title" style={{color:color}}>{title}</CardTitle>
      <SmallDesc class="small-desc" style={{color:color}}>
      {description}
      </SmallDesc>
      <GoCorner class="go-corner">
        <GoArrow class="go-arrow">→</GoArrow>
      </GoCorner>
    </Card>
  )
}

export default CardWithDescription