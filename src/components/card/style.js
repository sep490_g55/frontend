import * as React from 'react';
import styled from 'styled-components';
import Button from '@mui/material/Button';
import bgsearch from '@/assets/bgsearch2.jpg'
export const CardTitle = styled('div')(({theme}) => ({
    color: "#262626",
  fontSize: '1.5em',
  lineHeight: 'normal',
  fontWeight: '700',
  marginBottom: '0.5em',
    
  }));

  export const SmallDesc = styled('div')(({theme}) => ({
    color: "#452c2c",
  fontSize: '1em',
  lineHeight: '1.5em',
  fontWeight: '400',
  marginBottom: '0.5em',
    
  }));


  export const GoCorner = styled('div')(({theme}) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    width: '2em',
    height: '2em',
    overflow: 'hidden',
    top: 0,
    right: 0,
    background: "linear-gradient(135deg, #6293c8, #384c6c)",
    borderRadius: "0 4px 0 32px",
    
  }));


  export const GoArrow = styled('div')(({theme}) => ({
    marginTop: '-4px',
  marginRight:' -4px',
  color: 'white',
  fontFamily:" courier, sans",
    
  }));


  export const Card= styled('div')(({theme}) => ({
    display: 'block',
    position: 'relative',
    width: '250px',
    height: '250px',
    backgroundColor: '#f2f8f9',
    borderRadius: '10px',
    padding: '2em 1.2em',
    margin: '12px',
    textDecoration: 'none',
    zIndex: 0,
    overflow: 'hidden',
    background: 'linear-gradient(to bottom, #c3e6ec, #a7d1d9)',
    fontFamily: 'Arial, Helvetica, sans-serif',
    cursor:"pointer",
    
    "&:before":{
        content: '""',
        position: 'absolute',
        zIndex: '-1',
        top: '-16px',
        right: '-16px',
        background: 'linear-gradient(135deg, #364a60, #384c6c)',
        height: '32px',
        width: '32px',
        borderRadius: '32px',
        transform: 'scale(1)',
        transformOrigin: '50% 50%',
        transition: 'transform 0.35s ease-out',
    },
    "&:hover:before":{
        transform: 'scale(28)',
    },
    "&:hover": {
        transition: 'all 0.5s ease-out',
        color: 'rgba(255, 255, 255, 0.8)',
        [`${CardTitle},${SmallDesc}`]: {
          transition: 'all 0.5s ease-out',
          color: '#ffffff',
        },
      },
  }));

