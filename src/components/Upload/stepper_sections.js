import {
  Avatar,
  Box,
  Chip,
  Container,
  FormControl,
  Link,
  MenuItem,
  Select,
  styled,
  InputLabel,
} from "@mui/material";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import Radio from "@mui/material/Radio";
import axios from "axios";
import {
  BorderInput,
  CustomUploadInput,
  FileUploadDiv,
  FileUploadLabel,
  ChooseFileStepperDiv,
  ContainerInputDiv,
} from "./styles";
import { LinearWithValueLabel } from "../Process/LinearWithLabel";
import { Form } from "reactstrap";
import { useEffect, useState } from "react";
import { getFolderSrc, getMaterials } from "@/utils/folderSrc";
import { ButtonAction, DeleteButton, HoverCenterFlexBox } from "../common_sections";
import Image from "next/image";
import { useDispatch, useSelector } from "react-redux";
import {
  MUIBookSeriesSelect,
  MUIBookVolumeSelect,
  MUIChapterSelect,
  MUIClassSelect,
  MUILessonSelect,
  MUISubjectSelect,
} from "../Select/Folder/MUI/folder";
import {
  setDescription,
  setFile,
  setMode,
  setName,
  setNewMaterial,
  setViewClass,
  setViewSubjectBookSeries,
  setTags,
  setIsDetail,
} from "@/redux/slice/NewMaterialSlice";
import {
  uploadFile,
  getLocalStorage,
  getALLTagGlobal,
  getALLSubject,
  getVisualTypes,
  getApproveTypes,
  getMethodTypes
} from "@/dataProvider/agent";
import { toast,ToastContainer } from "react-toastify";
import { getSuggestTagList, getALLTag } from "@/dataProvider/agent";
import LoadingTab from "../loading-screen/LoadingTab";
import { FaLongArrowAltDown } from "react-icons/fa";
import { FcDown, FcDownRight } from "react-icons/fc";
import { FaDeleteLeft } from "react-icons/fa6";
import {CenterFlexBox,
} from "../common_sections";
import { Button } from "react-bootstrap";
import { LogoSystem, NameSystem } from "@/theme/system";
export const HeaderUploadPage = () => {
  return (
    <Box
      sx={{
        width: "100%",
        borderBottom: "1px solid #CCCCCC",
        padding: "30px 0",
        marginBottom: "30px",
      }}
    >
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          color: "green",
          fontWeight: "500",
          fontSize: "26px",
        }}
      >
        <Link href="/home" sx={{ marginRight: "20px" }}>
          {/* <Avatar src="https://c8.alamy.com/comp/2C6PYE2/business-meeting-group-presentation-icon-vector-graphics-for-various-use-2C6PYE2.jpg" /> */}
        <LogoSystem/>
        </Link>
        <NameSystem/>
      </Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          color: "green",
          fontWeight: "500",
          fontSize: "16px",
          marginTop: "30px",
        }}
      >
        Chia sẻ tài liệu, học liệu để cùng tạo lên một cộng đồng hữu ích
      </Box>
    </Box>
  );
};

// export const InputStepper = () => {
//     return(
//         <ContainerInputDiv >
//           <BorderInput >
//             <FileUploadDiv >
//               <Box sx={{fontSize:'24px'}}>Đưa tài liệu lên hệ thống</Box>
//               <FileUploadLabel>Chọn File</FileUploadLabel>
//               <CustomUploadInput type="file" />
//             </FileUploadDiv>
//         </BorderInput>
//         </ContainerInputDiv>
//     )
// }

export const DetailFolderMaterial = ({ type, newMaterial, setFileInput }) => {
  
  const [classe, setClasses] = useState("");
  const [bookSeries, setBookSeries] = useState("");
  const [lesson, setLesson] = useState("");
  const [bookVolume, setBookVolume] = useState("");
  const [chapter, setChapter] = useState("");
  const [nameFile, setNameFile] = useState(newMaterial?.resourceSrc);

  // console.log("value detail",newMaterial)
  const dispatch = useDispatch();
  const handleInputChange = (e) => {
    const files = e.target.files;
    const reader = new FileReader();
    // console.log("files", files);
    setFileInput(files[0]);
    setNameFile(files[0].name);
  };
  const handleChangeMode = (value) => {
    // console.log('value',value)
    dispatch(setMode({ id: value.id, name: value.name }));
    setM(value);
  };

  const [mode, setM] = useState(
    newMaterial
      && newMaterial.visualType === "PUBLIC"
        ? { id: "PUBLIC", name: "Công khai" }
        :newMaterial.visualType === "RESTRICT"
        ? { id: "RESTRICT", name: "Hạn chế" }
        : { id: "PRIVATE", name: "Chỉ mình tôi" }
      
  );
  const [modes, setModes] = useState([
    { id: "PUBLIC", name: "Công khai" },
    { id: "RESTRICT", name: "Hạn chế" },
    { id: "PRIVATE", name: "Chỉ mình tôi" },

  ]);
  const [name, setN] = useState(newMaterial?.name ? newMaterial?.name : "");
  const [description, setD] = useState(
    newMaterial?.description === undefined ? "" : newMaterial?.description
  );


  const getModeList = async()=>{
    const res = await getApproveTypes();

    if(res.status === 200){
      // setModes(res.data)
      // console.log("res.data",res.data)
    }
  }
  // useEffect(()=>{
  //   getModeList()
  // },[])

  const handleChangeName = (e) => {
    dispatch(setName(e.target.value));
    setN(e.target.value);
  };
  const handleChangeDescription = (e) => {
    dispatch(setDescription(e.target.value));
    setD(e.target.value);
  };
  return (
    <>
      <Box
        sx={{
          width: "100%",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "flex-start",
          flexDirection:{xs:"column",md:"row"}
        }}
      >
        <Box>
          <Box
            sx={{
              width: "100%",
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "center",
              margin: "20px 0",
              
            }}
          >
            <Box>Nhập tên : </Box>
            <TextField
              value={name}
              sx={{ width: {xs:200,md:400}, marginLeft: "20px " }}
              id="outlined-basic"
              placeholder="Nhập tên"
              variant="outlined"
              onChange={(e) => handleChangeName(e)}
            />
          </Box>
          {type == "edit" ? (
            // <MUISubjectSelect api="subject/list" type={true}  />
            <LabelWSelectV2 label="Môn học*:">
              <MUISubjectSelect value={newMaterial?.subjectId} />
            </LabelWSelectV2>
          ) : (
            ""
          )}
          {/* <LabelWSelect label="Lớp* :" prop={classesProps} list={classes} placeholder="Chọn lớp" /> */}
          {newMaterial?.lessonId !== null?<>
          <LabelWSelectV2 label="Lớp*:">
            <MUIClassSelect value={newMaterial?.classId} />
          </LabelWSelectV2>
          <LabelWSelectV2 label="Bộ sách*:">
            <MUIBookSeriesSelect value={newMaterial?.bookSeriesId} />
          </LabelWSelectV2>
          <LabelWSelectV2 label="Đầu sách*:">
            <MUIBookVolumeSelect value={newMaterial?.bookVolumeId} />
          </LabelWSelectV2>
          <Box sx={{}}>
            <FcDownRight />
          </Box>

          </>
          :<>
          <LabelWMuitiInput
          isMedia={true}
          newMaterial={newMaterial}
          label="Thêm tag* :"
          placeholder="điền thẻ tag"
        />
            <Box sx={{width:"50%",display:"flex",justifyContent:"flex-end"}}>
            <FcDownRight />
            </Box>
      
          </>}
          {/* <LabelWSelect label="Bài* :" prop={lessonsProps} list={lessons} placeholder="Chọn bài"/> */}
        </Box>
        <Box sx={{ marginLeft: {xs:"0px",md:"40px"} ,display:{xs:"flex",md:"block",}, flexDirection:{xs:"column",md:"row"}}}>
          {newMaterial?.lessonId !== null?<>
            <LabelWSelectV2 label="Chương*:">
              {" "}
              <MUIChapterSelect value={newMaterial?.chapterId} />
            </LabelWSelectV2>
            <LabelWSelectV2 label="Bài học*:">
              <MUILessonSelect value={newMaterial?.lessonId} />
            </LabelWSelectV2>
            </>
          :<></>}
            <LabelWSelectV2 label="Chế độ*:">
              <Autocomplete
                value={modes[modes.findIndex((item) => item.id === mode.id)]}
                id="mode"
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
                options={modes}
                autoHighlight
                getOptionLabel={(option) => option.name}
                // isOptionEqualToValue={(option) => option.name}
                renderOption={(props, option) => (
                  <Box
                    component="li"
                    sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                    {...props}
                    key={option.id}
                  >
                    {option.name}
                  </Box>
                )}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Chế độ"
                    inputProps={{
                      ...params.inputProps,
                      autoComplete: "new-password", // disable autocomplete and autofill
                    }}
                  />
                )}
                onChange={(event, value) => handleChangeMode(value)}
              />
            </LabelWSelectV2>
          
          <LabelWTextAria
            value={description}
            label="Mô tả :"
            placeholder="Điền thông tin mô tả"
            onChangeValue={(e) => handleChangeDescription(e)}
          />
          {type == "edit" && (
            <LabelWSelectV2>
              <FileUploadDiv style={{ width: "100%", height: "50px" }}>
                <Box>{nameFile}</Box>

                <CustomUploadInput
                  type="file"
                  name="file"
                  multiple
                  onChange={handleInputChange}
                />
              </FileUploadDiv>
            </LabelWSelectV2>
          )}
        </Box>
      </Box>
    </>
  );
};
export const LabelWSelectV2 = ({ label, children }) => {
  // const [value, setValue] = useState(list[0].name);
  const handleChange = (e) => {
    setValue(e.target.value);
  };
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "center",
        margin: "20px 0",
      }}
    >
      <Box sx={{width:"max-content"}}>{label}</Box>
      <Box sx={{ width: {xs:200,md:400}, marginLeft: "20px" }}>{children}</Box>

      {/* <Select
        sx={{ width: 400, marginLeft: "20px" }}
        id="demo-simple-select"
        value={value}
        label="Age"
        placeholder={placeholder}
        onChange={handleChange}
        displayEmpty
        inputProps={{ "aria-label": "Without label" }}
      >
        <MenuItem value="">
          <em>Chọn</em>
        </MenuItem>
        {list.map((e, index) => (
          <MenuItem value={e.id}>{e.name}</MenuItem>
        ))}
      </Select> */}
    </Box>
  );
};
export const LabelWTextAria = ({
  value,
  label,
  placeholder,
  onChangeValue,
}) => {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "center",
        margin: "20px 0",
        height: "auto",
      }}
    >
      <Box sx={{width:"max-content"}}>{label}</Box>
      {/* <InputLabel id={label}>{label}</InputLabel> */}
      <TextField
        value={value}
        id={label}
        multiline
        placeholder={placeholder}
        sx={{ width: {xs:200,md:400}, marginLeft: "20px" }}
        maxRows={5}
        onChange={onChangeValue}
      />
    </Box>
  );
};
export const LabelWSelect = ({ label, prop, list, placeholder }) => {
  return (
    <Box
      sx={{
        width: "100%",
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "center",
        margin: "20px 0",
      }}
    >

      <Box sx={{width:"max-content"}}>{label}</Box>
      {/* <InputLabel id={label}>{label}</InputLabel> */}

      <Autocomplete
        {...prop}
        disablePortal
        id={label}
        options={list}
        sx={{ width: {xs:200,md:400}, marginLeft: "20px" }}
        renderInput={(params) => (
          <TextField
            placeholder={placeholder}
            sx={{ borderRadius: "15px" }}
            {...params}
          />
        )}
      />
    </Box>
  );
};

export const LeftLabelWSelect = ({
  label,
  prop,
  list,
  placeholder,
  defaultSelect,
  handleChangeMode,
}) => {
  const newMaterial = useSelector((state) => state.newMaterial.material)

  const [value, setValue] = useState(list[0].name);
  const handleChange = (e) => {
    setValue(e.target.value);
  };
  const [mode, setM] = useState(
    newMaterial
      ? newMaterial.visualType === "PUBLIC"
        ? { id: "PUBLIC", name: "Công khai" }
        :newMaterial.visualType === "RESTRICT"
        ? { id: "RESTRICT", name: "Hạn chế" }
        : { id: "PRIVATE", name: "Chỉ mình tôi" }
      : { id: "PUBLIC", name: "Công khai" }
  );
  const [modes, setModes] = useState([
    { id: "PUBLIC", name: "Công khai" },
    { id: "RESTRICT", name: "Hạn chế" },
    { id: "PRIVATE", name: "Chỉ mình tôi" },

  ]);
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "center",
        margin: "20px 0",
      }}
    >
      <Box sx={{width:"max-content"}}>{label}</Box>

      <Autocomplete
        value={mode}
        id="mode"
        sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}
        options={modes}
        autoHighlight
        getOptionLabel={(option) => option.name}
        // isOptionEqualToValue={(option) => option.name}
        renderOption={(props, option) => (
          <Box
            component="li"
            sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
            {...props}
            key={option.id}
          >
            {option.name}
          </Box>
        )}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Chế độ"
            inputProps={{
              ...params.inputProps,
              autoComplete: "new-password", // disable autocomplete and autofill
            }}
          />
        )}
        onChange={(event, value) => handleChangeMode(value)}
      />
    </Box>
  );
};
export const NoneLessonDiv = ({ newMaterial }) => {
  const dispatch = useDispatch();


  const handleChangeMode = (value) => {
    console.log("value",value)
    dispatch(setMode({ id: value.id, name: value.name }));
    setM({ id: value.id, name: value.name });
  };
  const [mode, setM] = useState(
    newMaterial
      ? newMaterial.visualType === "PUBLIC"
        ? { id: "PUBLIC", name: "Công khai" }
        :newMaterial.visualType === "RESTRICT"
        ? { id: "RESTRICT", name: "Hạn chế" }
        : { id: "PRIVATE", name: "Chỉ mình tôi" }
      : { id: "PUBLIC", name: "Công khai" }
  );
  const [modes, setModes] = useState([
    { id: "PUBLIC", name: "Công khai" },
    { id: "RESTRICT", name: "Hạn chế" },
    { id: "PRIVATE", name: "Chỉ mình tôi" },

  ]);
  return (
    <Box
      sx={{
        width: "100%",
        display: "flex",
        justifyContent: "space-between",
        alignItems: {xs:"flex-end",md:"flex-start"},
        flexDirection:{xs:"column",md:"row"}
      }}
    >
      <Box>
        <LabelWInput
          label="Nhập tên :"
          onChangeValue={(e) => dispatch(setName(e.target.value))}
          value={newMaterial?.name}
        />
        <LabelWMuitiInput
          newMaterial={newMaterial}
          label="Thêm tag* :"
          placeholder="điền thẻ tag"
        />
      </Box>
      <Box>
        <LabelWSelectV2 label="Chế độ*:">
          <Autocomplete
            value={modes[modes.findIndex((item) => item.id === mode.id)]}
            id="mode"
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
            options={modes}
            autoHighlight
            getOptionLabel={(option) => option.name}
            // isOptionEqualToValue={(option) => option.name}
            renderOption={(props, option) => (
              <Box
                component="li"
                sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                {...props}
                key={option.id}
              >
                {option.name}
              </Box>
            )}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Chế độ"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password", // disable autocomplete and autofill
                }}
              />
            )}
            onChange={(event, value) => handleChangeMode(value)}
          />
        </LabelWSelectV2>
        <LabelWTextAria
          label="Mô tả :"
          placeholder="Điền thông tin mô tả"
          onChangeValue={(e) => dispatch(setDescription(e.target.value))}
        />
      </Box>
    </Box>
  );
};

export const LabelWInput = ({ label, onChangeValue, value }) => {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "center",
        margin: "20px 0",
      }}
    >
      <Box sx={{width:"max-content"}}>{label}</Box>
      <TextField
        value={value}
        sx={{ width: {xs:200,md:400}, marginLeft: "20px" }}
        id="outlined-basic"
        placeholder={label}
        variant="outlined"
        onChange={onChangeValue}
      />
    </Box>
  );
};
export const LabelWMuitiInput = ({ label, placeholder, newMaterial,isMedia }) => {
  // console.log("newMaterial?.tags", newMaterial?.tags);
 
  const dispatch = useDispatch();
  const [tags, setTagList] = useState(null);
  const [selectedTags, setSelectedTags] = useState(newMaterial?.tagList?newMaterial?.tagList:[]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    console.log("selectedTags: ",selectedTags);
    // console.log("PushId(selectedTags): ",PushId(selectedTags));

    getSuggestTag();
    dispatch(setTags(PushId(selectedTags)));

  }, []);
  function findIndexes(tags, selectedTags) {
    // Sử dụng map để tạo mảng chỉ mục
    return selectedTags.map((oldItem) => {
      // Sử dụng findIndex để tìm chỉ mục của phần tử trong subjectList có id tương ứng
      return tags.findIndex((tag) => tag.tagId === oldItem.tagId);
    });
  }
  const getSuggestTag = async () => {
    setIsLoading(true);
    try {
      const res = await getALLTagGlobal();
      if (res.status === 200) {
        // console.log("getSuggestTag", res.data);

        setTagList(res.data);
      }
    } catch (e) {
      console.log("e", e);
    }
    setIsLoading(false);
  };
  const handleChangeInputSearch = (value) => {
    dispatch(setTags(PushId(value)));

    setSelectedTags(value);
  };
  const PushId = (tags) => {
    return tags.map((tag) => {
      return tag.tagId;
    });
  };
  const tagsProps = {
    options: tags,
    isOptionEqualToValue: (option) => option.tagName,
  };
  // console.log(tags);
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        margin: "20px 0",
      }}
    >
      <Box sx={{width:"max-content"}}>{label}</Box>
      {!isLoading && (
        <Autocomplete
          // {...tagsProps}
          sx={{ width: {xs:200,md:400} , marginLeft: "20px" }}
          multiple
          id="tags-outlined"
          options={tags}
          getOptionLabel={(option) => option?.tagName}
          // defaultValue={[tags[0].tagName]}
          value={findIndexes(tags, selectedTags).map((item) => tags[item])}
          filterSelectedOptions
          renderInput={(params) => (
            <TextField {...params} label="Thẻ tag" placeholder="ít nhất 3 thẻ tag" />
          )}
          onChange={(event, value) => handleChangeInputSearch(value)}
        />
      )}
    </Box>
  );
};

export const areAllFilesImagesOrVideos = (fileList) => {
  const imageExtensions = ['.jpg', '.jpeg', '.png', '.gif', '.bmp'];
  const videoExtensions = ['.mp4', '.avi', '.mov', '.mkv', '.wmv'];

  // Kiểm tra đuôi của mỗi tệp trong danh sách
  for (const filePath of fileList) {
    const fileExtension = filePath.name.toLowerCase().split('.').pop();

    // Kiểm tra xem có phải là hình ảnh hoặc video không
    if (
      imageExtensions.includes(`.${fileExtension}`) ||
      videoExtensions.includes(`.${fileExtension}`)
    ) {
      // Tiếp tục kiểm tra các tệp khác
      continue;
    } else {
      // Nếu có tệp không phải là hình ảnh hoặc video, trả về false
      return false;
    }
  }

  // Nếu tất cả các tệp là hình ảnh hoặc video, trả về true
  return true;
}

export const  convertFilesToFormData = (files) => {
  const fileObject = {};

  // Lặp qua mỗi đối tượng File trong mảng
  for (let i = 0; i < files.length; i++) {
    // Thêm đối tượng File vào đối tượng với key là số thứ tự
    fileObject[i] = files[i];
  }

  return fileObject;
}
export const  convertFilesToArrayFile = (files) => {
  let fileList = []
    
    
    try{
      for (const file of files) {
      
      
        fileList.push(file)
        
       }
    }catch(e){
      return []
    }
    return fileList
    
}

/// 3 step

export const ChooseFileStepper = ({ fileInput, setFileInput }) => {
  const dispatch = useDispatch();
  const [progress, setProgress] = useState(0);
  useEffect(()=>{
    dispatch(setIsDetail(true));
  },[])
  function isValidFile(fileName) {
    // Danh sách các đuôi file hợp lệ
    const validFileExtensions = ["PPTX", "PDF", "DOCX", "JPG", "PNG", "JPEG", "MP3", "MP4"];
  
    // Lấy đuôi file từ tên file
    const fileExtension = fileName.split('.').pop().toUpperCase();
  
    // Kiểm tra xem đuôi file có nằm trong danh sách hợp lệ không
    if (validFileExtensions.includes(fileExtension)) {
      return true; // Đuôi file hợp lệ
    } else {
      return false; // Đuôi file không hợp lệ
    }
  }
  const handleInputChange = (e) => {
    setProgress(0);

    const files = e.target.files;
    const reader = new FileReader();
    console.log("files", files);
  let fileList = []
    
    let checkHasMaxSize = false;
    for (const file of files) {
      const { name, type, size, lastModified } = file;
      // Làm gì đó với các thông tin trên
      // console.log("type",type)
      // if(size / (1024*1024) > 24){
      //   checkHasMaxSize = true
      //   toast.warning(name+" quá 24MB");
      // }
      // if(!isValidFile(name)){
      //   checkHasMaxSize = true
      //   toast.warning(name+" không hợp lệ");
      // }
     fileList.push(file)
     
    }
    // if(!checkHasMaxSize){
      setProgress(100);
      setFileInput(fileList);
    // }
    
  };
  // const arrayFile = convertFilesToArrayFile(fileInput)
const handleDeleteFile = (name) =>{
  const newFileList = fileInput.filter((file) => file.name !== name)
  setFileInput(newFileList);

}
  return (
    <ChooseFileStepperDiv>
        <ToastContainer />

      <ContainerInputDiv>
        <Form encType="multipart/form-data" style={{ width: "100%" }}>
          <BorderInput>
            <FileUploadDiv>
              <Box sx={{ fontSize: "24px" }}>Đưa tài liệu lên hệ thống</Box>
              <FileUploadLabel>Chọn File</FileUploadLabel>
              <Box sx={{ fontSize: "16px", color: "gray" }}>
                hỗ trợ các file .docs .pptx .pdf .jpg ...
              </Box>
              <CustomUploadInput
             accept=".doc,.docx,.pptx,.ppt,.jpg,.png,.jpeg,.mp3,.mp4"
                type="file"
                name="file"
                multiple
                onChange={handleInputChange}
              />
            </FileUploadDiv>
          </BorderInput>
        </Form>
      </ContainerInputDiv>
      <Box sx={{ width: "80%",mt:"30px" }}>
        <Box sx={{ width: "100%",maxHeight:"300px",mt:"30px",overflowX:"auto" }}>
          
            {fileInput === null || fileInput?.length  === 0? "": 
              fileInput.map((file,index) =>
              <HoverCenterFlexBox style={{justifyContent: "space-between", }} key={index}>
              <Box >{file.name}</Box>
              <DeleteButton onClick={()=>handleDeleteFile(file.name)}style={{display:"flex",justifyContent: "center",alignItems:"center",borderRadius:"25px",}}><FaDeleteLeft /></DeleteButton>
            </HoverCenterFlexBox>
              
              )

            }
          </Box>
        <Box sx={{mt:"30px"}}>
          {(fileInput !== null &&fileInput?.length  !== 0)
          && <>Tổng có {fileInput?.length} file được chọn
              <LinearWithValueLabel progress={progress} />
              </>}
          
        </Box>

      </Box>
    </ChooseFileStepperDiv>
  );
};

export const DetailStepper = ({ type, newMaterial, fileInput }) => {
  // const newMaterial = useSelector((state) => state.newMaterial)
  // console.log("fileInput",fileInput)
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();
  // const arrayFile = convertFilesToArrayFile(fileInput)
  useEffect(() => {
    getSubjects();
    if(fileInput.length === 1){
        dispatch(setName(fileInput[0].name))

    }else{
      dispatch(setName(""))

    }
  }, []);
  const [subjects, setSubjects] = useState([]);
  // const [subject, setSubject] = useState({id: newMaterial.subject.id, name: newMaterial.subject.name, active: true, userId:0, createdAt: ''});
  const getSubjects = async () => {
    // const api = "subject/list";
    setIsLoading(true)
    try {
      const res = await getALLSubject();

      if (res.status === 200) {
        setSubjects(res.data);
      } else {
        console.log(`subjects not found`);
      }
    } catch (error) {
      console.log(`Can not get subjects ${error}`);
    }
    setIsLoading(false)

  };
 
  //
  const [selectedValue, setSelectedValue] = useState(newMaterial.isDetail);


  return (
    <Container>
      {!isLoading ? <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "flex-start",
          alignItems: " flex-start",
          border: "1px solid #E3E3E3",
          padding: "30px",
          margin: "20px 0",
          borderRadius: "15px",
        }}
      >
        <Box sx={{ width: "100%",mt:"10px" }}>
        <Box sx={{ width: "100%",maxHeight:"200px",overflowX:"auto" }}>
          
            {fileInput === null ? "":fileInput?.length > 1 ? 
              fileInput?.map((file) =>
                  
                    <Box sx={{ marginBottom: "20px" }}>{file.name}</Box>
                    
              
              ):
              <Box sx={{ marginTop: "50px" }}>
                {fileInput ? fileInput[0].name : ""}
              </Box>

            }
          </Box>
        <Box sx={{mt:"30px" ,mb:"30px",borderBottom:"1px solid gray"}}>
          {fileInput !== null 
          && <>Tổng có {fileInput?.length} file được chọn
             
              </>}
          
        </Box>

      </Box>
            <Box sx={{width:"50%",display:"flex",justifyContent:"flex-end"}}>
              <FcDown />
            </Box>
            <LabelWSelectV2 label="Môn học*:">
              <MUISubjectSelect value={newMaterial?.subjectId} />
            </LabelWSelectV2>
       
        {selectedValue ? (
          <DetailFolderMaterial newMaterial={newMaterial} />
        ) : (
          <NoneLessonDiv newMaterial={newMaterial} />
        )}
      </Box>
      :<LoadingTab />
      }
    </Container>
  );
};
export const FinishStepper = ({
  fileInput,
  newMaterial,
  isLoading,
  isSuccess,
}) => {
  // const [isLoading, setIsLoading] = useState(true)
  // const [isSuccess, setIsSuccess] = useState(false)

  // useEffect(() => {
  //   UploadFile();
  // }, [])

  // const UploadFile = async() =>{
  //   // const newMaterial = useSelector(state => state.newMaterial?.material)
  //   console.log("newMaterial first upload",newMaterial)
  //   console.log("fileInput",fileInput)

  //   const formData = new FormData();
  //   formData.append("files", fileInput);
  //   formData.append("subjectId", newMaterial?.subject.id+"");
  //   formData.append("visualType", newMaterial?.mode.id);
  //   formData.append("description", newMaterial?.description);
  //   formData.append("name", newMaterial?.name);

  //     if(newMaterial.isDetail){
  //       formData.append("LessonId", newMaterial?.lesson.id+"");

  //     }else{
  //       formData.append("tagList", newMaterial?.tags);

  //     }

  //   console.log("formData",formData)

  //   const res = await uploadFile(formData);
  //   console.log("res upload",res)
  //   if(res.status === 200){
  //     toast.success("Đã đưa tài liệu lên hệ thống ");
  //     setIsSuccess(true)
  //   }else{
  //     toast.error("Không thể đưa tài liệu lên hệ thống ");

  //   }
  //   setIsLoading(false)
  // }

  return (
    <Container>
      {isLoading ? (
        <LoadingTab />
      ) : (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            marginTop: "40px",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          {isSuccess ? (
            <>
              <Avatar
                sx={{ width: 200, height: 200 }}
                alt="Remy Sharp"
                src="https://cdn-icons-png.flaticon.com/512/148/148767.png"
              />
              <Box
                sx={{
                  marginTop: "40px",
                  fontSize: "26px",
                  color: "green",
                  fontWeight: 500,
                }}
              >
                Tải lên học liệu thành công
              </Box>
              <Box sx={{ marginTop: "20px", fontSize: "18px", color: "green" }}>
                Cảm ơn bạn đã đóng góp học liệu vào hệ sinh thái của chúng tôi.
              </Box>
            </>
          ) : (
            "Không thành công"
          )}
        </Box>
      )}
    </Container>
  );
};
export const FailFinishStepper = ({ fileInput }) => {
  return (
    <Container>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          marginTop: "40px",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        Chưa thể đưa tài liệu lên, vui lòng kiểm tr lại !
      </Box>
    </Container>
  );
};
