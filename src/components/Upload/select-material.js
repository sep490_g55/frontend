import {
    Avatar,
    Box,
    Chip,
    Container,
    FormControl,
    Link,
    MenuItem,
    Select,
    styled,
    InputLabel,
  } from "@mui/material";
  import TextField from "@mui/material/TextField";
  import Autocomplete from "@mui/material/Autocomplete";
  import Radio from "@mui/material/Radio";
  
  import {
    BorderInput,
    CustomUploadInput,
    FileUploadDiv,
    FileUploadLabel,
    ChooseFileStepperDiv,
    ContainerInputDiv,
  } from "./styles";
  import { LinearWithValueLabel } from "../Process/LinearWithLabel";
  import { Form } from "reactstrap";
  import { useEffect, useState } from "react";
  import { getFolderSrc } from "@/utils/folderSrc";
  import { ButtonAction } from "../common_sections";
  import Image from "next/image";
  
// choose material
export const ClassSelect = () =>{
    const [nameClass, setNameClass] = useState('');
  
    const handleChange = (event) => {
        setNameClass(event.target.value);
    };
  
    useEffect(() => {
        getClasses();
  
    }, []);
    const [classes, setClasses] = useState([]);
    const getClasses = async () => {
      const api = "classes/";
  
      try {
        const res = await getFolderSrc(api);
  
        if (res) {
          setClasses(res);
          
        } else {
          console.log(`classes not found`);
        }
      } catch (error) {
        console.log(`Can not get classes ${error}`);
      }
    };
    return (
      <Box sx={{ minWidth: 120 ,maxWidth:'100%'}}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Lớp</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={nameClass}
            label="Lớp"
            onChange={handleChange}
            
          >
            {classes.map((clas,index) =>
              <MenuItem key={index} value={clas.class_id}>{clas.name}</MenuItem>
            )}
            
            
          </Select>
        </FormControl>
      </Box>
    );
  }

  export const BookSeriesSelect = () =>{
    const [bookSeriesName, setBookSeriesName] = useState('');
  
    const handleChange = (event) => {
      setBookSeriesName(event.target.value);
    };
  
    useEffect(() => {
      getBookSeries();
  
    }, []);
    const [bookSeries, setBookSeries] = useState([]);
    const getBookSeries = async () => {
      const api = "bookseries/";
  
      try {
        const res = await getFolderSrc(api);
  
        if (res) {
          setBookSeries(res);
          
        } else {
          console.log(`setBookSeries not found`);
        }
      } catch (error) {
        console.log(`Can not get BookSeries ${error}`);
      }
    };
    return (
      <Box sx={{ minWidth: 120 ,maxWidth:'100%'}}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Bộ sách</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={bookSeriesName}
            label="Bộ sách"
            onChange={handleChange}
            
          >
            {bookSeries.map((bs,index) =>
              <MenuItem key={index} value={bs.book_series_id}>{bs.name}</MenuItem>
            )}
            
            
          </Select>
        </FormControl>
      </Box>
    );
  }

  export const SubjectSelect = () =>{
    const [subjectName, setSubjectName] = useState('');
  
    const handleChange = (event) => {
      setSubjectName(event.target.value);
    };
  
    useEffect(() => {
      getSubjects();
  
    }, []);
    const [subjects, setSubjects] = useState([]);
    const getSubjects = async () => {
      const api = "subjects/";
  
      try {
        const res = await getFolderSrc(api);
  
        if (res) {
          setSubjects(res);
          
        } else {
          console.log(`setSubjects not found`);
        }
      } catch (error) {
        console.log(`Can not get setSubjects ${error}`);
      }
    };
    return (
      <Box sx={{ minWidth: 120 ,maxWidth:'100%'}}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Môn học</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={subjectName}
            label="Môn học"
            onChange={handleChange}
            
          >
            {subjects.map((subject,index) =>
              <MenuItem key={index} value={subject.subject_id}>{subject.name}</MenuItem>
            )}
            
            
          </Select>
        </FormControl>
      </Box>
    );
  }

  export const BookVolumeSelect = () =>{
    const [bookVolume, setBookVolume] = useState('');
  
    const handleChange = (event) => {
      setBookVolume(event.target.value);
    };
  
    useEffect(() => {
      getBookVolume();
  
    }, []);
    const [bookVolumes, setBookVolumes] = useState([]);
    const getBookVolume = async () => {
      const api = "bookvolume/";
  
      try {
        const res = await getFolderSrc(api);
  
        if (res) {
          setBookVolumes(res);
          
        } else {
          console.log(`setBookVolumes not found`);
        }
      } catch (error) {
        console.log(`Can not get setBookVolumes ${error}`);
      }
    };
    return (
      <Box sx={{ minWidth: 120 ,maxWidth:'100%'}}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Đầu sách</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={bookVolume}
            label="Đầu sách"
            onChange={handleChange}
            
          >
            {bookVolumes.map((bookVolume,index) =>
              <MenuItem key={index} value={bookVolume.book_volume_id}>{bookVolume.name}</MenuItem>
            )}
            
            
          </Select>
        </FormControl>
      </Box>
    );
  }

  export const ChapterSelect = () =>{
    const [chapter, setChapter] = useState('');
  
    const handleChange = (event) => {
      setChapter(event.target.value);
    };
  
    useEffect(() => {
      getChapters();
  
    }, []);
    const [chapters, setChapters] = useState([]);
    const getChapters = async () => {
      const api = "chpaters/";
  
      try {
        const res = await getFolderSrc(api);
  
        if (res) {
          setChapters(res);
          
        } else {
          console.log(`setChapters not found`);
        }
      } catch (error) {
        console.log(`Can not get setChapters ${error}`);
      }
    };
    return (
      <Box sx={{ minWidth: 120 ,maxWidth:'100%'}}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Chương</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={chapter}
            label="Chương"
            onChange={handleChange}
            
          >
            {chapters.map((chapter,index) =>
              <MenuItem key={index} value={chapter.class_id}>{chapter.name}</MenuItem>
            )}
            
            
          </Select>
        </FormControl>
      </Box>
    );
  }

  export const LessonSelect = () =>{
    const [lesson, setLesson] = useState('');
  
    const handleChange = (event) => {
      setLesson(event.target.value);
    };
  
    useEffect(() => {
      getLessons();
  
    }, []);
    const [lessons, setLessons] = useState([]);
    const getLessons = async () => {
      const api = "lessons/";
  
      try {
        const res = await getFolderSrc(api);
  
        if (res) {
          setLessons(res);
          
        } else {
          console.log(`setLessons not found`);
        }
      } catch (error) {
        console.log(`Can not get setLessons ${error}`);
      }
    };
    return (
      <Box sx={{ minWidth: 120 ,maxWidth:'100%'}}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Bài học</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={lesson}
            label="Bài học"
            onChange={handleChange}
            
          >
            {lessons.map((lesson,index) =>
              <MenuItem key={index} value={lesson.class_id}>{lesson.name}</MenuItem>
            )}
            
            
          </Select>
        </FormControl>
      </Box>
    );
  }