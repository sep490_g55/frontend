import {
    Avatar,
    Box,
    Chip,
    Container,
    FormControl,
    Link,
    MenuItem,
    Select,
    styled,
    InputLabel,
  } from "@mui/material";
  import TextField from "@mui/material/TextField";
  import Autocomplete from "@mui/material/Autocomplete";
  import Radio from "@mui/material/Radio";
  
  import {
    BorderInput,
    CustomUploadInput,
    FileUploadDiv,
    FileUploadLabel,
    ChooseFileStepperDiv,
    ContainerInputDiv,
  } from "./styles";
  import { LinearWithValueLabel } from "../Process/LinearWithLabel";
  import { Form } from "reactstrap";
  import { useEffect, useState } from "react";
  import { getFolderSrc } from "@/utils/folderSrc";
  import { ButtonAction } from "../common_sections";
  import Image from "next/image";
import { getAddress } from "@/utils/addressApi";
  
// choose address
export const ProvinceSelect = ({provinceID,setProvinceID}) =>{
  
    const handleChange = (event) => {
        setProvinceID(event.target.value);
    };
  
    useEffect(() => {
        getProvinces();
  
    }, []);
    const [provinces, setProvinces] = useState([]);
    const getProvinces = async () => {
      const api = "?depth=1";
  
      try {
        const res = await getAddress(api);
  
        if (res) {
            setProvinces(res);
          
        } else {
          console.log(`setProvinces not found`);
        }
      } catch (error) {
        console.log(`Can not get setProvinces ${error}`);
      }
    };
    return (
      <Box sx={{ width:'200px',height: '45px',

      borderRadius: '13px',
      backgroundColor: '#eeeeef',
      border: 'none',}}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Tỉnh/TP</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="province-select"
            value={provinceID}
            label="Tỉnh/TP"
            onChange={handleChange}
           
          >
            <MenuItem key="999" value="">Không chọn gì cả</MenuItem>
            {provinces.map((province,index) =>
              <MenuItem key={index} value={province.code}>{province.name}</MenuItem>
            )}
            
            
          </Select>
        </FormControl>
      </Box>
    );
  }

  export const DistrictSelect = ({districtID,setDistrictID,provinceID}) =>{
  
    const handleChange = (event) => {
        setDistrictID(event.target.value);
    };
  
    useEffect(() => {
        getProvince();
  
    }, [provinceID]);
    const [province, setProvince] = useState({});
    const [count, setCount] = useState(0);

    const getProvince = async () => {
      const api = provinceID === "" ? "d/" : `p/${provinceID}?depth=2`
        
      try {
        const res = await getAddress(api);
  
        if (res) {
            // console.log("res: ",res);
            setProvince(res);
          
        } else {
          console.log(`province not found`);
        }
      } catch (error) {
        console.log(`Can not get province ${error}`);
      }
    };
    
    return (
      <Box sx={{ minWidth: 120 ,maxWidth:'100%'}}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Quận/huyện</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={districtID}
            label="Quận/huyện"
            onChange={handleChange}
            
          >
            <MenuItem key="999" value="">Không chọn gì cả</MenuItem>

            {
                province.districts === undefined ? "" : province.districts.map((district,index) => <MenuItem key={index} value={district.code}>{district.name}</MenuItem> )

                
            }
            
            
          </Select>
        </FormControl>
      </Box>
    );
  }

  export const WardSelect = ({wardID,setWardID,districtID}) =>{
  
    const handleChange = (event) => {
        setWardID(event.target.value);
    };
  
    useEffect(() => {
        getProvince();
  
    }, [districtID]);
    const [district, setDistrict] = useState({});

    const getProvince = async () => {
      const api = districtID === "" ? "w/" : `d/${districtID}?depth=2`
        
      try {
        const res = await getAddress(api);
  
        if (res) {
            // console.log("res: ",res);
            setDistrict(res);
          
        } else {
          console.log(`setDistrict not found`);
        }
      } catch (error) {
        console.log(`Can not get setDistrict ${error}`);
      }
    };
    
    return (
      <Box sx={{ minWidth: 120 ,maxWidth:'100%'}}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Phường/Xã</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={wardID}
            label="Phường/Xã"
            onChange={handleChange}
            
          >
            <MenuItem key="999" value="">Không chọn gì cả</MenuItem>

            {
                district.wards === undefined ? "" : district.wards.map((ward,index) => <MenuItem key={index} value={ward.code}>{ward.name}</MenuItem> )

                
            }
            
            
          </Select>
        </FormControl>
      </Box>
    );
  }

