import * as React from "react";
import { styled, alpha } from "@mui/material/styles";
import Button from "@mui/material/Button";

export const ChooseFileStepperDiv = styled('div')(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
  }));
  export const ContainerInputDiv = styled('div')(({ theme }) => ({
    width:'90%',
    padding: 0,
            margin: 0,
            boxSizing: 'border-box',
            display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      color:'green'
  }));
export const CustomUploadInput = styled("input")(() => ({
  display: "block",
  height: "100%",
  width: "100%",
  position: "absolute",
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  opacity: 0,
  cursor: "pointer",
}));

export const BorderInput = styled("div")(() => ({
  width: "100%",
  margin: "auto",
  padding: "2rem",
  backgroundColor: "#ffffff",
  borderRadius: "25px",
  boxShadow: "7px 20px 20px rgb(210, 227, 244)",
  
}));

export const FileUploadDiv = styled("div")(() => ({
  textAlign: "center",
  border: "3px dashed rgb(210, 227, 244)",
  borderRadius:'20px',
  padding: "1.5rem",
  position: "relative",
  cursor: "pointer",
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
      justifyContent: 'center',
}));

export const FileUploadLabel = styled("div")(() => ({
  width: "100px",
  height: "40px",
  border: "1px solid green",
  borderRadius:'20px',
  display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
  margin: '20px 0',

}));

