import * as React from 'react';
import { emphasize, styled } from '@mui/material/styles';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Chip from '@mui/material/Chip';
import HomeIcon from '@mui/icons-material/Home';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { MdAddModerator, MdPublic } from 'react-icons/md';
import { AiOutlineOrderedList } from 'react-icons/ai';
import { Box } from '@mui/material';

const StyledBreadcrumb = styled(Chip)(({ theme }) => {
  const backgroundColor =
    theme.palette.mode === 'light'
      ? theme.palette.grey[100]
      : theme.palette.grey[800];
  return {
    backgroundColor,
    height: theme.spacing(3),
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular,
    '&:hover, &:focus': {
      backgroundColor: emphasize(backgroundColor, 0.06),
    },
    '&:active': {
      boxShadow: theme.shadows[1],
      backgroundColor: emphasize(backgroundColor, 0.12),
    },
  };
}); // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591

function handleClick(event) {
  event.preventDefault();
//   console.info('You clicked a breadcrumb.');
}

export const ModerateBreadcrumbs=({label1,icon1,label2,icon2})=> {
  return (
    <Box role="presentation" onClick={handleClick}>
      <Breadcrumbs aria-label="breadcrumb">
        <StyledBreadcrumb
          component="a"
          href="#"
          label="Học liệu kiểm duyệt"
          icon={<MdAddModerator fontSize="small" />}
        />
        <StyledBreadcrumb component="a" href="#"
         label={label1}
        //  "Kho học liệu chung" 
         icon={icon1}
        //  {<MdPublic/>}
         
         />
        <StyledBreadcrumb
          label={label2}
        //   "Danh sách học liệu"
          deleteIcon={<ExpandMoreIcon />}
          onDelete={handleClick}
          icon={icon2}
        //   {<AiOutlineOrderedList />}
        />
      </Breadcrumbs>
    </Box>
  );
}