import * as React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { styled, alpha } from "@mui/material/styles";
import {
  changePassword,
  changeStatusBookSeries,
  getAllPermission,
  getFolderSRC,
  getLocalStorage,
  getMyMaterialByID,
  getUserToShare,
  setPermissionRole,
  ShareMaterial,
  updateMaterial,
  updatePermission,
  changeStatusResource,
  getALLTagGlobal,
  getResourceTag,
  setResourceTags,
  rejectResource,
  reportMaterial,
  createReportComment,
  actionResource,
  deleteComment,
  unShareResource,
  getResourceSharedUsers,
} from "@/dataProvider/agent";
import { useForm } from "react-hook-form";
import yup from "@/utils/yupGlobal";
import { yupResolver } from "@hookform/resolvers/yup";
import Clipboard from "react-clipboard.js";
import { format } from "date-fns";

import {
  Autocomplete,
  Avatar,
  Box,
  FormControl,
  InputLabel,
  OutlinedInput,
  Stack,
} from "@mui/material";
import { BiSolidSchool } from "react-icons/bi";

import {
  AvatarAndInfoAnDeleteDiv,
  AvatarAndInfoDiv,
  DeleteButton,
  AvatarAndInfoOwnerDiv,
  IconAndInfoDiv,
  AccessibleDiv,
  ButtonAction,
  WhiteButtonAction,
  IconAndText,
  SaveButton,
  GreenButtonAction,
  FullColorAvatarAndInfoDiv,
  CenterFlexBox,
  TitleWithinInfo,
} from "../common_sections";
import { NextUIButton } from "../NextUIComponents/custom-components";
//icon
import { AiFillCaretDown, AiFillDelete } from "react-icons/ai";
import { useState } from "react";
import { Form } from "reactstrap";
import { MdOutlinePublic } from "react-icons/md";
import { BsLink } from "react-icons/bs";
import { RiGitRepositoryPrivateLine, RiLockPasswordLine } from "react-icons/ri";
import { BiSolidEditAlt } from "react-icons/bi";

import { BsShareFill } from "react-icons/bs";

//toast
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { DetailFolderMaterial } from "../Upload/stepper_sections";
import { IconAndTitleDiv } from "@/components/DashBoard/MyMaterial/sections";
import { FaTags } from "react-icons/fa";
import { PiPlusCircleDuotone } from "react-icons/pi";
import {
  BookSeriesSelect,
  BookVolumeSelect,
  ChapterSelect,
  ClassSelect,
  SubjectSelect,
} from "../Upload/select-material";
import {
  MUIBookSeriesSelect,
  MUIClassSelect,
  MUISubjectSelect,
} from "../Select/AddressSelect/NEXUI/folder";
import { useEffect } from "react";
// import { getMaterials,  updateFolder } from "@/utils/folderSrc";
import { Input } from "@nextui-org/react";
import {
  getALLSubject,
  getALLSubjectBookSeriesByBookSeriesID,
  getBookSeries,
  getClass,
  setBookSeries,
  setBookVolume,
  setChapter,
  setClass,
  setLesson,
  setSubject,
  setSubjectBookSeries,
  updateBookSeries,
  updateBookVolume,
  updateChapter,
  updateClass,
  updateLesson,
  updateSubject,
  addNewPermission,
  addNewRole,
} from "@/dataProvider/agent";
import { LoadingButton } from "@mui/lab";
import { BeatLoader } from "react-spinners";
import { useSelector } from "react-redux";
import LoadingTab from "../loading-screen/LoadingTab";
import { useDispatch } from "react-redux";
import { setIsUpdate } from "@/redux/slice/ViewFolderSRCSlice";
import { useRouter } from "next/router";
import { useAuthContext } from "@/auth/useAuthContext";
import {
  setTags,
  setName,
  setDescription,
  setViewBookSeries,
  setViewSubject,
  setViewBookVolume,
  setViewClass,
  setViewLesson,
  setViewChapter,
  setIsDetail,
  setMode,
} from "@/redux/slice/NewMaterialSlice";
import CardWithDescription from "../card/CardWithDescription";
import { setIsReload } from "@/redux/slice/IsReLoad";
import { ACTION_UNSAVED } from "@/config";
import { IoMdPeople } from "react-icons/io";

const CustomDialog = styled((props) => <Dialog {...props} />)(({ theme }) => ({
  "& .MuiPaper-elevation": {
    maxWidth: "100%",
  },
}));
export default function FormDialog({
  children,
  title,
  label,
  open,
  setOpen,
  setClose,
  handleSubmit,
  type,
  valueText,
  setValueText,
  classID,
  bookSeriesID,
  subjectID,
  chapterID,
  bookVolumeID,
  lessonID,
  isLoading,
  setPass,
  setOldPass,
  setRePass,
  setVisualType,
  setUserShareIds,
  id,
  value,
  setFileInput,
  setContentReport,
  contentReport,
  setDescription,
  setMethodType,
  setPath,
  description,
  methodType,
  path,
  permission,
  setPermission,
  setUpdatePermission,
  viewType,
  folderID,
}) {
  // const [open, setOpen] = React.useState(false);

  // const handleClickOpen = () => {
  //   setOpen(true);
  // };

  // const handleClose = () => {
  //   setOpen(false);

  // };
  // console.log("value  dialog",value)
  return (
    <>
      {/* <Button variant="outlined" onClick={handleClickOpen}>
        Open form dialog
      </Button> */}
      <Box onClick={setOpen}>{children}</Box>
      <CustomDialog open={open} onClose={setClose}>
        {/* <ToastContainer /> */}

        <DialogTitle sx={{ textAlign: "center" }}>{title}</DialogTitle>
        {type == "change-store" ? (
          <ChangeStoreUpload
            valueText={valueText}
            setValueText={setValueText}
          />
        ) : type == "modaratate" ? (
          <ModaratateResource />
        ) : type == "addFolderTag" ? (
          <SetFolderTagDialog
            viewType={viewType}
            setValueText={setValueText}
            folderID={folderID}
          />
        ) : type == "addPermission" ? (
          <AddPermissionDialogContent
            description={description}
            methodType={methodType}
            path={path}
            permission={permission}
            setValueText={setValueText}
            setPath={setPath}
            setMethodType={setMethodType}
            setDescription={setDescription}
          />
        ) : type == "viewCommentReported" ? (
          <ViewCommentDialogContent 
          value={value}
          valueText={valueText}
          setValueText={setValueText}/>
        ) : type == "viewMaterialReported" ? (
          <ViewCommentMaterialReportedContent
            value={value}
            valueText={valueText}
            setValueText={setValueText}
          />
        ) : type == "setPermission" ? (
          <SetPermissionDialog
            valueText={valueText}
            permission={permission}
            description={description}
            setValueText={setValueText}
            setDescription={setDescription}
            setPermission={setPermission}
            value={value}
            isLoading={isLoading}
            setUpdatePermission={setUpdatePermission}
          />
        ) : type == "addRole" ? (
          <AddRoleDialog
            setValueText={setValueText}
            setDescription={setDescription}
          />
        ) : type == "addLesson" ? (
          <AddLessonDialog
            valueText={valueText}
            setValueText={setValueText}
            lessonID={lessonID}
          />
        ) : type == "addChapter" ? (
          <AddChapterDialog
            valueText={valueText}
            setValueText={setValueText}
            classID={classID}
            bookSeriesID={bookSeriesID}
            subjectID={subjectID}
            bookVolumeID={bookVolumeID}
            chapterID={chapterID}
          />
        ) : type == "addBookVolume" ? (
          <AddBookVolumeDialog
            valueText={valueText}
            setValueText={setValueText}
            classID={classID}
            bookSeriesID={bookSeriesID}
            subjectID={subjectID}
            bookVolumeID={bookVolumeID}
          />
        ) : type == "addSubjectBookSeries" ? (
          <AddSubjectBookSeriesDialog
            valueText={valueText}
            setValueText={setValueText}
            classID={classID}
            bookSeriesID={bookSeriesID}
            subjectID={subjectID}
          />
        ) : type == "addSubject" ? (
          <AddSubjectDialog
            valueText={valueText}
            setValueText={setValueText}
            classID={classID}
            bookSeriesID={bookSeriesID}
            subjectID={subjectID}
          />
        ) : type == "addBookSeries" ? (
          <AddBookSeriesDialog
            valueText={valueText}
            setValueText={setValueText}
            classID={classID}
          />
        ) : type == "addClass" ? (
          <AddClassDialog
            valueText={valueText}
            setValueText={setValueText}
            classID={classID}
          />
        ) : type == "addtag" ? (
          <AddTagDialog />
        ) : type == "changepassword" ? (
          <ChangePassword
            setPass={setPass}
            setOldPass={setOldPass}
            setRePass={setRePass}
          />
        ) : type == "share" ? (
          <ShareDialog
            id={id}
            setVisualType={setVisualType}
            setUserShareIds={setUserShareIds}
          />
        ) : type == "editmaterial" ? (
          <EditMaterialDialog value={value} setFileInput={setFileInput} />
        ) : type == "deletematerial" ? (
          <DeleteMaterialDialog />
        ) :
        type == "unsharematerial" ? (
          <UnShareMaterialDialog />
        ) :type == "unsavematerial" ? (
          <UnSaveMaterialDialog />
        ) : type == "save" ? (
          <SaveMaterialDialog />
        ) : (
          <ComDialogContent
            label={label}
            setContentReport={setContentReport}
            contentReport={contentReport}
          />
        )}
        <DialogActions>
          <DeleteButton
            type="button"
            // sx={{ backgroundColor: "#fb5731", color: "white" }}
            onClick={setClose}
          >
            Hủy
          </DeleteButton>

          <LoadingButton
            color="inherit"
            size="large"
            type="submit"
            variant="contained"
            loading={isLoading}
            sx={{
              bgcolor: "#198754 !important",
              color: "white",
              "&:hover": {
                bgcolor: "#00D974 !important",
              },
            }}
            onClick={handleSubmit}
          >
            Xác nhận
          </LoadingButton>
        </DialogActions>
      </CustomDialog>
    </>
  );
}

export const ChangeStoreUpload = ({ valueText, setValueText }) => {
  // const [selected, setSelected] = useState(null);
  // valueText is card selected
  console.log("valueText", valueText);
  const handleClick = (value) => {
    setValueText(value);
  };
  const color = "#ffffff";
  const backgroundColor = "linear-gradient(135deg, #364a60, #384c6c)";
  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "300px" }}
    >
      <DialogContentText></DialogContentText>

      <CenterFlexBox>
        <Box onClick={() => handleClick(true)}>
          <CardWithDescription
            color={valueText && color}
            backgroundColor={valueText && backgroundColor}
            title="Kho học liệu"
            description="Học liệu liên quan tới chi tiết tới từng bài giảng, môn học (slide, hình ảnh, video, ...)"
          />
        </Box>
        <Box onClick={() => handleClick(false)}>
          <CardWithDescription
            color={!valueText && color}
            backgroundColor={!valueText && backgroundColor}
            title="Kho media"
            description="Chỉ bao gồm các hình ảnh, video liên quan tới môn học "
          />
        </Box>
      </CenterFlexBox>
    </DialogContent>
  );
};
export const SetFolderTagDialog = ({ setValueText, viewType, folderID }) => {
  const PushTags = (list) => {
    return list.map((item) => {
      return item.tagDTOResponse;
    });
  };
  const table_tbl =
    viewType === "CLASS"
      ? "class_tbl"
      : viewType === "BOOK-SERIES"
      ? "book_series_tbl"
      : viewType === "SUBJECT-BOOK-SERIES"
      ? "book_series_subject_tbl"
      : viewType === "BOOK-VOLUME"
      ? "book_volume_tbl"
      : viewType === "CHAPTER"
      ? "chapter_tbl"
      : "lesson_tbl";
  const [tags, setTagList] = useState(null);
  const [selectedTags, setSelectedTags] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getSuggestTag();
    // getOldResourceTags()
  }, []);
  function findIndexes(tags, selectedTags) {
    // Sử dụng map để tạo mảng chỉ mục
    return selectedTags.map((oldItem) => {
      // Sử dụng findIndex để tìm chỉ mục của phần tử trong subjectList có id tương ứng
      return tags.findIndex(
        (tag) => tag.tagId === oldItem.id || tag.tagId === oldItem.tagId
      );
    });
  }
  const getSuggestTag = async () => {
    setIsLoading(true);
    try {
      const res = await getALLTagGlobal();
      if (res.status === 200) {
        // console.log("getSuggestTag", res.data);

        setTagList(res.data);
      }
    } catch (e) {
      console.log("e", e);
    }
    try {
      const res = await getResourceTag({
        tableType: table_tbl,
        detailId: folderID,
      });
      if (res.status === 200) {
        // console.log("getSuggestTag", res.data);
        const oldTags = PushTags(res.data);

        if (oldTags?.length > 0) {
          setSelectedTags(oldTags);
        }
      }
    } catch (e) {
      console.log("e", e);
    }
    setIsLoading(false);
  };
  
  const handleChangeInputSearch = (value) => {
    setSelectedTags(value);
    setValueText(PushId(value));
    console.log("value", value);
  };
  const PushId = (tags) => {
    return tags.map((tag) => {
      return tag.tagId;
    });
  };

  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "250px" }}
    >
      <DialogContentText></DialogContentText>

      <Box
        component="form"
        sx={{
          "& > :not(style)": { mt: 3, width: "100%" },
        }}
        noValidate
        autoComplete="off"
      >
        {/* <TextField  onChange={handleInputRole} id="outlined-basic" label="Tên" variant="outlined" /> */}

        {isLoading ? (
          <LoadingTab />
        ) : (
          <Autocomplete
            // {...tagsProps}
            sx={{ width: "100%", marginLeft: "20px" }}
            multiple
            id="tags-outlined"
            options={tags}
            getOptionLabel={(option) => option?.tagName}
            // defaultValue={[tags[0].tagName]}
            value={findIndexes(tags, selectedTags).map((item) => tags[item])}
            filterSelectedOptions
            renderInput={(params) => (
              <TextField
                {...params}
                label="Thẻ tag"
                placeholder="ít nhất 3 thẻ tag"
              />
            )}
            onChange={(event, value) => handleChangeInputSearch(value)}
          />
        )}
      </Box>
    </DialogContent>
  );
};
export const AddClassDialog = ({ valueText, setValueText, classID }) => {
  const handleInputClassName = (e) => {
    setValueText(e.target.value);
  };
  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "100px" }}
    >
      <DialogContentText></DialogContentText>

      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "100%" },
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          id="outlined-basic"
          onChange={handleInputClassName}
          value={valueText}
          label="Tên lớp"
          variant="outlined"
        />
      </Box>
    </DialogContent>
  );
};
// within classID to show old classID  of old book series when update and add new is ingore
export const AddBookSeriesDialog = ({
  valueText,
  setValueText,
  classID,
  bookSeriesID,
}) => {
  const handleInputBookSeries = (e) => {
    setValueText(e.target.value);
  };
  const [cID, setClassID] = useState(1);
  // const [objClass, setObjClass] = useState(null)
  const [isLoading, setIsLoading] = useState(true);

  const viewFolder = useSelector((state) => state.viewFolder);

  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "180px" }}
    >
      <DialogContentText></DialogContentText>

      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "100%" },
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          id="outlined-basic"
          onChange={handleInputBookSeries}
          value={valueText}
          label="Tên bộ sách"
          variant="outlined"
        />
        {/* <MUIClassSelect handleChange={setClassID} value={cID}/> */}
        <Input
          isDisabled
          type="text"
          label="Lớp"
          defaultValue={viewFolder?.class.name}
          className="max-w-xs"
        />
      </Box>
    </DialogContent>
  );
};
export const AddSubjectBookSeriesDialog = ({
  valueText,
  bookSeriesID,
  classID,
  setValueText,
  subjectID,
}) => {
  const handleChange = (value) => {
    // console.log("value", value);
    setOldSelectedSubjectList(value);
    let selectedSubjectIDList = [];
    for (let index = 0; index < value.length; index++) {
      selectedSubjectIDList.push(value[index].id);
    }
    // console.log("selectedSubjectIDList", selectedSubjectIDList);
    setValueText(selectedSubjectIDList);
  };

  const [isLoading, setIsLoading] = useState(false);
  const [subjectList, setSubject] = useState(null);
  const [oldSelectedSubjectList, setOldSelectedSubjectList] = useState([]);

  const viewFolder = useSelector((state) => state.viewFolder);

  useEffect(() => {
    getOldSelectedSubjectList();
  }, []);
  function findIndexes(subjectList, oldSelectedSubjectList) {
    // Sử dụng map để tạo mảng chỉ mục
    return oldSelectedSubjectList.map((oldItem) => {
      // Sử dụng findIndex để tìm chỉ mục của phần tử trong subjectList có id tương ứng
      return subjectList.findIndex(
        (subjectItem) => subjectItem.id === oldItem.subjectId || subjectItem.id === oldItem.id
      );
    });
  }
  const getSubjects = async () => {
    const subjects = await getALLSubject();
    if (subjects.status === 200) {
      setSubject(subjects?.data);
    }
    setIsLoading(false);
  };
  const getOldSelectedSubjectList = async () => {
    setIsLoading(true);
    const subjects = await getALLSubjectBookSeriesByBookSeriesID(
      viewFolder?.bookSeries.id
    );
    // console.log("subjects", subjects);

    if (subjects.status === 200) {
      setOldSelectedSubjectList(subjects?.data);
    }
    getSubjects();
  };
  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "250px" }}
    >
      <DialogContentText></DialogContentText>

      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "100%" },
        }}
        noValidate
        autoComplete="off"
      >
        {/* <TextField id="outlined-basic" onChange={handleInputSubjectName} value={valueText} label="Tên môn học" variant="outlined" /> */}
        {isLoading ? (
          <LoadingTab />
        ) : subjectList ? (
          <>
            <Autocomplete
              multiple
              id="tags-outlined"
              options={subjectList}
              getOptionLabel={(option) => option?.name}
              // defaultValue={subjectList.map((option,index) => checkDefaultSubject(subjectList[index].id) ? subjectList[index].name :"")}
              // defaultValue={findIndexes(subjectList, oldSelectedSubjectList).map((item) => subjectList[item])}
              value={findIndexes(subjectList, oldSelectedSubjectList).map(
                (item) => subjectList[item]
              )}
              filterSelectedOptions
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Môn học"
                  placeholder="Thêm môn học vào bộ sách"
                />
              )}
              onChange={(event, value) => handleChange(value)}
            />
            <Input
              isDisabled
              type="text"
              label="Lớp"
              defaultValue={viewFolder?.class.name}
              className="max-w-xs"
            />
            <Input
              isDisabled
              type="text"
              label="Bộ sách"
              defaultValue={viewFolder?.bookSeries.name}
              className="max-w-xs"
            />
          </>
        ) : (
          ""
        )}
      </Box>
    </DialogContent>
  );
};
export const AddSubjectDialog = ({
  valueText,
  bookSeriesID,
  classID,
  setValueText,
  subjectID,
}) => {
  const handleInputSubjectName = (e) => {
    setValueText(e.target.value);
  };
  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "150px" }}
    >
      <DialogContentText></DialogContentText>

      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "100%" },
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          id="outlined-basic"
          onChange={handleInputSubjectName}
          value={valueText}
          label="Tên môn học"
          variant="outlined"
        />
      </Box>
    </DialogContent>
  );
};
export const AddBookVolumeDialog = ({ valueText, setValueText }) => {
  const handleInputBookVolumeName = (e) => {
    setValueText(e.target.value);
  };
  const viewFolder = useSelector((state) => state.viewFolder);

  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "250px" }}
    >
      <DialogContentText></DialogContentText>

      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "100%" },
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          id="outlined-basic"
          onChange={handleInputBookVolumeName}
          value={valueText}
          label="Tên đầu sách"
          variant="outlined"
        />
        <Input
          isDisabled
          type="text"
          label="Lớp"
          defaultValue={viewFolder?.class.name}
          className="max-w-xs"
        />

        <Input
          isDisabled
          type="text"
          label="Bộ sách"
          defaultValue={viewFolder?.bookSeries.name}
          className="max-w-xs"
        />
        <Input
          isDisabled
          type="text"
          label="Môn học"
          defaultValue={viewFolder?.subject.name}
          className="max-w-xs"
        />
      </Box>
    </DialogContent>
  );
};
export const AddChapterDialog = ({ valueText, setValueText }) => {
  const handleInputBookVolumeName = (e) => {
    setValueText(e.target.value);
  };
  const viewFolder = useSelector((state) => state.viewFolder);

  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "250px" }}
    >
      <DialogContentText></DialogContentText>

      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "100%" },
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          id="outlined-basic"
          onChange={handleInputBookVolumeName}
          value={valueText}
          label="Tên đầu sách"
          variant="outlined"
        />
        <Input
          isDisabled
          type="text"
          label="Lớp"
          defaultValue={viewFolder?.class.name}
          className="max-w-xs"
        />

        <Input
          isDisabled
          type="text"
          label="Bộ sách"
          defaultValue={viewFolder?.bookSeries.name}
          className="max-w-xs"
        />
        <Input
          isDisabled
          type="text"
          label="Môn học"
          defaultValue={viewFolder?.subject.name}
          className="max-w-xs"
        />
        <Input
          isDisabled
          type="text"
          label="Đầu sách"
          defaultValue={viewFolder?.bookVolume.name}
          className="max-w-xs"
        />
      </Box>
    </DialogContent>
  );
};

export const AddLessonDialog = ({ valueText, setValueText }) => {
  const handleInputBookVolumeName = (e) => {
    setValueText(e.target.value);
  };
  const viewFolder = useSelector((state) => state.viewFolder);

  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "250px" }}
    >
      <DialogContentText></DialogContentText>

      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "100%" },
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          id="outlined-basic"
          onChange={handleInputBookVolumeName}
          value={valueText}
          label="Tên bài học"
          variant="outlined"
        />
        <Input
          isDisabled
          type="text"
          label="Lớp"
          defaultValue={viewFolder?.class.name}
          className="max-w-xs"
        />

        <Input
          isDisabled
          type="text"
          label="Bộ sách"
          defaultValue={viewFolder?.bookSeries.name}
          className="max-w-xs"
        />
        <Input
          isDisabled
          type="text"
          label="Môn học"
          defaultValue={viewFolder?.subject.name}
          className="max-w-xs"
        />
        <Input
          isDisabled
          type="text"
          label="Đầu sách"
          defaultValue={viewFolder?.bookVolume.name}
          className="max-w-xs"
        />
        <Input
          isDisabled
          type="text"
          label="Chương"
          defaultValue={viewFolder?.chapter?.name}
          className="max-w-xs"
        />
      </Box>
    </DialogContent>
  );
};
export const AddRoleDialog = ({ setValueText, setDescription }) => {
  const handleInputRole = (e) => {
    setValueText(e.target.value);
  };
  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "200px" }}
    >
      <DialogContentText></DialogContentText>

      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "100%" },
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          onChange={handleInputRole}
          id="outlined-basic"
          label="Đối tượng*"
          variant="outlined"
        />
        <TextField
          id="outlined-basic"
          label="Mô tả*"
          variant="outlined"
          onChange={(e) => setDescription(e.target.value)}
        />
      </Box>
    </DialogContent>
  );
};
export const SetPermissionDialog = ({
  value,
  valueText,
  permission,
  description,
  setValueText,
  setDescription,
  setPermission,
  setUpdatePermission,
}) => {
  const handleInputRole = (e) => {
    setValueText(e.target.value);
  };
  const [allPermissions, setAllPermission] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    // console.log("suggestTags được thay đổi: ",suggestTags);
    getPermissions();
  }, []);
  const getPermissions = async () => {
    setIsLoading(true);
    const res = await getAllPermission();
    if (res.status === 200) {
      setAllPermission(res.data);
    }
    setIsLoading(false);
  };

  function findIndexes(permissions, oldPermissions) {
    // Sử dụng map để tạo mảng chỉ mục
    return oldPermissions.map((oldPermission) => {
      // Sử dụng findIndex để tìm chỉ mục của phần tử trong subjectList có id tương ứng
      return permissions.findIndex(
        (permission) => permission.permissionId === oldPermission?.permissionId
      );
    });
  }
  const PushId = (permissions) => {
    return permissions.map((permission) => {
      return permission.permissionId;
    });
  };
  console.log("value.createdAt", value.createdAt);
  const handleChange = (value) => {
    setPermission(value);
    setUpdatePermission(PushId(value));
  };
  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "500px" }}
    >
      <DialogContentText></DialogContentText>

      <Box
        component="form"
        sx={{
          "& > :not(style)": { mt: 3, width: "100%" },
        }}
        noValidate
        autoComplete="off"
      >
        {isLoading ? (
          <LoadingTab />
        ) : (
          <>
            <TextField
              id="outlined-basic"
              label="Đối tượng"
              variant="outlined"
              value={valueText}
              onChange={(e) => setValueText(e.target.value)}
            />
            <TextField
              id="outlined-basic"
              label="Mô tả"
              variant="outlined"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
            <TextField
              value={format(new Date(value?.createdAt), "MM/dd/yyyy hh:mm:ss")}
              disabled
              id="outlined-basic"
              label="Ngày tạo"
              variant="outlined"
            />
            <TextField
              value={value?.active ? "Hoạt động" : "Không hoạt động"}
              disabled
              id="outlined-basic"
              label="Trạng thái"
              variant="outlined"
            />
            <Autocomplete
              value={findIndexes(allPermissions, permission).map(
                (item) => allPermissions[item]
              )}
              multiple
              id="tags-outlined"
              options={allPermissions}
              getOptionLabel={(option) => (
                <Box title={option.description}>{option.permissionName}</Box>
              )}
              filterSelectedOptions
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Quyền truy cập"
                  placeholder="Thêm quyền truy cập"
                />
              )}
              onChange={(event, value) => handleChange(value)}
            />
          </>
        )}
      </Box>
    </DialogContent>
  );
};
export const AddTagDialog = () => {
  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "100px" }}
    >
      <DialogContentText></DialogContentText>
      <Box
        sx={{
          display: "flex",
          justifyContent: "flex-end",
          alignItems: "center",
          marginLeft: "40px",
        }}
      >
        <Box
          component="form"
          sx={{
            "& > :not(style)": { m: 1, width: "25ch" },
          }}
          noValidate
          autoComplete="off"
        >
          <TextField id="outlined-basic" label="Tag mới" variant="outlined" />
        </Box>
        <GreenButtonAction> Thêm tag</GreenButtonAction>
      </Box>
    </DialogContent>
  );
};
export const ComDialogContent = ({
  label,
  contentReport,
  setContentReport,
}) => {
  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "200px",mb:1 }}
    >
      <DialogContentText></DialogContentText>
      {/* <TextField
        // autoFocus
        value={contentReport}
        margin="dense"
        id="content"
        label={label}
        // type="text"
        fullWidth
        variant="standard"
        // onChange={(e) => setContentReport(e.target.value)}
      /> */}
      <TextField
        id="outlined-multiline-static"
        placeholder={label}
        multiline
        rows={4}
        sx={{ width: "100%" }}
        value={contentReport}
        onChange={(e) => setContentReport(e.target.value)}
      />
    </DialogContent>
  );
};
export const ModaratateResource = ({ setValueText }) => {
  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "100px" }}
    >
      <DialogContentText></DialogContentText>
      <TextField
        autoFocus
        margin="dense"
        id="content"
        label="Phản hồi"
        type="text"
        fullWidth
        variant="standard"
        onChange={(e) => setValueText(e.target.value)}
      />
    </DialogContent>
  );
};
export const ViewCommentDialogContent = ({ value,
  setValueText, }) => {
  //call api in here
  console.log("value comment",value)
  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "300px" }}
    >
      <DialogContentText></DialogContentText>
      <TitleWithinInfo title="Nội dung" info={value?.content} />
      <TitleWithinInfo title="Người bình luận" info={value?.commenter} />
      {/* <TitleWithinInfo
        title="Bình luận ngày"
        info={format(new Date(value?.createdAt), "MM/dd/yyyy hh:mm:ss")}
      /> */}
    

      <Box
        sx={{
          height: "100px",
          maxHeight: "200px",
          overflowX: "auto",
          margin: "20px",
        }}
      >
        {value?.reportCommentDTOResponses?.map((report) => (
          <FullColorAvatarAndInfoDiv
            avatarsrc={report.reporter.avatar}
            title1={report.reporter.username}
            title2={report.message}
          />
        ))}
      </Box>
      {/* <TextField
        id="outlined-multiline-static"
        label="Gửi báo cáo"
        multiline
        rows={4}
        sx={{ width: "100%" }}
        onChange={(e) => setValueText(e.target.value)}
      /> */}
    </DialogContent>
  );
};

export const ViewCommentMaterialReportedContent = ({
  value,
  setValueText,
  valueText,
}) => {
  //call api in here
  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "500px" }}
    >
      <DialogContentText></DialogContentText>
      <TitleWithinInfo title="Tên bài" info={value?.name} />
      <TitleWithinInfo
        title="Đăng ngày"
        info={format(new Date(value?.createdAt), "MM/dd/yyyy hh:mm:ss")}
      />
      <TitleWithinInfo title="Dạng file" info={value?.resourceType} />

      <Box
        sx={{
          height: "100px",
          maxHeight: "200px",
          overflowX: "auto",
          margin: "20px",
        }}
      >
        {value?.reportResourceDTOResponse?.map((report) => (
          <FullColorAvatarAndInfoDiv
            avatarsrc={report.reporter.avatar}
            title1={report.reporter.username}
            title2={report.message}
          />
        ))}
      </Box>
      <TextField
        id="outlined-multiline-static"
        label="Gửi báo cáo"
        multiline
        rows={4}
        sx={{ width: "100%" }}
        onChange={(e) => setValueText(e.target.value)}
      />
    </DialogContent>
  );
};
export const AddPermissionDialogContent = ({
  description,
  methodType,
  path,
  permission,
  setValueText,
  setPath,
  setMethodType,
  setDescription,
}) => {
  //call api in here
  return (
    <DialogContent
      sx={{ width: { md: "500px", lg: "600px" }, height: "400px" }}
    >
      <DialogContentText></DialogContentText>

      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "100%" },
          display: "flex",
          flexDirection: "column",
          justifyContent: "flex-end",
          alignItems: "center",
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          id="outlined-basic"
          value={permission}
          label="Tên*"
          variant="outlined"
          onChange={(e) => setValueText(e.target.value)}
        />
        <TextField
          id="outlined-basic"
          value={path}
          label="Path*"
          variant="outlined"
          onChange={(e) => setPath(e.target.value)}
        />
        <TextField
          id="outlined-basic"
          value={methodType}
          label="Method type*"
          variant="outlined"
          onChange={(e) => setMethodType(e.target.value)}
        />
        {/* <TextField
          id="outlined-basic"
          label="Dependency permission"
          variant="outlined"
        /> */}
        <TextField
          id="outlined-basic"
          value={description}
          label="Mô tả"
          variant="outlined"
          onChange={(e) => setDescription(e.target.value)}
        />
      </Box>
    </DialogContent>
  );
};
export const ChangePassword = ({ setOldPass, setPass, setRePass }) => {
  const schema = yup
    .object({
      oldPass: yup
        .string()
        .required("Không để trống mật khẩu")
        .password("Mật khẩu không đúng định dạng"),
      pass: yup
        .string()
        .required("Không để trống mật khẩu")
        .password("Mật khẩu không đúng định dạng"),
      repass: yup
        .string()
        .required("Xác nhận lại mật khẩu")
        .test("passwords-match", "Mật khẩu không khớp", function (value) {
          return value === this.parent.pass;
        }),
    })
    .required();
  const {
    reset,
    setError,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const onSubmit = async (data) => {};

  return (
    <DialogContent
      sx={{ width: { md: "400px", lg: "600px" }, height: "300px" }}
    >
      <Form onSubmit={handleSubmit(onSubmit)}>
        <FormControl
          sx={{ width: "100%", maxWidth: "100%", marginTop: 2 }}
          variant="outlined"
        >
          <InputLabel htmlFor="old-password">Mật khẩu cũ</InputLabel>
          <OutlinedInput
            error={errors.oldPass}
            helperText={errors.oldPass && errors.oldPass.message}
            {...register("oldPass")}
            id="oldPass"
            type="password"
            // onKeyUp={(e) => setData({...data,password: e.target.value})}
            onChange={(e) => setOldPass(e.target.value)}
            label="Mật khẩu cũ"
          />
        </FormControl>
        <FormControl
          sx={{ width: "100%", maxWidth: "100%", marginTop: 2 }}
          variant="outlined"
        >
          <InputLabel htmlFor="new-password">Mật khẩu mới</InputLabel>
          <OutlinedInput
            error={errors.pass}
            helperText={errors.pass && errors.pass.message}
            {...register("pass")}
            id="pass"
            type="password"
            // onKeyUp={(e) => setData({...data,password: e.target.value})}
            onChange={(e) => setPass(e.target.value)}
            label="Mật khẩu mới"
          />
        </FormControl>
        <FormControl
          sx={{ width: "100%", maxWidth: "100%", marginTop: 2 }}
          variant="outlined"
        >
          <InputLabel htmlFor="comfirm-password">
            Nhập lại mật khẩu mới
          </InputLabel>
          <OutlinedInput
            error={errors.rePass}
            helperText={errors.rePass && errors.rePass.message}
            {...register("rePass")}
            id="rePass"
            type="password"
            // onKeyUp={(e) => setData({...data,password: e.target.value})}
            onChange={(e) => setRePass(e.target.value)}
            label="Nhập lại mật khẩu mới"
          />
        </FormControl>
      </Form>
    </DialogContent>
  );
};

export const ShareDialog = ({ id, setVisualType, setUserShareIds }) => {
  const route = useRouter();
  const accessMap = {
    PUBLIC: "Công khai" ,
    RESTRICT: "Hạn chế" ,
    PRIVATE: "Chỉ mình tôi",
  };
  //users =>  user total in system, shareUsers => shared user total with this material
  const [users, setUsers] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [text, setText] = useState("");
  const { user } = useAuthContext();
  useEffect(() => {
    const getUsers = async () => {
      setIsLoading(true);
      const res = await getUserToShare();
      
      
      if(res.status === 200){
        const res2 = await getResourceSharedUsers(id);
        if(res2.status === 200){
          setShareUsers(res2.data.userSharedDTOResponses)
          setUsers(getUnSharedUsers(res2.data.userSharedDTOResponses,res.data));
          setAccessible(accessMap[res2.data.visualType])
          setVisualType(res2.data.visualType)
          setUserShareIds(extractUserShareIds(res2.data.userSharedDTOResponses));

        }
      }
      setIsLoading(false);
    };
    getUsers();
  }, []);
  const [shareUsers, setShareUsers] = useState([]);

  const [inputUser, setInputUser] = useState(null);
  function getUnSharedUsers(sharedUserList,userList) {
    // Sử dụng hàm map để trích xuất giá trị userShareId từ mỗi đối tượng
    const unShareUsers = userList?.filter((user) =>
      sharedUserList.find(sharedUser =>  sharedUser.userShareId == user.userShareId) == null
    );

    return unShareUsers;
  }
  function extractUserShareIds(userList) {
    // Sử dụng hàm map để trích xuất giá trị userShareId từ mỗi đối tượng
    const userShareIds = userList.map((user) => user.userShareId);

    return userShareIds;
  }
  const handleChange = (e, newValue) => {
    // setInputUser(newValue);
    AddUsertoList(newValue)
    let templeSharedUsers = [...shareUsers,newValue]
    setUsers(getUnSharedUsers(templeSharedUsers,users));// filter users after select a account

  };
  const handleKeyUp = (e) => {
    setText(e.target.value);
  };
  const AddUsertoList = (inputUser)=>{
    setText("");

    if (inputUser != null) {
      // alert(inputUser);
      const emailValid = shareUsers.filter(
        (user) =>
          user.email == inputUser.email || user.username === inputUser.username
      );

      const userValid = users.find(
        (user) =>
          user.email === inputUser.email || user.username === inputUser.username
      );

      if (emailValid.length == 0) {
        if (userValid) {
          setUserShareIds(extractUserShareIds([...shareUsers, inputUser]));
          setShareUsers([...shareUsers, inputUser]);

          toast.info("Đã thêm thành công");
          setUsers(
            users.filter(
              (user) =>
                user.email !== inputUser.email ||
                user.username === inputUser.username
            )
          );
        }
      } else {
        toast.warning("Người dùng đã được được thêm vào trước đó");
      }
    } else {
      toast.warning("Nhập email người muốn share");
    }
  }
  const handleAddUser = (e) => {
    e.preventDefault();
    
    
  };
  const handleDelete = (email) => {
    setShareUsers(shareUsers.filter((user) => user.email != email));
    setUserShareIds(extractUserShareIds(shareUsers.filter((user) => user.email != email)));
    const deletedUser = shareUsers.find(user => user.email == email)
    const templeSharedUsers = [...users,deletedUser]
    setUsers(templeSharedUsers);//  add again deleted user in to user total 

  };

  // accessiable
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [accessible, setAccessible] = React.useState("Hạn chế");
  // const [accessibleIcon, setAccessibleIcon] = React.useState({<RiGitRepositoryPrivateLine/>});

  const handleSetPublic = () => {
    setAccessible("Công khai");
    setVisualType("PUBLIC");
    setAnchorEl(null);
  };
  const handleSetRestrict = () => {
    setAccessible("Hạn chế");
    setVisualType("RESTRICT");
    setAnchorEl(null);
  };
  const handleSetPrivate = () => {
    setAccessible("Chỉ mình tôi");
    setVisualType("PRIVATE");
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  // end accessiable
  const handleCopyLink = async(text) => {
    try {
      await navigator.clipboard.writeText(text);
      // setCopySuccess('Copied!');
      toast.info("Đã copy")
    } catch (err) {
      // setCopySuccess('Failed to copy!');
    }
  };

  //end handleCopyLink

  return (
    <DialogContent
      sx={{ width: { md: "400px", lg: "600px" }, height: "300px" }}
    >
      <>
        <Stack spacing={2} sx={{ width: "100%", marginTop: "10px" }}>
          <Autocomplete
            freeSolo
            id="email"
            options={users}
            autoHighlight
            getOptionLabel={(user) => user.email || ""}
            renderOption={(props, user) => (
              <>
                {isLoading ? (
                  "loading..."
                ) : (
                  <Box component="li" {...props}>
                    <AvatarAndInfoDiv
                      avatarsrc={user.avatar}
                      title1={user.username}
                      title2={user.email}
                      {...props}
                    />
                  </Box>
                )}
              </>
            )}
            renderInput={(params) => (
              <Form onSubmit={handleAddUser}>
                <TextField
                  {...params}
                  label="Email hoặc tên tài khoản muốn chia sẻ"
                  InputProps={{
                    ...params.InputProps,
                    type: "text", // disable autocomplete and autofill
                  }}
                  // value=""
                  // onChange={setText}
                />
              </Form>
            )}
            value={inputUser}
            onChange={handleChange}
            // onKeyUp={handleKeyUp}
          />
        </Stack>
        <Box sx={{ marginTop: "20px" }}>
          <Box>Những người có quyền truy cập</Box>
          <Box sx={{ height: "auto", maxHeight: 150, overflowY: "auto" }}>
            <AvatarAndInfoOwnerDiv
              avatarsrc={user.avatar}
              title1={user.username}
              title2={user.email}
            />
            {isLoading ? <CenterFlexBox><BeatLoader
                color="#197854"
                size={5}
                style={{ marginLeft: "5px", marginTop: "5px" }}
              /></CenterFlexBox>:
               shareUsers?.map((userS, index) => (
                  <AvatarAndInfoAnDeleteDiv
                  key={index}
                    avatarsrc={userS.avatar}
                    title1={userS.username}
                    title2={userS.email}
                    mode={
                      userS.username == user?.username
                        ? "Chủ sở hữu"
                        : "Chỉ xem"
                    }
                    handleDelete={handleDelete}
                  />
                ))
              }
          </Box>
        </Box>
        <Box sx={{ marginTop: "20px" }}>
          <Box>Quyền truy cập chung</Box>
          <IconAndInfoDiv
            title1={
              <>
                <Button
                  id="basic-button"
                  aria-controls={open ? "basic-menu" : undefined}
                  aria-haspopup="true"
                  aria-expanded={open ? "true" : undefined}
                  onClick={handleClick}
                >
                  {accessible} <AiFillCaretDown />
                </Button>
                <Menu
                  id="basic-menu"
                  anchorEl={anchorEl}
                  open={open}
                  onClose={handleClose}
                  MenuListProps={{
                    "aria-labelledby": "basic-button",
                  }}
                >
                  <MenuItem onClick={handleSetPublic}>Công khai</MenuItem>
                  <MenuItem onClick={handleSetRestrict}>Hạn chế</MenuItem>
                  <MenuItem onClick={handleSetPrivate}>Chỉ mình tôi</MenuItem>

                </Menu>
              </>
            }
            title2={accessible === "Công khai"?"Bất kỳ ai có kết nối internet và có đường dẫn này đều có thể xem"
                                :accessible === "Hạn chế" ?"Chỉ người được chia sẻ có thể xem":"Chỉ mình tôi xem"}
          >
            <AccessibleDiv>
              {accessible == "Hạn chế" ? (
                <IoMdPeople />
              ) :accessible == "Chỉ mình tôi" ? (
                <RiGitRepositoryPrivateLine />
              ) : (
                <MdOutlinePublic />
              )}
            </AccessibleDiv>
          </IconAndInfoDiv>
        </Box>
        <Box sx={{ marginTop: "20px" }}>
          <WhiteButtonAction onClick={()=>handleCopyLink(
                `${window.location.protocol}` +
                "//"+`${window.location.host}`+"/detail_material?id=" +
                `${id}`
                )} onC>
            <Clipboard
              data-clipboard-text={
                `${window.location.protocol}` +
                "//"+`${window.location.host}`+"/detail_material?id=" +
                `${id}`
              }
              button-title={`${window.location.protocol}//detail_material?id=${id}`}
            >
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "start",
                  alignItems: "center",
                }}
              >
                <BsLink /> <Box sx={{ ml: "3px" }}>Sao chép liên kết</Box>
              </Box>
            </Clipboard>
          </WhiteButtonAction>
        </Box>
      </>
    </DialogContent>
  );
};

export const EditMaterialDialog = ({ value, setFileInput }) => {
  // console.log("value edit",value)
  return (
    <DialogContent sx={{ width: "auto", height: "auto" }}>
      <DetailFolderMaterial
        type="edit"
        newMaterial={value}
        setFileInput={setFileInput}
      />
    </DialogContent>
  );
};

export const DeleteMaterialDialog = () => {
  return (
    <DialogContent sx={{ width: "auto", height: "auto" }}>
      Bạn có chắc chắn muốn xóa tài liệu này?
    </DialogContent>
  );
};
export const UnSaveMaterialDialog = () => {
  return (
    <DialogContent sx={{ width: "auto", height: "auto" }}>
      Bạn có chắc chắn muốn bỏ lưu tài liệu này?
    </DialogContent>
  );
};
export const UnShareMaterialDialog = () => {
  return (
    <DialogContent sx={{ width: "auto", height: "auto" }}>
      Bạn có chắc chắn muốn xóa tài liệu này ra khỏi danh sách chia sẻ với tôi?
    </DialogContent>
  );
};
export const SaveMaterialDialog = () => {
  return (
    <DialogContent sx={{ width: "auto", height: "auto", fontSize: "20px" }}>
      Bạn có chắc chắn muốn bỏ lưu tài liệu này?
    </DialogContent>
  );
};

export const FullSaveMaterialDialog = ({
  name,
  nameButton,
  handleCloseMenu,
  isSave,
}) => {
  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = async () => {
    setOpen(false);

    if (isSave == "true") {
      toast.success("Đã bỏ lưu thành công");
    } else {
      toast.success("Đã lưu thành công");
    }
  };
  const title = "Lưu tài liệu: " + name;
  const style = isSave == "true" ? { color: "yellow" } : { color: "black" };
  return (
    <FormDialog
      title={title}
      label="Lưu"
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="save"
    >
      <SaveButton style={style} />
    </FormDialog>
  );
};

export const FullShareMaterialDialog = ({
  name,
  nameButton,
  handleCloseMenu,
  id,
}) => {
  const [open, setOpen] = useState(false);
  const [userShareIds, setUserShareIds] = useState([]);
  const [visualType, setVisualType] = useState("RESTRICT");
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    handleCloseMenu();
  };

  const handleSubmit = async () => {
    setIsLoading(true);
    // nếu thành công thì setOpent false
    const res = await ShareMaterial(id, {
      userShareIds: userShareIds,
      visualType: visualType,
    });
    if (res.status === 200) {
      dispatch(setIsReload((pre) => !pre));
      setOpen(false);
      toast.success("Đã chia sẻ thành công");
      handleClose()
    } else if (res.status === 202) {
      toast.warning("Tài liệu này không thể chia sẻ tới những người đã chọn");
    } else {
      toast.error("Chia sẻ thất bại");
    }
    setIsLoading(false);

    // toast.error("Đăng nhập không thành công")
  };
  const title = "Chia sẻ tài liệu: " + name;
  return (
    <FormDialog
      title={title}
      label="chia sẻ"
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="share"
      setUserShareIds={setUserShareIds}
      setVisualType={setVisualType}
      id={id}
      isLoading={isLoading}
    >
      <IconAndText text={nameButton}>
        <BsShareFill />
      </IconAndText>
    </FormDialog>
  );
};

export const FullDeleteMaterialDialog = ({
  value,
  name,
  nameButton,
  handleCloseMenu,
  id,
}) => {
  const [open, setOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    handleCloseMenu();
  };

  const handleSubmit = async () => {
    setIsLoading(false);
    try {
      const res = await changeStatusResource(value.resourceId);
      if (res.status === 200) {
        dispatch(setIsReload((pre) => !pre));
        setOpen(false);
        toast.success("Xóa bỏ thành công");
      } else {
        // setOpen(false);
        toast.success("Xóa thất bại");
      }
    } catch (e) {
      toast.success("Xóa thất bại");
    }
    setIsLoading(true);

    // nếu thành công thì setOpent false
    // const res = await changeStatusBookSeries

    // toast.error("Đăng nhập không thành công")
  };
  const title = "Xóa bỏ tài liệu: " + name;
  return (
    <FormDialog
      title={title}
      label="xóa"
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="deletematerial"
      isLoading={isLoading}
    >
      <IconAndText text={nameButton}>
        <AiFillDelete />
      </IconAndText>
    </FormDialog>
  );
};
export const FullEditMaterialDialog = ({
  value,
  name,
  nameButton,
  handleCloseMenu,
}) => {
  const newMaterial = useSelector((state) => state.newMaterial.material);
  const dispatch = useDispatch();

  const [open, setOpen] = useState(false);
  const [material, setMaterial] = useState(null);
  const [fileInput, setFileInput] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isLoading2, setIsLoading2] = useState(false);

  useEffect(() => {
    // setIsLoading(true);
    getMaterialDetail();
  }, []);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    handleCloseMenu();
  };

  const getMaterialDetail = async () => {
    // console.log("value",value)
    setIsLoading(true);
    const res = await getMyMaterialByID(value?.resourceId);
    // console.log("value res",res)
    if (res.status === 200) {
      setMaterial(res.data);

      dispatch(setName(res.data?.name));
      dispatch(setTags(res.data?.tagList));
      dispatch(setDescription(res.data?.description));
      dispatch(setViewLesson({ id: res.data?.lessonId, name: "" }));
      dispatch(setViewChapter({ id: res.data?.chapterId, name: "" }));
      dispatch(setViewBookVolume({ id: res.data?.bookVolumeId, name: "" }));
      dispatch(setViewSubject({ id:"",subjectId: res.data?.subjectId, name: "" }));
      dispatch(setViewBookSeries({ id: res.data?.bookSeriesId, name: "" }));
      dispatch(setViewClass({ id: res.data?.classId, name: "" }));
      dispatch(setMode({id:res.data?.visualType,name:""}));
    }
    setIsLoading(false);
  };

  const handleSubmit = async () => {
    // nếu thành công thì setOpent false
    setIsLoading2(true);

    const formData = new FormData();

    if (fileInput !== null) {
      formData.append("file", fileInput);
    }
    // formData.append("file", fileInput);

    formData.append("subjectId", newMaterial?.subject.subjectId + "");
    formData.append("name", newMaterial?.name);
    formData.append("description", newMaterial?.description);
    formData.append("visualType", newMaterial?.mode?.id);
    if (newMaterial?.lesson.id !== null) {
      formData.append("LessonId", newMaterial?.lesson.id + "");
    } else {
      console.log("tagList", newMaterial?.tags);

      formData.append("tagList", newMaterial?.tags);
    }
    const res = await updateMaterial(value?.resourceId, formData);
    // console.log("res change",res)
    if (res.status === 200) {
      dispatch(setIsReload((pre) => !pre));

      toast.success("Chỉnh sửa thành công");
      setOpen(false);
      handleCloseMenu()
    } else if (res.status === 200) {
      toast.warning("Kiểm tra lại thông tin 1 lần nữa");
    } else {
      toast.error("Chỉnh sửa thất bại");
    }
    setIsLoading2(false);

    // toast.error("Đăng nhập không thành công")
  };
  const title = "Chỉnh sửa tài liệu: " + name;

  return (
    <>
      {" "}
      {isLoading ? (
        <BeatLoader
          color="#197854"
          size={5}
          style={{ marginLeft: "5px", marginTop: "5px" }}
        />
      ) : (
        <FormDialog
          title={title}
          label="Chỉnh sửa tài liệu"
          open={open}
          setOpen={handleOpen}
          setClose={handleClose}
          handleSubmit={handleSubmit}
          type="editmaterial"
          sx={{ width: "100%" }}
          value={material}
          setFileInput={setFileInput}
          isLoading={isLoading2}
        >
          <IconAndText text={nameButton}>
            <BiSolidEditAlt />
          </IconAndText>
        </FormDialog>
      )}
    </>
  );
};

export const FullAddTagDialog = ({ name, handleCloseMenu }) => {
  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    handleCloseMenu();
  };
  const dispatch = useDispatch();

  const handleSubmit = async () => {
    // nếu thành công thì setOpent false
    dispatch(setIsReload((pre) => !pre));

    setOpen(false);
    toast.success("Thêm thành công");
    // toast.error("Đăng nhập không thành công")
  };
  const title = "Thêm tag cho " + name + " ";
  return (
    <FormDialog
      title={title}
      label="Thêm tag"
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="addtag"
    >
      <IconAndText text="Thêm tag">
        <FaTags />
      </IconAndText>
    </FormDialog>
  );
};

// subject ID, ... ID use for update when call api

export const FullAddClassDialog = ({
  children,
  name,
  handleCloseMenu,
  classID,
  addOrUpdate,
}) => {
  const [open, setOpen] = useState(false);
  const [className, setClassName] = useState(name);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    addOrUpdate === "ADD" ? "" : handleCloseMenu();
  };

  const handleSubmit = async () => {
    // nếu thành công thì setOpent false
    setIsLoading(true);
    const res =
      addOrUpdate === "ADD"
        ? await setClass({ name: className })
        : await updateClass(classID, { name: className });
    if (res.status === 200) {
      dispatch(setIsReload((pre) => !pre));

      setOpen(false);
      addOrUpdate === "ADD"
        ? toast.success("Thêm thành công")
        : toast.success("Cập nhật thành công");
    } else if (res.status === 202) {
      toast.warning("Tên lớp đã tồn tại");
    } else {
      addOrUpdate === "ADD"
        ? toast.error("Thêm không thành công")
        : toast.error("Cập nhật không thành công");
    }
    setIsLoading(false);

    // toast.error("Đăng nhập không thành công")
  };
  const title = addOrUpdate === "ADD" ? name : "Cập nhập tên lớp: " + name;
  return (
    <FormDialog
      title={title}
      label={name}
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="addClass"
      valueText={className}
      setValueText={setClassName}
      isLoading={isLoading}
    >
      {addOrUpdate === "UPDATE" ? (
        "Cập nhật"
      ) : (
        <Box
          sx={{
            width: "100%",
            height: "auto",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          Thêm lớp
        </Box>
      )}
    </FormDialog>
  );
};

export const FullAddBookSeriesDialog = ({
  children,
  name,
  handleCloseMenu,
  bookSeriesID,
  classID,
  addOrUpdate,
}) => {
  const [open, setOpen] = useState(false);
  const [bookSeries, setBs] = useState(name);
  const [isLoading, setIsLoading] = useState(false);
  const viewFolder = useSelector((state) => state.viewFolder);

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    addOrUpdate === "ADD" ? "" : handleCloseMenu();
  };
  const dispatch = useDispatch();

  const handleSubmit = async () => {
    setIsLoading(true);
    // nếu thành công thì setOpent false
    const res =
      addOrUpdate === "ADD"
        ? await setBookSeries(viewFolder?.class.id, { name: bookSeries })
        : await updateBookSeries(bookSeriesID, { name: bookSeries });
    if (res.status === 200) {
      dispatch(setIsReload((pre) => !pre));

      setOpen(false);
      addOrUpdate === "ADD"
        ? toast.success("Thêm thành công")
        : toast.success("Cập nhật thành công");
    } else if (res.status === 202) {
      toast.warning("Tên bộ sách đã tồn tại");
    } else {
      addOrUpdate === "ADD"
        ? toast.error("Thêm không thành công")
        : toast.error("Cập nhật không thành công");
    }
    setIsLoading(false);
    // toast.error("Đăng nhập không thành công")
  };
  const title =
    addOrUpdate === "ADD"
      ? "Thêm bộ sách mới vào '" + viewFolder?.class.name + "'"
      : "Cập nhập bộ sách: " + name;
  return (
    <FormDialog
      title={title}
      label={name}
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="addBookSeries"
      valueText={bookSeries}
      bookSeriesID={bookSeriesID}
      classID={classID}
      setValueText={setBs}
      isLoading={isLoading}
    >
      {addOrUpdate === "UPDATE" ? (
        "Cập nhật"
      ) : (
        <Box
          sx={{
            width: "100%",
            height: "auto",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          Thêm bộ sách
        </Box>
      )}
    </FormDialog>
  );
};

// subject ID, ... ID use for update when call api, after inout value text then back this const to that handle submit
export const FullAddSubjectBookSeriesDialog = ({
  children,
  name,
  handleCloseMenu,
  addOrUpdate,
}) => {
  const [open, setOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const viewFolder = useSelector((state) => state.viewFolder);
  const dispatch = useDispatch();

  const [subjectIDList, setSubjectIDList] = useState([]);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    addOrUpdate === "ADD" ? "" : handleCloseMenu();
  };

  const handleSubmit = async () => {
    setIsLoading(true);

    const res = await setSubjectBookSeries(viewFolder?.bookSeries.id, {
      subjects: subjectIDList,
    });
    // console.log("res", res);
    if (res.status === 200) {
      dispatch(setIsReload((pre) => !pre));
      setOpen(false);
      if (subjectIDList.length === 0) {
        toast.warning("Xóa bỏ hết môn học trong bộ sách");
      } else {
        toast.success("Cập nhật thành công");
      }
    } else if (res.status === 202) {
      toast.warning(
        `Một số môn học đã tồn tại trong '${viewFolder?.bookSeries.name}'`
      );
    } else {
      toast.error(`Cập nhật không thành công'`);
    }

    setIsLoading(false);

    // toast.error("Đăng nhập không thành công")
  };
  const title =
    addOrUpdate === "ADD" ? "Thêm môn học" : "Cập nhập môn học: " + name;
  return (
    <FormDialog
      title={title}
      label={name}
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="addSubjectBookSeries"
      valueText=""
      subjectID={subjectIDList}
      setValueText={setSubjectIDList}
      isLoading={isLoading}
    >
      {addOrUpdate === "UPDATE" ? (
        "Cập nhật"
      ) : (
        <Box
          sx={{
            width: "100%",
            height: "auto",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          Thêm môn học
        </Box>
      )}
    </FormDialog>
  );
};
export const FullAddSubjectDialog = ({
  children,
  name,
  handleCloseMenu,
  bookSeriesID,
  classID,
  subjectID,
  addOrUpdate,
}) => {
  const [open, setOpen] = useState(false);
  const [subjectName, setSubjectName] = useState(name);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    addOrUpdate === "ADD" ? "" : handleCloseMenu();
  };

  const handleSubmit = async () => {
    setIsLoading(true);
    // nếu thành công thì setOpent false
    const res =
      addOrUpdate === "ADD"
        ? await setSubject({ name: subjectName })
        : await updateSubject(subjectID, { name: subjectName });
    if (res.status === 200) {
      dispatch(setIsReload((pre) => !pre));

      setOpen(false);
      addOrUpdate === "ADD"
        ? toast.success("Thêm thành công")
        : toast.success("Cập nhật thành công");
      // const dispatch = useDispatch();
      // const isUpdate = useSelector((state) => state.viewFolder.isUpdate);
      // dispatch(setIsUpdate(!isUpdate))
    } else if (res.status === 202) {
      toast.warning("Tên sách đã tồn tại");
    } else {
      addOrUpdate === "ADD"
        ? toast.error("Thêm không thành công")
        : toast.error("Cập nhật không thành công");
    }
    setIsLoading(false);
    // toast.error("Đăng nhập không thành công")
  };
  const title = addOrUpdate === "ADD" ? name : "Cập nhập môn học: " + name;
  return (
    <FormDialog
      title={title}
      label={name}
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="addSubject"
      bookSeriesID={bookSeriesID}
      classID={classID}
      subjectID={subjectID}
      valueText={subjectName}
      setValueText={setSubjectName}
      isLoading={isLoading}
    >
      {addOrUpdate === "UPDATE" ? (
        "Cập nhật"
      ) : (
        <Box
          sx={{
            width: "100%",
            height: "auto",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          Thêm môn học
        </Box>
      )}
    </FormDialog>
  );
};

// subject ID, ... ID use for update when call api, after inout value text then back this const to that handle submit
export const FullAddBookVolumeDialog = ({
  name,
  handleCloseMenu,
  addOrUpdate,
  bookVolumeID,
}) => {
  const [open, setOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const viewFolder = useSelector((state) => state.viewFolder);
  const dispatch = useDispatch();

  const [bookVolume, setBVolume] = useState(name);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    addOrUpdate === "ADD" ? "" : handleCloseMenu();
  };

  const handleSubmit = async () => {
    setIsLoading(true);
    // nếu thành công thì setOpent false
    const res =
      addOrUpdate === "ADD"
        ? await setBookVolume(viewFolder?.subject.id, { name: bookVolume })
        : await updateBookVolume(bookVolumeID, { name: bookVolume });
    if (res.status === 200) {
      
      dispatch(setIsReload((pre) => !pre));
      setOpen(false);
      addOrUpdate === "ADD"
        ? toast.success("Thêm thành công")
        : toast.success("Cập nhật thành công");
    } else if (res.status === 202) {
      toast.warning("Tên đầu sách đã tồn tại");
    } else {
      addOrUpdate === "ADD"
        ? toast.error("Thêm không thành công")
        : toast.error("Cập nhật không thành công");
    }
    setIsLoading(false);
    // toast.error("Đăng nhập không thành công")
  };
  const title = addOrUpdate === "ADD" ? name : "Cập nhập đầu sách: " + name;
  return (
    <FormDialog
      title={title}
      label={name}
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="addBookVolume"
      valueText={bookVolume}
      setValueText={setBVolume}
      isLoading={isLoading}
    >
      {addOrUpdate === "UPDATE" ? (
        "Cập nhật"
      ) : (
        <Box
          sx={{
            width: "100%",
            height: "auto",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          Thêm đầu sách
        </Box>
      )}
    </FormDialog>
  );
};

// subject ID, ... ID use for update when call api, after inout value text then back this const to that handle submit
export const FullAddChapterDialog = ({
  children,
  name,
  handleCloseMenu,
  bookSeriesID,
  classID,
  subjectID,
  addOrUpdate,
  bookVolumeID,
  chapterID,
}) => {
  const [open, setOpen] = useState(false);
  const [chapter, setCh] = useState(name);
  const [isLoading, setIsLoading] = useState(false);
  const viewFolder = useSelector((state) => state.viewFolder);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    addOrUpdate === "ADD" ? "" : handleCloseMenu();
  };
  const dispatch = useDispatch();

  const handleSubmit = async () => {
    setIsLoading(true);
    // nếu thành công thì setOpent false
    const res =
      addOrUpdate === "ADD"
        ? await setChapter(viewFolder?.bookVolume.id, { name: chapter })
        : await updateChapter(chapterID, { name: chapter });
    if (res.status === 200) {
      dispatch(setIsReload((pre) => !pre));
      setOpen(false);
      addOrUpdate === "ADD"
        ? toast.success("Thêm thành công")
        : toast.success("Cập nhật thành công");
    } else if (res.status === 202) {
      toast.warning("Tên chương đã tồn tại");
    } else {
      addOrUpdate === "ADD"
        ? toast.error("Thêm không thành công")
        : toast.error("Cập nhật không thành công");
    }
    setIsLoading(false);
    // toast.error("Đăng nhập không thành công")
  };
  const title = addOrUpdate === "ADD" ? name : "Cập nhập chương: " + name;
  return (
    <FormDialog
      title={title}
      label={name}
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="addChapter"
      valueText={chapter}
      bookSeriesID={bookSeriesID}
      classID={classID}
      subjectID={subjectID}
      bookVolumeID={bookVolumeID}
      chapterID={chapterID}
      setValueText={setCh}
      isLoading={isLoading}
    >
      {addOrUpdate === "UPDATE" ? (
        "Cập nhật"
      ) : (
        <Box
          sx={{
            width: "100%",
            height: "auto",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          Thêm chương
        </Box>
      )}
    </FormDialog>
  );
};

// subject ID, ... ID use for update when call api, after inout value text then back this const to that handle submit
export const FullAddLessonDialog = ({
  children,
  name,
  handleCloseMenu,
  addOrUpdate,
  lessonID,
}) => {
  const [open, setOpen] = useState(false);
  const [lesson, setL] = useState(name);
  const [isLoading, setIsLoading] = useState(false);
  const viewFolder = useSelector((state) => state.viewFolder);
  const dispatch = useDispatch();
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    addOrUpdate === "ADD" ? "" : handleCloseMenu();
  };

  const handleSubmit = async () => {
    setIsLoading(true);
    // nếu thành công thì setOpent false
    const res =
      addOrUpdate === "ADD"
        ? await setLesson(viewFolder?.chapter.id, { name: lesson })
        : await updateLesson(lessonID, { name: lesson });
    if (res.status === 200) {
      dispatch(setIsReload((pre) => !pre));
      setOpen(false);
      addOrUpdate === "ADD"
        ? toast.success("Thêm thành công")
        : toast.success("Cập nhật thành công");
    } else if (res.status === 202) {
      toast.warning("Tên bài đã tồn tại");
    } else {
      addOrUpdate === "ADD"
        ? toast.error("Thêm không thành công")
        : toast.error("Cập nhật không thành công");
    }
    setIsLoading(false);
    // toast.error("Đăng nhập không thành công")
  };
  const title = addOrUpdate === "ADD" ? name : "Cập nhập bài học: " + name;
  return (
    <FormDialog
      title={title}
      label={name}
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="addLesson"
      valueText={lesson}
      setValueText={setL}
      isLoading={isLoading}
    >
      {addOrUpdate === "UPDATE" ? (
        "Cập nhật"
      ) : (
        <Box
          sx={{
            width: "100%",
            height: "auto",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          Thêm bài học
        </Box>
      )}
    </FormDialog>
  );
};

// subject ID, ... ID use for update when call api, after inout value text then back this const to that handle submit
export const FullAddRoleDialog = () => {
  const [open, setOpen] = useState(false);
  const [role, setRole] = useState("");
  const [description, setDescription] = useState("");
  const [isLoading2, setLoading2] = useState(false);
  const dispatch = useDispatch();

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    // addOrUpdate === 'ADD' ? '' : handleCloseMenu()
  };

  const handleSubmit = async () => {
    setLoading2(true);
    const res = await addNewRole({
      roleName: role,
      description: description,
    });
    if (res.status === 200) {
      dispatch(setIsReload((pre) => !pre));
      setOpen(false);

      toast.success("Thêm thành công");
    } else if (res.status === 202) {
      toast.warning("Kiểm tra lại thông tin");
    } else {
      toast.error("Thêm vai trò thất bại");
    }
    setLoading2(false);
  };
  const title = "Thêm vai trò mới";
  return (
    <FormDialog
      title={title}
      label=""
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="addRole"
      setValueText={setRole}
      setDescription={setDescription}
      isLoading={isLoading2}
    >
      <Box
        sx={{
          width: "100%",
          height: "auto",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        Thêm mới
      </Box>
    </FormDialog>
  );
};
export const FullSetPermissionDialog = ({
  id,
  name,
  handleCloseMenu,
  value,
}) => {
  const [open, setOpen] = useState(false);
  const [selectedRole, setSelectedRole] = useState("");
  const [description, setDescription] = useState("");
  const [permissions, setPermission] = useState([]);
  const [updatePermissions, setUpdatePermission] = useState([]);

  const [isLoading, setLoading] = useState(false);
  const [isLoading2, setLoading2] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    // handleCloseMenu()
  };
  const dispatch = useDispatch();
  useEffect(() => {
    getRole();
  }, []);
  const PushId = (permissions) => {
    return permissions.map((permission) => {
      return permission.permissionId;
    });
  };
  const getRole = async () => {
    setLoading(true);
    try {
      const res = await getFolderSRC(`/role/${id}`);
      if (res.status === 200) {
        dispatch(setIsReload((pre) => !pre));
        setSelectedRole(res.data.roleName);
        setDescription(res.data.description);
        setPermission(res.data.permissions);
        setUpdatePermission(PushId(res.data.permissions));
      }
    } catch (e) {
      console.log(e);
    }
    setLoading(false);
  };

  const handleSubmit = async () => {
    setLoading2(true);
    const res = await setPermissionRole(value.roleId, {
      roleName: selectedRole,
      description: description,
      permission: updatePermissions,
    });
    if (res.status === 200) {
      setOpen(false);

      toast.success("Phân quyền thành công");
    } else if (res.status === 202) {
      setOpen(false);

      toast.warning("Kiểm tra lại các quyền được gán");
    } else {
      toast.error("Phân quyền thất bại");
    }

    setLoading2(false);

    // toast.error("Đăng nhập không thành công")
  };
  const title = "Phân quyền cho " + name;
  return (
    <FormDialog
      title={title}
      label=""
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="setPermission"
      value={value}
      valueText={selectedRole}
      description={description}
      permission={permissions}
      setValueText={setSelectedRole}
      isLoading={isLoading2}
      setDescription={setDescription}
      setPermission={setPermission}
      setUpdatePermission={setUpdatePermission}
    >
      <Box
        sx={{
          width: "100%",
          height: "auto",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        Phân quyền
      </Box>
    </FormDialog>
  );
};

export const FullViewCommentDialog = ({value, handleCloseMenu}) => {
  const [open, setOpen] = useState(false);
  
  const [valueText, setValueText] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    handleCloseMenu();
  };
  const handleSubmitReject = async () => {
    // toast.error("Đăng nhập không thành công")
  };
  const handleSubmit = async () => {
    setIsLoading(true);

    // nếu thành công thì setOpent false

    const res = await deleteComment(value?.commentId);
    if (res.status === 200) {
      dispatch(setIsReload((pre) => !pre));
      setOpen(false);
      toast.success("Đã xóa comment");
    } else if (res.status === 202) {
      toast.warning("Kiểm tra lại thông tin");
    } else {
      toast.error("Thất bại");
    }
    setIsLoading(false);
  };
  const title = "Xóa bình luận bị báo cáo";
  return (
    <FormDialog
      title={title}
      label=""
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      value={value}
      type="viewCommentReported"
      setValueText={setValueText}
      valueText={valueText}
      isLoading={isLoading}
    >
      <Box
        sx={{
          width: "100%",
          height: "auto",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        Xem chi tiết
      </Box>
    </FormDialog>
  );
};

export const FullViewMaterialReportedDialog = ({ value, handleCloseMenu }) => {
  const [open, setOpen] = useState(false);
  
  const [valueText, setValueText] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    handleCloseMenu();
  };
  const handleSubmitReject = async () => {
    // toast.error("Đăng nhập không thành công")
  };
  const handleSubmit = async () => {
    setIsLoading(true);

    // nếu thành công thì setOpent false

    const res = await rejectResource({
      message: valueText,
      resourceId: value?.id,
      // "reporterId": 1
    });
    if (res.status === 200) {
      dispatch(setIsReload((pre) => !pre));
      setOpen(false);
      toast.success("Đã từ chối tài liệu");
    } else if (res.status === 202) {
      toast.warning("Kiểm tra lại thông tin");
    } else {
      toast.error("Thất bại");
    }
    setIsLoading(false);
  };
  const title = "Chi tiết tài liệu bị báo cáo";
  return (
    <FormDialog
      title={title}
      label=""
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      value={value}
      type="viewMaterialReported"
      setValueText={setValueText}
      valueText={valueText}
      isLoading={isLoading}
    >
      <Box
        sx={{
          width: "100%",
          height: "auto",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        Xem chi tiết
      </Box>
    </FormDialog>
  );
};

export const FullUpdateRoleDialog = ({ id, handleCloseMenu }) => {
  const [open, setOpen] = useState(false);
  const [role, setRole] = useState("");
  const handleOpen = () => {
    setOpen(true);
  };
  const dispatch = useDispatch();
  const handleClose = () => {
    setOpen(false);
    // addOrUpdate === 'ADD' ? '' : handleCloseMenu()
  };

  const handleSubmit = async () => {
    // handle api in here
    // nếu thành công thì setOpent false
    dispatch(setIsReload((pre) => !pre));
    setOpen(false);

    toast.success("Cập nhật thành công");

    // toast.error("Đăng nhập không thành công")
  };
  const title = "Đối tượng truy cập: Admin";
  return (
    <FormDialog
      title={title}
      label=""
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="setPermission"
    >
      <Box
        sx={{
          width: "100%",
          height: "auto",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        Xem chi tiết
      </Box>
    </FormDialog>
  );
};

// subject ID, ... ID use for update when call api, after inout value text then back this const to that handle submit
export const FullAddPermissionDialog = () => {
  const [open, setOpen] = useState(false);
  const [permission, setPermission] = useState("");
  const [path, setPath] = useState("");
  const [methodType, setMethodType] = useState("");
  const [description, setDescription] = useState("");
  const [isLoading2, setLoading2] = useState(false);
  const dispatch = useDispatch();

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    // addOrUpdate === 'ADD' ? '' : handleCloseMenu()
  };

  const handleSubmit = async () => {
    setLoading2(true);

    const res = await addNewPermission({
      permissionName: permission,
      path: path,
      methodType: methodType,
      description: description,
    });
    if (res.status === 200) {
      dispatch(setIsReload((pre) => !pre));
      setOpen(false);

      toast.success("Thêm quyền thành công");
    } else if (res.status === 202) {
      toast.warning("Kiểm tra lại thông tin");
    } else {
      toast.error("Thêm quyền thất bại");
    }
    setLoading2(false);
  };
  const title = "Thêm mới quyền truy cập";
  return (
    <FormDialog
      title={title}
      label=""
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="addPermission"
      setValueText={setPermission}
      setPath={setPath}
      setMethodType={setMethodType}
      setDescription={setDescription}
      isLoading={isLoading2}
    >
      <Box
        sx={{
          width: "100%",
          height: "auto",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        Thêm quyền
      </Box>
    </FormDialog>
  );
};

// subject ID, ... ID use for update when call api, after inout value text then back this const to that handle submit
export const FullUpdatePermissionDialog = ({ value }) => {
  const [open, setOpen] = useState(false);
  const [permission, setPermission] = useState(value.permissionName);
  const [path, setPath] = useState(value.path);
  const [methodType, setMethodType] = useState(value.methodType);
  const [description, setDescription] = useState(value.description);
  const [isLoading2, setLoading2] = useState(false);
  const dispatch = useDispatch();

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    // addOrUpdate === 'ADD' ? '' : handleCloseMenu()
  };

  const handleSubmit = async () => {
    setLoading2(true);

    const res = await updatePermission(value.systemPermissionId, {
      permissionName: permission,
      path: path,
      methodType: methodType,
      description: description,
    });
    if (res.status === 200) {
      dispatch(setIsReload((pre) => !pre));
      setOpen(false);

      toast.success("Cập nhât quyền thành công");
    } else if (res.status === 202) {
      toast.warning("Kiểm tra lại thông tin");
    } else {
      toast.error("Cập nhât quyền thất bại");
    }
    setLoading2(false);
  };
  const title = "Chi tiết quyền truy cập";
  return (
    <FormDialog
      title={title}
      label=""
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="addPermission"
      setValueText={setPermission}
      setPath={setPath}
      setMethodType={setMethodType}
      setDescription={setDescription}
      description={description}
      methodType={methodType}
      path={path}
      permission={permission}
      isLoading={isLoading2}
    >
      <Box
        sx={{
          width: "100%",
          height: "auto",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        Xem chi tiết
      </Box>
    </FormDialog>
  );
};

// folder ID, ... ID use for update when call api

export const FullAddFolderTagDialog = ({
  children,
  name,
  handleCloseMenu,
  folderID,
  addOrUpdate,
  viewType,
  value,
}) => {
  const [open, setOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  // const [className, setClassName] = useState(name);
  const [listTag, setListTag] = useState([]);
  const table_tbl =
    viewType === "CLASS"
      ? "class_tbl"
      : viewType === "BOOK-SERIES"
      ? "book_series_tbl"
      : viewType === "SUBJECT-BOOK-SERIES"
      ? "book_series_subject_tbl"
      : viewType === "BOOK-VOLUME"
      ? "book_volume_tbl"
      : viewType === "CHAPTER"
      ? "chapter_tbl"
      : "lesson_tbl";
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    addOrUpdate === "ADD" ? "" : handleCloseMenu();
  };

  const handleSubmit = async () => {
    // nếu thành công thì setOpent false
    setIsLoading(true);
    try {
      console.log("table_tbl", table_tbl);
      console.log("folderID", folderID);
      console.log("listTag", listTag);
      const res = await setResourceTags({
        tableType: table_tbl,
        detailId: folderID,
        tagList: listTag,
      });
      if (res.status === 200) {
        setOpen(false);
        toast.success("Cập thành công");
      } else {
        toast.warning("Cập nhật thất bại");
      }
    } catch (e) {
      toast.warning("Cập nhật thất bại");
    }
    setIsLoading(false);

    // toast.error("Đăng nhập không thành công")
  };
  // const title =  addOrUpdate === 'ADD' ?  name : 'Cập nhập tag: '+name ;
  return (
    <FormDialog
      title="Thêm tag"
      label="Tag"
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="addFolderTag"
      folderID={folderID}
      viewType={viewType}
      setValueText={setListTag}
      isLoading={isLoading}
    >
      {addOrUpdate === "UPDATE" ? (
        "Cập nhật"
      ) : (
        <Box
          sx={{
            width: "100%",
            height: "auto",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          Thêm tag
        </Box>
      )}
    </FormDialog>
  );
};

export const FullChangePasswordDialog = () => {
  const [open, setOpen] = useState(false);
  const [oldPass, setOldPass] = useState("");
  const [pass, setPass] = useState("");
  const [rePass, setRePass] = useState("");

  const [isLoading, setIsLoading] = useState(false);
  const { logout, user, updateProfile } = useAuthContext();

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const router = useRouter();
  const handleSubmit = async () => {
    // nếu thành công thì setOpent false
    setIsLoading(true);
    const res = await changePassword({
      currentPassword: oldPass,
      newPassword: pass,
      confirmationPassword: rePass,
    });
    // console.log("res res", res);
    if (res.status === 200) {
      setOpen(false);
      toast.success("Cập nhật thành công, vui lòng đăng nhập lại");
      await logout();
      setTimeout(() => {
        push("/auth/login");
      }, 1000);
    } else if (res.status === 202) {
      toast.warning("Kiểm tra lại thông tin");
    } else {
      toast.error("Cập nhật không thành công");
    }
    setIsLoading(false);
  };
  // toast.error("Đăng nhập không thành công")
  return (
    <FormDialog
      title="Đổi mật khẩu"
      label="Đổi mật khẩu"
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="changepassword"
      setPass={setPass}
      setOldPass={setOldPass}
      setRePass={setRePass}
      isLoading={isLoading}
    >
      <GreenButtonAction sx={{ width: 180 }}>
        <CenterFlexBox>
          <RiLockPasswordLine /> <Box sx={{ ml: "3px" }}>Đổi mật khẩu</Box>
        </CenterFlexBox>
      </GreenButtonAction>
    </FormDialog>
  );
};

export const FullChangeStoreDialog = ({ children, handleNext }) => {
  const newMaterial = useSelector((state) => state.newMaterial.material);
  const isDetail = newMaterial?.isDetail;
  const [open, setOpen] = useState(false);
  const [valueText, setValueText] = useState(isDetail);

  const [isLoading, setIsLoading] = useState(false);

  const dispatch = useDispatch();
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleSubmit = () => {
    setOpen(false);
    dispatch(setIsDetail(valueText));
    handleNext();
  };
  // toast.error("Đăng nhập không thành công")
  return (
    <FormDialog
      title="Bạn muốn đưa tài liệu vào kho nào ?"
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="change-store"
      valueText={valueText}
      setValueText={setValueText}
    >
      {children}
    </FormDialog>
  );
};

export const FullReportDialog = ({ children,title,label,id,typeReport }) => {
  const newMaterial = useSelector((state) => state.newMaterial.material);
  const isDetail = newMaterial?.isDetail;
  const [open, setOpen] = useState(false);
  const [valueText, setValueText] = useState(isDetail);

  const [isLoading, setIsLoading] = useState(false);

  const dispatch = useDispatch();
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const [contentReport, setContentReport] = useState("");
  const handleSubmit = async() => {
    // nếu thành công thì setOpent false
    setIsLoading(true)
    const res = await reportMaterial({
      resourceId: id,
      message: contentReport,
      // "reporterId": 1
    });
    if (res.status === 200) {
      setOpen(false);
      toast.success("Báo cáo thành công");
    } else if (res.status === 202) {
      toast.warning("Kiểm tra lại thông tin");
    } else {
      toast.error("Báo cáo thất bại");
    }
    // toast.error("Đăng nhập không thành công")
    setIsLoading(false)

  };

  const handleReportCommentSubmit = async() => {
    // nếu thành công thì setOpent false
    setIsLoading(true)

    const res = await createReportComment({
      commentId: id,
      message: contentReport,
      // "reporterId": 1
    });
    if (res.status === 200) {
      setOpen(false);
      toast.success("Báo cáo thành công");
    } else if (res.status === 202) {
      toast.warning("Kiểm tra lại thông tin");
    } else {
      toast.error("Báo cáo thất bại");
    }
  setIsLoading(false)

    // toast.error("Đăng nhập không thành công")
  };
  // toast.error("Đăng nhập không thành công")

  return (
    <FormDialog
      title={title}
      label={label}
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={typeReport==="material"?handleSubmit:handleReportCommentSubmit}
      setContentReport={setContentReport}
      contentReport={contentReport}
      isLoading ={isLoading}
    >
      {children}
    </FormDialog>
  );
};

export const FullUnSaveMaterialDialog = ({
  value,
  handleCloseMenu,

}) => {
  const [open, setOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    handleCloseMenu();
  };

  const handleSubmit = async () => {
    setIsLoading(true);
    try {
      const res = await actionResource({
        resourceId: value?.resourceId,
        actionType: ACTION_UNSAVED ,
      });
      if (res.status === 200) {
        dispatch(setIsReload((pre) => !pre));
        setOpen(false);
        toast.success("Bỏ lưu thành công");
      } else {
        // setOpen(false);
        toast.success("Bỏ lưu thất bại");
      }
    } catch (e) {
      toast.success("Bỏ lưu thất bại");
    }
    setIsLoading(false);

    // nếu thành công thì setOpent false
    // const res = await changeStatusBookSeries

    // toast.error("Đăng nhập không thành công")
  };
  const title = "Bỏ lưu tài liệu: " + value?.name;
  return (
    <FormDialog
      title={title}
      label="xóa"
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="unsavematerial"
      isLoading={isLoading}
    >
      <IconAndText text="Bỏ lưu">
        <AiFillDelete />
      </IconAndText>
    </FormDialog>
  );
};

export const FullUnShareMaterialDialog = ({
  value,
  handleCloseMenu,

}) => {
  const [open, setOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    handleCloseMenu();
  };

  const handleSubmit = async () => {
    setIsLoading(true);
    try {
      const res = await unShareResource(value?.resourceId);
      if (res.status === 200) {
        dispatch(setIsReload((pre) => !pre));
        setOpen(false);
        toast.success("Xóa bỏ thành công");
      } else {
        // setOpen(false);
        toast.success("Xóa bỏ thất bại");
      }
    } catch (e) {
      toast.success("Xóa bỏ thất bại");
    }
    setIsLoading(false);

    // nếu thành công thì setOpent false
    // const res = await changeStatusBookSeries

    // toast.error("Đăng nhập không thành công")
  };
  const title = "Xóa bỏ tài liệu: " + value?.name+", khỏi danh sách chia sẻ với tôi";
  return (
    <FormDialog
      title={title}
      label="xóa"
      open={open}
      setOpen={handleOpen}
      setClose={handleClose}
      handleSubmit={handleSubmit}
      type="unsharematerial"
      isLoading={isLoading}
    >
      <IconAndText text="Xóa bỏ">
        <AiFillDelete />
      </IconAndText>
    </FormDialog>
  );
};
