import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import Button from '@mui/material/Button';
export const StyledSection = styled('img')(({ theme }) => ({
    width:'70%',
    marginTop:'100px'
  }));

  export  const ModalBackground= styled('div')(({ theme }) => ({
    width: '100vw',
    height: '100vh',
    backgroundColor: 'rgba(200, 200, 200)',
    position: 'fixed',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  }));
  
  export const ModalContainer = styled('div')(({ theme }) => ({
    width: '500px',
    height: '500px',
    borderRadius: '12px',
    backgroundColor: 'white',
    boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px',
    display: 'flex',
    flexDirection: 'column',
    padding: '25px',
}));
  
export const ModalContainerTitle= styled('div')(({ theme }) => ({
    display: 'inline-block',
    textAlign: 'center',
    marginTop: '10px',
}));
  
export const TitleCloseBtn = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'flex-end',
}));
  
export const TitleCloseBtnButton = styled('div')(({ theme }) => ({
    backgroundColor: 'transparent',
    border: 'none',
    fontSize: '25px',
    cursor: 'pointer',
}));
  