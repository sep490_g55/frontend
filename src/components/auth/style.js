import * as React from "react";
import { styled, alpha } from "@mui/material/styles";
import Button from "@mui/material/Button";
import { TextField } from "@mui/material";
export const StyledSection = styled("img")(({ theme }) => ({
  width: "70%",
  marginTop: "100px",
}));
export const LoginLayoutContainer = styled("div")(({ theme }) => ({
  display: "flex",
  justifyContent: "space-evenly",
  color: "#198754",
  alignItems: "stretch",
  
  [theme.breakpoints.down("xs")]: {
    flexDirection: "column",
    alignItems: "center",
  },
  [theme.breakpoints.up("xs")]: {
    flexDirection: "column",
    alignItems: "center",
  }
  ,
  [theme.breakpoints.down("md")]: {
    flexDirection: "column",
    alignItems: "center",
  },
  [theme.breakpoints.up("md")]: {
    flexDirection: "column",
    alignItems: "center",
  },
  [theme.breakpoints.up("lg")]: {
    flexDirection: "row",
   
    alignItems: "stretch",
  },
  [theme.breakpoints.up("xl")]: {
    // width: "500px",
    alignItems: "stretch",
  },
}));
export const CustomTextField = styled(TextField)(({ theme }) => ({
  margin: 2,
  
  [theme.breakpoints.down("xs")]: {
    marginBottom: 30,
  },
  [theme.breakpoints.up("xs")]: {
    marginBottom: 30,

  }
  ,
  [theme.breakpoints.down("md")]: {
    marginBottom: 30,

  },
  [theme.breakpoints.up("md")]: {
    marginBottom: 30,

  },
  [theme.breakpoints.up("lg")]: {
    marginBottom: 2,

  },
  [theme.breakpoints.up("xl")]: {
    marginBottom: 2,

  },
 
}));
export const StyledLayout = styled("div")(({ theme }) => ({
  width: "600px",
  display: "flex",
  justifyContent: "center",
  color: "#198754",
  textAlign: "center",
  alignAlign: "center",
  height:"auto",
  flexDirection: "column",
  [theme.breakpoints.down("xs")]: {
    width: "400px",
  },
  [theme.breakpoints.up("xs")]: {
    width: "400px",
  },
  [theme.breakpoints.down("md")]: {
    width: "400px",
  },
  [theme.breakpoints.up("md")]: {
    width: "600px",
  },
  [theme.breakpoints.up("lg")]: {
    width: "600px",
  },
  [theme.breakpoints.up("xl")]: {
    width: "600px",
    height:"800px",

  },
}));

export const StyledDivLeftContent = styled("div")(({ theme }) => ({
  textAlign: "start",
}));

export const StyledDivRightContent = styled("div")(({ theme }) => ({
  textAlign: "end",
}));

export const StyledDivContent = styled("div")(({ theme }) => ({
  margin: "20px 0",
}));
export const StyledButtonFull = styled("button")(({ theme }) => ({
  width: "100%",
  backgroundColor: "#198754 ",
  border: "none",
  padding: "10px 0",
  borderRadius: "5px",
  color: "white",
}));

export const ColInformation = styled("div")(({ theme }) => ({
  width: "100%",
  height:"88px",
  display: "flex",
  justifyContent: "center",

  
  [theme.breakpoints.down("xs")]: {
    width: "100%",
    flexDirection: "column",
    height:"auto"

  },
  [theme.breakpoints.up("xs")]: {
    width: "100%",
    flexDirection: "column",
    height:"auto"
  },
  
  [theme.breakpoints.down("md")]: {
    width: "100%",
    flexDirection: "column",
  height:"auto",

  },
  [theme.breakpoints.up("md")]: {
    width: "100%",
    flexDirection: "row",
  height:"88px",

  },
  [theme.breakpoints.up("lg")]: {
    width: "100%",
    flexDirection: "row",
  height:"88px",

  },
  [theme.breakpoints.up("xl")]: {
    width: "100%",
    flexDirection: "row",
  height:"88px",

  },
}));
