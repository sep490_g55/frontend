import React, { useEffect, useState } from "react";
import { ToastContainer, toast } from 'react-toastify';
import {
  StyledDivContent,
  StyledSection,
  StyledLayout,
  StyledDivLeftContent,
  StyledDivRightContent,
  StyledButtonFull,
  ColInformation,
  CustomTextField,
} from "./style";
import { Col, Form, Row } from "reactstrap";
// import { getProvinces } from "@/utils/provinceAxios";

import { getDistricts } from "@/utils/districtAxios";
import { getWards } from "@/utils/wardAxios";

// @mui/material/Box';
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Input from "@mui/material/Input";
import FilledInput from "@mui/material/FilledInput";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import Alert from "@mui/material/Alert";
//date
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { DateTimePicker } from "@mui/x-date-pickers";
import dayjs from "dayjs";
import { getProvinces } from "@/utils/addressApi";
import { MUIDistrictSelect, MUIProvinceSelect,MUIWardSelect } from "@/components/Select/AddressSelect/MUI/address";
import { useSelector } from "react-redux";
import LoadingTab from "../loading-screen/LoadingTab";
import { NameSystem } from "@/theme/system";
import { getAllClass,setRoleUser } from "@/dataProvider/agent";
import { useForm } from 'react-hook-form';
import { BeatLoader } from "react-spinners";
import yup from "@/utils/yupGlobal";
import { yupResolver } from "@hookform/resolvers/yup";
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
// import { FormGetInfo } from "./common";
import { useAuthContext } from "@/auth/useAuthContext";

export const OverView = () => {
  return (
    <StyledLayout>
      <Box className="title" sx={{ fontSize: "18px", marginTop: "50px" }}>
        Chào mừng đến với <NameSystem/>
      </Box>
      <Box sx={{ fontSize: "18px" }}>
        Nơi cung cấp học liệu, công cụ hỗ trợ tài liệu bài giảng
      </Box>
      <StyledSection
        src="https://platform.cloudclass.edu.vn/wp-content/uploads/2023/08/nen-tang-hoc-va-thi-truc-tuyen.jpg"
        alt="background"
      />
    </StyledLayout>
  );
};
export const FormGetInfo = ({newUser,isLoadingSubmit, titleBtn ,profile,handleSubmitForm,setProvince,setDistrict,setClas,setGender,setUsername,setFirstName,setLastName,setEmail,setPhone,setSchool,setDateOfBirth,setWard}) => {
 
  
  const [user,setUser]  = useState(profile)
  const [oldUser,setOldUser]  = useState(user)
  //console.log("oldUser",oldUser)
  
  //console.log("newUser",newUser)
useEffect(() => {
    checkiIsChange()
    
   
  }, [newUser.firstname,newUser.district,newUser.province,newUser.classId,newUser.school,newUser.dateOfBirth,newUser.gender,newUser.phone,newUser.lastname])

  const [provinceID, setProvinceID] = useState("");
  const [districtID, setDistrictID] = useState("");
  // const [dob, setDistrictID] = useState("");
  const [classes, setClasses] = useState([{id:1,name:"lớp 1"},{id:2,name:"lớp 2"},{id:3,name:"lớp 3"},{id:4,name:"lớp 4"},{id:5,name:"lớp 5"}]);

  const [clas, setC] = useState(user?.classId);
  const [gender, setG] = useState(user?.gender);
  const [isLoading, setIsLoading] = React.useState(true);
  const [isChange, setIsChange] = React.useState(true);


  const handleChangeGender = (event) => {
    setG(event.target.value);
    setGender(event.target.value);

  };
  const schema = yup
    .object({
      // username: yup
      //   .string()
      //   .required("Không để trống tên tài khoản")
      //   .username("Tên tài khoản không hợp lệ. Gợi ý: thoconxinhxan"),
      // email: yup
      //   .string()
      //   .required("Không để trống email")
      //   .email("Email không hợp lệ Gợi ý: thoconxinhxan@gmail.com"),
      firstName: yup
        .string()
        .required("Không để trống ")
        .name("Mật khẩu không đúng định dạng Gợi ý: Nguyễn Hương"),
        lastName: yup
        .string()
        .required("Không để trống ")
        .name("Mật khẩu không đúng định dạng Gợi ý: Giang"),
        dateOfBirth: yup
        .string()
        .required("Không để trống "),
        clas: yup
        .string()
        .required("Không để trống "),
        // .name("Mật khẩu không đúng định dạng Gợi ý: xinhxan"),
        phone: yup
        .string()
        .required("Không để trống ")
        .phone("SĐT không đúng định dạng Gợi ý: 0988909999"),
        school: yup
        .string()
        .required("Không để trống ")
        .school("Tên trường không đúng định dạng Gợi ý: THPT Phạm Văn Nghị"),
    })
    .required();

  const {
    reset,
    setError,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm(
    {
    resolver: yupResolver(schema),
  }
  )
  
  useEffect(() => {
  
    getClassList();
  }, []);
  const onSubmit = async(data) => {
    
  try{
    //console.log("data")
    
  } catch (error) {
    console.error(error);

    // reset();

    setError("afterSubmit", {
      ...error,
      message: error.message,
    });
  }
  }
  
  
  const getClassList = async() =>{
    setIsLoading(true)
    try{
      const classList = await getAllClass();
      // //console.log("classList",classList)
      if(classList.status ===200){
        setClasses(classList.data)
      }
    }catch(e){
      ////////console.log(e)
    }
    setIsLoading(false)
  };
  const handleGetInfo = async()=>{
    try{
      
    }catch(e){}
  }
  const changeClass = (id) =>{
    setC(id)
    setClas(id)

  }
  
const checkiIsChange = () =>{
    if(!oldUser ||oldUser === undefined ||!newUser||newUser === undefined ){
      return
    }
    try{
      if(oldUser.firstname.trim() !== newUser.firstname.trim()){
        // //console.log("firstname")
        setIsChange(false)
        return 
      }
      if(oldUser.lastname.trim()!== newUser.lastname.trim()){
        // //console.log("lasstname")
        setIsChange(false)
        return 
      }
      if(oldUser.phone.trim() !== newUser.phone.trim()){
        // //console.log("phonename")
        setIsChange(false)
        return 
      }
  
      if(oldUser.gender!== newUser.gender){
        // //console.log("gener")
        setIsChange(false)
        return 
      }if(oldUser.dateOfBirth!== newUser.dateOfBirth){
        // //console.log("dob")
        setIsChange(false)
        return 
      }if(oldUser.school.trim() !== newUser.school.trim()){
        // //console.log("school")
        setIsChange(false)
        return 
      }if(oldUser.classId !== newUser.classId){
        // //console.log("class")
        setIsChange(false)
        return 
      }if(oldUser.province !== newUser.province){
        // //console.log("provinbe")
        setIsChange(false)
        return 
      }if(oldUser.district !== newUser.district){
        // //console.log("uistrict")
        setIsChange(false)
        return 
      }
    }catch(e){
      return 
    }
    setIsChange(true)
    

    return ;
}

  return (
    <>{user && !isLoading && <Form onSubmit={handleSubmit(onSubmit)}>
    {!!errors.afterSubmit && (
    <Alert severity="error">{errors.afterSubmit.message}</Alert>
  )}

    <ColInformation>
      <CustomTextField
      value={user?.username}
        // error={errors.username}
        // defaultValue={user?.username}
        // helperText={errors.username && errors.username.message}
        fullWidth
        {...register("username")}
        label="Tên đăng nhập"
        type="text"
        id="username"
        disabled
        // onKeyUp={(e) => setData({...data,email: e.target.value})}
        onChange={(event) =>{setUsername(event.target.value)}}
      />
    </ColInformation>
    <ColInformation>
      <CustomTextField
        error={errors.firstName}
        defaultValue={user?.firstname}
        helperText={errors.firstName && errors.firstName.message}
        fullWidth
        {...register("firstName")}
        label="Họ"
        type="text"
        id="firstName"
        onChange={(e)=>{setFirstName(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
      <CustomTextField
        error={errors.lastName}
        defaultValue={user?.lastname}
        helperText={errors.lastName && errors.lastName.message}
        fullWidth
        {...register("lastName")}
        label="Tên"
        type="text"
        id="lastName"
        onChange={(event) => {
          setLastName(event.target.value)
        }}
        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
    </ColInformation>
    <ColInformation>
    <CustomTextField
        disabled
        error={errors.email}
        defaultValue={user?.email}
        helperText={errors.email && errors.email.message}
        fullWidth
        {...register("email")}
        label="Email"
        type="email"
        id="email"
        onChange={(e)=>{setEmail(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
      <CustomTextField
        error={errors.dateOfBirth}
        defaultValue={user?.dateOfBirth}
        helperText={errors.dateOfBirth && errors.dateOfBirth.message}
        fullWidth
        {...register("dateOfBirth")}
        label="Ngày sinh"
        type="date"
        id="dateOfBirth"
        
        onChange={(e)=>{setDateOfBirth(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
    </ColInformation>
    <ColInformation>
      <CustomTextField
        error={errors.phone}
        defaultValue={user?.phone}
        helperText={errors.phone && errors.phone.message}
        fullWidth
        {...register("phone")}
        label="Số điện thoại"
        type="text"
        id="phone"
        onChange={(e)=>{setPhone(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
      <FormControl sx={{width:"100%",margin:"2px"}} >
        <InputLabel id="demo-simple-select-error-label">Giới tính</InputLabel>
        <Select
          labelId="demo-simple-select-error-label"
          id="demo-simple-select-error"
          value={gender? "Nam":"Nữ"}
          label="Giới tính"
          onChange={(e) =>{handleChangeGender(e)}}
          renderValue={(value) => `${value}`}
        >
          
          <MenuItem value={true}>Nam</MenuItem>
          <MenuItem value={false}>Nữ</MenuItem>
        </Select>
      </FormControl>
    </ColInformation>

    <ColInformation>
      <MUIProvinceSelect
        errors={errors}
        register={register}
        province={user?.province}
        setProvince={setProvince}

        setProvinceID={setProvinceID}
      />
      <MUIDistrictSelect
        errors={errors}
        register={register}
        district={user?.district}
        provinceID={provinceID}
        setDistrict={setDistrict}

        districtID={districtID}
        setDistrictID={setDistrictID}
      />
    </ColInformation>
    <ColInformation>
      <CustomTextField
        error={errors.school}
        defaultValue={user?.school}
        helperText={errors.school && errors.school.message}
        fullWidth
        {...register("school")}
        label="Tên trường"
        type="text"
        id="school"
        onChange={(e) =>{setSchool(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
      <FormControl sx={{width:"100%",margin:"2px"}} >
        <InputLabel id="demo-simple-select-error-label">Lớp</InputLabel>
        <Select
          labelId="demo-simple-select-error-label"
          id="demo-simple-select-error"
          error={errors.clas}
          {...register("clas")}
        
        helperText={errors.clas && errors.clas.message}
          value={ (classes.find((cl) => cl.id === clas))?.name}
          label="Lớp"
          onChange={(e) =>{changeClass(e.target.value)}}
          renderValue={(value) =>value}
        >
          {classes.map((cl,index) => 
            <MenuItem key={index} value={cl.id}>{cl.name}</MenuItem>
            )}
          
        </Select>
      </FormControl>
    </ColInformation>

    <StyledDivContent>
      <StyledButtonFull
        disabled={isChange }
        outline
        className="login-page__btn-login"
        type="submit"
        style={{backgroundColor:isChange ?"gray":"#197854"}}
        onClick={errors.school || errors.email || errors.username || errors.firstName || errors.lastName || errors.phone    ? ()=>{toast.warning("Kiểm tra lại thông tin")}:() => {handleSubmitForm()}} 
      >
        {titleBtn}
        {isLoadingSubmit ? (
          <BeatLoader
            color="white"
            size={5}
            style={{ marginLeft: "5px", marginTop: "5px" }}
          />
        ) : (
          ""
        )}
      </StyledButtonFull>
    </StyledDivContent>
  </Form>
        }</>
  );
};

export const FormGetInfo2 = ({isLoadingSubmit, titleBtn ,profile,handleSubmitForm,setProvince,setDistrict,setWard,setClas,setGender,setUsername,setFirstName,setLastName,setEmail,setPhone,setSchool,setDateOfBirth}) => {
  const { user } = useAuthContext();
  //console.log("user before",user)
  
  const [provinces, setProvinces] = useState([]);
  const [districts, setDistricts] = useState([]);
  const [fiDistricts, setFiDistricts] = useState([]);
  const [wards, setWards] = useState([]);
  const [fiWards, setFiWards] = useState([]);
  const [provinceID, setProvinceID] = useState("");
  const [districtID, setDistrictID] = useState("");
  // const [dob, setDistrictID] = useState("");

  const [province, setProvinceOBJ] = useState(user?.province);
  const [district, setDistrictOBJ] = useState(user?.district);
  const [ward, setWardOBJ] = useState(null);

 
  const [classes, setClasses] = useState([{id:1,name:"lớp 1"},{id:2,name:"lớp 2"},{id:3,name:"lớp 3"},{id:4,name:"lớp 4"},{id:5,name:"lớp 5"}]);

  const [clas, setC] = useState(user?.classId);
  const [gender, setG] = useState(user?.gender);
  const [isLoading, setIsLoading] = React.useState(false);
  const [age, setAge] = useState('');
  const [username, setU] = useState(user?.username);
  const [firstName, setF] = useState(user?.firstname);
  const [lastName, setL] = useState(user?.lastname);
  const [phone, setP] = useState(user?.phone);
  const [school, setS] = useState(user?.school);
  const [objClass, setOs] = useState("");
  const [dateOfBirth, setD] = useState(user?.dateOfBirth);


  const handleChangeGender = (event) => {
    setG(event.target.value);
    setGender(event.target.value);

  };
  const schema = yup
    .object({
    
      firstName: yup
        .string()
        .required("Không để trống ")
        .name("Mật khẩu không đúng định dạng Gợi ý: Nguyễn Hương"),
        lastName: yup
        .string()
        .required("Không để trống ")
        .name("Mật khẩu không đúng định dạng Gợi ý: Giang"),
        dateOfBirth: yup
        .string()
        .required("Không để trống "),
        clas: yup
        .string()
        .required("Không để trống "),
        // .name("Mật khẩu không đúng định dạng Gợi ý: xinhxan"),
        phone: yup
        .string()
        .required("Không để trống ")
        .phone("SĐT không đúng định dạng Gợi ý: 0988909999"),
        school: yup
        .string()
        .required("Không để trống ")
        .school("Tên trường không đúng định dạng Gợi ý: THPT Phạm Văn Nghị"),
    })
    .required();

  const {
    reset,
    setError,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm(
    {
    resolver: yupResolver(schema),
  }
  )
  
  useEffect(() => {
  
    getClassList();
  }, []);
  const onSubmit = async(data) => {
    
  try{
    //console.log("data")
    
  } catch (error) {
    console.error(error);

    // reset();

    setError("afterSubmit", {
      ...error,
      message: error.message,
    });
  }
  }
  
  
  const getClassList = async() =>{
    setIsLoading(true)
    try{
      const classList = await getAllClass();
      //console.log(classList)
      if(classList.status === 200){
        setClasses(classList.data)
      }
    }catch(e){
      //console.log(e)
    }
    setIsLoading(false)
  };
  const handleGetInfo = async()=>{
    try{
      
    }catch(e){}
  }
  const changeClass = (id) =>{
    setC(id)
    setClas(id)

  }
  function findIndexes(list, value) {
    
    return list.findIndex(item => item.code === value?.code);
  
  }
  return (
   <>{user && !isLoading && <Form onSubmit={handleSubmit(onSubmit)}>
    {!!errors.afterSubmit && (
    <Alert severity="error">{errors.afterSubmit.message}</Alert>
  )}
    
    <ColInformation>
      <CustomTextField
        error={errors.firstName}
        defaultValue={user?.firstname}
        helperText={errors.firstName && errors.firstName.message}
        fullWidth
        {...register("firstName")}
        label="Họ"
        type="text"
        id="firstName"
        onChange={(e)=>{setFirstName(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
      <CustomTextField
        error={errors.lastName}
        defaultValue={user?.lastname}
        helperText={errors.lastName && errors.lastName.message}
        fullWidth
        {...register("lastName")}
        label="Tên"
        type="text"
        id="lastName"
        onChange={(event) => {
          setLastName(event.target.value)
        }}
        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
    </ColInformation>
    <ColInformation>
    
      <CustomTextField
        error={errors.dateOfBirth}
        // defaultValue={user?.dateOfBirth}
        helperText={errors.dateOfBirth && errors.dateOfBirth.message}
        fullWidth
        {...register("dateOfBirth")}
        label="Ngày sinh"
        type="date"
        id="dateOfBirth"
        onChange={(e)=>{setDateOfBirth(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
      <FormControl sx={{width:"100%",margin:"2px"}} >
        <InputLabel id="demo-simple-select-error-label">Giới tính</InputLabel>
        <Select
          labelId="demo-simple-select-error-label"
          id="demo-simple-select-error"
          value={gender? "Nam":"Nữ"}
          label="Giới tính"
        onChange={(e) =>{handleChangeGender(e)}}
          renderValue={(value) => `${value}`}
        >
          
          <MenuItem value={true}>Nam</MenuItem>
          <MenuItem value={false}>Nữ</MenuItem>
        </Select>
      </FormControl>
    </ColInformation>
    
    <ColInformation>
      <CustomTextField
        error={errors.phone}
        defaultValue={user?.phone}
        helperText={errors.phone && errors.phone.message}
        fullWidth
        {...register("phone")}
        label="Số điện thoại"
        type="text"
        id="phone"
        onChange={(e)=>{setPhone(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
      
      <MUIProvinceSelect
        errors={errors}
        register={register}
        province={province}
        setProvince={setProvince}

        setProvinceID={setProvinceID}
      />
    </ColInformation>

    <ColInformation>
      
      <MUIDistrictSelect
        errors={errors}
        register={register}
        district={district}
        provinceID={provinceID}
        setDistrict={setDistrict}
        districtID={districtID}
        setDistrictID={setDistrictID}
      />
      <MUIWardSelect
        errors={errors}
        register={register}
        ward={ward}
        setWard={setWard}
        districtID={districtID}
       
      />

    </ColInformation>
    <ColInformation>
      <CustomTextField
        error={errors.school}
        defaultValue={user?.school}
        helperText={errors.school && errors.school.message}
        fullWidth
        {...register("school")}
        label="Tên trường"
        type="text"
        id="school"
        onChange={(e) =>{setSchool(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
      <FormControl sx={{width:"100%",margin:"2px"}} >
        <InputLabel id="demo-simple-select-error-label">Lớp</InputLabel>
        <Select
          labelId="demo-simple-select-error-label"
          id="demo-simple-select-error"
          error={errors.clas}
          {...register("clas")}
        
        helperText={errors.clas && errors.clas.message}
          // value={  (classes?.find((cl) => cl.id === clas))?.name}
          label="Lớp"
          onChange={(e) =>{changeClass(e.target.value)}}
          // renderValue={(value) => (classes?.find((cl) => cl.id === value))?.name}
        >
          {classes?.map(cl => 
            <MenuItem value={cl.id}>{cl.name}</MenuItem>
            )}
          
        </Select>
        
      </FormControl>
    </ColInformation>

    <StyledDivContent>
      <StyledButtonFull
      style={{backgroundColor:"#197854"}}
        outline
        className="login-page__btn-login"
        type="submit"
        onClick={!(errors.school || errors.email || errors.username || errors.firstName || errors.lastName || errors.phone  )  ? () => {handleSubmitForm()}: ()=>{toast.warning("Kiểm tra lại thông tin")}} 
      >
        {titleBtn}
        {isLoadingSubmit ? (
          <BeatLoader
            color="white"
            size={5}
            style={{ marginLeft: "5px", marginTop: "5px" }}
          />
        ) : (
          ""
        )}
      </StyledButtonFull>
    </StyledDivContent>
  </Form>
        }
        </>
  );
};
const ItemColInfor = ({ border,children }) => {
  return (
    <Col
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "start",
        margin: "0 10px",
       
      }}
    >
      <Box
        sx={{
          width: "100%",
          maxWidth: "100%",
          marginTop: 2,
          marginBottom: 2,
          border:{ border}
        }}
      >
        {children}
      </Box>
    </Col>
  );
};
export const TextItemColInfor = ({register, border,id,label,type,value }) => {
  
  
  return (
    <ItemColInfor border={border}>
      {/* <TextField register={...register("username")} fullWidth label={label} type={type} id={id} value={value}/> */}
    </ItemColInfor>
  );
};
const DateItemColInfor = ({ id,label,value }) => {
  return (
    <ItemColInfor>
      <LocalizationProvider
              dateAdapter={AdapterDayjs}
              sx={{
                width: "100%",
                maxWidth: "100%",
              }}
            >
              <DateTimePicker
                label={label}
                // value='2022/04/17'
                format="YYYY-MM-DD"
                defaultValue={dayjs(`${value}`)}
                // onChange={(newValue) => setValue(newValue)}
                sx={{
                  width: "100%",
                  maxWidth: "100%",
                }}
              />
            </LocalizationProvider>
    </ItemColInfor>
  );
};
const SelectItemColInfor = ({ id,label,value,list,handleChange }) => {
  return (
    <ItemColInfor>
      <FormControl fullWidth>
            <InputLabel id={id}>{label}</InputLabel>
            <Select
              labelId={id}
              // id="classSelect"
              value={value}
              label={label}
              // onChange={handleChange}
              onChange={handleChange}
            >
              {/* <MenuItem value="">Chọn lớp</MenuItem> */}
              {list?.map((elm, index) => (
                <MenuItem key={index} value={elm.name}>
                  {elm.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
    </ItemColInfor>
  );
};
export const FormGetInfoUser = ({newUser,isLoadingSubmit, titleBtn ,profile,handleSubmitForm,setProvince,setDistrict,setClas,setGender,setUsername,setFirstName,setLastName,setEmail,setPhone,setSchool,setDateOfBirth,setWard}) => {
 
  
  const [user,setUser]  = useState(profile)
  const [oldUser,setOldUser]  = useState(user)


  const [provinceID, setProvinceID] = useState("");
  const [districtID, setDistrictID] = useState("");
  // const [dob, setDistrictID] = useState("");
  const [classes, setClasses] = useState([{id:1,name:"lớp 1"},{id:2,name:"lớp 2"},{id:3,name:"lớp 3"},{id:4,name:"lớp 4"},{id:5,name:"lớp 5"}]);

  const [clas, setC] = useState(user?.classId);
  const [gender, setG] = useState(user?.gender);
  const [isLoading, setIsLoading] = React.useState(true);
  const [isChange, setIsChange] = React.useState(true);


  const handleChangeGender = (event) => {
    setG(event.target.value);
    setGender(event.target.value);

  };
  const schema = yup
    .object({
      // username: yup
      //   .string()
      //   .required("Không để trống tên tài khoản")
      //   .username("Tên tài khoản không hợp lệ. Gợi ý: thoconxinhxan"),
      // email: yup
      //   .string()
      //   .required("Không để trống email")
      //   .email("Email không hợp lệ Gợi ý: thoconxinhxan@gmail.com"),
      firstName: yup
        .string()
        .required("Không để trống ")
        .name("Mật khẩu không đúng định dạng Gợi ý: Nguyễn Hương"),
        lastName: yup
        .string()
        .required("Không để trống ")
        .name("Mật khẩu không đúng định dạng Gợi ý: Giang"),
        dateOfBirth: yup
        .string()
        .required("Không để trống "),
        clas: yup
        .string()
        .required("Không để trống "),
        // .name("Mật khẩu không đúng định dạng Gợi ý: xinhxan"),
        phone: yup
        .string()
        .required("Không để trống ")
        .phone("SĐT không đúng định dạng Gợi ý: 0988909999"),
        school: yup
        .string()
        .required("Không để trống ")
        .school("Tên trường không đúng định dạng Gợi ý: THPT Phạm Văn Nghị"),
    })
    .required();

  const {
    reset,
    setError,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm(
    {
    resolver: yupResolver(schema),
  }
  )
  
  useEffect(() => {
  
    getClassList();
  }, []);
  const onSubmit = async(data) => {
    
  try{
    //console.log("data")
    
  } catch (error) {
    console.error(error);

    // reset();

    setError("afterSubmit", {
      ...error,
      message: error.message,
    });
  }
  }
  
  
  const getClassList = async() =>{
    setIsLoading(true)
    try{
      const classList = await getAllClass();
      // //console.log("classList",classList)
      if(classList.status ===200){
        setClasses(classList.data)
      }
    }catch(e){
      ////////console.log(e)
    }
    setIsLoading(false)
  };
  const handleGetInfo = async()=>{
    try{
      
    }catch(e){}
  }
  const changeClass = (id) =>{
    setC(id)
    setClas(id)

  }
  


  return (
    <>{user && !isLoading && <Form onSubmit={handleSubmit(onSubmit)}>
    {!!errors.afterSubmit && (
    <Alert severity="error">{errors.afterSubmit.message}</Alert>
  )}

    <ColInformation>
      <CustomTextField
      value={user?.username}
        // error={errors.username}
        // defaultValue={user?.username}
        // helperText={errors.username && errors.username.message}
        fullWidth
        {...register("username")}
        label="Tên đăng nhập"
        type="text"
        id="username"
        disabled
        // onKeyUp={(e) => setData({...data,email: e.target.value})}
        onChange={(event) =>{setUsername(event.target.value)}}
      />
    </ColInformation>
    <ColInformation>
      <CustomTextField
        disabled
        error={errors.firstName}
        defaultValue={user?.firstname}
        helperText={errors.firstName && errors.firstName.message}
        fullWidth
        {...register("firstName")}
        label="Họ"
        type="text"
        id="firstName"
        onChange={(e)=>{setFirstName(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
      <CustomTextField
        disabled
        error={errors.lastName}
        defaultValue={user?.lastname}
        helperText={errors.lastName && errors.lastName.message}
        fullWidth
        {...register("lastName")}
        label="Tên"
        type="text"
        id="lastName"
        onChange={(event) => {
          setLastName(event.target.value)
        }}
        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
    </ColInformation>
    <ColInformation>
    <CustomTextField
        disabled
        error={errors.email}
        defaultValue={user?.email}
        helperText={errors.email && errors.email.message}
        fullWidth
        {...register("email")}
        label="Email"
        type="email"
        id="email"
        onChange={(e)=>{setEmail(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
      <CustomTextField
        disabled
        error={errors.dateOfBirth}
        defaultValue={user?.dateOfBirth}
        helperText={errors.dateOfBirth && errors.dateOfBirth.message}
        fullWidth
        {...register("dateOfBirth")}
        label="Ngày sinh"
        type="date"
        id="dateOfBirth"
        
        onChange={(e)=>{setDateOfBirth(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
    </ColInformation>
    <ColInformation>
      <CustomTextField
        disabled
        error={errors.phone}
        defaultValue={user?.phone}
        helperText={errors.phone && errors.phone.message}
        fullWidth
        {...register("phone")}
        label="Số điện thoại"
        type="text"
        id="phone"
        onChange={(e)=>{setPhone(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
      <FormControl disabled sx={{width:"100%",margin:"2px"}} >
        <InputLabel id="demo-simple-select-error-label">Giới tính</InputLabel>
        <Select
        disabled
        labelId="demo-simple-select-error-label"
          id="demo-simple-select-error"
          value={gender? "Nam":"Nữ"}
          label="Giới tính"
        onChange={(e) =>{handleChangeGender(e)}}
          renderValue={(value) => `${value}`}
        >
          
          <MenuItem value={true}>Nam</MenuItem>
          <MenuItem value={false}>Nữ</MenuItem>
        </Select>
      </FormControl>
    </ColInformation>

    <ColInformation>
      <MUIProvinceSelect
        disabled
        errors={errors}
        register={register}
        province={user?.province}
        setProvince={setProvince}

        setProvinceID={setProvinceID}
      />
      <MUIDistrictSelect
        disabled={true}
        errors={errors}
        register={register}
        district={user?.district}
        provinceID={provinceID}
        setDistrict={setDistrict}

        districtID={districtID}
        setDistrictID={setDistrictID}
      />
    </ColInformation>
    <ColInformation>
      <CustomTextField
        disabled={true}
        error={errors.school}
        defaultValue={user?.school}
        helperText={errors.school && errors.school.message}
        fullWidth
        {...register("school")}
        label="Tên trường"
        type="text"
        id="school"
        onChange={(e) =>{setSchool(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
      <CustomTextField
        disabled
        // error={errors.phone}
        defaultValue={user?.className}
        // helperText={errors.phone && errors.phone.message}
        fullWidth
        {...register("phone")}
        label="Lớp"
        type="text"
        id="class"
        // onChange={(e)=>{setPhone(e.target.value)}}

        // onKeyUp={(e) => setData({...data,email: e.target.value})}
      />
    </ColInformation>

    <StyledDivContent>
      
    </StyledDivContent>
  </Form>
        }</>
  );
};