'use client'
import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import Button from '@mui/material/Button';
import bgsearch from '@/assets/bgsearch2.jpg'
export const ShowMaterial = styled('div')(({ theme }) => ({
    width: '70%',
    height: 'auto',
    display: 'block',
    position:'relative',
    float: 'right',
    right: 0,
    textAlign: 'center',
     webkitTransition: 'width 2s', 
     /* Safari */  transition: 'width 2s',
     marginLeft: '36px',
     
    
    [theme.breakpoints.up("xs")]: {
      width: '100% !important',
      marginLeft: 0,
    },
    [theme.breakpoints.up("sm")]: {
      width: '100% !important',
      marginLeft: 0,
    },
    [theme.breakpoints.up("md")]: {
      width: '100% !important',
      marginLeft: 0,
      
    },
    [theme.breakpoints.up("lg")]: {
      marginLeft: '20px',
      width: '74% !important',
      
    },
    [theme.breakpoints.up("xl")]: {
      marginLeft: '20px',
      width: '74% !important',

    }
  }));

  export const TitleMaterial = styled('div')(({ theme }) => ({
    width:'100%',
    borderRadius: '25px',
    padding: '5px 0',
    // border: '2px solid #b2b2b2',
    // borderBottom: 'white',
    margin: 'auto',
    textAlign: 'center',
    color: '#183700',
    fontSize:"20px",
    fontWeight:"400",
    fontStyle:"italic"
  }));

  
  export const Material = styled('div')(({ theme }) => ({
    display:'flex',
    marginTop:'5%',[theme.breakpoints.up("xs")]: {
      display:'block',
    },
    [theme.breakpoints.up("sm")]: {
      display:'block',


    },
  }));

  export const BodyMaterial = styled('div')(({ theme }) => ({
    width: '100%',
    height: 'auto',
    marginTop: '10px',
    borderRadius: '15px',
  }));