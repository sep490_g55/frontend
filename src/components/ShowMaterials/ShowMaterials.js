import React from "react";
import HeaderMaterial from "@/components/HeaderMaterial/headerMaterial";
import TableMaterial from "../TableMaterial/TableMaterial";
import { ShowMaterial, TitleMaterial, BodyMaterial } from "./styles";
import { Box, Button } from "@mui/material";
import { IoMdAddCircleOutline } from "react-icons/io";
import { MdDoubleArrow, MdNavigateNext } from "react-icons/md";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";

import { CenterFlexBox } from "../common_sections";

import { BeatLoader } from 'react-spinners';
const ShowMaterials = ({
  resource,
  isLoading,
  isShow,
  setIsShow,
  slideThumbnails,
}) => {
  const newMaterial = useSelector(state => state.newMaterial.material)
// console.log("newMaterial",newMaterial);
  const listTitle = [
    {
      id: "SLIDE",
      name: "Slides",
    },
    {
      id: "SHEET",
      name: "Phiếu học tập",
    },
    {
      id: "IMAGE",
      name: "Hình ảnh",
    },
    {
      id: "VIDEO",
      name: "Video",
    },
    {
      id: "AUDIO",
      name: "Audio",
    },
  ];
  const expandStyleTable = isShow ? { width: "74%" } : { width: "100% " };
const router = useRouter();
  return (
    <ShowMaterial className="show-material" style={expandStyleTable}>
      <TitleMaterial
        className="title-material"
        // onClick={() => {
        //   isShow ? setIsShow(false) : setIsShow(true);
        // }}
      >
        {
          newMaterial === null? 
          <Box sx={{ fontSize: "20px" }}>Thông tin tài liệu<BeatLoader
          color="#198754"
          size={5}
        />
          </Box>
          :
          <CenterFlexBox>{newMaterial.class.name}{newMaterial.bookSeries.name !== ""&&<MdDoubleArrow color="gray" style={{margin:"0 5px",fontSize:"10px" }}/>}{newMaterial.bookSeries.name}{newMaterial.subject.name !== ""&&<MdDoubleArrow color="gray" style={{margin:"0 5px",fontSize:"10px" }}/>}{newMaterial.subject.name}{newMaterial.bookVolume.name !== ""&&<MdDoubleArrow color="gray" style={{margin:"0 5px",fontSize:"10px" }}/>}{newMaterial.bookVolume.name}{newMaterial.chapter.name !== ""&&<MdDoubleArrow color="gray" style={{margin:"0 5px",fontSize:"10px" }}/>}{newMaterial.chapter.name}{newMaterial.lesson.name !== ""&&<MdDoubleArrow color="gray" style={{margin:"0 5px",fontSize:"10px" }}/>}{newMaterial.lesson.name}</CenterFlexBox>
        }
      </TitleMaterial>
      <BodyMaterial className="body-material">
        <HeaderMaterial listTitle={listTitle} />
        <Box sx={{textAlign: 'right'}}><Button sx={{mr: '50px',mt:"20px"}} onClick={() =>router.push("/materials/upload/stepper")}> <IoMdAddCircleOutline />Thêm mới</Button></Box>
        <TableMaterial
          isLoading={isLoading}
          slideThumbnails={resource}
          
        />
      </BodyMaterial>
    </ShowMaterial>
  );
};
export default ShowMaterials;
