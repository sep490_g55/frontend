import { Avatar, Box, Paper, TextField } from '@mui/material'
import React from 'react'
import { styled, alpha } from '@mui/material/styles';
import { AiFillDelete } from 'react-icons/ai';
import { BsBookmark, BsBookmarkCheckFill, BsFillBookmarksFill } from 'react-icons/bs';
import {Button, Select, SelectItem} from "@nextui-org/react";
import { MdPublic } from "react-icons/md";
import { IoMdPeople } from "react-icons/io";
import { RiGitRepositoryPrivateLine } from "react-icons/ri";
import { TbPointFilled } from "react-icons/tb";
import Link from 'next/link';
export const AvatarAndInfoTimeDiv = ({avatarsrc,fullName,time,accessType}) => {
  const accessMap = {
    "PUBLIC" :<MdPublic/>,
    "RESTRICT": <IoMdPeople/>,
    "PRIVATE":<RiGitRepositoryPrivateLine/>
  }
  return (
    <AvatarAndInfoDiv
    avatarsrc={avatarsrc}
    title1={fullName}
    title2={
       <Box 
       sx={{display: "flex",
       justifyContent: "start",
       alignItems: 'center',}}>
        <Box
      
      > {time}</Box>
      <TbPointFilled
      style={{fontSize:"6px",margin: "0 5px"}}
      />
      { accessMap[accessType]} 
      </Box>
      }
    
  />
  )
}
export const AvatarAndInfoDiv = ({avatarsrc,title1,title2}) => {
  return (
  <IconAndInfoDiv title1={title1} title2={title2}>
   <Avatar src={avatarsrc} /> 
  </IconAndInfoDiv>
  )
}
export const ButtonCustomNextUI = ({active,title,link,ml,mr}) => {
  return (
  // <Link href={link} style={{marginLeft:ml,marginRight:mr}}>
    <Button color={active?`primary`:``} radius="sm">
          {title}
    </Button>
  // </Link>
  )
}

export const AccessibleDiv = ({children,title1,title2}) => {
  return (
  <IconAndInfoDiv title1={title1} title2={title2}>
   <Box sx={{fontSize:'20px', borderRadius: '50%',backgroundColor:'#e3e3e3',width: '30px',height: '30px',display:'flex',justifyContent:'center',alignItems:'center'}}>
                {/* <MdOutlinePublic/> */}
                {children}
                </Box>
  </IconAndInfoDiv>
  )
}
export const IconAndInfoDiv = ({children,title1,title2}) => {
  return (
    <Box
    sx={{
      hight: "50px",
      display: "flex",
      justifyContent: "flex-start",
      margin: '10px 0',
      paddingLeft: "10px"
    }}
  >
    <Box sx={{ width: "15px", height: "15px",marginRight:'35px' ,color:"#050505"}}>
    {children}
    </Box>
    <Box 
    sx={{width:"100%"}}
    >
      <Box>
        {title1}
        </Box>
      <Box sx={{width:"100%",color: 'gray', fontSize: '12px'}}>
        {title2}
      </Box>
    </Box>
  </Box>
  )
}
export const FullColorAvatarAndInfoDiv = ({avatarsrc,title1,title2}) => {
    return (
      <Box
      sx={{
        width: 300,
        hight: "50px",
        display: "flex",
        justifyContent: "flex-start",
        margin: '10px 0'
      }}
    >
      <Box sx={{ width: "15px", height: "15px",marginRight:'35px' }}>
      <Avatar src={avatarsrc} /> 
        {/* <Avatar src="https://d.ibtimes.co.uk/en/full/1620522/johnny-depp.jpg" /> */}
      </Box>
      <Box>
        <Box sx={{fontWeight:"500"}}>
          {title1}
          {/* Viet Hoang */}
          </Box>
        <Box>
          {title2}
          {/* <BiSolidSchool /> THPT Pham Van Nghi */}
        </Box>
      </Box>
    </Box>
    )
  }
  export const FullColorInfoAndAvatarDiv = ({avatarsrc,title1,title2}) => {
    return (
      <Box
      sx={{
        width: 300,
        hight: "15px",
        display: "flex",
        justifyContent: "flex-end",
        alignItems: 'center',
        margin: '10px 0'
      }}
    >
      
      <Box>
        <Box>
          {title1}
          {/* Viet Hoang */}
          </Box>
        <Box>
          {title2}
          {/* <BiSolidSchool /> THPT Pham Van Nghi */}
        </Box>
      </Box>
      <Box sx={{ width: "15px", height: "15px",marginLeft:'15px',display:'flex',alignItems:'center' }}>
      <Avatar src={avatarsrc} /> 
        {/* <Avatar src="https://d.ibtimes.co.uk/en/full/1620522/johnny-depp.jpg" /> */}
      </Box>
    </Box>
    )
  }

export const AvatarAndInfoAnDeleteDiv = ({avatarsrc,title1,title2,mode,handleDelete}) => {
    return (
        <Box sx={{ display: "flex", alignItems: "center",justifyContent:"space-between" }}>
        <AvatarAndInfoDiv
          avatarsrc={avatarsrc}
          title1={title1}
          title2={title2}
        />
    {/* <Box sx={{color: 'gray'}}>{mode}</Box> */}

    <DeleteButton onClick={()=>handleDelete(title2)}>
         <Box sx={{ display: "flex",alignItems: "center"}}><AiFillDelete /> <Box sx={{ml:"3px"}}>Xóa</Box></Box>
        </DeleteButton>
      </Box>
    )
  }
  export const AvatarAndInfoOwnerDiv = ({avatarsrc,title1,title2}) => {
    return (
        <Box sx={{ display: "flex", alignItems: "center",justifyContent:"space-between" }}>
        <AvatarAndInfoDiv
          avatarsrc={avatarsrc}
          title1={title1}
          title2={title2}
        />
    <Box sx={{color: 'gray'}}>Chủ sở hữu</Box>

    <Box sx={{width: '100px'}}>

    </Box>
      </Box>
    )
  }

//style

export const CenterFlexBox = styled('div')(({ theme }) => ({
  width:"100%",
  display:'flex',
  justifyContent:"center",
  alignItems:"center",
 
 }));
 export const ResponsiveCenterFlexBox = styled('div')(({ theme }) => ({
  width:"100%",
  display:'flex',
  justifyContent:"center",
  alignItems:"center",
 [theme.breakpoints.up('xs')]: {
  display:'none',

    },
    [theme.breakpoints.up('md')]: {
      display:'none',


    },
    [theme.breakpoints.up('lg')]: {
      display:'flex',

  
    },
    [theme.breakpoints.up('xl')]: {
      display:'flex',

  
    },
 }));
 export const WrapCenterFlexBox = styled('div')(({ theme }) => ({
  width:"100%",
  display:'flex',
  justifyContent:"center",
  alignItems:"center",
 [theme.breakpoints.up('xs')]: {
  flexWrap:"wrap"

    },
    [theme.breakpoints.up('md')]: {
      flexWrap:"wrap"


    },
    [theme.breakpoints.up('lg')]: {
      flexWrap:"nowrap"

  
    },
    [theme.breakpoints.up('xl')]: {
      flexWrap:"nowrap"

  
    },
 }));
 export const HoverCenterFlexBox = styled('div')(({ theme }) => ({
  width:"100%",
  display:'flex',
  justifyContent:"center",
  alignItems:"center",
  '&:hover':{
    backgroundColor:"#d4e4f4"
  }
 }));

export const ButtonAction = styled('button')(({ theme }) => ({
    backgroundColor:'#f0f0f0',
     border: 'none',
     width: '100px',
     height: '35px',
     borderRadius: '17px',
     color: '#050505',
     marginLeft: '5px',
     '&:hover':{
       backgroundColor: '#ceced8',
       fontWeight: 500
     },
     '&:focus':{
       fontWeight: 'none'
     }
   }));
   export const WhiteButtonAction = styled('button')(({ theme }) => ({
    backgroundColor:'#FFFFFF',
     border: '1px solid #CECED8',
    //  width: '100px',
    //  height: '35px',
    padding: '5px 10px',
     borderRadius: '17px',
     color: '#296bd5',
    //  marginLeft: '5px',
     '&:hover':{
       backgroundColor: '#F4FCFF',
      //  fontWeight: 'bold'
     },
     '&:focus':{
       fontWeight: 'none'
     },
     [theme.breakpoints.down('md')]: {
      marginLeft:''
    },
    [theme.breakpoints.up('md')]: {
      flexWrap: 'wrap'

    },
    [theme.breakpoints.up('lg')]: {
      // backgroundColor: 'green',
    // marginLeft: '17px',
  
    },
    [theme.breakpoints.up('xl')]: {
      // backgroundColor: 'green',
    // width: '900px',
  
    },
    
   }));

   export const ImageDetail = styled('img')(({ theme }) => ({
   width:400,
     '&:hover':{
       backgroundColor: '#F4FCFF',
      //  fontWeight: 'bold'
     },
     '&:focus':{
       fontWeight: 'none'
     },
     [theme.breakpoints.down('md')]: {
      width:400,
    },
    [theme.breakpoints.up('md')]: {
      width:400,


    },
    [theme.breakpoints.up('lg')]: {
      width:500,

  
    },
    [theme.breakpoints.up('xl')]: {
      width:700,

  
    },
    
   }));
   export const Style2LeftInfoDiv = styled("div")(({ theme }) => ({
    width:"380px",
    height:"850px",
    overflowX:"auto",
    paddingRight:"20px",
    [theme.breakpoints.down("xs")]: {
        paddingLeft:"0px",
  
    },
    [theme.breakpoints.up("xs")]: {
      paddingLeft:"0px",
  
    }
    ,
    [theme.breakpoints.down("md")]: {
      paddingLeft:"0px",
  
    },
    [theme.breakpoints.up("md")]: {
      paddingLeft:"0px",
  
    },
    [theme.breakpoints.up("lg")]: {
      paddingLeft:"0px",
  
    },
    [theme.breakpoints.up("xl")]: {
      paddingLeft:"20px",
  
    },
  }));
   export const GreenButtonAction = styled('button')(({ theme }) => ({
     backgroundColor:'#198754',
      border: 'none',
      width: '100px',
      height: '35px',
      borderRadius: '8px',
      color: 'white',
      marginLeft: '15px',
      cursor: 'pointer',
      '&:hover':{
       //  backgroundColor: '#ceced8',
        fontWeight: 'bold'
      },
      '&:focus':{
        fontWeight: 'none'
      }
    }));
 
    export const BrGreenButtonAction = styled('button')(({ theme }) => ({
     backgroundColor:'#198754',
      border: 'none',
      width: '100px',
      height: '35px',
      borderRadius: '25px',
      color: 'white',
      
     //  marginLeft: '15px',
      cursor: 'pointer',
      '&:hover':{
       //  backgroundColor: '#ceced8',
        fontWeight: 'bold'
      },
      '&:focus':{
        fontWeight: 'none'
      }
    }));

    export const IconButtonAction = styled('button')(({ theme }) => ({
     backgroundColor:'inherit',
      border: 'none',
      width: '50px',
      height: '50px',
      borderRadius: '10px',
      color: '#050505',
     position: 'relative',
     zIndex: 222,
      cursor: 'pointer',
      '&:hover':{
       border: '1px solid #DEDEDE',
        fontWeight: 'bold',
        fontSize: '18px'
      },
      '&:focus':{
        fontWeight: 'none'
      }
    }));
    export const ActiveGreenButton = styled('button')(({ theme }) => ({
      backgroundColor:'#198754',
       border: 'none',
       width: '182px',
       height: '52px',
       borderRadius: '8px',
       color: 'white',
       fontWeight:"500",
      //  marginLeft: '15px',
       cursor: 'text',
      //  '&:hover':{
        //  backgroundColor: 'gray',
        //  fontWeight: 'bold'
      //  },
       
     }));
     export const UnActiveGreenButton = styled('button')(({ theme }) => ({
      backgroundColor:'#EEEEEF',
       border: 'none',
       width: '182px',
       height: '52px',
       borderRadius: '8px',
       color: 'black',
       fontWeight:"500",
      //  marginLeft: '15px',
       cursor: 'pointer',
       '&:hover':{
         backgroundColor: '#197854',
       color: 'white',
       //  fontWeight: 'bold'
       },
       
     }));
    export const DeleteButton = styled('button')(({ theme }) => ({
         border: 'none',
         width: '80px',
         height: '45px',
         borderRadius: '4px',
         color: 'black',
        position: 'relative',
        zIndex: 222,
         cursor: 'pointer',
         '&:hover':{
        backgroundColor:'#FD5D31',
        color: 'white',
           fontWeight: 'bold',
        //    fontSize: '16px'
         },
         '&:focus':{
           fontWeight: 'none'
         }
       }));

       export const CustomStyleTab = styled('div')(({ theme }) => ({
        padding: '0 16px',
         cursor: 'pointer',
         display: "flex",
        justifyContent: "flex-end",
        alignItems: 'center',
         '&:hover':{
          color: 'green !important',
          // fontWeight: 'bold',
         },
         '&:focus':{
           
         }
       }));

       export const Item = styled(Paper)(({ theme }) => ({
        backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
        ...theme.typography.body2,
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      }));

       //component
      export const TitleWithinInfo = ({title,info}) =>{
        return(
          <CenterFlexBox style={{justifyContent:"start"}}>
            <Box sx={{width: "100px",fontWeight:"500"}}>{title}</Box>
            <Box>{info}</Box>
          </CenterFlexBox>

        )
      }

       export const IconAndText = ({children,text}) =>{
        return(
          <Box
            sx={{display:'flex',justifyContent: 'center',alignItems: 'center'}}  
          >{children}<Box sx={{marginLeft: '10px',}}>{text}</Box></Box>
        )
       }

       export const SaveButton = ({style}) =>{
        const onSave = (e) =>{
          
          e.target.style.color == 'yellow' ? e.target.style = "color: #050505" : e.target.style = "color: yellow"
        }
        return(
          <Box
          sx={{
            width: "50px",
            height: "50px",
            float: "right",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            cursor:  'pointer',
            // margin: '10px',

            "&:hover": {
              // border: "1px solid gray",
              color: "gray",
              borderRadius:'10px',
              fontSize: '18px'
              // backgroundColor: "lightblue",
            },
            
            
          }}
          onClick={onSave}
        >
          <BsBookmarkCheckFill style={style} className="faBookmark" />
        </Box>
        )
       }
       export const ShowTabDiv = ({style}) =>{
        const onSave = (e) =>{
          
          e.target.style.color == 'yellow' ? e.target.style = "color: #050505" : e.target.style = "color: yellow"
        }
        return(
          <Box
          sx={{
            width: "50px",
            height: "50px",
            float: "right",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            cursor:  'pointer',
            // margin: '10px',

            "&:hover": {
              // border: "1px solid gray",
              color: "gray",
              borderRadius:'10px',
              fontSize: '18px'
              // backgroundColor: "lightblue",
            },
            
            
          }}
          onClick={onSave}
        >
          <BsBookmarkCheckFill style={style} className="faBookmark" />
        </Box>
        )
       }

export const ModerateSearchSection = ({label}) =>{
  return (
    <Box
        sx={{display: 'flex',justifyContent: "flex-end",
        alignItems: "center",marginLeft:'40px'}}
      
      >
        <Box
          component="form"
          sx={{
            "& > :not(style)": { m: 1, width: "25ch" },
           
          }}
          noValidate
          autoComplete="off"
        >
          <TextField
            id="outlined-basic"
            label={label}
            variant="outlined"
          />

        </Box>
        <GreenButtonAction> Tìm kiếm</GreenButtonAction>
      </Box>
  )
}
export const RouterLink = ({router,url,title,description})=>{
  const titleUrl = "Xem chi tiết "+title
  return(
    <Box
    sx={{
      cursor:"pointer",
      "&:hover":{
        color:"blue",
        textDecoration: 'underline',
      }
    }} 
    onClick={() =>router.push(url)}
     title={description}
     >{title}</Box>
  )
}



