import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';

import { SearchSectionDiv, CommonTabDiv, CommonLi, CommonLabel, CommonStyleUl } from './styles';
import { Box, Skeleton } from '@mui/material';
import { StyleUlClass, LiClass, LabelClass, DivClassName } from './styles';

import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import { useEffect, useRef } from 'react';
import { useState } from 'react';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import { setTagList } from '@/redux/slice/SearchTagSlice';
import { setIsSearch } from '@/redux/slice/IsReLoad';

import { setSearchInputText } from '@/redux/slice/SearchInputTextSlice';
import { getALLBookSeriesByClassID, getALLBookVolumeBySubjectBookSeriesID, getALLSubjectBookSeriesByBookSeriesID, getAllClass, getSuggestTagList } from '@/dataProvider/agent';
import { setViewBookSeries,setViewSubject, setViewBookVolume, setViewClass, setViewLesson, setViewChapter } from '@/redux/slice/NewMaterialSlice';


export const SearchSection = () => {
  const route = useRouter();
  const dispatch = useDispatch();
  const searchInputText = useSelector(state => state.searchInputText.text);
  // console.log("searchInputText: ",searchInputText);

  const currentUrl = route.asPath;
  const [tags, setTags] = useState([])
  const [suggestTags, setSuggestTags] = useState(searchInputText || searchInputText === null || searchInputText === undefined? searchInputText:"")
  const inputRef = useRef(null);

  useEffect(() => {
    //console.log("suggestTags được thay đổi: ",suggestTags);
    getSuggestTag()
    // inputRef.current?.focus();
  }, [suggestTags]);
  const getSuggestTag = async () =>{

    try{
      const res = await getSuggestTagList({tagSuggest: suggestTags});
      if(res.status === 200){
      //console.log("getSuggestTag",res.data)

      setTags(res.data);
      }
    }catch(e){
      //console.log("e",e)
    }
  };
  const handleChangeInputSearch = (e,value) =>{
    dispatch(setSearchInputText(value));
    setSuggestTags(value);
    // console.log("value 61",e.target)
    
  }
  const isSearch = useSelector(state => state.reloading.isSearch);
  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      // console.log("dispatch(setIsSearch(!isSearch))")
      // dispatch(setIsSearch(!isSearch))
    }
  };
  const handleSelectTag = (value)=>{
    dispatch(setSearchInputText(""));
    dispatch(setTagList(value))
  }
  return (
    <SearchSectionDiv >
      {currentUrl.includes("/home") ? "" : <Paper
      component="form"
      sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: "60%",borderRadius:10 }}
    >
      
      {/* <InputBase
        sx={{ ml: 1, flex: 1 ,with:'60%'}}
        placeholder="Tìm kiếm hình ảnh, video..."
        inputProps={{ 'aria-label': 'Tìm kiếm hình ảnh, video...' }}
      /> */}
      <Autocomplete
      sx={{
        width:"100%"
      }}
        multiple
        id="tags-outlined"
        options={tags}
        getOptionLabel={(option) => option.tagName}
        // defaultValue={[tags[0]]}
        freeSolo
        onChange={(event,value) => handleSelectTag(value)}
        placeholder={searchInputText}
        renderInput={(params) => (
          <TextField
          // ref={inputRef}
          value={searchInputText}
            {...params}
            // label="Tìm kiếm"
            placeholder={searchInputText?searchInputText:"Tìm kiếm hình ảnh, video,..."}
            sx={{
              '& .css-1d3z3hw-MuiOutlinedInput-notchedOutline': {
                border: 'none',
              },
            }}
            onChange={(event) => handleChangeInputSearch(event,event.target.value)}
            onKeyPress={(e) => handleKeyPress(e)}
          />
        )}
      
      />
      <IconButton type="button" sx={{ p: '10px' }} aria-label="search" onKeyPress={(e) => handleKeyPress(e)} onClick={()=>dispatch(setIsSearch(!isSearch))}>
        <SearchIcon />
      </IconButton>
      
      
     
    </Paper>}  
    </SearchSectionDiv>
  )
}
export const TitleCategory  = ({title})=>{
  return(
    <Box
              sx={{
                  // width: '100%',
                  height: '100%',
                  // zIndex: 1000,
                  marginLeft: '5%',
                  // marginTop: '-40px'
              }}
            >
              <Box
                sx={{
                  width: '100px',
                  height: '40px',
                  backgroundColor:'#198754',
                  color: 'white',
                  fontWeight:'bold',
                  fontSize: '18px',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems:'center',
                  borderTopLeftRadius: '20px',
                  borderTopRightRadius: '20px',
                  borderBottomLeftRadius: '20px',
                  
                }}
              >
                {title}
              </Box>
            </Box>
  )
}
export const HightTitleCategory  = ({title})=>{
  return(
    <Box
              sx={{
                  // width: '100%',
                  height: '100%',
                  // zIndex: 1000,
                  marginLeft: '5%',
                  // marginTop: '-40px'
              }}
            >
              <Box
                sx={{
                  width: '100px',
                  // height: '40px',
                  // backgroundColor:'#198754',
                  fontStyle:"italic",
                  color: 'gray',
                  // fontWeight:'bold',
                  fontSize: '18px',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems:'center',
                  borderTopLeftRadius: '20px',
                  borderTopRightRadius: '20px',
                  borderBottomLeftRadius: '20px',
                  
                }}
              >
                {title}
              </Box>
            </Box>
  )
}
export const ClassCategoryMaterial = ({classList,setClassList}) =>{
  // const [classList, setClassList] = useState([]);  
  const [isLoading, setIsLoading] = useState(true);  
  const dispatch = useDispatch()
const handleSet =  (value)=>{
    dispatch(setViewClass({
            
              id:value?.id,
              name:value?.name
            }))
            dispatch(setViewBookSeries({
            
              id:"",
              name:""
            }))
            dispatch(setViewSubject({
            
              id:"",
              name:""
            }))
            dispatch(setViewBookVolume({
            
              id:"",
              name:""
            }))
            dispatch(setViewChapter({
            
              id:"",
              name:""
            }))
            dispatch(setViewLesson({
            
              id:"",
              name:""
            }))
}
  useEffect(() => {
    GetAll()
  }, [])
  
  const GetAll = async() =>{
    setIsLoading(true)
    const res = await  getAllClass();
    if(res.status ===  200){
      setClassList(res.data)
    }
    setIsLoading(false)
  }
  return(
    <CategoryMaterial
    title={<TitleCategory title="Lớp"/>}
       isLoading ={isLoading}
       list={classList}
      //  setOpenCategory={setBookSeriesListAfter}
       setCategory = {handleSet}
    />
  )
}
export const BookSeriesCategoryMaterial = ({classID,bookSeriesList, setBookSeries,classList}) =>{
  const [isLoading, setIsLoading] = useState(true);  
  const dispatch = useDispatch()
const handleSet =  (value)=>{
  //console.log("value bs",value)
    dispatch(setViewBookSeries({
            
              id:value?.id,
              name:value?.name
            }))
            dispatch(setViewSubject({
            
              id:"",
              name:""
            }))
            dispatch(setViewBookVolume({
            
              id:"",
              name:""
            }))
            dispatch(setViewChapter({
            
              id:"",
              name:""
            }))
            dispatch(setViewLesson({
            
              id:"",
              name:""
            }))
}
useEffect(() => {
    GetAll()
  }, [classID,classList])
  const GetAll = async() =>{
    setIsLoading(true)
    const res = await  getALLBookSeriesByClassID(classID === "" || classID === undefined? (classList[0]?.id === undefined ?"0":classList[0]?.id):classID);
   //console.log("classList[0]?.id",classList[0]?.id)
    if(res.status === 200){
      setBookSeries(res.data)
    }
    setIsLoading(false)
  }
  return(
    <CategoryMaterial
    title="Bộ sách"
       isLoading ={isLoading}
       list={bookSeriesList}
      //  setOpenCategory={setBookSeriesListAfter}
       setCategory = {handleSet}
    />
  )
}

export const SubjectBookSeriesCategoryMaterial = ({setOSubjectBookSeries,bookSeriesID,subjectBookSeriesList, setSubjectBookSeriesList,bookSeriesList}) =>{
  const [isLoading, setIsLoading] = useState(true);  
  const dispatch = useDispatch()
const handleSet =  (value)=>{
    dispatch(setViewSubject({
            
              id:value?.id,
              subjectId:value?.subjectId,
              name:value?.name
            }))
            dispatch(setViewBookVolume({
            
              id:"",
              name:""
            }))
            dispatch(setViewChapter({
            
              id:"",
              name:""
            }))
            dispatch(setViewLesson({
            
              id:"",
              name:""
            }))
}
  useEffect(() => {
    GetAll()
  }, [bookSeriesID,bookSeriesList])
  const GetAll = async() =>{
    setIsLoading(true)
    //console.log("bookSeriesList",bookSeriesList)
    //console.log("bookSeriesList[0]id",bookSeriesList[0]?.id)

    const res = await  getALLSubjectBookSeriesByBookSeriesID(bookSeriesID=== ""|| bookSeriesID === undefined?  (bookSeriesList[0]?.id === undefined ? "0":bookSeriesList[0]?.id):bookSeriesID);
    if(res.status === 200) {setSubjectBookSeriesList(res.data)}
    setIsLoading(false)
  }
  return(
    <CategoryMaterial
    title="Môn học"
       isLoading ={isLoading}
       list={subjectBookSeriesList}
      //  setOpenCategory={setBookSeriesListAfter}
       setCategory = {handleSet}
    />
  )
}


export const BookVolumeCategoryMaterial = ({setOBookVolume,subjectID,bookVolumeList, setBookVolumeList,subjectBookSeriesList}) =>{
  const [isLoading, setIsLoading] = useState(true);
  const dispatch = useDispatch()
const handleSet =  (value)=>{
    dispatch(setViewBookVolume({
            
              id:value?.id,
              name:value?.name
            }))
            dispatch(setViewChapter({
            
              id:"",
              name:""
            }))
            dispatch(setViewLesson({
            
              id:"",
              name:""
            }))
}  
  useEffect(() => {
    GetAll()
  }, [subjectID,subjectBookSeriesList])
  const GetAll = async() =>{
    setIsLoading(true)
    const res = await  getALLBookVolumeBySubjectBookSeriesID(subjectID=== ""|| subjectID === undefined?(subjectBookSeriesList[0]?.id === undefined ? "0": subjectBookSeriesList[0]?.id):subjectID);
    if(res.status === 200) {setBookVolumeList(res.data)}
    setIsLoading(false)
  }
  return(
    <CategoryMaterial
    title="Đầu sách"
       isLoading ={isLoading}
       list={bookVolumeList}
      //  setOpenCategory={setBookSeriesListAfter}
       setCategory = {handleSet}
    />
  )
}

export const CategoryMaterial = ({title,isLoading,list,setOpenCategory,containerStyle,setCategory}) =>{
  const [selectedID, setSelectedID] = useState(0)

  const handleSelect = (obj)  =>{
    //console.log(title+"",obj)
    setSelectedID(obj?.id)
    // setOpenCategory()
    setCategory(obj)
  }
  useEffect(()=>{
    setSelectedID(0)
  },[])
  return(
    <CommonTabDiv 
      style={containerStyle}
    >
      <Box sx={{color:"gray",ml:"5px"}}>{title}</Box>
      <CommonStyleUl 
        style={{color: 'black'}}
      >
          { 
          list?.length === 0 && !isLoading? "Không có dữ liệu" :
          list?.map((item, index) => (
            <CommonLi key={index}style={{border:selectedID === item.id ? "2px solid #198754":"",}}>
              
                <CommonLabel
                //  onMouseOverCapture={()=>onSelectClass(clas)}
                 >
                    
                    <DivClassName
                   style={{
                    color:selectedID === item.id ? "#198754":"",
                    fontWeight:selectedID === item.id ? "500":"",

                  }}
                  onClick={() => handleSelect(item)}
                    className='class-name'
                     ><Box sx={{width:'auto',}}>{item.name}</Box></DivClassName>
                </CommonLabel>
            </CommonLi>
          ))} </CommonStyleUl>
    </CommonTabDiv>
  )
}


export const ViewClass = ({isLoading,classes,setIsClass,setClassID}) => {
  const handleSelect = (id)  =>{
    setIsClass(true)
    setClassID(id)
  }
    return (
      <>
            <Box
              sx={{
                  // width: '100%',
                  height: '100%',
                  // zIndex: 1000,
                  marginLeft: '15%',
                  marginTop: '-3%'
              }}
            >
              <Box
                sx={{
                  width: '100px',
                  height: '40px',
                  backgroundColor:'#198754',
                  color: 'white',
                  fontWeight:'bold',
                  fontSize: '18px',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems:'center',
                  borderTopLeftRadius: '20px',
                  borderTopRightRadius: '20px',
                  borderBottomLeftRadius: '20px',
                  
                }}
              >
                Lớp
              </Box>
            </Box>
            
            <StyleUlClass > 
            {classes.map((clas, index) => (
              <LiClass key={index}  >
                  <LabelClass
                  //  onMouseOverCapture={()=>onSelectClass(clas)}
                   >
                      
                      <DivClassName
                     style={{
                      color:clas.active ? "#198754":"",
                      borderBottom:clas.active ? "2px solid #198754":"none",

                    }}
                    onClick={() => handleSelect(clas.id)}

                      className='class-name'
                      id={clas.id} >{clas.name}</DivClassName>
                  </LabelClass>
              </LiClass>
            ))} </StyleUlClass>
        </>
    )
  }
  export const BookSeries = ({isLoading,bookSeries,setIsBookSeries,styleBookSeries,setBookSeriesID}) =>{
    const handleSelect = (id)  =>{
      setIsClass(true)
      setClassID(id)
    }
    return(
      <CommonTabDiv 
        style={styleBookSeries}
      >
        
        <CommonStyleUl 
          style={{color: 'black'}}
        >
            { 
            
            bookSeries.map((bs, index) => (
              <CommonLi key={index}>
                  <CommonLabel
                  //  onMouseOverCapture={()=>onSelectClass(clas)}
                   >
                      
                      <DivClassName
                     style={{
                      color:bs.active ? "#198754":"",
                      borderBottom:bs.active ? "2px solid #198754":"none",

                    }}
                    onClick={() => setIsBookSeries(bs.id)}
                      className='class-name'
                       ><Box sx={{width:'auto'}}>{bs.name}</Box></DivClassName>
                  </CommonLabel>
              </CommonLi>
            ))} </CommonStyleUl>
      </CommonTabDiv>
    )
  }

  export const Subjects = ({subjects,styleSubjects,setSubjectID,setIsSubject}) =>{
    const handleSelect = (id)  =>{
      setIsClass(true)
      setClassID(id)
    }
    return(
      <CommonTabDiv
        style={styleSubjects}
      >
        <CommonStyleUl >
            {subjects.map((subject, index) => (
              <CommonLi key={index}>
                  <CommonLabel
                  //  onMouseOverCapture={()=>onSelectClass(clas)}
                   >
                     
                      <DivClassName
                      style={{
                        width:'auto',
                        color:subject.active ? "#198754":"",
                        borderBottom:subject.active ? "2px solid #198754":"none",
  
                      }}
                      onClick={() =>setIsSubject(subject.id)}
                      className='class-name'
                      ><Box sx={{width:'auto'}}>{subject.name}</Box></DivClassName>
                  </CommonLabel>
              </CommonLi>
            ))} </CommonStyleUl>
      </CommonTabDiv>
    )
  }

  export const BookVolume = ({bookVolumes,styleBookVolumes,setBookVolumeID}) =>{
    const handleSelect = (id)  =>{
      setIsClass(true)
      setClassID(id)
    }
    return(
      <CommonTabDiv
        style={styleBookVolumes}
      >
        <CommonStyleUl >
            {bookVolumes.length === 0 ? (<Box sx={{ width: 300 }}>
      <Skeleton />
      <Skeleton animation="wave" />
    </Box>):bookVolumes.map((bookVolume, index) => (
              <CommonLi key={index}>
                  <CommonLabel
                  //  onMouseOverCapture={()=>onSelectClass(clas)}
                   >
                      
                      <DivClassName
                     style={{
                      width:'auto',
                      color:bookVolume.active ? "#198754":"",
                      borderBottom:bookVolume.active ? "2px solid #198754":"none",

                    }}
                    onClick={() =>setBookVolumeID(bookVolume.id)} 
                      className='class-name'
                      ><Box sx={{width:'auto'}}>{bookVolume.name}</Box></DivClassName>
                  </CommonLabel>
              </CommonLi>
            ))} </CommonStyleUl>
      </CommonTabDiv>
    )
  }
