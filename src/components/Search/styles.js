import * as React from 'react';
import styled from 'styled-components';
import Button from '@mui/material/Button';
import bgSearch from '@/assets/slide/bg-ky.jpg'
export const SearchSectionDiv = styled('div')(({ theme }) => ({
    width:'100%',
    height: '370px',
    // borderRadius:'10px',
    display: 'flex',
    justifyContent: 'center',
    alignItems:'center',
    marginBottom: '20px',
    backgroundImage:  `url(${bgSearch.src})`,
    // backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    // backgroundPosition: 'center center',
  }));

  export const SearchInput = styled('div')(({ theme }) => ({
    width: '60%',
    display: 'flex',
    justifyContent: 'center',
    alignItems:'center',
  }));

  export const SearchIcon = styled('div')(({ theme }) => ({
    position: 'relative',
    zIndex: 1000,
    right: '0'    
  }));

  export const TabClassDiv = styled('div')(({ theme }) => ({
    width: '100%',
    height: '65px',  
    backgroundColor:'#183700',
    borderRadius: '10px',
    display: 'flex',
    justifyContent: 'center',
    alignItems:'center',
  }));
  export const StyleUlClass = styled('ul')(({ theme }) => ({
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems:'center',
    flexWrap:"wrap",
    margin: '0',
    textAlign:'center',
    fontWeight:"bold",
  }));

  export const LiClass = styled('li')(({ theme }) => ({
    display: 'inline-block',
    width: '60px',
    height: '100%',
    lineHeight: '40px',
    margin:"0 20px",
    color: 'black', 
  }));

  export const LabelClass = styled('label')(({ theme }) => ({
    width: '60px',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems:'center',
    fontWeight: 500,
  }));

  export const DivClassName = styled('div')(({ theme }) => ({
    //IS MAIN COLOR
    // width:"fit-content",
    // minWidth: '70px',
    height: 'auto',
    color: '#686868',
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'center',
    alignItems:'center',
    // borderBottomLeftRadius: '15px',
    // borderBottomRightRadius: '15px',
    borderBottom: '2px solid white', 
    // fontWeight: 'bold',
    "&:hover": {
      
      color: 'black',
      borderBottom: '2px solid gray',
    },
  }));
  

  
  export const StyleInputClass = styled.input.attrs({ type: 'radio' })`
  display:'none';
  color: '#686868 !important';
  cursor: 'pointer';
  width: '100%';
  &:checked + .class-name{
    font-weight: bold;
    color: #686868!important;
    border-bottom: 2px solid #44642C
   
  }
`;


export const CommonTabDiv = styled('div')(({ theme }) => ({
  width: '100%',
  height: 'auto',  
  backgroundColor:'#E6FFF3',
  borderRadius: '10px',
  // display: 'block',
  // // justifyContent: 'center',
  // // alignItems:'center',
  borderBottom: '2px solid #00000026',
  boxShadow: "rgba(33, 35, 38, 0.1) 0px 10px 10px -10px"
 
}));
export const CommonStyleUl = styled('ul')(({ theme }) => ({
  width: '100%',
  color: 'black',
  display: 'flex',
    justifyContent: 'center',
    alignItems:'center',
    flexWrap: 'wrap',
    margin:0,
}));

export const CommonLi = styled('li')(({ theme }) => ({
  display: 'inline-block',
  width:'auto',
  height: '40px',
  color: '#686868',
  padding: "5px 10px",
    border: '1px solid gray',
    borderRadius: '8px',
    backgroundColor: 'white',
    margin: '5px',
}));

export const CommonLabel = styled('label')(({ theme }) => ({
  width: '100%',
  height: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems:'center',
  
}));

export const CommonDiv = styled('div')(({ theme }) => ({
  width: '100%',
  height: '100%',
  padding: '5px',
  color: 'gray ',
  cursor: 'pointer',
  display: 'flex',
  justifyContent: 'center',
  alignItems:'center',
  borderBottomLeftRadius: '15px',
  borderBottomRightRadius: '15px',
  "&:hover": {
    fontWeight: 'bolder',
    color: 'gray',
    backgroundColor: '#A4B494'
  },
}));



export const CommonStyleInput = styled.input.attrs({ type: 'radio' })`
display:'none';
color: 'black !important';
cursor: 'pointer';
width: '100%';
&:checked + .class-name{
  font-weight: bold;
    color: #44642C!important;
    border-bottom: 2px solid #44642C
 
}
`;