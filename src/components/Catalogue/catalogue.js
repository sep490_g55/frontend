import React,{useEffect, useState}from 'react'
import {FaBars} from   'react-icons/fa';
import {FaBookReader} from   'react-icons/fa';
import {FaArrowUp} from   'react-icons/fa';
import {FaChevronUp} from   'react-icons/fa';
import {FaChevronDown} from   'react-icons/fa';
// import Chapter from '../Chapter/Chapter';
import TableOfContents from '../TableOfContents/TableOfContents';
import {BodyCatalogue, CatalogueDiv,HeaderCatalogue,IconHeaderCatalogue,TitleHeaderCatalogue} from './styles'
import { getALLChapterByBookVolumeID } from '@/dataProvider/agent';
import { useSelector } from 'react-redux';
 const Catalogue = ({isShow,setIsShow,bookVolumeList}) => {
  const newMaterial = useSelector(state => state.newMaterial.material)

  const [chapterList, setChapterList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);  
  useEffect(() => {
    GetAll()
  }, [newMaterial?.bookVolume?.id,bookVolumeList])
  const GetAll = async() =>{
    setIsLoading(true)
    const res = await  getALLChapterByBookVolumeID(newMaterial?.bookVolume?.id === null || newMaterial?.bookVolume?.id  === undefined  ||newMaterial?.bookVolume?.id  ===""? bookVolumeList[0]?.id === null  ||  bookVolumeList[0]?.id  === undefined ?  "0":bookVolumeList[0]?.id:newMaterial?.bookVolume?.id);
    // console.log("chapters",res)
    if(res.status === 200){
      setChapterList(res.data)
    }
    setIsLoading(false)

  }
      const [openChapter, setOpenChapter] = useState(0);
      const handleChapterClick = (index) => {
        if (openChapter === index) {
          // Nếu Chương đã mở là Chương đang được bấm, đóng nó.
          setOpenChapter(null);
        } else {
          // Nếu không, mở Chương mới và đóng Chương cũ (nếu có).
          setOpenChapter(index);
        }
      };

      const [isOpen, setIsOpen] = useState(false)
    
      const [isExpand, setIsExpand] = useState(false)
      
      const onExpand = () =>{
        // const expandDiv = event.target;
        // expandDiv.classList.add('none-body-lessons');
        isExpand ? setIsExpand(false) : setIsExpand(true)
       }
       
        const styleShow = isShow ? {display:'block'} :{display:'none'};
              const styleShow2 = isShow ? {width:'250px'} :{width:'80px',height:'50px'};
               
  
  return (
   
    <CatalogueDiv style={styleShow2} >
      <HeaderCatalogue >
        <TitleHeaderCatalogue style={styleShow}   className='title-cata'>Mục lục</TitleHeaderCatalogue>
        
        {/* <IconHeaderCatalogue><FaBars  onClick={()=>{isShow ? setIsShow(false):setIsShow(true)}}  className='bar'/></IconHeaderCatalogue> */}
      </HeaderCatalogue>
      
      <BodyCatalogue className='body-catalogue' style={styleShow} >
       <TableOfContents
       isLoading={isLoading}
       
        chapters={chapterList}
        openChapter={openChapter}
        onChapterClick={handleChapterClick}
      />  
      </BodyCatalogue>
    </CatalogueDiv>
  )
}

export default Catalogue