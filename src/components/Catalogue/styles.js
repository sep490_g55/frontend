import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import Button from '@mui/material/Button';
import bgsearch from '@/assets/bgsearch2.jpg'
export const CatalogueDiv = styled('div')(({theme}) => ({
    width: '20%',
    height: '100%',
    float: 'left',
    position: 'relative',
    zIndex: 100,
    borderRadius: '10px',
    backgroundColor: 'white',
    color: 'white',
    webkitTransition: 'width 2s', /* Safari */  
    transition: 'width 2s',
    boxShadow: "0px -5px 10px rgba(0, 0, 0, 0.06), 5px 0px 10px rgba(0, 0, 0, 0.06)",
    
    [theme.breakpoints.up("xs")]: {
      display:'none'
    },
    [theme.breakpoints.up("sm")]: {
      display:'none'

    },
    [theme.breakpoints.up("md")]: {
      display:'none'

    },
    [theme.breakpoints.up("lg")]: {
      display:'block'

    },
    [theme.breakpoints.up("xl")]: {
      display:'block'

    }
    
  }));
  export const CatalogueResponsiveDiv = styled('div')(({theme}) => ({
    width: '100%',
    height: '100%',
    float: 'left',
    position: 'relative',
    zIndex: 100,
    borderRadius: '10px',
    backgroundColor: 'white',
    color: 'white',
    webkitTransition: 'width 2s', /* Safari */  
    transition: 'width 2s',
    boxShadow: "0px -5px 10px rgba(0, 0, 0, 0.06), 5px 0px 10px rgba(0, 0, 0, 0.06)",
    
    
    
  }));

  export const HeaderCatalogue = styled('div')(() => ({
    display: 'flex',
    width: '100%',
    padding: '10px 10px 10px 30px' ,
    color:"#198754",
    display: 'flex',
    justifyContent:' flex-start',
    alignItems: 'center',
  }));

  export const TitleHeaderCatalogue = styled('div')(() => ({
    width: '80%',
    float: 'left',
    marginTop: '5px',
    color:"#198754",
    fontSize:"26px",
    fontWeight:"bold",
  }));

  export const IconHeaderCatalogue = styled('div')(() => ({
    display: { xs: "none", md: "block" },
    width: '10%',
    float: 'right',
    fontSize: '24px',
    paddingTop: '-105px',
    cursor: 'pointer',
  }));

  export const BodyCatalogue = styled('div')(() => ({
    width: '100%',
    height: '450px',
    overflowY:'scroll',
    
  }));