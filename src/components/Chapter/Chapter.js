import React,{useState} from 'react'
import {PiBookOpenTextLight} from   'react-icons/pi';
import {FaArrowUp} from   'react-icons/fa';
import {FaChevronUp} from   'react-icons/fa';
import {FaChevronDown} from   'react-icons/fa';
import { Box } from '@mui/material';
import {HeaderLesson, LabelChapter,SelectLesson,TitleLesson,ChapterDiv, UlChapter, LiChapter} from './styles'
import { useDispatch, useSelector } from 'react-redux';
import { setMaterials } from '@/redux/slice/MaterialSlice';
import { setViewChapter, setViewLesson } from '@/redux/slice/NewMaterialSlice';
const Chapter = ({isLoading,isLoading2,title,chapterID, lessons, isOpen, onChapterClick,toggleDrawer ,selectedChapter, setSelectedChapter,id}) => {
  const newMaterial = useSelector(state => state.newMaterial.material)
  const dispatch = useDispatch();
    const handleClickChapter = () =>{
      dispatch(setViewChapter({
            
        id:chapterID,
        name:title
      }))
    
      dispatch(setViewLesson({
      
        id:"",
        name:""
      }))
      if(selectedChapter !== id){
        onChapterClick();
        setSelectedChapter(id);
      }
      
    }
    let selectedLesson = null
    if(lessons?.length !== 0){
      selectedLesson = lessons?.find((lesson) => lesson.active === true)

    }
    const [selectedLessonID, setSelectedLessonID] = useState( lessons?.length !== 0 ? selectedLesson?.id: newMaterial?.lesson?.id)

    const handleSelectLesson= (lesson)  =>{
      setSelectedLessonID(lesson.id);
      dispatch(setViewLesson({
            
        id:lesson?.id,
        name:lesson?.name
      }))
    }
  return (
    <ChapterDiv >
      
         <HeaderLesson onClick={handleClickChapter}  style={{color:newMaterial?.chapter?.id === id ?  "#198754" : "gray",fontWeight:newMaterial?.chapter?.id == id && 'bold',}}>
         <PiBookOpenTextLight style={{marginRight:"5px"}}/> <Box>{title}</Box>  
        </HeaderLesson>   
    {isOpen && (
     isLoading ? "..." : <UlChapter>
        {isLoading2
        ?<BeatLoader
            color="#198754"
            size={5}
            style={{marginLeft:"5px",marginTop:"5px"}}
        />
        :!isLoading2&&lessons?.length === 0  
        ? <Box sx={{color:"blue",fontSize:"10px",marginLeft:"20px"}}>Không có dữ liệu</Box> 
        
        : (lessons?.map((lesson, index) => (
          <LiChapter key={index}>
              <LabelChapter >
                  <SelectLesson type="radio" name="class" onChange={() =>handleSelectLesson(lesson)} checked={lesson.id === newMaterial?.lesson?.id}/>
                  <TitleLesson className='title-lesson' style={{color:newMaterial?.lesson?.id === lesson.id ?  "#198754" : "gray",fontWeight:newMaterial?.lesson?.id == lesson.id && 'bold',}}> {lesson.name}</TitleLesson>
             </LabelChapter></LiChapter>
        )))}
      </UlChapter>
    )}
      </ChapterDiv>
  )
}

export default Chapter