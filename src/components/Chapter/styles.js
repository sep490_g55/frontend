import * as React from 'react';
import styled from 'styled-components';
import Button from '@mui/material/Button';
import bgsearch from '@/assets/bgsearch2.jpg'

export const ChapterDiv = styled('div')(({ theme }) => ({
  width: '100%',
  
  
}));
export const HeaderLesson = styled('div')(({ theme }) => ({
    width: '100%',
    height: '30px',
    backgroundColor: 'white',
    
    paddingLeft: '2%',
    cursor: 'pointer',
    display: 'flex',
    justifyContent:' flex-start',
    alignItems: 'center',
    color:"gray",
    fontSize: '16px',
    '&:hover': {
      backgroundColor: '#F6F7FB',
      }
  }));
export const LabelChapter = styled('label')(({ theme }) => ({
    marginTop: '5px',
    marginBottom: '5px',
    cursor: 'pointer',
    width: '100%',
    height:"30px"

  }));
  export const UlChapter = styled('ul')(({ theme }) => ({
    height:"auto",
    width: '100%',
     padding:0,
  }));
  export const LiChapter = styled('li')(({ theme }) => ({
    height:"30px",
    width: '100%',
    paddingLeft:"20px",
     '&:hover': {
      backgroundColor: '#F6F7FB',
      }
  }));

  export const TitleLesson = styled('div')(({ theme }) => ({
    width: '100%',
    color: "gray",
    // fontWeight: 'bolder',
    
   
  }));

  export const SelectLesson = styled.input.attrs({ type: 'radio' })`
  display: none;

  &:checked + .title-lesson{
   font-weight: 500;

    color: #198754;
  }
`;

  