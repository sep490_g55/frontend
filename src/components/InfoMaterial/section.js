import { useState, useEffect } from "react";
import { Avatar, Box } from "@mui/material";
import { format } from "date-fns";
//react-icons
import { FaBars } from "react-icons/fa";
import { FcInfo } from "react-icons/fc";
import { BeatLoader } from "react-spinners";
import { setIsReload } from "@/redux/slice/IsReLoad";

import Image from "react-bootstrap/Image";
import { Col, Container, Row } from "react-bootstrap";
import { BsTrophy, BsFillFlagFill } from "react-icons/bs";
import { BiSolidLike } from "react-icons/bi";
import { BiSolidDislike, BiSolidReport } from "react-icons/bi";
import { AiOutlineDownload } from "react-icons/ai";
import { CiCircleRemove } from "react-icons/ci";
//style components
import {
  ContainerDiv,
  LeftDiv,
  RightDiv,
  HeaderLeftLeft,
  HeaderRightLeft,
  ActionLabel,
  SelectAction,
  InputCmt,
  GroupLike,
  HeaderInfo,
  ActionButton,
  MdActionButton,
} from "./styles";
import { ButtonAction } from "../FolderImage/styles";
import FormDialog, { FullReportDialog } from "../Dialog/section";

//toast
import { ToastContainer, toast } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";
import { AvatarAndInfoTimeDiv } from "../common_sections";
import { getMaterials } from "@/utils/folderSrc";
//icon
import { AiOutlineCamera, AiOutlineVideoCamera } from "react-icons/ai";
import { BsDownload, BsBookmark, BsShareFill } from "react-icons/bs";
import { BiInfoCircle } from "react-icons/bi";
import { MdPublic } from "react-icons/md";
import { IoMdPeople } from "react-icons/io";
import { RiGitRepositoryPrivateLine } from "react-icons/ri";
import { TbPointFilled } from "react-icons/tb";
import { RxDividerVertical } from "react-icons/rx";
import { FaRegComment } from "react-icons/fa";
import { LuCornerDownRight } from "react-icons/lu";
import { useDispatch, useSelector } from "react-redux";
import SpeedDialTooltipOpen from "../action/SpeedDial";
import {
  ContainerDetailMaterialLeftDiv,
  GroupComment,
  InfoMaterialDiv,
} from "@/components/DetailMaterial/styles";
import { CenterFlexBox } from "../common_sections";
import { TiTick } from "react-icons/ti";
import { VscVerifiedFilled } from "react-icons/vsc";
import {
  reportMaterial,
  actionResource,
  showMoreComment,
  createReportComment,
  deleteComment,
  getCommentResourcesByResourceId,
} from "@/dataProvider/agent";
import { ACTION_UNLIKE, ACTION_LIKE } from "@/config";
import { useAuthContext } from "@/auth/useAuthContext";
import { setInfoUserReply } from "@/redux/slice/ReplyUser";

export const LeftInfoDiv = ({ titleheader, children, title, onExpand }) => {
  return (
    <>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          padding: "5px",
        }}
        className="header-left"
      >
        <HeaderLeftLeft>{titleheader}</HeaderLeftLeft>

        {/* <HeaderRightLeft onClick={onExpand}>
          <FaBars title="Click để thu gọn" />
        </HeaderRightLeft> */}
      </Box>
      {children}
    </>
  );
};

export const DetailMaterialLeftSection = ({
  material,
  owner,
  detailMaterial,
  focusInput,
  inputRef,
  isF5,
}) => {
  const { user, isAuthenticated } = useAuthContext();
  const dispatch = useDispatch();

  const originalDate = detailMaterial?.createdAt;
  //console.log(originalDate)
  const formattedDate = format(
    new Date(originalDate === undefined ? "2023-11-02T18:54:00" : originalDate),
    "dd/MM/yyyy HH:mm"
  );

  const [contentReport, setContentReport] = useState("");

  const handleSubmit = async () => {
    // nếu thành công thì setOpent false
    const res = await reportMaterial({
      resourceId: material?.resourceDTOResponse.id,
      message: contentReport,
      // "reporterId": 1
    });
    if (res.status === 200) {
      setOpen(false);
      toast.success("Báo cáo thành công");
    } else if (res.status === 202) {
      toast.warning("Kiểm tra lại thông tin");
    } else {
      toast.error("Báo cáo thất bại");
    }
    // toast.error("Đăng nhập không thành công")
  };

  const accessMap = {
    PUBLIC: <MdPublic />,
    RESTRICT: <IoMdPeople />,
    PRIVATE: <RiGitRepositoryPrivateLine />,
  };
  const [isLike, setIsLike] = useState(material?.isLike);
  const [isUnLike, setIsUnLike] = useState(material?.isUnLike);
  const [numLike, setNumLike] = useState(material?.numberOfLike);
  const [numUnLike, setNumUnLike] = useState(material?.numberOfUnlike);

  const handleSetIsLike = () => {
    setIsLike(true);
    setIsUnLike(false);
  };
  const handleSetIsUnLike = () => {
    setIsLike(false);
    setIsUnLike(true);
  };

  const handleActive = async (id, type) => {
    if (type === ACTION_LIKE) {
      //liked and like again => not like
      if (isLike) {
        if (numLike !== 0) {
          setNumLike((pre) => pre - 1);
          setIsLike(false);
        }

        //not like and like
      } else {
        setIsLike(true);
        setNumLike((pre) => pre + 1);
        // handleActive(id, ACTION_UNLIKE);
        if (isUnLike) {
          setNumUnLike((pre) => pre - 1);
          setIsUnLike(false);
          await actionResource({
            resourceId: id,
            actionType: "" + ACTION_UNLIKE,
          });
        }
      }
    } else if (type === ACTION_UNLIKE) {
      //unlike and unlike again => not unlike

      if (isUnLike) {
        if (numUnLike !== 0) {
          setNumUnLike((pre) => pre - 1);

          setIsUnLike(false);
        }
      } else {
        setNumUnLike((pre) => pre + 1);

        setIsUnLike(true);
        // handleActive(id, ACTION_LIKE);

        if (isLike) {
          setNumLike((pre) => pre - 1);
          setIsLike(false);
          await actionResource({
            resourceId: id,
            actionType: "" + ACTION_LIKE,
          });
        }
      }
    }

    const res = await actionResource({ resourceId: id, actionType: "" + type });
    if (res.status === 200) {
    } else {
      //console.log("thất bại "+type)
    }
  };
  const actions = [
    { icon: <BsDownload />, name: "Tải về" },
    { icon: <BsBookmark />, name: "Save" },
    { icon: <BsShareFill />, name: "Chia sẻ" },
  ];
  const handleComment = () => {
    dispatch(setInfoUserReply(null));
    inputRef.current?.focus();
  };
  // const [isLike, setIsLike] = useState(material?.isLike);
  // const [isUnLike, setIsUnLike] = useState(material?.isUnLike);
  // const [numLike, setNumLike] = useState(material?.numberOfLike);
  // const [numUnLike, setNumUnLike] = useState(material?.numberOfUnlike);
  useEffect(()=>{
    setIsLike(material?.isLike)
    setIsUnLike(material?.isUnLike)
    setNumLike(material?.numberOfLike)
    setNumUnLike(material?.numberOfUnlike)
  },[material])
  return (
    <ContainerDetailMaterialLeftDiv>
      <Box>
        <CenterFlexBox style={{ justifyContent: "start" }}>
          <Box
            sx={{
              paddingLeft: "10px",
              cursor: "pointer",
              "&:hover": { textDecoration: "underline" },
            }}
          >
            {detailMaterial?.name}
          </Box>
          {detailMaterial?.approveType === "ACCEPTED" ? (
            <VscVerifiedFilled style={{ color: "#00ffff" }} />
          ) : (
            ""
          )}
        </CenterFlexBox>

        <Box
          className="trophy"
          sx={{ paddingLeft: "10px", marginBottom: "10px" }}
        >
          {detailMaterial?.point === 0 ? (
            <Box sx={{ color: "blue", fontSize: "10px" }}>Miễn phí</Box>
          ) : (
            <CenterFlexBox>
              <Box sx={{ marginRight: "10px" }}>{detailMaterial?.point}</Box>{" "}
              <BsTrophy className="faTrophy" />
            </CenterFlexBox>
          )}
        </Box>
      </Box>
      <InfoMaterialDiv>
        <Box className="info-doc">
          <AvatarAndInfoTimeDiv
            avatarsrc={owner?.avatar}
            fullName={owner?.firstname + " " + owner?.lastname}
            time={"Đã đăng: " + formattedDate}
            accessType={detailMaterial?.visualType}
          />

          <Box sx={{ wordWrap: "break-word", paddingLeft: "10px" }}>
            Mô tả: {detailMaterial?.description}
            {/* Sep 20, 2023 — NextJS là framework mã ngSep 20, 2023 — NextJS là framework mã nguồJS là framework mã ngSep 20, 2023 — NextJS là framework mã nguồn mở được xây dựng trên nền tảng của React, cho phép chúng ta xây dựng các trang web tĩnh có tốc độ siêu nhanh vàuồn mở được xây dựng trên nền tảng của React, cho phép chúng ta xây dựng các trang web tĩnh có tốc độ siêu nhanh vàSep 20, 2023 — NextJS là framework mã nguồn mở được xây dựng trên nền tảng của React, cho phép chúng ta xây dựng các trang web tĩnh có tốc độ siêu nhanh và */}
          </Box>
        </Box>
        {detailMaterial?.approveType === "ACCEPTED" ? (
          <>
            <Box
              sx={{
                display: "flex",
                width: "100%",
                height: "auto",
                borderBottom: "1px solid #EEEEEF",
                borderTop: "1px solid #EEEEEF",
                margin: "15px 0",
              }}
            >
              <HeaderInfo></HeaderInfo>
              <GroupLike className="danhgia">
                <CenterFlexBox
                  style={{
                    width: "100px",
                  }}
                >
                  <ActionButton
                    title="Tôi thích tài liệu này"
                    onClick={() =>
                      handleActive(
                        material?.resourceDTOResponse.id,
                        ACTION_LIKE
                      )
                    }
                  >
                    <BiSolidLike
                      className="title-action"
                      color={isLike ? "#198754" : ""}
                      style={{ marginRight: "3px" }}
                    />{" "}
                    {numLike}
                  </ActionButton>
                  <RxDividerVertical
                    style={{ width: "25px", fontSize: "20px" }}
                  />
                  <ActionButton
                    title="Tôi không thích tài liệu này"
                    onClick={() =>
                      handleActive(
                        material?.resourceDTOResponse.id,
                        ACTION_UNLIKE
                      )
                    }
                  >
                    <BiSolidDislike
                      className="title-action"
                      color={isUnLike ? "#198754" : ""}
                      style={{ marginRight: "3px" }}
                    />
                    {numUnLike}
                  </ActionButton>
                </CenterFlexBox>
                <ActionButton
                  style={{}}
                  title="Bình luận"
                  onClick={() => handleComment()}
                >
                  <FaRegComment color="#050505" />
                </ActionButton>

                {owner?.username === user?.username ? (
                  ""
                ) : (
                  <FullReportDialog
                    title="Báo cáo tài liệu"
                    label="Nội dung báo cáo"
                    id={material?.resourceDTOResponse.id}
                    typeReport="material"
                  >
                    <ButtonAction style={{ backgroundColor: "inherit" }}>
                      <BsFillFlagFill />
                    </ButtonAction>
                  </FullReportDialog>
                )}
              </GroupLike>
            </Box>

            <GroupComment>
              <HeaderInfo>Bình luận</HeaderInfo>

              <Box
                sx={{
                  marginTop: "25px",
                }}
              >
                <CommentUserResource
                  resourceId={material?.resourceDTOResponse?.id}
                  focusInput={focusInput}
                  inputRef={inputRef}
                />
                {/* {material?.commentDetailDTOResponses?.map((comment, index) => (
                  <CommentUser
                    isF5={isF5}
                    isSubComment={true}
                    setMoreComment={setMoreComment}
                    comment={comment}
                    focusInput={focusInput}
                    inputRef={inputRef}
                    key={index}
                    commenter={comment.commentDTOResponse.commenterDTOResponse}
                    content={comment.commentDTOResponse.content}
                    time={comment.commentDTOResponse.createdAt}
                    isSHowMore={moreComment}
                    subLength={comment.numberOfReplyComments}
                    amMine={
                      moreComment
                        ? comment.commentDTOResponse.commentId ===
                          moreComment[0]?.commentRootId
                        : false
                    }
                    isTextShowSubComment={moreComment ? "unShow" : "show"}
                    subComment={moreComment?.map((subComment, index) => (
                      <CommentUser
                        isF5={isF5}
                        isSubComment={false}
                        setMoreComment={setMoreComment}
                        comment={subComment}
                        focusInput={focusInput}
                        inputRef={inputRef}
                        key={index}
                        commenter={subComment.commenterDTOResponse}
                        content={subComment.content}
                        time={subComment.createdAt}
                        amMine={
                          comment.commentDTOResponse.commentId ===
                          subComment.commentRootId
                        }
                        // subComment={
                        //   material?.commentDetailDTOResponses.numberOfReplyComments === 0 ? "" :

                        // }
                        isTextShowSubComment="unShow"
                      />
                    ))}
                  />
                ))} */}
                {/* {material?.commentDetailDTOResponses.map((comment, index) => (
            <CommentUser
              key={index}
              user={comment.commentDTOResponse.commenterDTOResponse}
              content={comment.commentDTOResponse.content}
              time={comment.commentDTOResponse.createdAt}
              // subComment={
              //   material?.commentDetailDTOResponses.numberOfReplyComments === 0 ? "" :

              // }
            />
          ))} */}
              </Box>
            </GroupComment>
          </>
        ) : (
          ""
        )}
      </InfoMaterialDiv>
    </ContainerDetailMaterialLeftDiv>
  );
};
const CommentUserResource = ({ resourceId, focusInput, inputRef }) => {
  const [comments, setComments] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const [moreComment, setMoreComment] = useState(null);
  const reloading = useSelector((state) => state.reloading.isReLoad);

  const getCommentsMaterial = async () => {
    setIsLoading(true);
    const res = await getCommentResourcesByResourceId(resourceId);
    if (res.status === 200) {
      setComments(res?.data);
    }
    setIsLoading(false);
  };
  useEffect(() => {
    getCommentsMaterial();
  }, [resourceId,reloading]);
  return (
    <>
      {isLoading && comments?.length === 0
        ? <CenterFlexBox>
        <BeatLoader
      color="#198754"
      size={5}
      style={{ marginLeft: "5px", marginTop: "5px",marginBottom:"10px" }}
    />
    </CenterFlexBox>
        : isLoading && comments?.length > 0
        ?<>{comments?.map((comment, index) => (
            <CommentUser
              // isF5={isF5}
              isSubComment={true}
              setMoreComment={setMoreComment}
              comment={comment}
              focusInput={focusInput}
              inputRef={inputRef}
              key={index}
              commenter={comment.commentDTOResponse.commenterDTOResponse}
              content={comment.commentDTOResponse.content}
              time={comment.commentDTOResponse.createdAt}
              isSHowMore={moreComment}
              subLength={comment.numberOfReplyComments}
              amMine={
                moreComment
                  ? comment.commentDTOResponse.commentId ===
                    moreComment[0]?.commentRootId
                  : false
              }
              isTextShowSubComment={moreComment ? "unShow" : "show"}
              subComment={moreComment?.map((subComment, index) => (
                <CommentUser
                  // isF5={isF5}
                  isSubComment={false}
                  setMoreComment={setMoreComment}
                  comment={subComment}
                  focusInput={focusInput}
                  inputRef={inputRef}
                  key={index}
                  commenter={subComment.commenterDTOResponse}
                  content={subComment.content}
                  time={subComment.createdAt}
                  amMine={
                    comment.commentDTOResponse.commentId ===
                    subComment.commentRootId
                  }
                  // subComment={
                  //   material?.commentDetailDTOResponses.numberOfReplyComments === 0 ? "" :

                  // }
                  isTextShowSubComment="unShow"
                />
              ))}
            />
          ))
        }
        <CenterFlexBox>
                <BeatLoader
              color="#198754"
              size={5}
              style={{ marginLeft: "5px", marginTop: "5px",marginBottom:"10px" }}
            />
            </CenterFlexBox>
          </>
          :comments?.map((comment, index) => (
            <CommentUser
              // isF5={isF5}
              isSubComment={true}
              setMoreComment={setMoreComment}
              comment={comment}
              focusInput={focusInput}
              inputRef={inputRef}
              key={index}
              commenter={comment.commentDTOResponse.commenterDTOResponse}
              content={comment.commentDTOResponse.content}
              time={comment.commentDTOResponse.createdAt}
              isSHowMore={moreComment}
              subLength={comment.numberOfReplyComments}
              amMine={
                moreComment
                  ? comment.commentDTOResponse.commentId ===
                    moreComment[0]?.commentRootId
                  : false
              }
              isTextShowSubComment={moreComment ? "unShow" : "show"}
              subComment={moreComment?.map((subComment, index) => (
                <CommentUser
                  // isF5={isF5}
                  isSubComment={false}
                  setMoreComment={setMoreComment}
                  comment={subComment}
                  focusInput={focusInput}
                  inputRef={inputRef}
                  key={index}
                  commenter={subComment.commenterDTOResponse}
                  content={subComment.content}
                  time={subComment.createdAt}
                  amMine={
                    comment.commentDTOResponse.commentId ===
                    subComment.commentRootId
                  }

                  isTextShowSubComment="unShow"
                />
              ))}
            />
          ))
        
        }
    </>
  );
};
const CommentUser = ({
  isF5,
  commenter,
  content,
  time,
  subComment,
  subLength,
  focusInput,
  comment,
  setMoreComment,
  amMine,
  isTextShowSubComment,
  isSubComment,
  inputRef,
}) => {
  const { user, isAuthenticated } = useAuthContext();
  const reloading = useSelector((state) => state.reloading.isReLoad);

  const formattedDate = format(new Date(time), "dd/MM/yyyy HH:mm");
  const dispatch = useDispatch();
  const handleReply = (replyUser) => {
    dispatch(setInfoUserReply(replyUser));
    inputRef.current?.focus();
  };
  const handleMediaComment = () => {
    inputRef.current?.focus();
  };

  const [isLoadingDelete, setIsLoadingDelete] = useState(false);

  const [isLoading, setIsLoading] = useState(false);
  const [viewingCommentId, setViewingCommentId] = useState("");
  useEffect(() => {
    // console.log("viewingCommentId",viewingCommentId)
    if (viewingCommentId !== "") {
      handleShowMoreComment(viewingCommentId);
    }
  }, [reloading]);
  const handleShowMoreComment = async (id) => {
    setViewingCommentId(id);
    setIsLoading(true);
    const res = await showMoreComment(id);
    if (res.status === 200) {
      setMoreComment(res.data);
    }
    setIsLoading(false);
  };

  const handleDeleteComment = async (commentId) => {
    setIsLoadingDelete(true);
    const res = await deleteComment(commentId);
    if (res.status === 200) {
      dispatch(setIsReload(!reloading));
    }
    setIsLoadingDelete(false);
  };
  return (
    <Box
      sx={{
        width: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "flex-start",
        margin: "10px 0",
      }}
    >
      <Box sx={{ width: "50px" }}>
        <Avatar src={commenter.avatar} />{" "}
      </Box>
      <Box sx={{ width: "80%" }}>
        <Box
          sx={{
            borderRadius: "15px",
            backgroundColor: "#F2F2F2",
            wordWrap: "break-word",
            padding: "5px 10px",
          }}
        >
          <Box sx={{ fontWeight: 500 }}>
            {commenter.firstname + " " + commenter.lastname}
          </Box>
          {content}
        </Box>
        <Box
          sx={{
            padding: "5px 10px",
            fontSize: "10px",
            color: "#050505",
            fontWeight: 500,
            display: "flex",
            justifyContent: "start",
          }}
        >
          {commenter.id === user?.id &&
            (isLoadingDelete ? (
              <BeatLoader
                color="#198754"
                size={5}
                style={{ marginLeft: "5px", marginTop: "5px" }}
              />
            ) : (
              <ButtonAction
                style={{
                  backgroundColor: "inherit",
                  width: "20px",
                  height: "auto",
                }}
                onClick={async () =>
                  handleDeleteComment(
                    comment?.commentDTOResponse?.commentId
                      ? comment?.commentDTOResponse?.commentId
                      : comment?.commentId
                  )
                }
              >
                Xóa
              </ButtonAction>
            ))}
          {commenter.id !== user?.id && (
            <FullReportDialog
              title="Báo cáo bình luận"
              label="Nội dung báo cáo"
              id={
                comment?.commentDTOResponse?.commentId
                  ? comment?.commentDTOResponse?.commentId
                  : comment?.commentId
              }
            >
              <ButtonAction
                style={{
                  backgroundColor: "inherit",
                  width: "50px",
                  height: "auto",
                }}
              >
                <Box sx={{ cursor: "pointer" }}>Báo cáo</Box>
              </ButtonAction>
            </FullReportDialog>
          )}
          {isSubComment && (
            <ButtonAction
              style={{
                backgroundColor: "inherit",
                width: "50px",
                height: "auto",
              }}
              onClick={() => handleReply(comment)}
            >
              Phản hồi
            </ButtonAction>
          )}

          <Box sx={{ color: "gray", ml: "5px" }}>{formattedDate}</Box>
        </Box>
        {subLength !== 0 &&
          (isLoading ? (
            <BeatLoader
              color="#198754"
              size={5}
              style={{ marginLeft: "5px", marginTop: "5px" }}
            />
          ) : (
            (isTextShowSubComment === "show" || !amMine) && (
              <Box
                onClick={() =>
                  handleShowMoreComment(comment.commentDTOResponse.commentId)
                }
                sx={{ fontWeight: "bold", fontSize: "10px", cursor: "pointer" }}
              >
                <Box sx={{ display: "flex" }}>
                  <LuCornerDownRight />
                  Xem {subLength} phản hồi khác
                </Box>
              </Box>
            )
          ))}
        {amMine && subComment}
      </Box>
    </Box>
  );
};
