"use client";
import * as React from "react";
import styled from "styled-components";



export const HeaderLeftLeft = styled("div")(({ theme }) => ({
  width: "70%",
  height: "auto",
  float: "left",
  backgroundColor: "none",
  // color: "white",
  borderRadius: "20px",
}));
export const HeaderRightLeft = styled("div")(({ theme }) => ({
  // width: "20%",
  float: "right",
  fontSize: "26px",
  justifyContent: "center",
  alignItems: "center",
  cursor: "pointer",
  zIndex: 1000,
  padding:"10px",
  color: "#198754",
}));

export const StyleLeftInfoDiv = styled("div")(({ theme }) => ({
  width:"380px",
  height:"850px",
  overflowX:"auto",
  paddingRight:"20px",
  
}));

export const ActionLabel = styled("label")(({ theme }) => ({
  width: "80px",
  float: "right",
  fontSize: "16px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  cursor: "pointer",
  background: "inherit",
  color: "black",
  borderRadius: "15px",
  padding: "5px",
  "&:hover": {
    backgroundColor: "#198754",
    color: "white",
  },
  "&:focus": {
    fontWeight: "none",
  },
}));
export const ActionButton = styled("div")(({ theme }) => ({
  width: "fit-content",
  fontSize: "16px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  cursor: "pointer",
  background: "inherit",
  color: "black",
  borderRadius: "15px",
  padding: "5px",
  transition: "transform 0.5s ease",
  "&:hover": {
    backgroundColor: "#F2F2F2",
  },
  "&:active": {
    backgroundColor: "#F2F2F2",
    color: "red",
    fontSize: "20px",
    transition: "scale(1.1)",
  },
}));
export const ActionGreenButton = styled("div")(({ theme }) => ({
  width: "fit-content",
  fontSize: "16px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  cursor: "pointer",
  background: "inherit",
  color: "white",
  borderRadius: "15px",
  padding: "5px",
  transition: "transform 0.5s ease",
  backgroundColor: "#197854",
  "&:hover": {
    backgroundColor: "#008000",
  },
  "&:active": {
    backgroundColor: "#F2F2F2",
    color: "red",
    fontSize: "20px",
    transition: "scale(1.1)",
  },
}));
export const MdActionButton = styled("div")(({ theme }) => ({
  width: "80px",
  height: "40px",
  fontSize: "16px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  cursor: "pointer",
  background: "inherit",
  color: "black",
  borderRadius: "15px",
  padding: "5px",
  transition: "transform 0.5s ease",
  marginRight: "5px",
  "&:hover": {
    backgroundColor: "#F2F2F2",
  },
  "&:active": {
    backgroundColor: "#F2F2F2",
    color: "red",
    fontSize: "20px",
    transition: "scale(1.1)",
  },
}));
export const HeaderInfo = styled("div")(({ theme }) => ({
  fontSize: "16px",
  color: "gray",
  marginBottom: "10px",
  // fontWeight:"bold",
}));

export const InputCmt = styled("input")(({ theme }) => ({
  width: "100%",
  height: "40px",
  borderRadius: "15px",
  padding: "0 15px",
  backgroundColor: "#F2F2F2",
}));
export const SelectAction = styled.input.attrs({ type: "radio" })`
  color: 'greenyellow !important';
    cursor: 'pointer';
    width: '100%';
    visibility: hidden;
    position:absolute;
  &:checked + .title-action{
    background: '#14606C',
    color: white !important;
    font-weight: bold;
  }
`;

export const GroupLike = styled("div")(({ theme }) => ({
  display: "flex",
  width: "100%",
  height: "35x",
  margin: "0",
  justifyContent: "space-around",
  alignItems: "center",
}));


