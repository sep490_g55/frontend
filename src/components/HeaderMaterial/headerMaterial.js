import React from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import {HeaderMaterialDiv,LabelHeaderMaterial,SelectMaterial,LiMaterial,UlMaterial,NameMaterial} from './styles'
import { Box } from '@mui/material';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setPageIndex, setType, setViewLesson } from '@/redux/slice/NewMaterialSlice';
const  HeaderMaterial = ({listTitle}) => {
  const newMaterial = useSelector(state => state.newMaterial.material)

    const [isSelected, setIsSelected] = useState(newMaterial?.type)
  const dispatch = useDispatch();
  const handleSetTypeTab= (value) =>{
      dispatch(setType(value))
      setIsSelected(value);
      dispatch(setPageIndex(1))
    }
    const divListTitle = listTitle.map((title,index) =>
        
             <LiMaterial key={index} style={{backgroundColor:isSelected === title.id? "#197854":"white"}}>
                        <LabelHeaderMaterial onClick={() =>handleSetTypeTab(title.id)}>
                            <SelectMaterial type="radio" name="tile" />
                            <NameMaterial  style={{color:isSelected === title.id? "white":"black",}} className='title-material'>{title.name}</NameMaterial>
                        </LabelHeaderMaterial>
                    </LiMaterial>
        )
  return (
    <HeaderMaterialDiv className='header-material'>
        <UlMaterial className='row'>
        {divListTitle}</UlMaterial>
    </HeaderMaterialDiv>
  )
}
export default HeaderMaterial


