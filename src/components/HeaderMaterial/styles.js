import * as React from 'react';
import styled from 'styled-components';

import Button from '@mui/material/Button';
import bgsearch from '@/assets/bgsearch2.jpg'
export const HeaderMaterialDiv = styled('div')(({ theme }) => ({
    float:'right',
    width: '100%',
    height: '44px',
    // backgroundColor: '#198754',
    // borderTopRightRadius: '15px',
    // borderTopLeftRadius: '15px',
    // border: '2px solid #198754',
    paddingRight: '22px',
    color:"white",
  }));
  export const UlMaterial = styled('ul')(({ theme }) => ({
    listStyle:'none',
    height: '44px',
    // borderBottom:"2px solid #198754",
// padding:0,
  }));
  export const LiMaterial = styled('li')(({ theme }) => ({
    float: 'left',
    width: '20%',
    // height: '100%',
    textAlign: 'center',
    color:"black",
    borderTopLeftRadius:"8px",
    borderTopRightRadius:"8px",
    borderBottom:"2px solid #198754",

    
  }));
  export const LabelHeaderMaterial = styled('label')(({ theme }) => ({
    width: '100%',
    height: '100%',
    float: 'left',
    textAlign: 'center'
  }));

  export const NameMaterial = styled('div')(() => ({
    float: 'left',
    height: '44px',
    textAlign: 'center',
    color: 'black',
    cursor: 'pointer',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems:'center',
    "&:hover":{
    // backgroundColor: "#93AB93",
    // fontWeight: 'bold',

    }
  }));

  export const SelectMaterial = styled.input.attrs({ type: 'radio' })`
    cursor: 'pointer';
    width: '100%';
    visibility: hidden;
    position:absolute;
  &:checked + .title-material{
    // width: 100%;
    // height: '100%';
    // color: white !important;
    // font-weight: bold;
    // border-bottom: 3px solid white;
  }
`;

