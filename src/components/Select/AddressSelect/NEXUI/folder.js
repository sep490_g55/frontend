import React from "react";
import {Select, SelectItem} from "@nextui-org/react";
import { getMaterials } from "@/utils/folderSrc";
import { useState } from "react";
import { useEffect } from "react";
import { BeatLoader } from "react-spinners";
import { getALLBookSeriesByClassID, getALLBookVolumeBySubjectBookSeriesID, getALLChapterByBookVolumeID, getALLLessonByChapterID, getALLSubject, getALLSubjectBookSeriesByBookSeriesID, getAllClass, getBookSeries } from "@/dataProvider/agent";

export const MUIFolderSelect  = ({isLoading,label,list,handleChange,value}) => {
  //.log(label+"",list)
  return (
    
      <Select 
        label={label}
        // className="max-w-xs" 
        labelPlacement="outside-left"
        onChange={(e) => handleChange(e.target.value)}
        value={value}
        style={{width:"100%"}}
      >
     
        {isLoading ? <SelectItem >
            Đang tải <BeatLoader
          color="#198754"
          size={5}
          style={{marginLeft:"5px",marginTop:"5px"}}
        />
          </SelectItem> :
          list.length === 0 ?
          <SelectItem title="Không tìm thấy ">
            Không tìm thấy {label}
          </SelectItem>:
        list.map((item) => (
          <SelectItem key={item.id} value={item.id} title={item.name}>
            {item.name}
          </SelectItem>
        ))}
      </Select>
      
   
  );
}

export const MUIClassSelect  = ({handleChange,value}) => {
    const [classList, setClassList] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    
  
    useEffect(() => {
        getClasses()
      //.log("hêll");
    }, [])
    const getClasses = async() =>{
      setIsLoading(true)

      const api ="class/list"
      const res = await getAllClass()
      if(res){

        try{
            setClassList(res.data);
        //.log("res: ",res)

        }catch(e){
        //.log("Error: ",e)

        }
      }else{
        //.log("Khong tim thay classes")
      }
      setIsLoading(false)
    }
    return (
      
        <>
        {isLoading?"loading ...":<MUIFolderSelect 
          label="Lớp"
          list={classList}
          handleChange={handleChange}
          isLoading={isLoading}
          value={value}

        />                
    }</>
    );
  }

  export const MUIBookSeriesSelect  = ({classID,handleChange,value}) => {
    const [bookSeriesList, setBookSeriesList] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    
  
    useEffect(() => {
      getBookSeriesList()
    }, [classID])
    const getBookSeriesList = async() =>{
      setIsLoading(true)

      //.log("classID",classID);
      const api =`book-series/list-by-class?classId=${classID}`
      const res = await getALLBookSeriesByClassID(classID)
      if(res.status === 200){

        try{
          setBookSeriesList(res.data);
        //.log("res: ",res)

        }catch(e){
        //.log("Error: ",e)

        }
      }else{
        //.log("Khong tim thay bo sach")
      }
      setIsLoading(false)

    }
    return (
      
       <>{isLoading ? "":
        <MUIFolderSelect 
          label="Bộ sách"
          list={bookSeriesList}
          handleChange={handleChange}
          isLoading={isLoading}
          value={value}

        /> 
      }</>
    );
  }
  export const MUISubjectSelect  = ({bookSeriesID,handleChange,value}) => {
    const [subjectList, setSubjectList] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    
  
    useEffect(() => {
      getSubjectList()
    }, [])
    const getSubjectList = async() =>{
      setIsLoading(true)

      const res = await getALLSubject()
      if(res.status === 200){

        try{
          setSubjectList(res.data);
        //.log("res: ",res)

        }catch(e){
        //.log("Error: ",e)

        }
      }else{
        //.log("Khong tim thay  mon hoc")
      }
      setIsLoading(false)

    }
    return (
      <>{isLoading ? "":
      
        <MUIFolderSelect 
          label="Môn học"
          list={subjectList}
          handleChange={handleChange}
          isLoading={isLoading}
          value={value}

        />                
    }</>              
     
    );
  }
  export const MUISubjectBookSeriesSelect  = ({bookSeriesID,handleChange,value}) => {
    const [subjectList, setSubjectList] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    
  
    useEffect(() => {
      getSubjectList()
    }, [bookSeriesID])
    const getSubjectList = async() =>{
      setIsLoading(true)

      const api =`subject/list-by-book-series?bookSeriesId=${bookSeriesID}`
      const res = await getALLSubjectBookSeriesByBookSeriesID(bookSeriesID)
      if(res.status === 200){

        try{
          setSubjectList(res.data);
        //.log("res: ",res)

        }catch(e){
        //.log("Error: ",e)

        }
      }else{
        //.log("Khong tim thay  mon hoc")
      }
      setIsLoading(false)

    }
    return (
      <>{isLoading ? "":
      
        <MUIFolderSelect 
          label="Môn học"
          list={subjectList}
          handleChange={handleChange}
          isLoading={isLoading}
          value={value}

        />                
    }</>              
     
    );
  }

  export const MUIBookVolumeSelect  = ({subjectID,handleChange,value}) => {
    const [bookVolumeList, setBookVolumeList] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    
  
    useEffect(() => {
      getBookVolumeList()
    }, [subjectID])
    const getBookVolumeList = async() =>{
      setIsLoading(true)

      const api =`book-volume/list-by-book-series-subject?bookSeriesSubjectId=${subjectID}`
      const res = await getALLBookVolumeBySubjectBookSeriesID(subjectID)
      if(res.status === 200){

        try{
          setBookVolumeList(res.data);
        //.log("res: ",res)

        }catch(e){
        //.log("Error: ",e)

        }
      }else{
        //.log("Khong tim thay  dau sach")
      }
      setIsLoading(false)

    }
    return (
      <>{isLoading ? "":
      
        <MUIFolderSelect 
          label="Đầu sách"
          list={bookVolumeList}
          handleChange={handleChange}
          isLoading={isLoading}
          value={value}

        />                
    }</>              
     
    );
  }

  export const MUIChapterSelect  = ({bookVolumeID,handleChange,value}) => {
    const [chapterList, setChapterList] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    
  
    useEffect(() => {
      getChapterList()
    }, [bookVolumeID])
    const getChapterList = async() =>{
      setIsLoading(true)

      const api =`chapter/list-by-book-volume?bookVolumeId=${bookVolumeID}`
      const res = await getALLChapterByBookVolumeID(bookVolumeID)
      if(res.status === 200){

        try{
          setChapterList(res.data);
        //.log("res: ",res)

        }catch(e){
        //.log("Error: ",e)

        }
      }else{
        //.log("Khong tim thay  chuuong")
      }
      setIsLoading(false)

    }
    return (
      <>{isLoading ? "":
      
        <MUIFolderSelect 
          label="Chương"
          list={chapterList}
          handleChange={handleChange}
          isLoading={isLoading}
          value={value}

        />                
    }</>              
     
    );
  }

  export const MUILessonSelect  = ({chapterID,handleChange,value}) => {
    const [lessonList, setLessonList] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    
  
    useEffect(() => {
      getLessonList()
    }, [chapterID])
    const getLessonList = async() =>{
      setIsLoading(true)

      const api =`lesson/list-by-chapter?chapterId=${chapterID}`
      const res = await getALLLessonByChapterID(chapterID)
      if(res.status === 200){

        try{
          setLessonList(res.data);
        //.log("res: ",res)

        }catch(e){
        //.log("Error: ",e)

        }
      }else{
        //.log("Khong tim thay bai học")
      }
      setIsLoading(false)
    }
    return (
      <>{isLoading ? "":
      
        <MUIFolderSelect 
          label="Bài học"
          list={lessonList}
          handleChange={handleChange}
          isLoading={isLoading}
          value={value}
        />  
    }</>              
     
    );
  }