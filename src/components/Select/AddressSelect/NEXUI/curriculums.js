import React from "react";
import {Select, SelectItem} from "@nextui-org/react";
import { getMaterials } from "@/utils/folderSrc";
import { useState } from "react";
import { useEffect } from "react";
import { BeatLoader } from "react-spinners";
import { getALLBookSeriesByClassID, getALLBookVolumeBySubjectBookSeriesID, getALLChapterByBookVolumeID, getALLLessonByChapterID, getALLSubject, getALLSubjectBookSeriesByBookSeriesID, getAllClass, getBookSeries } from "@/dataProvider/agent";
import { setViewBookSeries,setViewSubject, setViewBookVolume, setViewClass, setViewLesson, setViewChapter } from '@/redux/slice/NewMaterialSlice';
import { useDispatch, useSelector } from 'react-redux';

export const MUIFolderSelect  = ({isLoading,label,list,handleChange,value}) => {
  //.log(label+"",list)
  const handleOnSelect = (index)=>{
    handleChange(list[index])
  }
  
  const text = label.toLowerCase();;
  const placeholder = "Chọn "+text;
  return (
      <Select 
        label={label}
        placeholder={placeholder}
        labelPlacement="outside"
        onChange={(e) => handleOnSelect(e.target.value)}
        style={{width:"190px"}}
      >
        {
          list?.map((item,index) => (
          <SelectItem key={index} value={item} title={item.name}>
            {item.name}
          </SelectItem>
        ))}
      </Select>
      
   
  );
}

export const MUIClassSelect  = () => {
    const [classList, setClassList] = useState([])
    const [isLoading, setIsLoading] = useState(true)
const dispatch = useDispatch();
const newMaterial = useSelector(state => state.newMaterial.material)

    const handleChange =  (value)=>{
      dispatch(setViewClass({
              
                id:value?.id,
                name:value?.name
              }))
              dispatch(setViewBookSeries({
              
                id:"",
                name:""
              }))
              dispatch(setViewSubject({
              
                id:"",
                name:""
              }))
              dispatch(setViewBookVolume({
              
                id:"",
                name:""
              }))
              dispatch(setViewChapter({
              
                id:"",
                name:""
              }))
              dispatch(setViewLesson({
              
                id:"",
                name:""
              }))
  }
  
    useEffect(() => {
        getClasses()
      //.log("hêll");
    }, [])
    const getClasses = async() =>{
      setIsLoading(true)

      const api ="class/list"
      const res = await getAllClass()
      if(res){

        try{
            setClassList(res.data);
        //.log("res: ",res)

        }catch(e){
        //.log("Error: ",e)

        }
      }else{
        //.log("Khong tim thay classes")
      }
      setIsLoading(false)
    }
    return (
      
        <>
        {<MUIFolderSelect 
          label="Lớp"
          list={classList}
          handleChange={handleChange}
          isLoading={isLoading}

        />                
    }</>
    );
  }

  export const MUIBookSeriesSelect  = () => {
    const [bookSeriesList, setBookSeriesList] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const dispatch = useDispatch();
    const newMaterial = useSelector(state => state.newMaterial.material)

    const handleChange =  (value)=>{
     
              dispatch(setViewBookSeries({
              
                
                id:value?.id,
                name:value?.name
              }))
              dispatch(setViewSubject({
              
                id:"",
                name:""
              }))
              dispatch(setViewBookVolume({
              
                id:"",
                name:""
              }))
              dispatch(setViewChapter({
              
                id:"",
                name:""
              }))
              dispatch(setViewLesson({
              
                id:"",
                name:""
              }))
  }
  
    useEffect(() => {
      getBookSeriesList()
    }, [newMaterial?.class?.id])
    const getBookSeriesList = async() =>{
      setIsLoading(true)
  
      //.log("classID",classID);
      const api =`book-series/list-by-class?classId=${newMaterial?.class?.id}`
      const res = await getALLBookSeriesByClassID(newMaterial?.class?.id)
      if(res.status === 200){

        try{
          setBookSeriesList(res.data);
        //.log("res: ",res)

        }catch(e){
        //.log("Error: ",e)

        }
      }else{
        //.log("Khong tim thay bo sach")
      }
      setIsLoading(false)

    }
    return (
      
       <>{
        <MUIFolderSelect 
          label="Bộ sách"
          list={bookSeriesList}
          handleChange={handleChange}
          isLoading={isLoading}

        /> 
      }</>
    );
  }
  export const MUISubjectSelect  = () => {
    const [subjectList, setSubjectList] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const dispatch = useDispatch();
  const newMaterial = useSelector(state => state.newMaterial.material)
  const handleChange =  (value)=>{

    dispatch(setViewSubject({
    
      id:value?.id,
      name:value?.name
    }))
    dispatch(setViewBookVolume({
    
      id:"",
      name:""
    }))
    dispatch(setViewChapter({
    
      id:"",
      name:""
    }))
    dispatch(setViewLesson({
    
      id:"",
      name:""
    }))
    }
    useEffect(() => {
      getSubjectList()
    }, [])
    const getSubjectList = async() =>{
      setIsLoading(true)

      const res = await getALLSubject()
      if(res.status === 200){

        try{
          setSubjectList(res.data);
        //.log("res: ",res)

        }catch(e){
        //.log("Error: ",e)

        }
      }else{
        //.log("Khong tim thay  mon hoc")
      }
      setIsLoading(false)

    }
    return (
      <>{
      
        <MUIFolderSelect 
          label="Môn học"
          list={subjectList}
          handleChange={handleChange}
          isLoading={isLoading}

        />                
    }</>              
     
    );
  }
  export const MUISubjectBookSeriesSelect  = () => {
    const [subjectList, setSubjectList] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    
    const dispatch = useDispatch();
  const newMaterial = useSelector(state => state.newMaterial.material)
  const handleChange =  (value)=>{
  
    dispatch(setViewSubject({
    
      id:value?.id,
      name:value?.name
    }))
    
    dispatch(setViewBookVolume({
    
      id:"",
      name:""
    }))
    dispatch(setViewChapter({
    
      id:"",
      name:""
    }))
    dispatch(setViewLesson({
    
      id:"",
      name:""
    }))
  }
    useEffect(() => {
      getSubjectList()
    }, [newMaterial?.bookSeries?.id])
    const getSubjectList = async() =>{
      setIsLoading(true)

      const api =`subject/list-by-book-series?bookSeriesId=${newMaterial?.bookSeries?.id}`
      const res = await getALLSubjectBookSeriesByBookSeriesID(newMaterial?.bookSeries?.id)
      if(res.status === 200){

        try{
          setSubjectList(res.data);
        //.log("res: ",res)

        }catch(e){
        //.log("Error: ",e)

        }
      }else{
        //.log("Khong tim thay  mon hoc")
      }
      setIsLoading(false)

    }
    return (
      <>{
      
        <MUIFolderSelect 
          label="Môn học"
          list={subjectList}
          handleChange={handleChange}
          isLoading={isLoading}

        />                
    }</>              
     
    );
  }

  export const MUIBookVolumeSelect  = () => {
    const [bookVolumeList, setBookVolumeList] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    
    const dispatch = useDispatch();
  const newMaterial = useSelector(state => state.newMaterial.material)
  const handleChange =  (value)=>{
  
    dispatch(setViewBookVolume({
    
      id:value?.id,
      name:value?.name
    }))
    dispatch(setViewChapter({
    
      id:"",
      name:""
    }))
    dispatch(setViewLesson({
    
      id:"",
      name:""
    }))
  }
    useEffect(() => {
      getBookVolumeList()
    }, [newMaterial?.subject?.id])
    const getBookVolumeList = async() =>{
      setIsLoading(true)

      const api =`book-volume/list-by-book-series-subject?bookSeriesSubjectId=${newMaterial?.subject?.id}`
      const res = await getALLBookVolumeBySubjectBookSeriesID(newMaterial?.subject?.id)
      if(res.status === 200){

        try{
          setBookVolumeList(res.data);
        //.log("res: ",res)

        }catch(e){
        //.log("Error: ",e)

        }
      }else{
        //.log("Khong tim thay  dau sach")
      }
      setIsLoading(false)

    }
    return (
      <>{
      
        <MUIFolderSelect 
          label="Đầu sách"
          list={bookVolumeList}
          handleChange={handleChange}
          isLoading={isLoading}

        />                
    }</>              
     
    );
  }

  export const MUIChapterSelect  = () => {
    const [chapterList, setChapterList] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    
  
    const dispatch = useDispatch();
  const newMaterial = useSelector(state => state.newMaterial.material)
  const handleChange =  (value)=>{
   
    dispatch(setViewChapter({
    
     
      id:value?.id,
      name:value?.name
    }))
    dispatch(setViewLesson({
    
      id:"",
      name:""
    }))
  }
    useEffect(() => {
      getChapterList()
    }, [newMaterial?.bookVolume?.id])
    const getChapterList = async() =>{
      setIsLoading(true)

      const api =`chapter/list-by-book-volume?bookVolumeId=${newMaterial?.bookVolume?.id}`
      const res = await getALLChapterByBookVolumeID(newMaterial?.bookVolume?.id)
      if(res.status === 200){

        try{
          setChapterList(res.data);
        //.log("res: ",res)

        }catch(e){
        //.log("Error: ",e)

        }
      }else{
        //.log("Khong tim thay  chuuong")
      }
      setIsLoading(false)

    }
    return (
      <>{
      
        <MUIFolderSelect 
          label="Chương"
          list={chapterList}
          handleChange={handleChange}
          isLoading={isLoading}

        />                
    }</>              
     
    );
  }

  export const MUILessonSelect  = () => {
    const [lessonList, setLessonList] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    
    const handleChange =  (value)=>{
  
    const dispatch = useDispatch();
  const newMaterial = useSelector(state => state.newMaterial.material)
    
    dispatch(setViewLesson({
    
      id:value?.id,
      name:value?.name
    }))
    useEffect(() => {
      getLessonList()
    }, [newMaterial?.chapter?.id])
    const getLessonList = async() =>{
      setIsLoading(true)

      const api =`lesson/list-by-chapter?chapterId=${newMaterial?.chapter?.id}`
      const res = await getALLLessonByChapterID(newMaterial?.chapter?.id)
      if(res.status === 200){

        try{
          setLessonList(res.data);
        //.log("res: ",res)

        }catch(e){
        //.log("Error: ",e)

        }
      }else{
        //.log("Khong tim thay bai học")
      }
      setIsLoading(false)
    }
  }
    return (
      <>{
      
        <MUIFolderSelect 
          label="Bài học"
          list={lessonList}
          handleChange={handleChange}
          isLoading={isLoading}
        />  
    }</>              
     
    );
  }