import React, { useEffect, useState } from "react";
import { Select, SelectItem } from "@nextui-org/react";
import { getAddress } from "@/utils/addressApi";

export const NextUIProvinceSelect = ({ setProvinceID }) => {
  const handleChange = (event) => {
    setProvinceID(event.target.value);
  };

  useEffect(() => {
    getProvinces();
  }, []);
  const [provinces, setProvinces] = useState([]);
  const getProvinces = async () => {
    const api = "?depth=1";

    try {
      const res = await getAddress(api);

      if (res) {
        setProvinces(res);
      } else {
        console.log(`setProvinces not found`);
      }
    } catch (error) {
      console.log(`Can not get setProvinces ${error}`);
    }
  };
  return (
    <div className="flex w-full flex-wrap md:flex-nowrap gap-4">
      <Select
        //   isRequired
        label="Tỉnh/TP"
        placeholder="Chọn tỉnh/tp"
        //   defaultSelectedKeys={["cat"]}
        className="max-w-xs"
        onChange={handleChange}
      >
        {provinces.map((province, index) => (
          <SelectItem key={province.code} value={province.code}>
            {province.name}
          </SelectItem>
        ))}
      </Select>
    </div>
  );
};


export const NextUIDistrictSelect = ({districtID,setDistrictID,provinceID}) =>{
  
    const handleChange = (event) => {
        setDistrictID(event.target.value);
    };
  
    useEffect(() => {
        getProvince();
  
    }, [provinceID]);
    const [province, setProvince] = useState({});

    const getProvince = async () => {
      const api = provinceID === "" ? "d/" : `p/${provinceID}?depth=2`
        
      try {
        const res = await getAddress(api);
  
        if (res) {
            // console.log("res: ",res);
            setProvince(res);
          
        } else {
          console.log(`province not found`);
        }
      } catch (error) {
        console.log(`Can not get province ${error}`);
      }
    };
    
    return (
        <div className="flex w-full flex-wrap md:flex-nowrap gap-4">
        <Select
          //   isRequired
          label="Quận/huyện"
          placeholder="Chọn quận/huyện"
          //   defaultSelectedKeys={["cat"]}
          className="max-w-xs"
        >
          
           {
                province.districts === undefined ? "" : province.districts.map((district,index) => <SelectItem key={index} value={district.code}>
                {district.name}
              </SelectItem> )

                
            }
        </Select>
      </div>
    );
  }