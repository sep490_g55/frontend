import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import { getAddress } from "@/utils/addressApi";
function findIndexes(list, value) {
    
  return list.findIndex(item => item.code === value?.code ||item.name === value?.name);

} 
export const MUIProvinceSelect = ({register,errors, province,setProvinceID,setProvince,disabled }) => {
  console.log("province",province)
  const [provinceOBJ, setProvinceOBJ] = useState({
    "name": province ,
    "code": 0,
    "division_type": "",
    "codename": "",
    "phone_code": 0,
    "districts": []
});
  const handleChange = (value) => {
    try {
      setProvinceID(value.code);
      setProvince&&setProvince(value.name);
      setProvinceOBJ(value)
    } catch (e) {
      setProvinceID("");
    }
  };

  useEffect(() => {
    getProvinces();
  }, []);
  const [provinces, setProvinces] = useState([]);
  const getProvinces = async () => {
    const api = "?depth=1";

    try {
      const res = await getAddress(api);

      if (res) {
        setProvinces(res.data);
      } else {
        console.log(`setProvinces not found`);
      }
    } catch (error) {
      console.log(`Can not get setProvinces ${error}`);
    }
  };
  return (
    <><Autocomplete
      disabled={disabled}
        {...register("province")}
      defaultValue={provinceOBJ}
        value={provinceOBJ[findIndexes(provinces,provinceOBJ)]?.name}
        id="country-select-demo"
        sx={{ width: "100%",display:"flex",justifyContent:"center",alignItems:"flex-start",margin: "2px", }}
        options={provinces}
        autoHighlight
        getOptionLabel={(option) => option.name}
        renderOption={(props, option) => (
          <Box
            component="li"
            sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
            {...props}
          >
            {option.name}
          </Box>
        )}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Tỉnh/Thành phố"
            inputProps={{
              ...params.inputProps,
              autoComplete: "new-password", // disable autocomplete and autofill
            }}
          />
        )}
        onChange={(event, value) => handleChange(value)}
      /></>
  );
};

export const MUIDistrictSelect = ({
  district,
  setDistrictID,
  provinceID,
  register,errors,
  setDistrict,
  disabled

}) => {
  const [districtOBJ, setDistrictOBJ] = useState({
    "name":district?district:"Chọn quận/huyện",
    "code": 0,
    "division_type": "",
    "codename": "",
    "province_code": 0,
    "wards": []
});

  const handleChange = (value) => {
    setDistrict&&setDistrict(value.name);
    setDistrictOBJ(value)
    setDistrictID(value.code)
  };

  useEffect(() => {
    getProvince();
  }, [provinceID]);
  const [province, setProvince] = useState(null);
  const [districts, setDistricts] = useState([]);

  const getProvince = async () => {
    const api = provinceID === "" ? "d/" : `p/${provinceID}?depth=2`;

    try {
      const res = await getAddress(api);

      if (res) {
        provinceID === "" ? setDistricts(res.data) : setProvince(res.data);
      } else {
        console.log(`district not found`);
      }
    } catch (error) {
      console.log(`Can not get district ${error}`);
    }
  };

  return (
    <Autocomplete
    disabled={disabled}
    {...register("district")}
    defaultValue={districtOBJ}

    value={province === null ? districts[findIndexes(districts,districtOBJ)] :   province.districts[findIndexes( province.districts,districtOBJ)] }
      id="district-select"
      sx={{ width: "100%",display:"flex",justifyContent:"center",alignItems:"flex-start",margin: {xs:"0px", md:"2px"},mb:{xs:"2px",md:"30px"} }}
      options={province === null ? districts : province.districts}
      autoHighlight
      getOptionLabel={(option) => option.name}
      renderOption={(props, option) => (
        <Box
          component="li"
          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
          {...props}
        >
          {option.name}
        </Box>
      )}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Quận/huyện"
          inputProps={{
            ...params.inputProps,
            autoComplete: "new-password", // disable autocomplete and autofill
          }}
        />
      )}
      onChange={(event,value) =>handleChange(value)}
    />
  );
};


export const MUIWardSelect = ({
  ward,
  setWardID,
  districtID,
  register,errors,
  setWard
}) => {
  const [wardOBJ, setWardOBJ] = useState( {
    "name": "Phường Phúc Xá",
    "code": 1,
    "division_type": "phường",
    "codename": "phuong_phuc_xa",
    "district_code": 1
});
  const handleChange = (value) => {
    setWard&&setWard(value.name);
    setWardOBJ(value)
  };

  useEffect(() => {
    getProvince();
  }, [districtID]);
  const [district, setDistrict] = useState(null);
  const [wards, setWards] = useState([]);

  const getProvince = async () => {
    const api = districtID === "" ? "w/" : `d/${districtID}?depth=2`;

    try {
      const res = await getAddress(api);

      if (res) {
        districtID === "" ? setWards(res.data) : setDistrict(res.data);
      } else {
        console.log(`ward not found`);
      }
    } catch (error) {
      console.log(`Can not get ward ${error}`);
    }
  };

  return (
    <Autocomplete
  
    
      id="ward-select"
      sx={{ width: "100%",display:"flex",justifyContent:"center",alignItems:"flex-start",margin: {xs:"0px", md:"2px"},mb:{xs:"2px",md:"30px"} }}
      options={district === null ? wards : district?.wards}
      value={district === null ? wards[findIndexes(wards,wardOBJ)] : district?.wards[findIndexes(district?.wards,wardOBJ)]}
      filterSelectedOptions
      getOptionLabel={(option) => option.name}
      renderOption={(props, option) => (
        <Box
        key={option.code}
          component="li"
          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
          {...props}
        >
          {option.name}
        </Box>
      )}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Phường/xã"
         
        />
      )}
      onChange={(event,value) =>handleChange(value)}
    />
  );
};