import * as React from 'react';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Autocomplete from '@mui/material/Autocomplete';
import { useState } from 'react';
import { useEffect } from 'react';
import { getMaterials } from '@/utils/folderSrc';
import { Box, Button, Grid, Skeleton, Typography } from "@mui/material";
import { getALLChapterByBookVolumeID, getALLLessonByChapterID, getALLSubject, getBookSeriesUPLOAD, getBookVolumeUPLOAD, getClassUPLOAD, getFolderSRC } from '@/dataProvider/agent';
import { useDispatch, useSelector } from 'react-redux';
import { setViewBookSeries, setViewBookVolume, setViewChapter, setViewClass, setViewClassList, setViewLesson, setViewSubject, setViewSubjectList } from '@/redux/slice/NewMaterialSlice';

export const MUIFolderSelect  = ({id,isLoading,label,list,handleChange,value}) => {
  // console.log("value",value);
  const newMaterial = useSelector(state => state.newMaterial.material)
   const[valueFolder,setValueFolder] = useState(value)
  useEffect(()=>{
    if(id === "class" ){
  // console.log("newMaterial class "+": ",newMaterial.class)

      setValueFolder(newMaterial.class)
    }else if(id === "book-series"){
      setValueFolder(newMaterial.bookSeries)
  
    }else if(id === "subject"){
  setValueFolder({id:newMaterial.subject.subjectId,})
  
    }else if(id === "book-volume"){
      // console.log("bookVolume "+": ",newMaterial.bookVolume)
      setValueFolder(newMaterial.bookVolume)
  
    }else if(id === "chapter"){
      setValueFolder(newMaterial.chapter)
  
    }else if(id === "lesson"){
      setValueFolder(newMaterial.lesson)
  
    }
  },[id,newMaterial.class,newMaterial.bookSeries,newMaterial.subject,newMaterial.bookVolume,newMaterial.chapter,newMaterial.lesson])
  // console.log("list",list)
  
  
  // console.log("newMaterial class "+": ",newMaterial.class)

  // console.log("value "+id+": ",value)

    return (
      
      !isLoading && <Autocomplete

      value={list?.length ===0 ?null:valueFolder?.id === ""?null:list[list?.findIndex((item) => item.id === valueFolder ||   item.id === valueFolder?.id||item?.id === valueFolder?.subjectId)]}
      id={id}
      sx={{ width: "100%",display:"flex",justifyContent:"center",alignItems:"center" }}
      options={list}
      autoHighlight
      getOptionLabel={(option) => option.name}
      // isOptionEqualToValue={(option) => option.name}
      renderOption={(props, option) => (
        <Box
          component="li"
          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
          {...props}
          key={option.id}
        >
          {option.name}
        </Box>
      )}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          inputProps={{
            ...params.inputProps,
            autoComplete: "new-password", // disable autocomplete and autofill
          }}
        />
      )}
      onChange={(event,value) =>handleChange(value)}
    />
        
     
    );
  }
    //type === true when select within subjectID,meaning useful for upload material
  //bd
  export const MUIClassSelect  = ({...props}) => {
      const [classList, setClassList] = useState([])
      const [isLoading, setIsLoading] = useState(true)
      const newMaterial = useSelector(state => state.newMaterial.material)

      const [objClass, setClass] = useState(props?.value> 0 ? props?.value : newMaterial?.class)
      const [count, setCount] = useState(0)


      const dispatch = useDispatch();
      const handleChange = (value)=>{
        console.log("value calkss",value)
        setClass(value)
        if(value !== undefined || value !== null || value !== ""){
        dispatch(setViewClass({id:value?.id=== undefined ? "" : value?.id,name:value?.name=== undefined ? "": value?.name}))
        dispatch(setViewBookSeries({id:"",name:""}))
        dispatch(setViewBookVolume({id:"",name:""}))
        dispatch(setViewChapter({id:"",name:""}))
        dispatch(setViewLesson({id:"",name:""}))
      }
      }
      
       useEffect(() => {
        
        getClasses()
      }, [newMaterial?.subject?.subjectId,])
      const getClasses = async() =>{
        !isLoading &&setIsLoading(true)

        // const api ="class/list"
        const res = await getClassUPLOAD(newMaterial?.subject?.subjectId)
        if(res.status  === 200){
  
          try{
              setClassList(res.data);
          // console.log("res class: ",res)
          dispatch(setViewClassList(res.data))
         
          }catch(e){
          console.log("Error: ",e)
  
          }
        }else{
          console.log("Khong tim thay classes")
        }
        setIsLoading(false)
      }
      return (
        
          <MUIFolderSelect 
            id="class"
            label="Lớp"
            list={classList}
            handleChange={handleChange}
            isLoading={isLoading}
            value={objClass}
  
          />                
       
      );
    }
  
    //type === true when select within subjectID,meaning useful for upload material
    export const MUIBookSeriesSelect  = ({...props}) => {
      const newMaterial = useSelector(state => state.newMaterial.material)
      const dispatch = useDispatch();
// console.log("props?.value",props?.value)
      const [bookSeriesList, setBookSeriesList] = useState([])
      const [bookSeries, setBookSeries] = useState(props?.value > 0 ? props?.value :newMaterial?.bookSeries)
      const [isLoading, setIsLoading] = useState(true)
      const [count, setCount] = useState(0)

      const handleChange = (value)=>{
        setBookSeries(value)
        if(value !== undefined || value !== null || value !== ""){
          dispatch(setViewBookSeries({id:value?.id=== undefined ? "": value?.id,name:value?.name=== undefined ? "": value?.name}))
    
          dispatch(setViewBookVolume({id:"",name:""}))
          dispatch(setViewChapter({id:"",name:""}))
          dispatch(setViewLesson({id:"",name:""}))
          
      }
      }
      useEffect(() => {
       
        getBookSeriesList()
      }, [newMaterial?.subject?.subjectId,newMaterial?.class?.id])
      const getBookSeriesList = async() =>{
        const res = await getBookSeriesUPLOAD(newMaterial?.class?.id,newMaterial?.subject?.subjectId)
        // console.log("res bo sach: ",res)
        if(res.status  === 200){
  
  
          try{
            setBookSeriesList(res.data);
           
            // setBookSeries(props?.value > 0 ? props?.value :newMaterial?.bookSeries)
          }catch(e){
          console.log("Error: ",e)
  
          }
        }else{
          console.log("Khong tim thay bo sach")
        }
        setIsLoading(false)
  
      }
      return (
        
          <MUIFolderSelect 
          // id={props.id}
          label="Bộ sách"
            list={bookSeriesList}
            handleChange={handleChange}
            isLoading={isLoading}
            value={bookSeries}
            id="book-series"
  
          />                
       
      );
    }
    //type === true when select within subjectID,meaning useful for upload material

    export const MUISubjectSelect  = ({...props}) => {
      const newMaterial = useSelector(state => state.newMaterial.material)
      const dispatch = useDispatch();

      const [subjectList, setSubjectList] = useState([])
      const [isLoading, setIsLoading] = useState(true)
      const [subject, setSubject] = useState(props?.value> 0 ? props?.value :{id:newMaterial?.subject.subjectId,name:newMaterial?.subject.name})
      const [count, setCount] = useState(0)

      const handleChange = (value)=>{
        // console.log("value subject",value)
        setSubject(value)
        if(value !== undefined || value !== null || value !== ""){
          dispatch(setViewSubject({id:"",subjectId:value?.id=== undefined ? "": value?.id,name:value?.name=== undefined ? "": value?.name}))
          dispatch(setViewClass({id:"",name:""}))
          dispatch(setViewBookSeries({id:"",name:""}))
          dispatch(setViewBookVolume({id:"",name:""}))
          dispatch(setViewChapter({id:"",name:""}))
          dispatch(setViewLesson({id:"",name:""}))
         
      }
    }
      
    
      useEffect(() => {
       
        getSubjectList()
      }, [])
      const getSubjectList = async() =>{
        // const api =`subject/list-by-book-series?bookSeriesId=${bookSeriesID}`
        const res = await getALLSubject()
        if(res.status  === 200){
  
          try{
            setSubjectList(res.data);
            dispatch(setViewSubjectList(res.data))
           
          }catch(e){
          console.log("Error: ",e)
  
          }
        }else{
          console.log("Khong tim thay  mon hoc")
        }
        setIsLoading(false)
  
      }
      return (
        
          <MUIFolderSelect 
          // id={props.id}
          label="Môn học"
            list={subjectList}
            handleChange={handleChange}
            isLoading={isLoading}
            value={subject}
            id="subject"
  
          />                
       
      );
    }
  
    //type === true when select within subjectID,meaning useful for upload material

    export const MUIBookVolumeSelect  = ({...props}) => {
      const dispatch = useDispatch();
const newMaterial = useSelector(state => state.newMaterial.material)
const [bookVolumeList, setBookVolumeList] = useState([])
      const [isLoading, setIsLoading] = useState(true)
  // console.log("value bv",props?.value > 0? props?.value :newMaterial?.bookVolume)

const [bookVolume, setBookVolume] = useState(props?.value> 0 ? props?.value :{id:newMaterial?.bookVolume.id,name:newMaterial?.bookVolume.name})
                                            
const [count, setCount] = useState(0)

      // const cloneAPI = type ? api:  api+`?subjectId=${subjectID}`
      
const handleChange = (value)=>{
  setBookVolume(value)
  if(value !== undefined || value !== null || value !== ""){
    dispatch(setViewBookVolume({id:value?.id=== undefined ? "": value?.id,name:value?.name=== undefined ? "": value?.name}))
    dispatch(setViewChapter({id:"",name:""}))
    dispatch(setViewLesson({id:"",name:""}))
  
}
}
    
      useEffect(() => {
      
        getBookVolumeList()
      }, [newMaterial?.subject?.subjectId,newMaterial?.bookSeries?.id])
      const getBookVolumeList = async() =>{


        const res = await getBookVolumeUPLOAD(newMaterial?.bookSeries?.id,newMaterial?.subject?.subjectId)
        if(res.status  === 200){
  
  
          try{
            setBookVolumeList(res.data);
           
          // console.log("res: ",res)
  
          }catch(e){
          console.log("Error: ",e)
  
          }
        }else{
          console.log("Khong tim thay  dau sach")
        }
        setIsLoading(false)
  
      }
      return (
        
          <MUIFolderSelect 
          // id={props.id}
          label="Đầu sách"
            list={bookVolumeList}
            handleChange={handleChange}
            isLoading={isLoading}
            value={bookVolume}
            id="book-volume"
  
          />                
       
      );
    }
    //type === true when select within subjectID,meaning useful for upload material
  
    export const MUIChapterSelect  = ({...props}) => {
      const dispatch = useDispatch();
      const newMaterial = useSelector(state => state.newMaterial.material)
      const [chapterList, setChapterList] = useState([])
      const [isLoading, setIsLoading] = useState(true)
      const [count, setCount] = useState(0)

      const [chapter, setChapter] = useState(props?.value > 0? props?.value :newMaterial?.chapter)
      const handleChange = (value)=>{
        // console.log("value subject",value)
        setChapter(value)
        if(value !== undefined || value !== null || value !== ""){
          dispatch(setViewChapter({id:value?.id=== undefined ? "": value?.id,name:value?.name=== undefined ? "": value?.name}))
          dispatch(setViewLesson({id:"",name:""}))
  
          
      }
    }
      useEffect(() => {
       

        getChapterList("newMaterial?.bookVolume.id",newMaterial?.bookVolume.id)
      }, [newMaterial?.bookVolume.id,])
      const getChapterList = async() =>{
        const res = await getALLChapterByBookVolumeID(newMaterial?.bookVolume.id)
        if(res.status  === 200){
  
          try{
            setChapterList(res.data);
           
          // console.log("res: ",res)
  
          }catch(e){
          console.log("Error: ",e)
  
          }
        }else{
          console.log("Khong tim thay  chuuong")
        }
        setIsLoading(false)
  
      }
      return (
        
          <MUIFolderSelect 
   
          id="chapter"

            label="Chương"
            list={chapterList}
            handleChange={handleChange}
            isLoading={isLoading}
            value={chapter}

          />                
       
      );
    }
    //type === true when select within subjectID,meaning useful for upload material
  
    export const MUILessonSelect  = ({...props}) => {
      const dispatch = useDispatch();
      const newMaterial = useSelector(state => state.newMaterial.material)
      const [count, setCount] = useState(0)

      const [lessonList, setLessonList] = useState([])
      const [isLoading, setIsLoading] = useState(true)
      const [lesson, setLesson] = useState(props?.value > 0 ? props?.value : newMaterial?.lesson)
      const handleChange = (value)=>{
        // console.log("value subject",value)
        setLesson(value)
        if(value !== undefined || value !== null || value !== ""){
          dispatch(setViewLesson({id:value?.id === undefined ? "": value?.id,name:value?.name=== undefined ? "": value?.name}))
          
      }
    }
      useEffect(() => {
       
        getLessonList()
      }, [newMaterial?.chapter.id])
      const getLessonList = async() =>{
        const res = await getALLLessonByChapterID(newMaterial?.chapter.id)
        if(res.status  === 200){
          try{
            setLessonList(res.data);
           
          // console.log("res: ",res)
  
          }catch(e){
          console.log("Error: ",e)
  
          }
        }else{
          console.log("Khong tim thay bai học")
        }
        setIsLoading(false)
      }
      return (
        
          <MUIFolderSelect 
          id="lesson"

            label="Bài học"
            list={lessonList}
            handleChange={handleChange}
            isLoading={isLoading}
            value={lesson}
          />                
       
      );
    }