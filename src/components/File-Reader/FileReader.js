import React, { useEffect, useRef } from "react";
import { IframeFileReader } from "./style";

export default function ReadALLFile({file}) {
  function encodeURL(originalURL) {
    const encodedURL = encodeURIComponent(originalURL);
    return encodedURL;
  }
  return (
    <IframeFileReader src={`https://view.officeapps.live.com/op/embed.aspx?src=
    ${encodeURL(file)}
    `}  frameborder='0'>
    </IframeFileReader>
   
  );
}