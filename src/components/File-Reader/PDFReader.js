import { IframeFileReader } from "./style";

export const PDFReader = ({file}) =>{
    function encodeURL(originalURL) {
        const encodedURL = encodeURIComponent(originalURL);
        return encodedURL;
      }
    return(
        <>
        <embed src="https://emss.s3.ap-southeast-1.amazonaws.com/ngu-van-ba-dao-1701107583276.pdf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20231219T120949Z&X-Amz-SignedHeaders=host&X-Amz-Expires=3600&X-Amz-Credential=AKIAU3HOZI4LFYW4E6K3%2F20231219%2Fap-southeast-1%2Fs3%2Faws4_request&X-Amz-Signature=06de3e35e8108f9034eeea6d1daa266ebda5466d271d71be28dbd207de057c6c" width="800" height="500" type="application/pdf"></embed>
        </>
    )
}