import * as React from "react";
import { styled, alpha } from "@mui/material/styles";
import Button from "@mui/material/Button";

export const IframeFileReader = styled("iframe")(({ theme }) => ({
    width:'100%' ,
    height:'500px',
  
  [theme.breakpoints.down("xs")]: {
    height:'500px',
    
  },
  [theme.breakpoints.up("xs")]: {
    height:'500px',
    
  }
  ,
  [theme.breakpoints.down("md")]: {
    height:'500px',
    
  },
  [theme.breakpoints.up("md")]: {
    height:'500px',
    
  },
  [theme.breakpoints.up("lg")]: {
    height:'700px',
    
  },
  [theme.breakpoints.up("xl")]: {
    height:'700px',
   
  },
}));