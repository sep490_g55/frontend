/** 
 * @type {import('next').NextConfig} 
 */

const withTM = require('next-transpile-modules')([
  '@fullcalendar/common',
  '@fullcalendar/daygrid',
  '@fullcalendar/interaction',
  '@fullcalendar/list',
  '@fullcalendar/react',
  '@fullcalendar/timegrid',
  '@fullcalendar/timeline',
]);

const nextConfig = {
  reactStrictMode: true,
  compiler: {
    styledComponents: true,
  },
  images: {
    unoptimized: true
  }
};

module.exports = {
 
  ...nextConfig,

  webpack: (config) => {
    // load worker files as a urls with `file-loader`
   
   
    return config;
  },
};
